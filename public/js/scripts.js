$(document).on('click', 'a[href^="#"]', function(e) {
  // target element id
  var id = $(this).attr('href');
  // target element
  var $id = $(id);
  if ($id.length === 0) {
    return;
  }
  // prevent standard hash navigation (avoid blinking in IE)
  e.preventDefault();

  // top position relative to the document
  var pos = $id.offset().top;
  var offset = 0;
  if($(this)[0].hasAttribute('scrolloffset')){
    offset = parseInt($(this).attr('scrolloffset'));
  }
  if($(this)[0].hasAttribute('toggleActiveLink')){
    var navbar = $(this).closest('.navbar-nav');
    navbar.find('li').removeClass('active');
    $(this).closest('li').addClass('active');
  }
  // animated top scrolling
  $('body, html').animate({scrollTop: pos + offset});
});

/*
$(function(){
  $(".owl-carousel").owlCarousel({
    nav:false,
    autoWidth:true,
    margin: 30,
    center: true,
    loop: true,
    autoplay:true,
    autoplayTimeout:5000,
    autoplayHoverPause:true
  });
})
*/

$(function(){

    $('a').click(function(){
        var a = $(this);
        $.post("/track-link-click", {
            _token: postToken,
            clicked_from:currentPageUrl,
            clicked_link: a.attr("href")
        });

    })

    $(window).scroll(function () {
        $('img[dq-src]').each(function (i, el) {

            if ($(this).isInViewport()) {
                $(this).attr('src',$(this).attr('dq-src'));
            }
        })

    });

    $('.submit-on-change').change(function(){
        $(this).closest('form').submit();
    })

})



var ft = {
    addToCart:function(cfg){

        var pushable = {
            'event': 'addToCart',
            'ecommerce': {
                'currencyCode': 'EUR',
                'add': {                                // 'add' actionFieldObject measures.
                    'products': cfg.products
                }
            }
        };

        console.log(pushable);
        dataLayer.push(pushable);

    },
   /* removeFromCart:function(cfg){
        dataLayer.push({
            'event': 'removeFromCart',
            'ecommerce': {
                'remove': {                               // 'remove' actionFieldObject measures.
                    'products': [{                          //  removing a product to a shopping cart.
                        'name': 'Triblend Android T-Shirt',
                        'id': '12345',
                        'price': '15.25',
                        'brand': 'Google',
                        'category': 'Apparel',
                        'variant': 'Gray',
                        'quantity': 1
                    }]
                }
            }
        });
    }, */
    productClick:function(cfg){
        dataLayer.push({
            'event': 'productClick',
            'ecommerce': {
                'click': {
                    'actionField': { 'list': 'Search Results' },      // Optional list property.
                    'products': [{
                        'name': productObj.name,                      // Name or ID is required.
                        'id': productObj.id,
                        'price': productObj.price,
                        'brand': productObj.brand,
                        'category': productObj.cat,
                        'variant': productObj.variant,
                        'position': productObj.position
                    }]
                }
            },
            'eventCallback': function () {
                document.location = productObj.url
            }
        });
    },
    productPageView:function(cfg){

        var pushable = {
            'event': 'productPageView',
            'ecommerce': {
                'detail': {
                    'actionField': { 'list': 'Product Page' },    // 'detail' actions have an optional list property.
                    'products': cfg.products
                }
            }
        };

        console.log(pushable);

        dataLayer.push(pushable);

    },
    cartPageView:function(cfg){

        var pushable = {
            'event': 'checkout',
            'ecommerce': {
                'checkout': {
                    'actionField': { 'step': 1 },
                    'products': cfg.products
                }
            },
            'eventCallback': function () {
                //document.location = 'checkout.html';
            }
        };

        console.log(pushable);

        dataLayer.push(pushable);
    },

    purchase:function(cfg){
        var pushable = {
            'event': 'purchase',
            'ecommerce': {
                'purchase': {
                    'actionField': cfg.actionfield,
                    'products': cfg.products
                }
            }
        };
        console.log(pushable);
        dataLayer.push(pushable);
    },
    productImpressions:function(cfg){

        var pushable = {
            'event': 'productImpression',
            'ecommerce': {
                'currencyCode': 'EUR',                       // Local currency is optional.
                'impressions': cfg.products
            }
        };

        dataLayer.push(pushable);

    }
};
