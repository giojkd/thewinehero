var adminPath = 'clxXmls-cls--nZYal';



function chargePurchase(button){
    if(confirm('Please confirm your intent')){
        var button = $(button);

        var buttonData = button.data();
        var route = '/'+adminPath+'/charge-purchase';

        var id = buttonData.id;
        $.ajax({
            url: route,
            data: {
                id: id,
            },
            type: 'POST',
            success: function (result) {
                // Show an alert with the result
                console.log(result, route);
                new Noty({
                    text: "Operazione conclusa con successo",
                    type: "success"
                }).show();

                // Hide the modal, if any
                $('.modal').modal('hide');

                crud.table.ajax.reload();
            },
            error: function (result) {
                // Show an alert with the result
                new Noty({
                    text: "Si è verificato un errore",
                    type: "warning"
                }).show();
            }
        });
    }
}



function sendFinalConfirmation(button) {
    if (confirm('Please confirm your intent')) {
        var button = $(button);

        var buttonData = button.data();
        var route = '/' + adminPath + '/send-final-confirmation';

        var id = buttonData.id;
        $.ajax({
            url: route,
            data: {
                id: id,
            },
            type: 'POST',
            success: function (result) {
                // Show an alert with the result
                console.log(result, route);
                new Noty({
                    text: "Operazione conclusa con successo",
                    type: "success"
                }).show();

                // Hide the modal, if any
                $('.modal').modal('hide');

                crud.table.ajax.reload();
            },
            error: function (result) {
                // Show an alert with the result
                new Noty({
                    text: "Si è verificato un errore",
                    type: "warning"
                }).show();
            }
        });
    }
}


var popupSize = {
    width: 780,
    height: 550
};

$(document).on('click', '.social-button', function (e) {
    var verticalPos = Math.floor(($(window).width() - popupSize.width) / 2),
        horisontalPos = Math.floor(($(window).height() - popupSize.height) / 2);

    var popup = window.open($(this).prop('href'), 'social',
        'width=' + popupSize.width + ',height=' + popupSize.height +
        ',left=' + verticalPos + ',top=' + horisontalPos +
        ',location=0,menubar=0,toolbar=0,status=0,scrollbars=1,resizable=1');

    if (popup) {
        popup.focus();
        e.preventDefault();
    }

});
