const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js').version();
mix.js('resources/js/tz-converter.js', 'public/js').version();

mix.sass('resources/sass/app.scss', 'public/css').version();
mix.sass('resources/sass/home.scss', 'public/css').version();
mix.sass('resources/sass/videos.scss', 'public/css').version();
mix.sass('resources/sass/video.scss', 'public/css').version();
mix.sass('resources/sass/events.scss', 'public/css').version();
mix.sass('resources/sass/event.scss', 'public/css').version();
mix.sass('resources/sass/recipes.scss', 'public/css').version();
mix.sass('resources/sass/recipe.scss', 'public/css').version();
mix.sass('resources/sass/blog.scss', 'public/css').version();
mix.sass('resources/sass/article.scss', 'public/css').version();
mix.sass('resources/sass/articlecategory.scss', 'public/css').version();
mix.sass('resources/sass/products.scss', 'public/css').version();
mix.sass('resources/sass/product.scss', 'public/css').version();

mix.sass('resources/sass/user.scss', 'public/css').version();

mix.sass('resources/sass/consortium.scss', 'public/css').version();
mix.sass('resources/sass/manufacturer.scss', 'public/css').version();

mix.sass('resources/sass/experience.scss', 'public/css').version();

mix.sass('resources/sass/cart.scss', 'public/css').version();

mix.sass('resources/sass/search.scss', 'public/css').version();

mix.sass('resources/sass/team-member.scss', 'public/css').version();

/*

mix.sass('resources/sass/fontawesome.scss', 'public/css').version();
mix.sass('resources/sass/quizz.scss', 'public/css').version();
mix.sass('resources/sass/course.scss', 'public/css').version();


*/


mix.browserSync('localhost:8000');
