<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubscriptionsTableNew extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->integer('enrollmentoption_id')->nullable()->index();
            $table->timestamp('expires_at')->nullable();
            $table->integer('user_id')->nullable()->index();
            $table->integer('events')->nullable()->default(0);
            $table->integer('videos')->nullable()->default(0);
            $table->integer('trainings')->nullable()->default(0);
            $table->integer('articles')->nullable()->default(0);
            $table->integer('recipes')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscriptions');
    }
}
