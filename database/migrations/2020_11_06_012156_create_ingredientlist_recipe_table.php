<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIngredientlistRecipeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ingredientslist_recipe', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->integer('recipe_id')->nullable()->index();
            $table->integer('ingredientslist_id')->nullable()->index();
            $table->unique(['recipe_id', 'ingredientslist_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ingredientlist_recipe');
    }
}
