<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConsortiumManufacturerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consortium_manufacturer', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->integer('consortium_id')->nullable()->index();
            $table->integer('manufacturer_id')->nullable()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consortium_manufacturer');
    }
}
