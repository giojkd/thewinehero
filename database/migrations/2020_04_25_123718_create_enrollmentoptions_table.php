<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEnrollmentoptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enrollmentoptions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->float('price')->nullable();
            $table->boolean('is_subscription')->nullable();
            $table->integer('frequency')->nullable(); #in case of subscription how many days between two billings
            $table->json('what_you_get')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enrollmentoptions');
    }
}
