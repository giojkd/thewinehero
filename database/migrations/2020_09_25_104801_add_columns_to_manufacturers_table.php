<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToManufacturersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('manufacturers', function (Blueprint $table) {
            //
            $table->json('short_description')->nullable();
            $table->json('description')->nullable();
            $table->json('photos')->nullable();

            $table->string('funded_at')->nullable();
            $table->string('wineyard_hectares')->nullable();
            $table->string('denominations')->nullable();
            $table->string('cultivars')->nullable();

            $table->json('history_description')->nullable();
            $table->json('geography_description')->nullable();
            $table->json('production_description')->nullable();

            $table->json('address')->nullable();

            $table->string('email')->nullable();
            $table->string('telephone')->nullable();
            $table->string('website')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('manufacturers', function (Blueprint $table) {
            //
        });
    }
}
