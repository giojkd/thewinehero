<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinkclicksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('linkclicks', function (Blueprint $table) {

            $table->id();
            $table->timestamps();
            $table->json('content')->nullable();
            $table->string('clicked_link')->nullable()->index();
            $table->string('clicked_from')->nullable()->index();
            $table->boolean('is_external')->default(0);
            $table->string('remote_ip')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('linkclicks');
    }
}
