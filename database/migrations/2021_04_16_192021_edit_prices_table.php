<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('prices', function (Blueprint $table) {
            //
            $table->dropColumn('product_id');
            $table->string('priceable_type')->nullable()->index();
            $table->integer('priceable_id')->nullable()->index();

            #$table->integer('country_id')->nullable();

            $table->float('price')->nullable();
            $table->float('price_compare')->nullable();

            $table->index(['priceable_type', 'priceable_id']);
            $table->index(['priceable_type', 'priceable_id', 'country_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
