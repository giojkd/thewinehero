<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateManufacturerRecipeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manufacturer_recipe', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->integer('manufacturer_id')->index();
            $table->integer('recipe_id')->index();
            $table->unique(['manufacturer_id', 'recipe_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manufacturer_recipe');
    }
}
