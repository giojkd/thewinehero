<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExperiencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('experiences', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->json('name')->nullable();
            $table->json('short_description')->nullable();
            $table->json('description')->nullable();
            $table->json('address')->nullable();
            $table->double('longitude')->nullable();
            $table->double('latitude')->nullable();
            $table->float('free_transfer')->nullable();
            $table->integer('free_transfer_up_to')->nullable();
            $table->boolean('can_be_private')->nullable()->default(0);
            $table->time('duration')->nullable();
            $table->integer('manufacturer_id')->nullable()->index();
            $table->json('photos')->nullable();
            $table->boolean('enabled')->nullable()->default(1);
            $table->json('subtitle')->nullable();
            $table->float('grand_total')->nullable();
            $table->json('whats_included')->nullable();
            $table->json('whats_not_included')->nullable();
            $table->boolean('interstitial_page_after_adding_to_cart')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('experiences');
    }
}
