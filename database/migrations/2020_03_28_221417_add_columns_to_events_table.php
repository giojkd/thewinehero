<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->json('name')->nullable();
            $table->json('description_short')->nullable();
            $table->json('description')->nullable();
            $table->json('complexity')->nullable();
            $table->date('starts_at_day')->nullable();
            $table->time('starts_at_time')->nullable();
            $table->json('images')->nullable();
            $table->integer('user_id')->nullable()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function (Blueprint $table) {
            //
        });
    }
}
