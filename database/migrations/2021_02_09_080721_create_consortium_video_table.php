<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConsortiumVideoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consortium_video', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->integer('consortium_id')->index();
            $table->integer('video_id')->index();
            $table->unique(['consortium_id', 'video_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consortium_video');
    }
}
