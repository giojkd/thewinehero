<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticleConsortiumTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article_consortium', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->integer('article_id')->index();
            $table->integer('consortium_id')->index();
            $table->unique(['article_id', 'consortium_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article_consortium');
    }
}
