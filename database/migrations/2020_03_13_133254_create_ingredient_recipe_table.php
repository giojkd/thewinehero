<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIngredientRecipeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reciperows', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->integer('recipe_id')->nullable()->index();
            $table->integer('ingredient_id')->nullable();
            $table->unique(['recipe_id','ingredient_id']);
            $table->float('quantity')->nullable();
            $table->string('measurement_unit')->nullable();
            $table->string('notes')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reciperows');
    }
}
