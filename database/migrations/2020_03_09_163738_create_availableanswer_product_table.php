<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAvailableanswerProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('availableanswer_product', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->integer('availableanswer_id')->index();
            $table->integer('product_id')->index();
            $table->unique(['availableanswer_id','product_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('availableanswer_product');
    }
}
