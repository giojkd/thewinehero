<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBenefitsColumnsToEnrollmentoptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('enrollmentoptions', function (Blueprint $table) {
            //
            $table->integer('events')->nullable()->default(0);
            $table->integer('videos')->nullable()->default(0);
            $table->integer('trainings')->nullable()->default(0);
            $table->integer('articles')->nullable()->default(0);
            $table->integer('recipes')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('enrollmentoptions', function (Blueprint $table) {
            //
        });
    }
}
