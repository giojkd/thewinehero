<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrainingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trainings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->json('name')->nullable();
            $table->json('subtitle')->nullable();
            $table->integer('user_id')->nullable();
            $table->json('description')->nullable();
            $table->integer('days_to_complete')->nullable();
            $table->json('what_you_will_learn')->nullable();
            $table->json('requirements')->nullable();
            $table->json('subscribers')->nullable();
            $table->float('rating')->nullable();
            $table->float('price')->nullable();
            $table->float('compare_price_with')->nullable();
            $table->json('welcome_message')->nullable();
            $table->json('congratulations_message')->nullable();
            $table->json('photos')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trainings');
    }
}
