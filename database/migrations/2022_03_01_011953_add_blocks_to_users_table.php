<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBlocksToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            //
            $table->json('block_1_title')->nullable();
            $table->json('block_2_title')->nullable();
            $table->json('block_3_title')->nullable();
            $table->json('block_4_title')->nullable();
            $table->json('block_5_title')->nullable();
            $table->json('block_1_content')->nullable();
            $table->json('block_2_content')->nullable();
            $table->json('block_3_content')->nullable();
            $table->json('block_4_content')->nullable();
            $table->json('block_5_content')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
