<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConsortiumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consortiums', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();

            $table->string('name')->nullable();
            $table->string('logo')->nullable();

            $table->json('short_description')->nullable();
            $table->json('description')->nullable();
            $table->json('photos')->nullable();

            $table->string('surface')->nullable();
            $table->string('production')->nullable();
            $table->string('consortium_members')->nullable();
            $table->string('funded_at')->nullable();

            $table->json('history_description')->nullable();
            $table->json('geography_description')->nullable();
            $table->json('production_description')->nullable();

            $table->json('address')->nullable();

            $table->string('email')->nullable();
            $table->string('telephone')->nullable();
            $table->string('website')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consortiums');
    }
}
