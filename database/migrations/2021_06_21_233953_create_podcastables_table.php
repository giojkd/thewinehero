<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePodcastablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('podcastables', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->integer('podcast_id')->nullable()->index();
            $table->string('podcastable_id')->nullable()->index();
            $table->string('podcastable_type')->nullable()->index();

            /*
              tag_id - integer
    taggable_id - integer
    taggable_type - string
            */
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('podcastables');
    }
}
