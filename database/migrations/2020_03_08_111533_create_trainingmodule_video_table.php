<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrainingmoduleVideoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trainingmodule_video', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->integer('trainingmodule_id')->index();
            $table->integer('video_id')->index();
            $table->unique(['video_id','trainingmodule_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trainingmodule_video');
    }
}
