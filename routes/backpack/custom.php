<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

use App\Http\Controllers\FrontController;

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', config('backpack.base.middleware_key', 'admin')],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    Route::crud('category', 'CategoryCrudController');
    Route::crud('article', 'ArticleCrudController');
    Route::crud('tag', 'TagCrudController');
    Route::crud('training', 'TrainingCrudController');
    Route::post('media-dropzone', ['uses' => 'TrainingCrudController@handleDropzoneUpload']);

    Route::crud('traininglevel', 'TraininglevelCrudController');
    Route::crud('trainingmodule', 'TrainingmoduleCrudController');

    Route::crud('trainingblock', 'TrainingblockCrudController');
    Route::crud('file', 'FileCrudController');
    Route::crud('video', 'VideoCrudController');
    Route::crud('quizz', 'QuizzCrudController');
    Route::crud('question', 'QuestionCrudController');
    Route::crud('availableanswer', 'AvailableanswerCrudController');
    Route::crud('manufacturer', 'ManufacturerCrudController');
    Route::crud('product', 'ProductCrudController');
    Route::crud('recipe', 'RecipeCrudController');
    Route::crud('ingredient', 'IngredientCrudController');
    Route::crud('reciperow', 'ReciperowCrudController');
    Route::crud('event', 'EventCrudController');
    Route::crud('content', 'ContentCrudController');
    Route::crud('complexity', 'ComplexityCrudController');
    Route::crud('dishcourse', 'DishcourseCrudController');
    Route::crud('cuisine', 'CuisineCrudController');
    Route::crud('recipestep', 'RecipestepCrudController');
    Route::crud('review', 'ReviewCrudController');
    Route::crud('favorite', 'FavoriteCrudController');
    Route::crud('enrollmentoption', 'EnrollmentoptionCrudController');
    Route::crud('country', 'CountryCrudController');
    Route::crud('lead', 'LeadCrudController');
    Route::crud('merchant', 'MerchantCrudController');

    Route::crud('pagetype', 'PagetypeCrudController');
    Route::crud('plan', 'PlanCrudController');
    Route::crud('purchase', 'PurchaseCrudController');
    Route::crud('enrollmentoptiongroup', 'EnrollmentoptiongroupCrudController');
    Route::crud('incredientslist', 'IncredientslistCrudController');
    Route::crud('experience', 'ExperienceCrudController');
    Route::crud('availability', 'AvailabilityCrudController');
    Route::crud('language', 'LanguageCrudController');


    Route::get('availability-bulk', 'AvailabilityCrudController@availabilityBulkGet')->name('availabilityBulkGet');
    Route::post('availability-bulk', 'AvailabilityCrudController@availabilityBulkCreate')->name('availabilityBulkCreate');

    Route::crud('consortium', 'ConsortiumCrudController');
    Route::crud('pricerule', 'PriceruleCrudController');
    Route::crud('articlecategory', 'ArticlecategoryCrudController');
    Route::crud('dump', 'DumpCrudController');

    Route::post('charge-purchase','PurchaseCrudController@chargePurchase');
    Route::post('send-final-confirmation','PurchaseCrudController@sendFinalConfirmation');
    Route::crud('recommend', 'RecommendCrudController');
    Route::crud('videocategory', 'VideocategoryCrudController');
    Route::crud('newslettersubscription', 'NewslettersubscriptionCrudController');
    Route::crud('guest', 'GuestCrudController');
    Route::crud('producttype', 'ProducttypeCrudController');
    Route::crud('price', 'PriceCrudController');

    Route::get('price-manager', 'PriceCrudController@priceManager')->name('priceManager');
    Route::post('price-manager', 'PriceCrudController@priceManagerSet')->name('priceManagerSet');

    Route::crud('release', 'ReleaseCrudController');
    Route::crud('deal', 'DealCrudController');
    Route::crud('landingpage', 'LandingpageCrudController');
    Route::crud('bundle', 'BundleCrudController');
    Route::crud('podcast', 'PodcastCrudController');
    Route::crud('dealsubscription', 'DealsubscriptionCrudController');
    Route::crud('cms', 'CmsCrudController');
    Route::crud('linkclick', 'LinkclickCrudController');
    Route::crud('banner', 'BannerCrudController');

    Route::get('vvveb',[FrontController::class,'vvveb']);

}); // this should be the absolute last line of this file
