<?php

use App\Http\Controllers\FrontController;
use App\Http\Controllers\RedirectsController;


Route::post('track-link-click', 'FrontController@trackLinkClick');

Route::get('home',function(){
    return redirect('');
});

Route::get('/session/test', 'SessionController@test');
Route::get('/geoip/test', 'GeoIpController@test');
Route::get('publish-translations', 'FrontController@publishTranslations');

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/test','TestController@index');


#Auth::routes();
#Route::get('/home', 'HomeController@index')->name('home');




/*
$this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('login', 'Auth\LoginController@login');
$this->post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
$this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
$this->post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
$this->post('password/reset', 'Auth\ResetPasswordController@reset');
*/

Route::group(['middleware' => 'auth'], function () {
    Route::get('/favorite-toggle/{model}/{id}', 'FrontController@toggleFavorite')->name('toggleFavorite');
});


Route::group(['middleware' => ['language'],'where' => ["locale"=>"(?:\b|')(it|en)(?:\b|')","url" => ".*"]], function () {

    Route::get('{locale}/email/verify', 'Auth\VerificationController@show')->name('verification.notice');
    Route::get('{locale}/email/verify/{id}', 'Auth\VerificationController@verify')->name('verification.verify');
    Route::get('{locale}/email/resend', 'Auth\VerificationController@resend')->name('verification.resend');

    #SOCIAL LOGIN
    Route::get('/social-login-with/{social}', 'SocialLoginController@returnRedirect')->name('socialLogin');
    Route::match(['get', 'post'], '/social-login-redirect/facebook', 'SocialLoginController@facebook');
    #Route::get('/logout','FrontController@logout')->name('logout');
    Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

    Route::get('{locale}/login', 'FrontController@login')->name('showLoginForm');
    Route::post('{locale}/login', 'Auth\LoginController@login')->name('login');

    Route::get('{locale}/register', 'FrontController@register')->name('showRegisterForm');
    Route::post('{locale}/register', 'Auth\RegisterController@register')->name('register');

    Route::get('{locale}/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('{locale}/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('{locale}/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('{locale}/password/reset', 'Auth\ResetPasswordController@reset');
    #SOCIAL LOGIN END

    Route::get('{locale?}', 'FrontController@home')->name('home');

    Route::get('{locale}/blog', 'FrontController@blog')->name('blog');
    Route::get('{locale}/blog/{name}/{id?}', 'FrontController@article');
    Route::get('{locale}/articlecategory/{name}/{id?}', 'FrontController@articlecategory');

    Route::get('{locale}/landingpage/{name}/{id}', 'FrontController@landingpage');

    Route::get('{locale}/recipes', 'FrontController@recipes')->name('recipes');
    Route::get('{locale}/recipes-old', 'FrontController@recipesOld')->name('recipesOld');

    Route::get('{locale}/recipe/{name}/{id?}', 'FrontController@recipe');

    Route::get('{locale}/events', 'FrontController@events')->name('events');
    Route::get('{locale}/event/{name}/{id?}', 'FrontController@event');

    Route::get('{locale}/event-enrollment/{id}', 'FrontController@enrollEvent')->name('enrollEvent');

    Route::get('{locale}/videos', 'FrontController@videos')->name('videos');
    Route::get('{locale}/video/{name}/{id?}', 'FrontController@video');

    Route::get('{locale}/search/{keyword}', 'FrontController@search')->where(["keyword" => ".*"])->name('search.en');
    Route::get('{locale}/cerca/{keyword}', 'FrontController@search')->where(["keyword" => ".*"])->name('search.it');

    Route::get('{locale}/search-gateway', 'FrontController@searchGateway')->name('search-gateway');

    Route::get('{locale}/user', 'FrontController@user')->name('user');

    Route::get('{locale}/products/{producttype?}', 'FrontController@products')->name('products');
    Route::get('{locale}/product/{name}/{id?}', 'FrontController@product');

    Route::get('{locale}/experience/{name}/{id?}', 'FrontController@experience');
    Route::get('{locale}/experience-checkout', 'FrontController@experienceCheckOut')->name('addToCartExperience');

    Route::get('{locale}/manufacturer/{name}/{id?}', 'FrontController@manufacturer');
    Route::get('{locale}/consortium/{name}/{id?}', 'FrontController@consortium');



    Route::get('{locale}/videocategory/{name}/{id?}', 'FrontController@videocategory')->name('videocategory');

    Route::get('{locale}/team-members', 'FrontController@teamMembers')->name('teamMembers');
    Route::get('{locale}/team-member/{name}/{id?}', 'FrontController@teamMember');

    Route::get('{locale}/courses', 'FrontController@courses')->name('courses');
    Route::get('{locale}/course/{name}/{id}', 'FrontController@course');

    Route::get('{locale}/deals', 'FrontController@deals')->name('deals');
    Route::get('{locale}/deal/{name}/{id?}', 'FrontController@deal');


    Route::get('{locale}/quizzs', 'FrontController@quizzs')->name('quizzs');
    Route::get('{locale}/quizz/{name}/{id}', 'FrontController@quizz');
    Route::post('{locale}/quizz-submit', 'FrontController@quizzSubmit')->name('quizzSubmit');
    Route::post('{locale}/enroll-training', 'FrontController@enrollTraining')->name('enrollTraining');

    Route::get('{locale}/cart', 'FrontController@cart')->name('cart');

    Route::get('{locale}/available-plans', 'FrontController@availablePlans')->name('availablePlans');

    Route::get('{locale}/enrollment-options/', 'FrontController@enrollmentOptions')->name('enrollmentOptions');

    Route::get('{locale}/buy-single-content-checkout/{model}/{id}', 'FrontController@buySingleContentCheckout')->name('buySingleContentCheckout');

    Route::get('{locale}/thank-you-page-plan-activation','FrontController@thankYouPagePlanActivation')->name('thankYouPagePlanActivation');

    Route::post('activate-plan', 'FrontController@activatePlan')->name('activatePlan');

    Route::get('{locale}/cart','FrontController@cart')->name('cart');
    Route::get('{locale}/thank-you-page-ecommerce-cart', 'FrontController@thankYouPageEcommerceCart')->name('thankYouPageEcommerceCart');
    Route::post('add-to-cart','FrontController@addToCart')->name('addToCart');
    Route::post('remove-from-cart', 'FrontController@removeFromCart')->name('removeFromCart');
    Route::post('confirm-cart','FrontController@confirmCart')->name('confirmCart');
    Route::post('set-cart-field','FrontController@setCartField')->name('setCartField');

    Route::get('{locale}/thank-you-page-experience', 'FrontController@thankYouPageExperience')->name('thankYouPageExperience');

    Route::get('/{locale}/pages/{name}/{id}', 'FrontController@pages');

    Route::get('{locale}/request-invoice/{id}','FrontController@requestInvoice')->name('requestInvoice');
    Route::post('confirm-invoice', 'FrontController@confirmInvoice')->name('confirmInvoice');

    Route::get('{locale}/request-purchase-invoice/{id}', 'FrontController@requestPurchaseInvoice')->name('requestPurchaseInvoice');
    Route::post('confirm-purchase-invoice', 'FrontController@confirmPurchaseInvoice')->name('confirmPurchaseInvoice');

    Route::get('/{locale}/i-mama', 'FrontController@imama')->name('iMama');

    Route::get('/{locale}/deal-subscription','FrontController@dealSubscription')->name('dealSubscription');

    Route::get('/{locale}/cms/{name}/{id}','FrontController@cms')->name('cms');
    Route::get('/{locale}/vvveb-preview', [FrontController::class, 'vvvebPreview']);
});




#Route::get('{locale}/blog/{}')


Route::crud(config('backpack.base')['route_prefix'].'/user', 'Admin\UserCrudControllerAdmin');


Route::post('training/{id}/media', 'Admin\TrainingCrudController@uploadMedia');
Route::delete('training/{id}/media/{mediaId}', 'Admin\TrainingCrudController@deleteMedia');
Route::post('training/{id}/media/reorder', 'Admin\TrainingCrudController@reorderMedia');

Route::post('traininglevel/{id}/media', 'Admin\TraininglevelCrudController@uploadMedia');
Route::delete('traininglevel/{id}/media/{mediaId}', 'Admin\TraininglevelCrudController@deleteMedia');
Route::post('traininglevel/{id}/media/reorder', 'Admin\TraininglevelCrudController@reorderMedia');

Route::post('trainingblock/{id}/media', 'Admin\TrainingblockCrudController@uploadMedia');
Route::delete('trainingblock/{id}/media/{mediaId}', 'Admin\TrainingblockCrudController@deleteMedia');
Route::post('trainingblock/{id}/media/reorder', 'Admin\TrainingblockCrudController@reorderMedia');

Route::post('trainingmodule/{id}/media', 'Admin\TrainingmoduleCrudController@uploadMedia');
Route::delete('trainingmodule/{id}/media/{mediaId}', 'Admin\TrainingmoduleCrudController@deleteMedia');
Route::post('trainingmodule/{id}/media/reorder', 'Admin\TrainingmoduleCrudController@reorderMedia');

Route::post('recipe/{id}/media', 'Admin\RecipeCrudController@uploadMedia');
Route::delete('recipe/{id}/media/{mediaId}', 'Admin\RecipeCrudController@deleteMedia');
Route::post('recipe/{id}/media/reorder', 'Admin\RecipeCrudController@reorderMedia');

Route::post('ingredient/{id}/media', 'Admin\IngredientCrudController@uploadMedia');
Route::delete('ingredient/{id}/media/{mediaId}', 'Admin\IngredientCrudController@deleteMedia');
Route::post('ingredient/{id}/media/reorder', 'Admin\IngredientCrudController@reorderMedia');

Route::post('article/{id}/media', 'Admin\ArticleCrudController@uploadMedia');
Route::delete('article/{id}/media/{mediaId}', 'Admin\ArticleCrudController@deleteMedia');
Route::post('article/{id}/media/reorder', 'Admin\ArticleCrudController@reorderMedia');

Route::post('experience/{id}/media', 'Admin\ExperienceCrudController@uploadMedia');
Route::delete('experience/{id}/media/{mediaId}', 'Admin\ExperienceCrudController@deleteMedia');
Route::post('experience/{id}/media/reorder', 'Admin\ExperienceCrudController@reorderMedia');

Route::post('recipestep/{id}/media', 'Admin\RecipestepCrudController@uploadMedia');
Route::delete('recipestep/{id}/media/{mediaId}', 'Admin\RecipestepCrudController@deleteMedia');
Route::post('recipestep/{id}/media/reorder', 'Admin\RecipestepCrudController@reorderMedia');

Route::post('product/{id}/media', 'Admin\ProductCrudController@uploadMedia');
Route::delete('product/{id}/media/{mediaId}', 'Admin\ProductCrudController@deleteMedia');
Route::post('product/{id}/media/reorder', 'Admin\ProductCrudController@reorderMedia');

Route::post('event/{id}/media', 'Admin\EventCrudController@uploadMedia');
Route::delete('event/{id}/media/{mediaId}', 'Admin\EventCrudController@deleteMedia');
Route::post('event/{id}/media/reorder', 'Admin\EventCrudController@reorderMedia');

Route::post('manufacturer/{id}/media', 'Admin\ManufacturerCrudController@uploadMedia');
Route::delete('manufacturer/{id}/media/{mediaId}', 'Admin\ManufacturerCrudController@deleteMedia');
Route::post('manufacturer/{id}/media/reorder', 'Admin\ManufacturerCrudController@reorderMedia');


Route::post('consortium/{id}/media', 'Admin\ConsortiumCrudController@uploadMedia');
Route::delete('consortium/{id}/media/{mediaId}', 'Admin\ConsortiumCrudController@deleteMedia');
Route::post('consortium/{id}/media/reorder', 'Admin\ConsortiumCrudController@reorderMedia');

Route::get('/thumbs/dropzone/{filename}', 'ImageResizeController@thumbs')->where(["filename" => ".*"]);

Route::get('/ir/{size}/{filename}', 'ImageResizeController@resize')->where(["filename" => ".*"])->name('ir');

Route::get('tok-box-create-session', 'TokBoxController@createSession');
Route::get('tok-box-subscribe', 'TokBoxController@subscribeSession');

Route::get('content-download/{model}/{id}', 'FrontController@contentDownload')->name('contentDownload');


Route::post('purchase-content/{model}/{id}', 'FrontController@purchaseContent')->name('purchaseContent');
Route::post('buy-single-content', 'FrontController@buySingleContent')->name('buySingleContent');

Route::post('buy-experience', 'FrontController@buyExperience')->name('buyExperience');

Route::post('remove-payment-method', 'FrontController@removePaymentMethod')->name('removePaymentMethod');
Route::post('delete-user-data', 'FrontController@deleteUserData')->name('deleteUserData');





#redirects written to avoid broken pages

/*
Route::get('it/recipe', function () {
    return Redirect::to('it/recipes', 302);
});
*/

Route::post('subscribe-newsletter','\App\Http\Controllers\Admin\NewslettersubscriptionCrudController@subscribe')->name('subscribeNewsletter');

Route::get('artisan/import-products', 'FrontController@scoutImportProducts')->name('artisanScoutImportProducts');


Route::post('leave-a-review', 'FrontController@leaveAReview')->name('leaveAReview');


Route::get('views-dashboard',  'FrontController@viewsDashboard')->middleware('admin')->name('viewsDashboard');



Route::resource('redirects','RedirectsController');


