<?php

namespace App\Observers;
use App\Models\Recipe;

class RecipeObserver
{
    //
    public function updated(Recipe $recipe)
    {

        $recipe->products()->sync([]);
        $recipe->products()->sync($recipe->product_pairings);

        $recipe->recommend->touch();
    }

    public function created(Recipe $recipe){
        $recipe->makeRecommend();
    }

}




