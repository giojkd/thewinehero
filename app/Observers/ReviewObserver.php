<?php

namespace App\Observers;

use App\Models\Review;

class ReviewObserver
{
    /**
     * Handle the review "created" event.
     *
     * @param  \App\Review  $review
     * @return void
     */
    public function created(Review $review)
    {
      $item = $review->reviewable_type::find($review->reviewable_id);
      $item->updateReviewsStatus();
    }

    /**
     * Handle the review "updated" event.
     *
     * @param  \App\Review  $review
     * @return void
     */
    public function updated(Review $review)
    {
        //
        $item = $review->reviewable_type::find($review->reviewable_id);
        $item->updateReviewsStatus();
    }

    /**
     * Handle the review "deleted" event.
     *
     * @param  \App\Review  $review
     * @return void
     */
    public function deleted(Review $review)
    {
        //
        $item = $review->reviewable_type::find($review->reviewable_id);
        $item->updateReviewsStatus();
    }

    /**
     * Handle the review "restored" event.
     *
     * @param  \App\Review  $review
     * @return void
     */
    public function restored(Review $review)
    {
        //
        $item = $review->reviewable_type::find($review->reviewable_id);
        $item->updateReviewsStatus();
    }

    /**
     * Handle the review "force deleted" event.
     *
     * @param  \App\Review  $review
     * @return void
     */
    public function forceDeleted(Review $review)
    {
        //
        $item = $review->reviewable_type::find($review->reviewable_id);
        $item->updateReviewsStatus();
    }
}
