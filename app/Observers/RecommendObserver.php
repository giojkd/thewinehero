<?php

namespace App\Observers;

use App\Models\Recommend;

class RecommendObserver
{
    /**
     * Handle the recommend "created" event.
     *
     * @param  \App\Recommend  $recommend
     * @return void
     */
    public function created(Recommend $recommend)
    {
        //
        $recommend->sendToRecombee();
    }

    /**
     * Handle the recommend "updated" event.
     *
     * @param  \App\Recommend  $recommend
     * @return void
     */
    public function updated(Recommend $recommend)
    {
        //
        $recommend->sendToRecombee();
    }

    /**
     * Handle the recommend "deleted" event.
     *
     * @param  \App\Recommend  $recommend
     * @return void
     */
    public function deleted(Recommend $recommend)
    {
        //
    }

    /**
     * Handle the recommend "restored" event.
     *
     * @param  \App\Recommend  $recommend
     * @return void
     */
    public function restored(Recommend $recommend)
    {
        //
    }

    /**
     * Handle the recommend "force deleted" event.
     *
     * @param  \App\Recommend  $recommend
     * @return void
     */
    public function forceDeleted(Recommend $recommend)
    {
        //
    }
}
