<?php

namespace App\Observers;

use App\Models\Plan;
use App\Models\Enrollmentoption;

class PlanObserver
{
    /**
     * Handle the plan "created" event.
     *
     * @param  \App\Plan  $plan
     * @return void
     */
    public function created(Plan $plan)
    {


        $Enrollmentoption = $plan->enrollmentoption;



        #dd($Enrollmentoption);

        $plan->fill([
            'events' => $Enrollmentoption->events,
            'videos' => $Enrollmentoption->videos,
            'trainings' => $Enrollmentoption->trainings,
            'articles' => $Enrollmentoption->articles,
            'recipes' => $Enrollmentoption->recipes,

            'events_discount' => $Enrollmentoption->events_discount,
            'videos_discount' => $Enrollmentoption->videos_discount,
            'trainings_discount' => $Enrollmentoption->trainings_discount,
            'articles_discount' => $Enrollmentoption->articles_discount,
            'recipes_discount' => $Enrollmentoption->recipes_discount,

            'expires_at' => date('Y-m-d H:i:s',strtotime('+'. $Enrollmentoption->frequency.'days')),
        ]);

        $plan->save();

        $user = $plan->user;
        $user->plan_id = $plan->id;
        $user->save();

    }

    /**
     * Handle the plan "updated" event.
     *
     * @param  \App\Plan  $plan
     * @return void
     */
    public function updated(Plan $plan)
    {
        //
    }

    /**
     * Handle the plan "deleted" event.
     *
     * @param  \App\Plan  $plan
     * @return void
     */
    public function deleted(Plan $plan)
    {
        //
    }

    /**
     * Handle the plan "restored" event.
     *
     * @param  \App\Plan  $plan
     * @return void
     */
    public function restored(Plan $plan)
    {
        //
    }

    /**
     * Handle the plan "force deleted" event.
     *
     * @param  \App\Plan  $plan
     * @return void
     */
    public function forceDeleted(Plan $plan)
    {
        //
    }
}
