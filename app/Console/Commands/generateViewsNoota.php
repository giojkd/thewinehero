<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\Product;
use App\Models\Manufacturer;
use App\Models\Consortium;
use App\Models\Recipe;
use Illuminate\Support\Carbon;

class generateViewsNoota extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mmblp:generateviewsnoota';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $now = Carbon::now();

        Product::orderBy('created_at', 'asc')->get()->each(function($p,$k) use($now ){

            $diffInDays = $p->created_at->diffInDays($now);
            $p->views = rand(20, 700 + $diffInDays * 2);
            $p->saveWithoutEvents();

        });

        Manufacturer::orderBy('created_at', 'asc')->get()->each(function ($p, $k) use ($now){
            $diffInDays = $p->created_at->diffInDays($now);
            $p->views = rand(20, 700 + $diffInDays * 2);
            $p->saveWithoutEvents();
        });

        Consortium::orderBy('created_at', 'asc')->get()->each(function ($p, $k) use ($now){
            $diffInDays = $p->created_at->diffInDays($now);
            $p->views = rand(20, 700 + $diffInDays * 2);
            $p->saveWithoutEvents();
        });

        Recipe::orderBy('created_at', 'asc')->get()->each(function ($p, $k) use ($now){
            $diffInDays = $p->created_at->diffInDays($now);
            $p->views = rand(20, 700 + $diffInDays * 2);
            $p->saveWithoutEvents();
        });
    }
}
