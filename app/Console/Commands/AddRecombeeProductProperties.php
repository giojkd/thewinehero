<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Recombee\RecommApi\Client;
use Recombee\RecommApi\Requests as Reqs;
use Recombee\RecommApi\Exceptions as Ex;
use Recombee\RecommApi\Requests\ListItemProperties;
use Recombee\RecommApi\Requests\AddItemProperty;
use Illuminate\Support\Str;

class AddRecombeeProductProperties extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mmblp:addrecombeeproductproperties {properties=none}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $client = new Client(env('RECOMBEE_DB_ID'), env('RECOMBEE_DB_KEY'));

        $lineProperties = $this->argument('properties');


        $properties = collect([
            'grape_variety_en' => '',
            'grape_variety_it' => '',
            'serving_temperature_en' => '',
            'serving_temperature_it' => '',
            'region_en' => '',
            'region_it' => '',
            'prodotti_en' => '',
            'prodotti_it' => '',
            'alcohol_en' => '',
            'alcohol_it' => '',
            'ageing_potential_en' => '',
            'ageing_potential_it' => '',
            'foods_en' => '',
            'foods_it' => '',
            'aromas_en' => '',
            'aromas_it' => '',
            'cheese_ingredients_en' => '',
            'cheese_ingredients_it' => '',
            'nina_article_categories_en' => '',
            'nina_article_categories_it' => '',
            'ageing_en' => ''
        ]);

        if($lineProperties != 'none'){

            $lineProperties = collect(Str::of($lineProperties)->explode(','))->transform(function($property,$key){
                return Str::of($property)->trim();
            });

            #dd($lineProperties);
        }else{
            $lineProperties = collect([]);
        }

        $currentProperties =  collect($client->send(new ListItemProperties()))->pluck('name');

        foreach($properties as $property => $type){

            $type = ($type != '') ? $type : 'string';

            if (!$currentProperties->contains($property)) {
                $status = $client->send(new AddItemProperty($property, $type));
                $this->line('Just added item property "'.$property.'"');

            }
        }

        if(count($lineProperties) > 0){
            foreach ($lineProperties as $property) {

                $type = 'string';

                if (!$currentProperties->contains($property)) {
                    $status = $client->send(new AddItemProperty($property, $type));
                    $this->line('Just added item property "' . $property . '"');
                }

            }
        }

        $currentProperties =  collect($client->send(new ListItemProperties()))->pluck('name');

        dd($currentProperties);

    }
}
