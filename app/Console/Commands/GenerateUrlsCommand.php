<?php

namespace App\Console\Commands;

use App\Models\Article;
use App\Models\Articlecategory;
use App\Models\BackpackUser;
use App\Models\Consortium;
use App\Models\Recipe;
use App\Models\Product;
use App\Models\Video;
use App\Models\Event;
use App\Models\Experience;
use App\Models\Manufacturer;
use App\Models\Videocategory;
use Illuminate\Console\Command;

class GenerateUrlsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mmblp:generateurls';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $locales = config('locales');

        Article::withoutSyncingToSearch(function () use($locales) {
            $articles = Article::get();
            $articles->each(function($article) use ($locales) {
                foreach($locales as $locale){
                    $article->generateUrl($locale);
                }
            });
        });

        Recipe::withoutSyncingToSearch(function () use ($locales) {
            $recipes = Recipe::get();
            $recipes->each(function ($recipe) use ($locales) {
                foreach ($locales as $locale) {
                    $recipe->generateUrl($locale);
                }
            });
        });

        Product::withoutSyncingToSearch(function () use ($locales) {
            $products = Product::get();
            $products->each(function ($product) use ($locales) {
                foreach ($locales as $locale) {
                    $product->generateUrl($locale);
                }
            });
        });

        Video::withoutSyncingToSearch(function () use ($locales) {
            $videos = Video::get();
            $videos->each(function ($video) use ($locales) {
                foreach ($locales as $locale) {
                    $video->generateUrl($locale);
                }
            });
        });

        Event::withoutSyncingToSearch(function () use ($locales) {
            $events = Event::get();
            $events->each(function ($event) use ($locales) {
                foreach ($locales as $locale) {
                    $event->generateUrl($locale);
                }
            });
        });

        Articlecategory::withoutSyncingToSearch(function () use ($locales) {
            $articlecategories = Articlecategory::get();
            $articlecategories->each(function ($articlecategory) use ($locales) {
                foreach ($locales as $locale) {
                    $articlecategory->generateUrl($locale);
                }
            });
        });

        Manufacturer::withoutSyncingToSearch(function () use ($locales) {
            $manufacturers = Manufacturer::get();
            $manufacturers->each(function ($manufacturer) use ($locales) {
                foreach ($locales as $locale) {
                    $manufacturer->generateUrl($locale);
                }
            });
        });

        Consortium::withoutSyncingToSearch(function () use ($locales) {
            $consortiums = Consortium::get();
            $consortiums->each(function ($consortium) use ($locales) {
                foreach ($locales as $locale) {
                    $consortium->generateUrl($locale);
                }
            });
        });



        Experience::withoutSyncingToSearch(function () use ($locales) {
            $experiences = Experience::get();
            $experiences->each(function ($experience) use ($locales) {
                foreach ($locales as $locale) {
                    $experience->generateUrl($locale);
                }
            });
        });

        #Videocategory::withoutSyncingToSearch(function () use ($locales) {
            $videocategories = Videocategory::get();
            $videocategories->each(function ($videocategory) use ($locales) {
                foreach ($locales as $locale) {
                    $videocategory->generateUrl($locale);
                }
            });
        #});


        #BackpackUser::withoutSyncingToSearch(function () use ($locales) {
            $BackpackUsers = BackpackUser::get();
            $BackpackUsers->each(function ($BackpackUser) use ($locales) {
                foreach ($locales as $locale) {
                    $BackpackUser->generateUrl($locale,'full_name');
                }
            });
        #});

    }
}
