<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CreateRecommends extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mmblp:createrecommends';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $models = ['\App\Models\Article', '\App\Models\Event', '\App\Models\Product', '\App\Models\Recipe', '\App\Models\Video'];
        foreach($models as $model){
            $items = $model::get();
            $items->each(function($item,$key){

                if(is_null($item->recommend)){
                    $item->makeRecommend();
                }

            });
        }
    }
}
