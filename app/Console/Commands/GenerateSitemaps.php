<?php

namespace App\Console\Commands;

use App\Models\Video;
use App\Models\Article;
use App\Models\Articlecategory;
use App\Models\Consortium;
use App\Models\Event;
use App\Models\Manufacturer;
use App\Models\Product;
use App\Models\BackpackUser;
use App\Models\Recipe;
use App\Models\Deal;
use App;
use App\Models\Experience;
use App\Models\Videocategory;
use Exception;
use Google\Api\Distribution\Exemplar;
use Illuminate\Console\Command;
use Lemaur\Sitemap\Sitemap;
use Spatie\Sitemap\SitemapIndex;
use Lemaur\Sitemap\Tags\Image;
use Lemaur\Sitemap\Tags\Url;
use PHPHtmlParser\Dom;
use GuzzleHttp;

class GenerateSitemaps extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mmblp:generatesitemaps';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */

    public static function checkUrlStatus($url)
    {
        try {
            $client = new GuzzleHttp\Client();
            $res = $client->request('GET', $url);
            return $res->getStatusCode();
        } catch (Exception $e) {
            return 500;
        }
    }

    public function altTagMl($param)
    {
        return (count(explode('_', pathinfo($param)['filename'])) > 1) ? __('tagAlt.' . str_replace('-', ' ', explode('_', pathinfo($param)['filename'])[0])) : '';
    }

    public function handle()
    {

        set_time_limit(0);







        $languages = Config('backpack.crud')['locales'];

        $smi = SitemapIndex::create();

        foreach ($languages as $locale => $langFull) {

            App::setlocale($locale);

            $contents = [];
            $contents = [


                'videos' => Video::whereActive(1)->whereEnabled(1)->get(),
                'articles' => Article::whereEnabled(1)->get(),
                'recipes' => Recipe::whereEnabled(1)->get(),
                'products' => Product::whereEnabled(1)->get(),
                'manufacturers' => Manufacturer::whereEnabled(1)->get(),
                'consortiums' => Consortium::whereEnabled(1)->get(),
                'teamMembers' => BackpackUser::where('has_profile', 1)
                                 ->where('active',1)
                                 ->whereNotNull('avatar')
                                 ->where('show_in_home',1)
                                 ->get(),
                'articleCategories' => Articlecategory::get(),
                'videoCategories' => Videocategory::get(),

                'experiences' => Experience::where('enabled',1)->get(),
                'events' => Event::where('enabled', 1)->get(),

                'deals' => Deal::where('active',1)->get()

                #'events' => Event::whereActive(1)->get(), temporarily suspended


            ];



            foreach ($contents as $contentType => $contentList) {
                $sitemap = Sitemap::create();
                foreach ($contentList as $content) {

                    $url = $content->makeUrl($locale);

                    try {
                        #$httpCode = self::checkUrlStatus($url);

                        #$this->line('Url ' . $url . ' has http response code:' . $httpCode);

                        #if ($httpCode == 200) {

                            $tmp = Url::create($url);

                            switch ($contentType) {

                                case 'deals':

                                    $deal = $content;

                                    break;

                                case 'experiences':
                                    $experience = $content;
                                    if (!is_null($experience->photos) && count($experience->photos) > 0) {
                                        foreach ($experience->photos as $photo) {
                                            $tmp->addImage(Image::create(url($photo))->setTitle($experience->name));
                                        }
                                    }

                                    break;

                                case 'events':

                                    $event = $content;
                                    if (!is_null($event->images) && count($event->images) > 0) {
                                        foreach ($event->images as $photo) {
                                            $tmp->addImage(Image::create(url($photo))->setTitle($event->name));
                                        }
                                    }

                                    break;

                                case 'teamMembers':
                                    $user = $content;
                                    try {
                                        $tmp->addImage(Image::create(url($user->avatar))->setTitle($user->full_name));
                                    } catch (Exception $e) {
                                    }

                                    break;

                                case 'articles':
                                    $article = $content;
                                    if ($article->is_legacy == 0) {
                                        foreach (json_decode($article->body, 1) as $bodyBlock) {
                                            $tmp->addImage(Image::create(url($bodyBlock['image']))->setTitle($this->altTagMl($bodyBlock['image_alt'])));
                                        }
                                    } else {
                                        $dom = new Dom;
                                        $dom->loadStr($article->content);
                                        $imgs = $dom->find('img');
                                        if (count($imgs) > 0) {
                                            foreach ($imgs as $img) {
                                                $tmp->addImage(Image::create(url($img->getAttribute('src')))->setTitle($img->getAttribute('alt')));
                                            }
                                        }
                                    }
                                    $tmp->addImage(Image::create(url($article->photos[0]))->setTitle($this->altTagMl($article->title)));
                                    break;

                                case 'videos':
                                    $video = $content;
                                    $tmp->addImage(Image::create(url($video->cover))->setTitle($video->name));
                                    if (!is_null($video->homepage_cover)) {
                                        $tmp->addImage(Image::create(url($video->homepage_cover))->setTitle($video->name));
                                    }
                                    break;

                                case 'recipes':

                                    $recipe = $content;
                                    if (!is_null($recipe->photos) && count($recipe->photos) > 0) {
                                        foreach ($recipe->photos as $photo) {
                                            $tmp->addImage(Image::create(url($photo))->setTitle($recipe->name));
                                        }
                                    }

                                    break;

                                case 'products':

                                    $product = $content;
                                    if (!is_null($product->photos) && count($product->photos) > 0) {
                                        foreach ($product->photos as $photo) {
                                            $tmp->addImage(Image::create(url($photo))->setTitle($product->name));
                                        }
                                    }

                                    break;

                                case 'manufacturers':
                                    $manufacturer = $content;
                                    if ($manufacturer->custom_blocks != '') {
                                        foreach (json_decode($manufacturer->custom_blocks, 1) as $block) {
                                            $dom = new Dom;
                                            $dom->loadStr($block['content']);
                                            $imgs = $dom->find('img');
                                            if (count($imgs) > 0) {
                                                foreach ($imgs as $img) {
                                                    $tmp->addImage(Image::create(url($img->getAttribute('src')))->setTitle($block['title']));
                                                }
                                            }
                                        }
                                    }

                                    if (!is_null($manufacturer->photos) && count($manufacturer->photos) > 0) {
                                        foreach ($manufacturer->photos as $photo) {
                                            $tmp->addImage(Image::create(url($photo))->setTitle($manufacturer->name));
                                        }
                                    }

                                    if (!is_null($manufacturer->logo)) {
                                        $tmp->addImage(Image::create(url($manufacturer->logo))->setTitle($manufacturer->name));
                                    }


                                    break;

                                case 'consortiums':
                                    $consortium = $content;
                                    if ($consortium->custom_blocks != '') {
                                        foreach (json_decode($consortium->custom_blocks, 1) as $block) {
                                            $dom = new Dom;
                                            $dom->loadStr($block['content']);
                                            $imgs = $dom->find('img');
                                            if (count($imgs) > 0) {
                                                foreach ($imgs as $img) {
                                                    $tmp->addImage(Image::create(url($img->getAttribute('src')))->setTitle($block['title']));
                                                }
                                            }
                                        }
                                    }

                                    if (!is_null($consortium->photos) && count($consortium->photos) > 0) {
                                        foreach ($consortium->photos as $photo) {
                                            $tmp->addImage(Image::create(url($photo))->setTitle($consortium->name));
                                        }
                                    }

                                    if (!is_null($consortium->logo)) {
                                        $tmp->addImage(Image::create(url($consortium->logo))->setTitle($consortium->name));
                                    }


                                    break;



                                default:

                                    break;
                            }

                            $sitemap->add($tmp);
                        #}
                    } catch (Exception $e) {
                    }
                }
                $sitemapPath = '/sitemaps/sitemap-' . $contentType . '-' . $locale . '.xml';
                $sitemap->writeToFile(public_path() . $sitemapPath);
                $smi->add($sitemapPath);
            }
        }

        // other url sitemap BEGIN

        $genericUrl = [
            'en' => [
                'en',
                'en/register',
                'en/recipes',
                'en/blog',
                'en/login',
                'en/events',
                'en/products',
                'en/products/Foods',
                'en/products/Wine',
                'en/products/Mixed',
                'en/videos'
            ],
            'it' => [
                'it',
                'it/register',
                'it/recipes',
                'it/blog',
                'it/login',
                'it/events',
                'it/products',
                'it/products/Foods',
                'it/products/Wine',
                'it/products/Mixed',
                'it/videos'
            ]
        ];

        foreach ($languages as $locale => $langFull) {

            $sitemapGeneric = Sitemap::create();

            foreach ($genericUrl[$locale] as $url) {

                #$httpCode = self::checkUrlStatus(url($url));
                #$this->line('Url ' . $url . ' has http response code:' . $httpCode);

                #if ($httpCode == 200) {
                    $tmp = Url::create(url($url));
                    $sitemapGeneric->add($tmp);
                #}
            }

            $sitemapPath = '/sitemaps/sitemap-generic-' . $locale . '.xml';
            $sitemapGeneric->writeToFile(public_path() . $sitemapPath);
            $smi->add($sitemapPath);
        }

        // other url sitemap END

        $smi->writeToFile(public_path() . '/sitemaps/sitemap-index.xml');
    }
}
