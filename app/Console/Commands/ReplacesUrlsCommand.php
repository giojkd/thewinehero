<?php

namespace App\Console\Commands;

use App\Models\Article;
use App\Models\Recipe;
use App\Models\Video;
use Exception;
use Illuminate\Console\Command;

class ReplacesUrlsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mmblp:replaceurls';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */

    public function newUrl($url, $lang)
    {

        $mapModel = [
            'blog' => 'App\Models\Article',
            'recipes' => 'App\Models\Recipe',
            'team-member' => 'App\Models\BackpackUser',
            'videocategory' => 'App\Models\Videocategory',
            'product' => 'App\Models\Product',
            'articlecategory' => 'App\Models\Articlecategory',
            'video' => 'App\Models\Video',
            'recipe' => 'App\Models\Recipe',
            'consortium' => 'App\Models\Consortium',
            'manufacturer' => 'App\Models\Manufacturer',
            'event' => 'App\Models\Event',
        ];

        $excluded = ['videos','recipes','search','products','host','cerca','Articles','events', 'o','c', 'OCM%20PSR'];
        $path = parse_url((string)$url);
        try{
        if($path){
            if(isset($path['path'])){

                $path = $path['path'];

                $explodedPath = explode('/', $path);
                if(isset($explodedPath[2])){
                    $type = $explodedPath[2];
                    if (!in_array($type, $excluded)) {
                        $id = collect(explode('/', $path))->last();

                        if(!is_numeric($id)){
                            return null;
                        }
                        $model  = $mapModel[$type];
                        $item = $model::find($id);

                        if (!is_null($item)) {
                            return ENV('APP_URL'). '/' . $lang . '/' . $type . '/' . $item->getTranslation('url', $lang);
                        }
                    }
                }
            }
        }
        }catch(Exception $e){
            $this->line('error with parsing old url');
            $this->line($id);
            dd($path);
        }


        return null;

    }

    public function handle()
    {


        $langs = array_keys(Config('backpack.crud.locales'));
        $pattern = "/\b(?:(?:https?|ftp|file):\/\/|www\.|ftp\.)[-A-Z0-9+&@#\/%=~_|$?!:,.]*[A-Z0-9+&@#\/%=~_|$]/i";




        Article::withoutSyncingToSearch(
            function () use ($langs, $pattern) {
                Article::limit(1000)->get()->each(function ($article, $key) use ($langs, $pattern) {
                    foreach ($langs as $lang) {
                        $content = $article->getTranslation('content', $lang);
                        preg_match_all($pattern, $content, $out);
                        $out = collect($out)->filter();
                        $replacedUrls = [];
                        if ($out->count() > 0) {
                            foreach ($out as $match) {
                                foreach ($match as $item) {
                                    $parsedUrl = parse_url((string)$item);
                                    if(count($parsedUrl) > 0){
                                        if(isset($parsedUrl['host'])){
                                            if (str_contains($parsedUrl['host'], 'mamablip')) {
                                                $replacedUrls[$item] = $this->newUrl($item, $lang);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        $replacedUrls = array_filter($replacedUrls);
                        if (count($replacedUrls) > 0) {
                            $this->line('Article '.$article->id.' had '.count($replacedUrls).' replaced urls in '.$lang);
                            foreach ($replacedUrls as $url => $replace) {
                                $content = str_replace($url, $replace, $content);
                            }
                        }
                        $article->setTranslation('content', $lang, $content)->saveWithoutEvents();
                    }
                });
            }
        );


        Video::withoutSyncingToSearch(
            function () use ($langs, $pattern) {
                Video::limit(1000)->get()->each(function ($article, $key) use ($langs, $pattern) {
                    foreach ($langs as $lang) {
                        $content = $article->getTranslation('description', $lang);
                        preg_match_all($pattern, $content, $out);
                        $out = collect($out)->filter();
                        $replacedUrls = [];
                        if ($out->count() > 0) {
                            foreach ($out as $match) {
                                foreach ($match as $item) {
                                    $parsedUrl = parse_url((string)$item);
                                    if (count($parsedUrl) > 0) {
                                        if (isset($parsedUrl['host'])) {
                                            if (str_contains($parsedUrl['host'], 'mamablip')) {
                                                $replacedUrls[$item] = $this->newUrl($item, $lang);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        $replacedUrls = array_filter($replacedUrls);
                        if (count($replacedUrls) > 0) {
                            $this->line('Video ' . $article->id . ' had ' . count($replacedUrls) . ' replaced urls in ' . $lang);
                            foreach ($replacedUrls as $url => $replace) {
                                $content = str_replace($url, $replace, $content);
                            }
                        }
                        $article->setTranslation('description', $lang, $content)->saveWithoutEvents();
                    }
                });
            }
        );


        Recipe::withoutSyncingToSearch(
            function () use ($langs, $pattern) {
                Recipe::limit(1000)->get()->each(function ($article, $key) use ($langs, $pattern) {
                    foreach ($langs as $lang) {
                        $content = $article->getTranslation('description', $lang);
                        preg_match_all($pattern, $content, $out);
                        $out = collect($out)->filter();
                        $replacedUrls = [];
                        if ($out->count() > 0) {
                            foreach ($out as $match) {
                                foreach ($match as $item) {
                                    $parsedUrl = parse_url((string)$item);
                                    if (count($parsedUrl) > 0) {
                                        if (isset($parsedUrl['host'])) {
                                            if (str_contains($parsedUrl['host'], 'mamablip')) {
                                                $replacedUrls[$item] = $this->newUrl($item, $lang);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        $replacedUrls = array_filter($replacedUrls);
                        if (count($replacedUrls) > 0) {
                            $this->line('Recipe ' . $article->id . ' had ' . count($replacedUrls) . ' replaced urls in ' . $lang);
                            foreach ($replacedUrls as $url => $replace) {
                                $content = str_replace($url, $replace, $content);
                            }
                        }
                        $article->setTranslation('description', $lang, $content)->saveWithoutEvents();
                    }
                });
            }
        );



    }
}
