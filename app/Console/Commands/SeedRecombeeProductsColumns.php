<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Recombee\RecommApi\Client;
use Recombee\RecommApi\Requests as Reqs;
use Recombee\RecommApi\Exceptions as Ex;

class SeedRecombeeProductsColumns extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mmblp:seedrecombeeproductscolumns';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $client = new Client(env('RECOMBEE_DB_ID'), env('RECOMBEE_DB_KEY'));

        $keys =  [
                1 => "name_en",
                2 => "name_it",
                3 => "subtitle_en",
                4 => "subtitle_it",
                5 => "brief_en",
                6 => "brief_it",
                12 => "description_short_en",
                13 => "description_short_it",
                14 => "description_en",
                15 => "description_it",
                19 => "short_description_en",
                24 => "short_description_it",
                25 => "url_it",
                20 => "url_en",
                27 => "brand",
                28 => "price",
                29 => "cover",
                37 => "ingredients_en",
                38 => "ingredients_it",
                39 => "category_en",
                40 => "category_it",
                41 => "photos",
                42 => "model",
                43 => "begins_at",
                44 => "ends_at",
                45 => "duration",
                46 => "reviews_avg",
                47 => "reviews_count",
                48 => "is_premium",
                49 => "views",
                50 => "created_at",
                51 => "calories",
                52 => "servings",
                53 => "preparation_time",
                54 => "complexity_id",
                55 => "servings_notes_it",
                56 => "servings_notes_en",
                57 => "general_notes_it",
                58 => "general_notes_en",
                59 => "preparation_time_notes_it",
                60 => "preparation_time_notes_en",
                61 => "wine_color_it",
                62 => "wine_color_en",
                63 => "denomination_it",
                64 => "denomination_en",
                65 => "wine_type_it",
                66 => "wine_type_en",
                67 => "year_en",
                68 => "year_it",
                69 => "enabled"



        ];


        $keyType = [
            'categories_it' => 'set',
            'categories_en' => 'set',
            'ingredients_it' => 'set',
            'ingredients_en' => 'set',
            'price' => 'double',
            'reviews_avg' => 'double',
            'reviews_count' => 'int',
            'duration' =>  'int',
            'photos' => 'imageList',
            'begins_at' => 'timestamp',
            'created_at' => 'timestamp',
            'ends_at' => 'timestamp',
            'is_premium' => 'boolean',
            'enabled' => 'boolean',
            'views' => 'int',
            'calories' => 'int',
            'servings' => 'int',
            'preparation_time' => 'int',
            'complexity_id' => 'int',




        ];


        #$client->send(new Reqs\ResetDatabase()); // Clear everything from the database

        foreach ($keys as $key) {
            $type = (isset($keyType[$key])) ? $keyType[$key] : 'string';
            $client->send(new Reqs\AddItemProperty($key, $type));
        }
    }
}
