<?php

namespace App\Console\Commands;

use App\Models\Release;
use Illuminate\Console\Command;

class ProcessReleasesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mmblp:processreleases';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $releases = Release::whereNull('processed_at')->where('release_at','<',date('Y-m-d H:i'))->get();
        if(!is_null($releases)){
            $releases->each(function($release,$key){

                list($id,$object) = explode('_',$release->object);
                $model = 'App\Models\\'.ucfirst($object);
                $item = $model::find($id);
                if (!is_null($item)) {
                    switch($object){
                        case 'article':
                            $item->enabled = 1;
                            $item->published_at = date('Y-m-d H:i');
                        break;
                        case 'event':

                        break;
                        case 'product':
                            $item->enabled = 1;
                        break;
                        case 'recipe':
                            $item->enabled = 1;
                            break;
                        case 'video':
                            $item->enabled = 1;
                            $item->active = 1;
                            break;
                    }
                    $item->save();

                }

                $release->processed_at = date('Y-m-d H:i');
                $release->save();
            });
        }
    }
}
