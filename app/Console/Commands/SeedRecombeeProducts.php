<?php

namespace App\Console\Commands;

use App\Models\Recommend;
use Illuminate\Console\Command;
use Recombee\RecommApi\Client;
use Recombee\RecommApi\Requests as Reqs;
use Recombee\RecommApi\Exceptions as Ex;

class SeedRecombeeProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mmblp:seedrecombeeproducts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $items = Recommend::get();
        $requests = array();

        $client = new Client(env('RECOMBEE_DB_ID'), env('RECOMBEE_DB_KEY'));
        #$bar = $this->output->createProgressBar(count($items));

        $items->each(function ($item, $key) use (&$requests) {

            $data = $item->getRecombeeData();



            if($data != false){

                $this->line($item->id . ' is a ' . $data['model']);

                $data['recommend_id'] = $item->id;

                $r = new Reqs\SetItemValues(
                    $item->id,
                    //values:
                    $data,
                    //optional parameters:
                    ['cascadeCreate' => true]
                    // Use cascadeCreate for creating item with given itemId, if it doesn't exist]
                );
                array_push($requests, $r);
            }
            #$bar->advance();

        });
        #$bar->finish();

#        dd($requests);

        $result =  $client->send(new Reqs\Batch($requests));

        dd($result);
    }
}
