<?php

namespace App\Console\Commands;

use App;
use App\Models\Article;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Spatie\ArrayToXml\ArrayToXml;


class GenerateAtomFeed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mmblp:generateatomfeed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $lang = 'en';

        App::setLocale($lang);

        $blog = Article::whereNotNull('published_at')
        ->where('published_at', '<=', date('Y-m-d'))
        ->orderBy('published_at', 'DESC')
        #->limit(10)
        ->get();

        $array = [];
        $array['title'] = 'MamaBlip Online Italian Food Cooking & Wine Classes. Video & Articles.';
        $array['link'] = ['_attributes' => ['href'=>'https://www.mamablip.com/en']];
        $array['updated'] = date('Y-m-d').'T'.date('H:i:s').'Z';
        $array['author'] = ['name' => ''];
        $array['id'] = '';

        foreach($blog as $article){
            $array['entry'][] = [
                'title' => $article->name,
                'link' => [
                    '_attributes' => [
                        'href' => $article->makeUrl()
                    ]
                ],
                'id' => $article->makeUrl(),
                'updated' => date('Y-m-d',strtotime($article->updated_at)).'T'. date('H:i:s', strtotime($article->updated_at)).'Z',
                'summary' => $article->subtitle
            ];
        }

        $result = ArrayToXml::convert(
            $array,
            [
                'rootElementName' => 'feed',
                '_attributes' => [
                    'xmlns' => 'http://www.w3.org/2005/Atom',
                ],
            ],
            true,
            'UTF-8'
        );

        Storage::disk('public')->put('atom_feed_'.$lang.'.xml', $result);

    }
}
