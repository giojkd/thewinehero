<?php

namespace App\Console\Commands;

use App\Models\Product;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class AssociateSponsorsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mmblp:associatesponsors';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $tables = [
            'manufacturer_recipe',
            'article_manufacturer',
            'event_manufacturer',
            'manufacturer_recipe',
            'manufacturer_video',
            'article_consortium',
            'consortium_event',
            'consortium_recipe',
            'consortium_video',
        ];


        foreach($tables as $table){
            DB::table($table)->truncate();
        }

        $products = Product::get();
        foreach($products as $product){

            foreach(['events','videos','articles','recipes'] as $model){
                $models = $product->$model;
                if(!is_null($models)){
                    foreach($models as $model){
                        if(is_object($product->manufacturer)){
                            $model->manufacturers()->attach($product->manufacturer->id);
                        }
                        if (is_object($product->consortium)) {
                            $model->consortiums()->attach($product->consortium->id);
                        }
                    }
                }
            }

        }
    }
}
