<?php

namespace App\Console\Commands;

use App\Models\Product;
use Illuminate\Console\Command;

class SyncCategoriesListWithProductCategories extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mmblp:synccategorieslist';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        #Product::withoutSyncingToSearch(function () {
        Product::get()->each(function ($product, $key) {
            $product->categories_list = $product->categories->pluck('id');
            $product->save();
        });
        #});
        #return 0;
    }
}
