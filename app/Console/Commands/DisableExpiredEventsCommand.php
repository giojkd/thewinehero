<?php

namespace App\Console\Commands;

use App\Models\Event;
use Illuminate\Console\Command;

class DisableExpiredEventsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mmblp:disableexpiredevents';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        Event::whereEnabled(1)->each(function($event,$key){
            if(date('YmdHis') > $event->endsAtUnixTimestamp()){
                $event->enabled = 0;
                $event->save();
            }
        });

    }
}
