<?php

namespace App\Console\Commands;

use App\Models\Article;
use Illuminate\Console\Command;

class NttsArticlesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mmblp:nttsarticles';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Article::where('is_ntts',1)
        ->where('is_ntts_processed',0)
        ->get()
        ->each(function($article){
            $article->ntts();
        });
    }
}
