<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;




class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive('img', function ($param) {
            return "<?php echo asset('/front/images/'.$param); ?>";
        });
        Blade::directive('hss', function ($param) {
            return "<?php echo '<div class=\"height-spacer\" style=\"height: '.$param.'px\"></div>'; ?>";
        });
        Blade::directive('altTag', function ($param) {
            return "<?php echo (count(explode('_', pathinfo($param)['filename'])) > 1) ? str_replace('-', ' ', explode('_', pathinfo($param)['filename'])[0]) : ''; ?>";
        });
        Blade::directive('altTagMl', function ($param) {
            return "<?php echo (count(explode('_', pathinfo($param)['filename'])) > 1) ? __('tagAlt.' . str_replace('-', ' ', explode('_', pathinfo($param)['filename'])[0])) : ''; ?>";
        });

        Blade::directive('cim', function ($param) {
            return "<?php echo 'https://aalsdnxkwq.cloudimg.io/v7/'.str_replace(['https://','http://'],'',$param); ?>";
        });




        Blade::directive('fp', function ($param) {
            #$param = (double)$param;
            return "<?php echo number_format($param,2).'<sup>&euro;</sup>' ?>";
        });


        /*
    Relation::morphMap([
      #'posts' => 'App\Post',
      'videos' => 'App\Models\Video',
    ]);
    */
    }
}
