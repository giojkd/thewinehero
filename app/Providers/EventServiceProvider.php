<?php

namespace App\Providers;

use App\Http\Requests\RecommendRequest;
use App\Models\Article;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use App\Models\Event;
use App\Models\Guest;
use App\Models\Review;
use App\Observers\ReviewObserver;
use App\Models\Plan;
use App\Models\Product;
use App\Models\Purchase;
use App\Observers\PlanObserver;
use App\Models\Recipe;
use App\Models\Recommend;
use App\Models\Video;
use App\Observers\ArticleObserver;
use App\Observers\EventObserver;
use App\Observers\GuestObserver;
use App\Observers\ProductObserver;
use App\Observers\PurchaseObserver;
use App\Observers\RecipeObserver;
use App\Observers\RecommendObserver;
use App\Observers\VideoObserver;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
     protected $listen = [
     Registered::class => [
       SendEmailVerificationNotification::class,
     ],
     \SocialiteProviders\Manager\SocialiteWasCalled::class => [
       // add your listeners (aka providers) here
       'SocialiteProviders\\Facebook\\FacebookExtendSocialite@handle',
     ],
   ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Article::observe(ArticleObserver::class);
        Event::observe(EventObserver::class);
        Video::observe(VideoObserver::class);

        Purchase::observe(PurchaseObserver::class);
        Review::observe(ReviewObserver::class);
        Plan::observe(PlanObserver::class);
        Recipe::observe(RecipeObserver::class);
        Product::observe(ProductObserver::class);

        Recommend::observe(RecommendObserver::class);

        Guest::observe(GuestObserver::class);
    }
}
