<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
#use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;
use DB;
use Laravel\Cashier\Billable;
use Auth;
use Str;

class User extends Authenticatable #implements MustVerifyEmail
{
    use Notifiable;
    use CrudTrait;
    use \App\Traits\Helpers;
    use Billable;
    #use HasTranslations;
    protected $table = 'users';

    public $translatable = ['biography', 'meta_title', 'url', 'h1', 'metatitle', 'meta_description'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    #protected $fillable = [ 'has_profile','h1','meta_description','metatitle','show_in_home', 'name', 'email', 'password','surname','heading','biography','social_twitter','social_facebook','social_youtube','social_linkedin','website','avatar'];

    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function favoriteType($type){
        return $type::find($this->favorites()->where('favoriteable_type',$type)->get()->pluck('favoriteable_id'));
    }

    public function purchaseType($type)
    {
        return $type::find($this->purchases()->where('purchaseable_type', $type)->get()->pluck('purchaseable_id'));
    }

    protected $modelNameOverride = 'team-member';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function favorites(){
      return $this->hasMany('App\Models\Favorite','user_id');
    }

    public function isFavorite($item){
        $model = 'App\Models\\'.Str::ucfirst($item->getModelName());

      return
      $this
      ->favorites()
      ->where('favoriteable_type', $model)
      ->where('favoriteable_id',$item->id)
      ->exists();
    }


    public function canPurchase($model){
        $plan = $this->plan;
        if (!is_null($plan) && strtotime($plan->expires_at) > time() && $plan->$model > 0) {
                return true;
        }

        return false;
    }



    public function purchases()
    {
        return $this->hasMany('App\Models\Purchase','user_id');
    }

    public function purchase_without_subscription($model,$id, $grandTotal){
        #$model = Str::plural($model);
        $this->purchases()->create([
            'purchaseable_type' =>  $model,
            'purchaseable_id' => $id,
            'grand_total' => $grandTotal/100
        ]);
    }

    public function purchase($model,$id){
        #$model = Str::plural($model);
        $user = $this;
        $model = Str::lower($model);

        if($user->canPurchase(Str::plural($model))){

           $purchase =  $user->purchases()->create([
                'purchaseable_type' => 'App\Models\\'.Str::ucfirst($model),
                'purchaseable_id' => $id
            ]);

            $plan = $user->plan;
            $model = Str::plural($model);
            $plan->$model--;
            $plan->save();

        }else{

            echo 'error';

        }
    }

    public function purchased($model, $id)
    {
        $model = Str::ucfirst($model);
        $purchase = $this->purchases()->where('purchaseable_type', 'App\Models\\'.$model)->where('purchaseable_id', $id)->first();

        if (!is_null($purchase)) {
            return true;
        }
        return false;
    }

    public function getPurchase($model, $id){
        $model = Str::ucfirst($model);
        $purchase = $this->purchases()->where('purchaseable_type', 'App\Models\\' . $model)->where('purchaseable_id', $id)->first();

        if (!is_null($purchase)) {
            return $purchase;
        }
        return null;
    }

    public function plan(){
        return $this->belongsTo('App\Models\Plan');
    }

    public function hasActivePlan()
    {
        $plan = $this->plan;
        if (!is_null($plan) && strtotime($plan->expires_at) > time()) {
            return true;
        }
        return false;
    }

    public function full_name(){
        return Str::ucfirst(Str::lower($this->name)).' '. Str::ucfirst(Str::lower($this->surname));

    }

    public function scopeShownInHome($query){
        return $query->where('active',1)->where('show_in_home',1);
    }

}
