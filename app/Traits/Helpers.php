<?php

namespace App\Traits;

use Str;
use Illuminate\Support\Facades\Schema;
use URL;
use Auth;
use Backpack\Settings\app\Models\Setting;
use App\Mail\SingleContentPurchaseConfirmation;
use App\Mail\SingleContentPurchaseFinalConfirmation;
use App\Models\Recommend;
use Mail;
use App;
use voku\helper\StopWords;

trait Helpers
{

    var $carriers = [
        'us' =>
        [
            [
                'name' => 'BRT',
                'vat' => 0.22,
                'includes_vat' => 0,
                'volume_weight' => 0.0000003,
                'rules' => [
                    [
                        'from' => '0', #g
                        'to' => '3000',
                        'price' => '5.4'
                    ],
                    [
                        'from' => '3001', #g
                        'to' => '5000',
                        'price' => '6.19'
                    ],
                    [
                        'from' => '5001', #g
                        'to' => '10000',
                        'price' => '7.12'
                    ],
                    [
                        'from' => '10001', #g
                        'to' => '20000',
                        'price' => '8.92'
                    ],
                    [
                        'from' => '20001', #g
                        'to' => '30000',
                        'price' => '10.62'
                    ],
                    [
                        'from' => '30001', #g
                        'to' => '50000',
                        'price' => '13.89'
                    ],
                    [
                        'from' => '50001', #g
                        'to' => '100000',
                        'price' => '18.92'
                    ]
                ],
                'over_highest_rule' => [
                    'price' => '21.32',
                    'every' => '100000' #g
                ]
            ]
        ],
        'it' =>
        [
            [
                'name' => 'BRT',
                'vat' => 0.22,
                'includes_vat' => 0,
                'volume_weight' => 0.0000003,
                'rules' => [
                    [
                        'from' => '0', #g
                        'to' => '3000',
                        'price' => '5.4'
                    ],
                    [
                        'from' => '3001', #g
                        'to' => '5000',
                        'price' => '6.19'
                    ],
                    [
                        'from' => '5001', #g
                        'to' => '10000',
                        'price' => '7.12'
                    ],
                    [
                        'from' => '10001', #g
                        'to' => '20000',
                        'price' => '8.92'
                    ],
                    [
                        'from' => '20001', #g
                        'to' => '30000',
                        'price' => '10.62'
                    ],
                    [
                        'from' => '30001', #g
                        'to' => '50000',
                        'price' => '13.89'
                    ],
                    [
                        'from' => '50001', #g
                        'to' => '100000',
                        'price' => '18.92'
                    ]
                ],
                'over_highest_rule' => [
                    'price' => '21.32',
                    'every' => '100000' #g
                ]
            ]
        ]
    ];

    function getShippingQuotationByWeight($country,$weight)
    {

        $country = strtolower($country);

        $we_ = $weight * 1000;

        if (is_null($we_)) {
            return 0;
        }

        $quotations = [];

        $carriers = $this->carriers[$country];

        foreach ($carriers as $carrier) {

            $we = $we_;

            $found = 0;
            foreach ($carrier['rules'] as $rule) {
                if ($rule['from'] < $we && $we < $rule['to']) {
                    $price = $rule['price'];
                    if ($carrier['includes_vat'] == false) {
                        $price = $price * (1 + $carrier['vat']);
                    }
                    $quotations[] = ['we' => $we_, 'carrier' => $carrier['name'], 'value' => number_format((float) $price, 2, '.', '')];
                    $found = 1;
                }
            }

            if ($found == 0) {
                $pvs = ceil($we / $carrier['over_highest_rule']['every']);
                $price = $carrier['over_highest_rule']['price'] * $pvs;
                $quotations[] = ['we' => $we_, 'carrier' => $carrier['name'], 'value' => number_format((float) $price, 2, '.', '')];
            }

        }

        $quotations = collect($quotations);

        return $quotations->sortBy('value');
    }

    public function getAlternateLangUrls(){
        #$langs = array_keys(Config('backpack.crud.locales'));
        $urls = [];
        #$langs[] = 'en-us';
        $langMap = ['en' => 'en', 'it' => 'it', 'en-us' => 'en'];
        foreach($langMap as $iso => $lang){
            $url = $this->makeUrl($lang);
            #must check if url is not legacy
            if(!self::isLegacyUrl($url)){
                $urls[$iso] = $url;
            }

        }

        return  ['urls' => $urls];

    }

    public static function isLegacyUrl($url){
        return is_numeric(collect(explode('/', $url))->last());
    }


    public function makeRecommend(){
        $this->recommend()->create([]);
    }

    public function recommend(){


        return $this->morphOne(Recommend::class, 'recommendable');

    }

    public function saveWithoutEvents(array $options = [])
    {
        return static::withoutEvents(function () use ($options) {
            return $this->save($options);
        });
    }

    public function sendContentPurchaseFinalNotification($user = null){

        $data['content'] = $this->getMailNotificationsDefaultData();
        $data['content']['content_id'] = $this->id;
        $data['content']['user_full_name'] = $user->full_name;
        $data['subject'] = 'Great!';

        $data['content']['confirmation_notes'] = $this->final_confirmation_notes;

        $data['content']['modelType'] = $this->getModelName();
        $data['content']['model'] = $this;
        $data['content']['user'] = $user;

        $recipients = explode(',', env('ADMIN_ORDER_NOTIFICATION_EMAILS'));
        $recipients[] = $user->email;

        foreach($recipients as $recipient){
            $result = Mail::to($recipient)->send(new SingleContentPurchaseFinalConfirmation($data));
        }

    }

    public function sendContentPurchaseNotification($user){

        $data['content'] = $this->getMailNotificationsDefaultData();
        $data['content']['content_id'] = $this->id;
        $data['content']['user_full_name'] = $user->full_name;
        $data['subject'] = 'Thank you, we are processing your order.';

        $data['content']['confirmation_notes'] = $this->confirmation_notes;

        $data['content']['modelType'] = $this->getModelName();
        $data['content']['model'] = $this;
        $data['content']['user'] = $user;

        $recipients = explode(',',env('ADMIN_ORDER_NOTIFICATION_EMAILS'));
        $recipients[] = $user->email;

        foreach($recipients as $recipient){
            $result = Mail::to($recipient)->send(new SingleContentPurchaseConfirmation($data));
        }

    }

    function getMailNotificationsDefaultData(){
        return [
            'highlight_color' => '#fab417',
            'light_logo' => url('front/images/logo-white.png'),
            'app_name' => 'MaMaBlip',
            'company_name' => 'MaMaBlip srl',
            'company_address' => Setting::get('address_line_1') . ' ' . Setting::get('address_line_2'),
            'help_center_link' => Setting::get('help_link'),
            'facebook_link' => Setting::get('facebook_link'),
            'twitter_link' => Setting::get('twitter_link'),
            'instagram_link' => Setting::get('instagram_link'),
            'youtube_link' => Setting::get('youtube_link'),
            'help_center_link' => Setting::get('help_link'),
            'resource_prefix' => url('/front/images/')
        ];
    }

    function addZ($value)
    {
        if ((float)$value < 10)
            return '0' . $value;
        return $value;
    }

    public function getModelName()
    {

        $reflection = new \ReflectionClass($this);
        $modelName = implode("\\", [$reflection->getNamespaceName(), ]);
        return Str::lower($reflection->getShortName());
        return $modelName;
    }

    public function getModelNameFull(){
        $modelName = Str::ucfirst($this->getModelName());
        return '\\App\Models\\'. $modelName;
    }

    public function applyPlanDiscount($content, $price)
    {
        if (Auth::check()) {
            $content = Str::plural($content);
            $user = Auth::user();

            $plan = $user->plan;

            if(!is_null($plan)){
                $discountName = $content . '_discount';
                return $price * (1 - $plan->$discountName / 100);
            }else{
                return $price;
            }

        } else {
            return $price;
        }
    }

    public function sameCategoryItems($limit = 10)
    {

        $model = $this->getModelNameFull();

        $return =  $model::orderBy('created_at', 'DESC')->where('id','!=',$this->id)->limit($limit);

        if ($model == '\App\Models\Article') {
            $return = $return->where('status', 1);
            $return = $return->whereHas('categories',function($query){
                return $query->whereIn('category_id',$this->categories->pluck('id'));
            });
        }

        return $return->get();

        /*
        $item = $this;
        return
        $this
        ->categories()
        ->with([$this->getTable() => function ($query) use ($item) {
            $query->where($this->getTable() . '.id', '!=', $item->id);
        }])
        ->get()
        ->pluck($this->getTable())
        ->flatten(1)
        ->unique('id');
        */
    }

    public function categories()
    {
        return $this->belongsToMany('App\Models\Category');
    }

    public function makeUrlDownload()
    {
        return URL::temporarySignedRoute('contentDownload', now()->addMinutes(5), ['model' => $this->getModelName(), 'id' => $this->id]);
    }

    public function checkUrl($name){
        if(isset($this->getTranslations('url')[App::getLocale()]) && $this->getTranslations('url')[App::getLocale()] != '' ){
            $url = explode('-',$name);
            unset($url[count($url)-1]);
            $url = implode('-',$url);
            if($url != Str::slug($this->url)){
                abort(404);
            }
        }else{
            $url = $name;
            if($url != Str::slug($this->name)){
                abort(404);
            }
        }

    }

    public function generateUrl($locale = '',$attributeToTranslate = 'name'){

        $url = [];

        if(in_array($attributeToTranslate, $this->translatable)){
            $name = Str::lower($this->getTranslation($attributeToTranslate, $locale));
        }else{
            $name = Str::lower($this->$attributeToTranslate);
        }



        $stopWords = new StopWords();
        $stopwords = $stopWords->getStopWordsFromLanguage($locale);



        $words = preg_split('/[^-\w\']+/', $name, -1, PREG_SPLIT_NO_EMPTY);
        $wordsAux = [];

        if (count($words) > 1) {
            foreach($words as $word){
                if(!in_array($word, $stopwords)){
                    $wordsAux[] = $word;
                }
            }
        }

        $auxName = implode(" ", $wordsAux);

        $url[] = Str::slug($auxName);

        $returnUrl = implode('/', $url);

        $this->setTranslation('url',$locale, $returnUrl);
        $this->saveWithoutEvents();


    }

    public function makeUrl($locale = '')
    {
        $url = [];
        if ($locale != '') {

        } else {
            $locale = session('locale');
        }

        $url[] = $locale;

        if (isset($this->modelNameOverride)) {
            $url[] = $this->modelNameOverride;
        } else {
            $reflection = new \ReflectionClass($this);
            $modelName = Str::lower($reflection->getShortName());
            $url[] = $modelName;
        }

        try{
            $name = $this->getTranslations()['name'][$locale];
        }catch(\Exception $e){
            $name = $this->name;
        }

        $legacyUrl = 1;

        try {

            $name =  $this->getTranslations()['url'][$locale];
            $legacyUrl = 0;

        } catch (\Exception $e) {

        }

        $url[] = Str::slug($name);

        if($legacyUrl){
            $url[] = $this->id;
        }

        return url(implode('/', $url));
    }

    public function makeUrlTag($crud = false)
    {
        $label = 'Link';
        $class = '';
        return '<a target="_blank" class="' . $class . '" href="' . $this->makeUrl(app()->getLocale()) . '">' . $label . '</a>';
    }

    public function updateReviewsStatus()
    {
        $reviews = $this->reviews;
        if (!\is_null($reviews)) {
            $this->reviews_avg = $reviews->avg('score');
            $this->reviews_count = $reviews->count();
        } else {
            $this->reviews_avg = 0;
            $this->reviews_count = 0;
        }
        $this->save();
    }


    public function reviews()
    {
        return $this->morphMany('App\Models\Review', 'reviewable');
    }

    public function getTable()
    {
        return $this->table;
    }


    public function getNotificationStandardData($id)
    {

        $data = [
            'dark_logo_1' => 'http://atlantidexdev.amtitalia.com/uploads/logo-dark.png',
            'dark_logo_2' => 'http://atlantidexdev.amtitalia.com/uploads/logo-dark.png',
            'light_logo_1' => 'http://atlantidexdev.amtitalia.com/uploads/logo-dark.png',
            'light_logo_2' => 'http://atlantidexdev.amtitalia.com/uploads/logo-dark.png',
            'highlight_color_1' => '#a8c8dc',
            'highlight_color_2' => '#a8c8dc',
            'company_name' => 'Atlantidex',
            'company_address' => 'Borgo San Frediano, 15 50124 Firenze',
            'facebook_link' => '',
            'twitter_link' => '',
            'instagram_link' => '',
            'help_center_link' => '',
            'preferences_link' => '',
            'unsubscribe_link' => '',
            'app_name' => env('APP_NAME'),
            'url' => config('app.url'),
            'resource_prefix' => config('app.url') . '/front/notification_resources/'
        ];

        $data['dark_logo'] = $data['dark_logo_' . $id];
        $data['light_logo'] = $data['light_logo_' . $id];
        $data['highlight_color'] = $data['highlight_color_' . $id];

        return $data;
    }

    public function sponsors(){

        $manufacturers = $this->manufacturers;
        $consortiums = $this->consortiums;

        $sponsors = collect([]);
        $sponsors = $sponsors->merge($manufacturers);
        $sponsors = $sponsors->merge($consortiums);

        return $sponsors;

    }

    public function getLinkNameAttribute(){
        return '<a href="'.$this->makeUrl().'">'.$this->name.'</a>';
    }

    public function getEntityLinkName($model = 'recipes'){

        $items = $this->$model;
        $return = [];
        if(!is_null($items)){
            foreach($items as $item){
                $return[] = $item->link_name;
            }
        }
        return implode(',',$return);

    }

    public function setUrlAttribute($value){


        $this->attributes['url'] = Str::slug(trim($value));

    }

}
