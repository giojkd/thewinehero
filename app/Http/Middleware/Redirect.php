<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class Redirect
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {



        $rows = preg_split('/\r\n|[\r\n]/', Storage::disk('local')->get('redirects.txt'));

        $url = [];



        foreach ($rows as $row) {

            $rowData = explode(' ', $row);

            if(isset($rowData[0]) && isset($rowData[1])){
                $url = $rowData[0];
                $code = $rowData[1];
                $urls[$url] = [
                    'code' => $code,
                ];
                if (isset($rowData[2])) {
                    $destination = $rowData[2];
                    $urls[$url]['destination'] = $destination;
                }
            }

        }



        if(!isset($urls)){
            return $next($request);
        }

        $url = trim($request->url(),'/');


        if(isset($urls[$url])){
            $foundUrl = $urls[$url];

            switch($foundUrl['code']){
                case '302':
                    return redirect($foundUrl['destination'], $foundUrl['code']);
                    break;
                case '301':
                    return redirect($foundUrl['destination'], $foundUrl['code']);
                    break;
                case '404':
                    abort(404);
                    break;
            }
        }

        return $next($request);

    }
}
