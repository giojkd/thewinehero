<?php
namespace App\Http\Middleware;

use Closure;
use Session;
use App;
use Config;
use Illuminate\Support\Facades\URL;

class LanguageSwitcher
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (!Session::has('locale'))
        {
           Session::put('locale',Config::get('app.locale'));
        }

        if ($request->locale) {

            if(!in_array($request->locale,['it','en'])){
                abort(404);
            }

            Session::put('locale', $request->locale);
        }

        App::setLocale(session('locale'));
        URL::defaults(['locale' => session('locale')]);

        return $next($request);
    }
}
