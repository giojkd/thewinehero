<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Session;
use DB;

class SessionController extends Controller
{
    //
    public function test(Request $request){

          $id = $request->session()->getId();
          $session = Session::findOrFail($id);
          DB::table('sessions')->where('id',$id)->update(['lead_id' => 1]);

    }
}
