<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Socialite;
use App\User;
use Auth;

class SocialLoginController extends Controller
{
    //
    public function facebook(){
      $user = Socialite::driver('facebook')->user();
      return $this->loginUserWithSocial($user);
    }

    public function returnRedirect($social){
      return Socialite::with($social)->redirect();
    }

    public function loginUserWithSocial($data)
    {
        $user = User::firstOrCreate([
            'email' => $data->user['email']
        ]);
        $name = explode(' ',trim($data->user['name']));
        $user->name = $name[0];
        $user->surname = $name[1];
        $user->save();
        Auth::loginUsingId($user->id);
        return redirect(Route('home'));
    }
}
