<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Image;
use Str;

class ImageResizeController extends Controller
{
    public function thumbs($filename)
    {
        $file = storage_path('app/public/' . $filename);

        $img = Image::make($file)
            ->resize(120, 120);
        return $img->response('jpg');
    }

    public function resize($size = null, $filename)
    {

        #$filename = basename($filename);
        $file = public_path($filename);

        $extension = collect(Str::of($filename)->explode('.'))->last();



        switch ($size[0]) {
            case 'h':
                $size = substr($size, 1);

                $img =  Image::cache(function ($image) use ($file, $size) {
                    $image->make($file)->resize(null, $size, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }, 14400, true);



                break;
            case 'w':
                $size = substr($size, 1);

                $img =  Image::cache(function ($image) use ($file, $size) {
                    $image->make($file)->resize($size, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }, 14400, true);

                break;
            default:

                $size = explode('x', $size);

                if (count($size) > 1) {

                    $img =  Image::cache(function ($image) use ($file, $size) {
                        $image->make($file)->resize($size[0], $size[1]);
                    }, 14400, true);

                }

                if (count($size) == 1) {
                    $size[0] = ($size[0] != '') ? $size[0] : 120;

                    $img =  Image::cache(function ($image) use ($file, $size) {
                        $image->make($file)->resize($size[0], null, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                    }, 14400, true);


                }

                break;
        }




        return $img->response($extension);
        #return $img->response('webp');
    }
}
