<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ExperienceRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ExperienceCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ExperienceCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Experience');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/experience');
        $this->crud->setEntityNameStrings('experience', 'experiences');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();

        $this->crud->addColumn(
            [
                'name' => 'id',
                'type' => 'text',
                'label' => 'ID'
            ]
        );

        $this->crud->addColumn([
            'name' => 'enabled',
            'label' => "Enabled",
            'type' => 'boolean',

        ]);


        $this->crud->addColumn([
            'label' => 'Name',
            'name' => 'name',
            'type' => 'text'
        ]);

        $this->crud->addColumn([
            'label' => 'Free Transfer',
            'name' => 'free_transfer',
            'type' => 'text'
        ]);
        $this->crud->addColumn([
            'label' => 'Up To',
            'name' => 'free_transfer_up_to',
            'type' => 'text'
        ]);


        $this->crud->addColumn([
            'name' => 'url',
            'type'     => 'closure',
            'function' => function ($entry) {
                $langs = array_keys(Config('backpack.crud.locales'));
                $return = '<table>';
                foreach ($langs as $lang) {
                    $return .= '<tr><td>' . $lang . '</td><td>' . parse_url($entry->makeUrl($lang))['path'] . '</td></tr>';
                }
                $return .= '</table>';
                return $return;
            }
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(ExperienceRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();
        $this->crud->addField([
            'label' => 'Enabled',
            'name' => 'enabled',
            'type' => 'checkbox',
        ]);

        $this->crud->addField([
            'label' => 'Interstitial Page',
            'name' => 'interstitial_page_after_adding_to_cart',
            'type' => 'checkbox',
        ]);

        $this->crud->addField([
            'label' => 'Name',
            'name' => 'name',
            'type' => 'text'
        ]);
        $this->crud->addField([
            'label' => 'Subtitle',
            'name' => 'subtitle',
            'type' => 'text'
        ]);


        $this->crud->addField([
            'name' => 'url',
            'label' => "Url",
            'type' => 'text',

        ]);

        $this->crud->addField([
            'label' => 'Starting price',
            'name' => 'starting_price',
            'prefix' => "€",
            'type' => 'number',
            'suffix' => ".00",
            'attributes' => ["step" => "any"]
        ]);

        $this->crud->addField([
            'label' => 'Short description',
            'name' => 'short_description',
            'type' => 'textarea'
        ]);

        $this->crud->addField([
            'label' => 'Description',
            'name' => 'description',
            'type' => 'tinymce'
        ]);

        $this->crud->addField([
            'label' => 'What is included',
            'name' => 'whats_included',
            'type' => 'tinymce'
        ]);

        $this->crud->addField([
            'label' => 'What is not included',
            'name' => 'whats_not_included',
            'type' => 'tinymce'
        ]);

        $this->crud->addField([
            'label' => 'Address',
            'name' => 'address',
            'type' => 'address_google',
            'store_as_json' => true,
        ]);

        $this->crud->addField([
            'label' => 'Available as private tour',
            'name' => 'can_be_private',
            'type' => 'checkbox',
        ]);

        $this->crud->addField([
            'label' => 'Duration',
            'name' => 'duration',
            'type' => 'time',
        ]);

        $this->crud->addField([
            'label' => 'Free transfer',
            'name' => 'free_transfer',
            'prefix' => "€",
            'type' => 'number',
            'suffix' => ".00",
        ]);

        $this->crud->addField([
            'label' => 'Transfer up to',
            'name' => 'free_transfer_up_to',
            'type' => 'radio',
            'inline' => true,
            'options'     => [
                // the key will be stored in the db, the value will be shown as label;
                1 => 1,
                2 => 2,
                3 => 3,
                4 => 4,
                5 => 5,
                6 => 6,
                7 => 7,
                8 => 8,
                9 => 9,
                10 => 10,
            ],
        ]);

        $this->crud->addField([  // Select2
            'label' => "Manufacturer",
            'type' => 'select2',
            'name' => 'manufacturer_id', // the db column for the foreign key
            'entity' => 'manufacturer', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => "App\Models\Manufacturer", // foreign key model

        ]);

        $this->crud->addField([

            'name' => 'photos', // db column name
            'label' => 'Photos', // field caption
            'type' => 'dropzone', // voodoo magic
            'prefix' => '/', // upload folder (should match the driver specified in the upload handler defined below)
            'upload-url' => '/' . config('backpack.base.route_prefix') . '/media-dropzone', // POST route to handle the individual file uploads
        ]);


        $this->crud->addField(
            [   // SelectMultiple = n-n relationship (with pivot table)
                'label' => "Languages",
                'type' => 'select2_multiple',
                'name' => 'languages', // the method that defines the relationship in your Model
                'entity' => 'languages', // the method that defines the relationship in your Model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'model' => "App\Models\Language", // foreign key model
                'pivot' => true, // on create&update, do you need to add/delete pivot table entries?

                // optional
                #'options'   => (function ($query) {
                #  return $query->orderBy('name', 'ASC')->where('depth', 1)->get();
                #}), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select
            ]
        );


    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function handleDropzoneUpload(DropzoneRequest $request)
    {
        $disk = "custom_public"; //
        $destination_path = "";
        $destination_path_filename = "storage";
        $file = $request->file('file');

        try {
            $image = \Image::make($file);
            $filename = md5($file->getClientOriginalName() . time()) . '.jpg';
            \Storage::disk($disk)->put($destination_path . '/' . $filename, $image->stream());
            return response()->json(['success' => true, 'filename' => $destination_path_filename . '/' . $filename]);
        } catch (\Exception $e) {
            dd($e);
            if (empty($image)) {
                return response('Not a valid image type', 412);
            } else {
                return response('Unknown error', 412);
            }
        }
    }
}
