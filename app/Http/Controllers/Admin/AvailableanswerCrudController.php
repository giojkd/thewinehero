<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AvailableanswerRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class AvailableanswerCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class AvailableanswerCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Availableanswer');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/availableanswer');
        $this->crud->setEntityNameStrings('availableanswer', 'availableanswers');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();
        $this->crud->addColumn([  // Select2
            'label' => "Question",
            'type' => 'select',
            'name' => 'question_id', // the db column for the foreign key
            'entity' => 'question', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            // optional
            'model' => "App\Models\Question", // foreign key model
        ]);


        $this->crud->addColumn([
            'name' => 'name',
            'label' => "Answer",
            'type' => 'text',
        ]);

        $this->crud->addColumn([   // Checkbox
            'name' => 'is_correct',
            'label' => 'Is correct',
            'type' => 'boolean',
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(AvailableanswerRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();

        $this->crud->addField([  // Select2
            'label' => "Question",
            'type' => 'select2',
            'name' => 'question_id', // the db column for the foreign key
            'entity' => 'question', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            // optional
            'model' => "App\Models\Question", // foreign key model
            'default' => 2, // set the default value of the select2
            'options' => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select
            'tab' => 'info'
        ]);


        $this->crud->addField([
            'name' => 'name',
            'label' => "Answer",
            'type' => 'text',
            'tab' => 'info'
        ]);

        $this->crud->addField([   // Checkbox
            'name' => 'is_correct',
            'label' => 'Is correct',
            'type' => 'checkbox',
            'tab' => 'info'
        ]);

    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
