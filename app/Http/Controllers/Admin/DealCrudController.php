<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\DealRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class DealCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class DealCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Deal::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/deal');
        CRUD::setEntityNameStrings('deal', 'deals');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
#        CRUD::setFromDb(); // columns

        $this->crud->addColumns([
            [
                'name' => 'active',
                'type' => 'boolean',
                'label' => 'Active'
            ],
            [
                'name' => 'name',
                'type' => 'text',
                'label' => 'Name'
            ],
            [
                'name' => 'starts_at',
                'type' => 'datetime',
                'label' => 'Stars at'
            ],
            [
                'name' => 'ends_at',
                'type' => 'datetime',
                'label' => 'Ends at'
            ],
            [
                'name' => 'ends_at',
                'type' => 'datetime',
                'label' => 'Ends at'
            ],
            [
                'name' => 'url',
                'type'     => 'closure',
                'function' => function ($entry) {
                    $langs = array_keys(Config('backpack.crud.locales'));
                    $return = '<table>';
                    foreach ($langs as $lang) {
                        $return .= '<tr><td>' . $lang . '</td><td>' . parse_url($entry->makeUrl($lang))['path'] . '</td></tr>';
                    }
                    $return .= '</table>';
                    return $return;
                }
            ],
            [
                'name' => 'is_featured',
                'type' => 'boolean',
                'label' => 'Is featured'
            ]
        ]);



        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(DealRequest::class);

        #CRUD::setFromDb(); // fields

        $this->crud->addFields([
            [
                'name' => 'active',
                'type' => 'boolean',
                'label' => 'Active'
            ],
            [
                'name' => 'is_featured',
                'type' => 'checkbox',
                'label' => 'Is featured'
            ],
            [
                'name' => 'name',
                'type' => 'text',
                'label' => 'Name'
            ],
            [
                'name' => 'short_description',
                'type' => 'textarea',
                'label' => 'Short description'
            ],
            [
                'name' => 'description',
                'type' => 'wysiwyg',
                'label' => 'Description'
            ],
            [
                'name' => 'starts_at',
                'type' => 'datetime',
                'label' => 'Stars at'
            ],
            [
                'name' => 'ends_at',
                'type' => 'datetime',
                'label' => 'Ends at'
            ],
            [

                'name' => 'images', // db column name
                'label' => 'Images', // field caption
                'type' => 'dropzone', // voodoo magic
                'prefix' => '/', // upload folder (should match the driver specified in the upload handler defined below)
                'upload-url' => '/' . config('backpack.base.route_prefix') . '/media-dropzone', // POST route to handle the individual file uploads
            ],
            [    // Select2Multiple = n-n relationship (with pivot table)
                'label' => "Products",
                'type' => 'select2_multiple',
                'name' => 'products', // the method that defines the relationship in your Model
                'entity' => 'products', // the method that defines the relationship in your Model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'pivot' => true, // on create&update, do you need to add/delete pivot table entries?
                'options' => (function ($query) {
                    return $query->where('type','bundle')->orderBy('name', 'ASC')->get();
                }),
            ],
            [
                'label' => "User",
                'type' => 'select2',
                'name' => 'user_id', // the db column for the foreign key
                'entity' => 'user', // the method that defines the relationship in your Model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'model' => "App\User", // foreign key model
                'default' => ''
            ],
            [  // Select2
                'label' => "Video",
                'type' => 'select2',
                'name' => 'video_id', // the db column for the foreign key
                'entity' => 'video', // the method that defines the relationship in your Model
                'attribute' => 'name', // foreign key attribute that is shown to user

                // optional
                'model' => "App\Models\Video", // foreign key model

            ],[   // repeatable
            'name'  => 'boxes',
            'label' => 'Boxes',
            'type'  => 'repeatable',
            'fields' => [
                [
                    'name'    => 'image',
                    'type'    => 'browse',
                    'label'   => 'Image',

                ],
                [
                    'name'    => 'title',
                    'type'    => 'text',
                    'label'   => 'Title',

                ],
                [
                    'name'  => 'content',
                    'type'  => 'ckeditor',
                    'label' => 'Content',
                ],
            ],
            #'tab' => 'info',

            // optional
            'new_item_label'  => 'Add icon', // customize the text of the button
        ],
        ]);

        $this->crud->addField([
            'name' => 'url',
            'label' => "Url",
            'type' => 'text',
        ]);

        $this->crud->addField([   // repeatable
            'name'  => 'custom_icons',
            'label' => 'Custom icons',
            'type'  => 'repeatable',
            'fields' => [
                [
                    'name'    => 'image',
                    'type'    => 'browse',
                    'label'   => 'Image',

                ],
                [
                    'name'    => 'title',
                    'type'    => 'text',
                    'label'   => 'Title',

                ],
                [
                    'name'  => 'content',
                    'type'  => 'ckeditor',
                    'label' => 'Content',
                ],
            ],
            #'tab' => 'info',

            // optional
            'new_item_label'  => 'Add icon', // customize the text of the button
        ],);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
