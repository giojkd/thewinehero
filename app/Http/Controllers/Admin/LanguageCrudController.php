<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\LanguageRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class LanguageCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class LanguageCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Language');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/language');
        $this->crud->setEntityNameStrings('language', 'languages');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();
        $this->crud->addColumn(
            [
                'name' => 'id',
                'type' => 'text',
                'label' => 'ID'
            ]
        );
        $this->crud->addColumn([
            'label' => 'Name',
            'name' => 'name',
            'type' => 'text'
        ]);

        $this->crud->addColumn([
            'label' => 'Iso',
            'name' => 'iso',
            'type' => 'text'
        ]);

    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(LanguageRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();


        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();

        $this->crud->addField([
            'label' => 'Name',
            'name' => 'name',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'label' => 'Iso',
            'name' => 'iso',
            'type' => 'text'
        ]);


        $this->crud->addField([
            'label' => 'As language for wesite',
            'name' => 'is_website_language',
            'type' => 'checkbox'
        ]);

    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
