<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PurchaseRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\Models\Event;
use App\Models\Purchase;
use Stripe;
/**
 * Class PurchaseCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PurchaseCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    #use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Purchase');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/purchase');
        $this->crud->setEntityNameStrings('purchase', 'purchases');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();
        $this->crud->addColumns([
            [
                'name' => 'id',
                'type' => 'text',
                'label' => 'ID'
            ],
            [
                'name' => 'purchaseable_type',
                'type' => 'model_function',
                'label' => 'Model',
                'function_name' => 'getPurchaseType'
            ],
            [
                'name' => 'model_name',
                'type' => 'model_function_attribute',
                'label' => 'Name',
                'function_name' => 'getPurchaseable',
                'attribute' => 'name_with_datetime',
                'limit' => '150',
            ],
            [
                'name' => 'user',
                'type' => 'relationship',
                'label' => 'User',
                'entity' => 'user',
                'attribute' => 'full_name_with_contacts',
                #'limit' => '150',
            ],

        ]);

        $this->crud->addButtonFromView('line', 'chargePurchase', 'chargePurchase', 'end');
        $this->crud->addButtonFromView('line', 'sendPurchaseFinalConfirmation', 'sendPurchaseFinalConfirmation', 'end');
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(PurchaseRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();
        $this->crud->addFields([
            [
                'name' => 'purchaseable_type',
                'type' => 'select_from_array',
                'label' => 'Model',
                'options' => [
                    'App\Models\Event' => 'Event'
                ],
            ],
            [
                'name' => 'purchaseable_id',
                'type' => 'select_from_array',
                'label' => 'Item',
                'options' => Event::where('starts_at_day', '>=', date('Y-m-d'))->orderBy('starts_at_day', 'ASC')->get()->pluck('name_with_datetime', 'id')

            ]
        ]);

    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function chargePurchase(PurchaseRequest $request){

        $purchase = Purchase::where('id', $request->id)->whereNull('paid_at')->firstOrFail();
        #return $purchase;
        $intent  = $purchase->payment_intent;
        $stripe = Stripe::make(config('services.stripe')['secret']);
        $pi = $stripe->paymentIntents()->capture($intent->id);

        if($pi['status'] = "succeeded"){
            $purchase->paid_at = date('Y-m-d H:i:s');
            $purchase->save();
        }else{
            abort(403);
        }

    }

    public function sendFinalConfirmation(PurchaseRequest $request){

        $purchase = Purchase::where('id', $request->id)->whereNull('final_confirmation_sent_at')->firstOrFail();

        $item = $purchase->purchaseable;
        $item->sendContentPurchaseFinalNotification($purchase->user);

        Purchase::where('id', $request->id)->update(['final_confirmation_sent_at' => date('Y-m-d H:i:s')]);

    }

}
