<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\TrainingblockRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\Http\Requests\DropzoneRequest;

/**
 * Class TrainingblockCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class TrainingblockCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Trainingblock');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/trainingblock');
        $this->crud->setEntityNameStrings('trainingblock', 'trainingblocks');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();
        $this->crud->addColumn(
            [
                'name' => 'id',
                'type' => 'text',
                'label' => 'ID'
            ]
        );
        $this->crud->addColumn([  // Select2
            'label' => "Level",
            'type' => 'select',
            'name' => 'traininglevel_id', // the db column for the foreign key
            'entity' => 'traininglevel', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            // optional
            'model' => "App\Models\Training", // foreign key model

        ]);

        $this->crud->addColumn([
            'name' => 'name',
            'label' => "Name",
            'type' => 'text',

        ]);

        $this->crud->addColumn([
            'name' => 'subtitle',
            'label' => "Subtitle",
            'type' => 'text',
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(TrainingblockRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();


        $this->crud->addField([  // Select2
            'label' => "Level",
            'type' => 'select2',
            'name' => 'traininglevel_id', // the db column for the foreign key
            'entity' => 'traininglevel', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            // optional
            'model' => "App\Models\Traininglevel", // foreign key model
            'default' => 2, // set the default value of the select2
            'options' => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select
            'tab' => 'info'
        ]);

        $this->crud->addField([
            'name' => 'name',
            'label' => "Name",
            'type' => 'text',
            'tab' => 'info'
        ]);

        $this->crud->addField([
            'name' => 'subtitle',
            'label' => "Subtitle",
            'type' => 'text',
            'tab' => 'info'
        ]);
        $this->crud->addField([
            'name' => 'description',
            'label' => "Description",
            'type' => 'wysiwyg',
            'tab' => 'info'
        ]);

        $this->crud->addField([
            'tab' => 'Images',
            'name' => 'photos', // db column name
            'label' => 'Photos', // field caption
            'type' => 'dropzone', // voodoo magic
            'prefix' => '/', // upload folder (should match the driver specified in the upload handler defined below)
            'upload-url' => '/' . config('backpack.base.route_prefix') . '/media-dropzone', // POST route to handle the individual file uploads
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
