<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\EnrollmentoptionRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class EnrollmentoptionCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class EnrollmentoptionCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CloneOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Enrollmentoption');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/enrollmentoption');
        $this->crud->setEntityNameStrings('enrollmentoption', 'enrollmentoptions');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();
        $this->crud->addColumn(
            [
                'name' => 'id',
                'type' => 'text',
                'label' => 'ID'
            ]
        );
        $this->crud->addColumn([
            'name' => 'name',
            'label' => "Name",
            'type' => 'text',
        ]);


        $this->crud->addColumn([
            'name' => 'price',
            'label' => "Price",
            'type' => 'text',
        ]);

        $this->crud->addColumn([   // Checkbox
            'name' => 'is_subscription',
            'label' => 'Is subscription',
            'type' => 'boolean',
        ]);

        $this->crud->addColumn([
            'name' => 'frequency',
            'label' => "Frequency",
            'type' => 'number',
        ]);


        $this->crud->addColumn([
            'name' => 'events',
            'label' => "Events",
            'type' => 'number',
        ]);

        $this->crud->addColumn([
            'name' => 'videos',
            'label' => "Videos",
            'type' => 'number',
        ]);

        $this->crud->addColumn([
            'name' => 'trainings',
            'label' => "Trainings",
            'type' => 'number',
        ]);

        $this->crud->addColumn([
            'name' => 'articles',
            'label' => "Articles",
            'type' => 'number',
        ]);

        $this->crud->addColumn([
            'name' => 'recipes',
            'label' => "Recipes",
            'type' => 'number',
        ]);

        $this->crud->addColumn([
            'name' => 'events_discount',
            'label' => "Events Discount",
            'type' => 'number',
        ]);

        $this->crud->addColumn([
            'name' => 'videos_discount',
            'label' => "Videos Discount",
            'type' => 'number',
        ]);

        $this->crud->addColumn([
            'name' => 'trainings_discount',
            'label' => "Trainings Discount",
            'type' => 'number',
        ]);

        $this->crud->addColumn([
            'name' => 'articles_discount',
            'label' => "Articles Discount",
            'type' => 'number',
        ]);

        $this->crud->addColumn([
            'name' => 'recipes_discount',
            'label' => "Recipes Discount",
            'type' => 'number',
        ]);

        $this->crud->addColumn([    // Select2Multiple = n-n relationship (with pivot table)
            'label' => "Option Group",
            'type' => 'select',
            'name' => 'enrollmentoptiongroup_id', // the method that defines the relationship in your Model
            'entity' => 'enrollmentoptiongroup', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            // optional
            'model' => "App\Models\Enrollmentoptiongroup", // foreign key model
            /*'options' => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }),*/
            #'tab' => 'Info',
        ]);

    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(EnrollmentoptionRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();

        $this->crud->addField([
            'name' => 'name',
            'label' => "Name",
            'type' => 'text',
        ]);

        $this->crud->addField([
            'name' => 'price',
            'label' => "Price",
            'type' => 'text',
        ]);

        $this->crud->addField([   // Checkbox
            'name' => 'is_subscription',
            'label' => 'Is subscription',
            'type' => 'checkbox',
        ]);

        $this->crud->addField([
            'name' => 'frequency',
            'label' => "Frequency",
            'type' => 'number',
        ]);

        $this->crud->addField([
            'name' => 'what_you_get',
            'label' => "What you get",
            'type' => 'wysiwyg',
        ]);

        $this->crud->addField([
            'name' => 'events',
            'label' => "Events",
            'type' => 'number',
        ]);

        $this->crud->addField([
            'name' => 'videos',
            'label' => "Videos",
            'type' => 'number',
        ]);

        $this->crud->addField([
            'name' => 'trainings',
            'label' => "Trainings",
            'type' => 'number',
        ]);

        $this->crud->addField([
            'name' => 'articles',
            'label' => "Articles",
            'type' => 'number',
        ]);

        $this->crud->addField([
            'name' => 'recipes',
            'label' => "Recipes",
            'type' => 'number',
        ]);

        $this->crud->addField([
            'name' => 'events_discount',
            'label' => "Events Discount",
            'type' => 'number',
        ]);

        $this->crud->addField([
            'name' => 'videos_discount',
            'label' => "Videos Discount",
            'type' => 'number',
        ]);

        $this->crud->addField([
            'name' => 'trainings_discount',
            'label' => "Trainings Discount",
            'type' => 'number',
        ]);

        $this->crud->addField([
            'name' => 'articles_discount',
            'label' => "Articles Discount",
            'type' => 'number',
        ]);

        $this->crud->addField([
            'name' => 'recipes_discount',
            'label' => "Recipes Discount",
            'type' => 'number',
        ]);

        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
            'label' => "Option Group",
            'type' => 'select2',
            'name' => 'enrollmentoptiongroup_id', // the method that defines the relationship in your Model
            'entity' => 'enrollmentoptiongroup', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            // optional
            'model' => "App\Models\Enrollmentoptiongroup", // foreign key model
            /*'options' => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }),*/
            #'tab' => 'Info',
        ]);


    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
