<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PlanRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class PlanCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PlanCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Plan');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/plan');
        $this->crud->setEntityNameStrings('plan', 'plans');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();

        $this->crud->addColumn(
            [
                'name' => 'id',
                'type' => 'text',
                'label' => 'ID'
            ]
        );
        $this->crud->addColumn([  // Select2
            'label' => "User",
            'type' => 'select',
            'name' => 'user_id', // the db column for the foreign key
            'entity' => 'user', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            // optional
            'model' => "App\User", // foreign key model
            #'default' => 2, // set the default value of the select2
            'tab' => 'info'
        ]);

        $this->crud->addColumn([  // Select2
            'label' => "Enrollment option",
            'type' => 'select',
            'name' => 'enrollmentoption_id', // the db column for the foreign key
            'entity' => 'enrollmentoption', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            // optional
            'model' => "App\Models\Enrollmentoption", // foreign key model
            #'default' => 2, // set the default value of the select2
            'tab' => 'info'
        ]);

        $this->crud->addColumn(['label' => 'Expires at','type'=> 'date','name' => 'expires_at']);
        $this->crud->addColumn(['label' => 'Videos','type'=> 'text','name' => 'videos']);
        $this->crud->addColumn(['label' => 'Events','type'=> 'text','name' => 'events']);
        $this->crud->addColumn(['label' => 'Trainigs','type'=> 'text','name' => 'trainings']);
        $this->crud->addColumn(['label' => 'Articles','type'=> 'text','name' => 'articles']);
        $this->crud->addColumn(['label' => 'Recipes','type'=> 'text','name' => 'recipes']);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(PlanRequest::class);

        $this->crud->addField([  // Select2
            'label' => "User",
            'type' => 'select2',
            'name' => 'user_id', // the db column for the foreign key
            'entity' => 'user', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            // optional
            'model' => "App\User", // foreign key model
            #'default' => 2, // set the default value of the select2
            'tab' => 'info'
        ]);

        $this->crud->addField([  // Select2
            'label' => "Enrollment option",
            'type' => 'select2',
            'name' => 'enrollmentoption_id', // the db column for the foreign key
            'entity' => 'enrollmentoption', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            // optional
            'model' => "App\Models\Enrollmentoption", // foreign key model
            #'default' => 2, // set the default value of the select2
            'tab' => 'info'
        ]);

        // TODO: remove setFromDb() and manually define Fields
     #   $this->crud->setFromDb();

    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }


}
