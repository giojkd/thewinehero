<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\LandingpageRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class LandingpageCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class LandingpageCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Landingpage::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/landingpage');
        CRUD::setEntityNameStrings('landing page', 'landing pages');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        #CRUD::setFromDb(); // columns

        $this->crud->addColumns([
            [
                'name' => 'name',
                'label' => "Name",
                'type' => 'text',
            ],
            [
                'name' => 'url',
                'type' => 'model_function',
                'function_name' => 'makeUrl',
                'limit' => 300
            ]
        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(LandingpageRequest::class);

#        CRUD::setFromDb(); // fields

        $this->crud->addFields([
            [
                'name' => 'name',
                'label' => "Name",
                'type' => 'text',
            ],
            [
                'name' => 'short_description',
                'label' => "Short descripton",
                'type' => 'textarea',
            ],
            [
                'name' => 'description',
                'label' => "Descrition",
                'type' => 'wysiwyg',
            ],
            [
               'name' => 'images', // db column name
                'label' => 'Images', // field caption
                'type' => 'dropzone', // voodoo magic
                'prefix' => '/', // upload folder (should match the driver specified in the upload handler defined below)
                'upload-url' => '/' . config('backpack.base.route_prefix') . '/media-dropzone', // POST route to handle the individual file uploads
            ],
            [
                'name' => 'view',
                'label' => "View",
                'type' => 'text',
            ],
            [
                'name' => 'embeddable_code',
                'label' => 'Embeddable code',
                'type' => 'textarea'
            ]
        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
