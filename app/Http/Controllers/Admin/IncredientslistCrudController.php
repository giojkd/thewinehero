<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\IncredientslistRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class IncredientslistCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class IncredientslistCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Incredientslist');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/incredientslist');
        $this->crud->setEntityNameStrings('incredientslist', 'incredientslists');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();
        $this->crud->addColumn(
            [
                'name' => 'id',
                'type' => 'text',
                'label' => 'ID'
            ]
        );

        $this->crud->addColumn([
            'name' => 'name',
            'label' => "Name",
            'type' => 'text'
        ]);

        /*

        $this->crud->addColumn([  // Select2
            'label' => "Recipe",
            'type' => 'select',
            'name' => 'recipe_id', // the db column for the foreign key
            'entity' => 'recipe', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            // optional
            'model' => "App\Models\Recipe", // foreign key model

        ]);

        */

    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(IncredientslistRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();

        $this->crud->addField([
            'name' => 'name',
            'label' => "Name",
            'type' => 'text'
        ]);


        /*
        $this->crud->addField([  // Select2
            'label' => "Recipe",
            'type' => 'select2',
            'name' => 'recipe_id', // the db column for the foreign key
            'entity' => 'recipe', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            // optional
            'model' => "App\Models\Recipe", // foreign key model
        ]);
        */

    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
