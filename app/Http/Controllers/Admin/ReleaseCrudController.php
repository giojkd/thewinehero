<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ReleaseRequest;
use App\Models\Article;
use App\Models\Event;
use App\Models\Product;
use App\Models\Recipe;
use App\Models\Video;


use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ReleaseCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ReleaseCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Release::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/release');
        CRUD::setEntityNameStrings('release', 'releases');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */

    public function getReleaseOptions(){

        $options = collect([]);
        Article::get()->sortBy('name')-> each(function($item,$key) use(&$options){ $options[$item->id.'_article'] =  $item->name.' ('.$item->id.' Article)'; });
        #Event::get()->sortBy('name')-> each(function($item,$key) use(&$options){ $options[$item->id.'_event'] =  $item->name. ' (' . $item->id . 'Event)'; });
        Product::get()->sortBy('name')-> each(function($item,$key) use(&$options){ $options[$item->id.'_product'] =  $item->name. ' (' . $item->id . 'Product)'; });
        Recipe::get()->sortBy('name')-> each(function($item,$key) use(&$options){ $options[$item->id.'_recipe'] =  $item->name. ' (' . $item->id . 'Recipe)'; });
        Video::get()->sortBy('name')-> each(function($item,$key) use(&$options){ $options[$item->id.'_video'] =  $item->name. ' (' . $item->id . 'Video)'; });

        return $options;

    }

    protected function setupListOperation()
    {
        #CRUD::setFromDb(); // columns

        $this->crud->addColumns([
            [
                'name' => 'release_at',
                'type' => 'datetime',
                'label' => 'Release at'
            ],
            [
                'name' => 'object',
                'type' => 'select_from_array',
                'options' => $this->getReleaseOptions(),
                'label' => 'Object of release'
            ],
            [
                'name' => 'processed_at',
                'type' => 'datetime',
                'label' => 'Processed at'
            ]
        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(ReleaseRequest::class);

#        CRUD::setFromDb(); // fields

        $this->crud->addFields([
            [
                'name' => 'release_at',
                'type' => 'datetime',
                'label' => 'Release at'
            ],
            [
                'name' => 'object',
                'type' => 'select2_from_array',
                'options' => $this->getReleaseOptions(),
                'label' => 'Object of release'
            ]
        ]);



        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
