<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\NewslettersubscriptionRequest;
use App\Models\Newslettersubscription;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Exception;

/**
 * Class NewslettersubscriptionCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class NewslettersubscriptionCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Newslettersubscription::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/newslettersubscription');
        CRUD::setEntityNameStrings('newslettersubscription', 'newslettersubscriptions');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::setFromDb(); // columns

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(NewslettersubscriptionRequest::class);

        CRUD::setFromDb(); // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function subscribe(NewslettersubscriptionRequest $request){


        $request->validate([
            'email' => 'required|email'
        ]);

        try{

            #$newsletterSubscription = Newslettersubscription::create(['email' => $email, 'full_name' => $full_name]);

            $config = \SendinBlue\Client\Configuration::getDefaultConfiguration()->setApiKey('api-key', env('SENDINBLUE_APIKEY'));

            $apiInstance = new \SendinBlue\Client\Api\ContactsApi(
                new \GuzzleHttp\Client(),
                $config
            );

            $email = $request->email;
            $name = $request->name;
            $surname = $request->surname;

            /*
            $createDoiContact = new \SendinBlue\Client\Model\CreateDoiContact([
                'templateId' => env('SENDINBLUE_DOI_TEMPLATE_ID'),
                'redirectionUrl' => env('SENDINBLUE_REDIRECTURL'),
                'email' => $email,
                'FIRSTNAME' => $name,
                'LASTNAME'=>$surname,
                'includeListIds' => explode(',',env('SENDINBLUE_INCLUDED_LISTS_IDS'))
            ]);
            */




            $createDoiContact = new \SendinBlue\Client\Model\CreateDoiContact([
                    'templateId' => 8,
                    'redirectionUrl' => 'https://www.mamablip.com',
                    'email' => $email,
                    'attributes' => [
                        'FIRSTNAME' => $name,
                        'LASTNAME' => $surname,
                    ],
                    'includeListIds' => [26],
                ]);

            #$result = $apiInstance->getAccount();
            $result = $apiInstance->createDoiContact($createDoiContact);

            return ['status' => 1];

        }catch(Exception $e){
            return ['status' => 0,'message'=> $e->getMessage()];
        }


    }
}
