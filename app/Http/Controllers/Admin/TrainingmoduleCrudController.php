<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\TrainingmoduleRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class TrainingmoduleCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class TrainingmoduleCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Trainingmodule');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/trainingmodule');
        $this->crud->setEntityNameStrings('trainingmodule', 'trainingmodules');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();
        $this->crud->addColumn(
            [
                'name' => 'id',
                'type' => 'text',
                'label' => 'ID'
            ]
        );
        $this->crud->addColumn([  // Select2
            'label' => "Training block",
            'type' => 'select',
            'name' => 'trainingblock_id', // the db column for the foreign key
            'entity' => 'trainingblock', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            // optional
            'model' => "App\Models\Trainingblock", // foreign key model
        ]);

        $this->crud->addColumn([
            'name' => 'name',
            'label' => "Name",
            'type' => 'text',
        ]);

        $this->crud->addColumn([
            'name' => 'subtitle',
            'label' => "Subtitle",
            'type' => 'text',
        ]);
    }


    protected function setupCreateOperation()
    {
        $this->crud->setValidation(TrainingmoduleRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();

        $this->crud->addField([  // Select2
            'label' => "Training block",
            'type' => 'select2',
            'name' => 'trainingblock_id', // the db column for the foreign key
            'entity' => 'trainingblock', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            // optional
            'model' => "App\Models\Trainingblock", // foreign key model
            'default' => 2, // set the default value of the select2
            'options' => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select
            'tab' => 'info'
        ]);

        $this->crud->addField([
            'name' => 'name',
            'label' => "Name",
            'type' => 'text',
            'tab' => 'info'
        ]);

        $this->crud->addField([
            'name' => 'subtitle',
            'label' => "Subtitle",
            'type' => 'text',
            'tab' => 'info'
        ]);

        $this->crud->addField([
            'name' => 'description',
            'label' => "Description",
            'type' => 'wysiwyg',
            'tab' => 'info'
        ]);

        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
            'label'     => "Files",
            'type'      => 'select2_multiple',
            'name'      => 'files', // the method that defines the relationship in your Model
            'entity'    => 'files', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'pivot'     => true,
            'model'     => "App\Models\File", // foreign key model
            'options'   => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }),
            'tab' => 'contents'
        ]);

        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
            'label'     => "Videos",
            'type'      => 'select2_multiple',
            'name'      => 'videos', // the method that defines the relationship in your Model
            'entity'    => 'videos', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'pivot'     => true,
            'model'     => "App\Models\Video", // foreign key model
            'options'   => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }),
            'tab' => 'contents'
        ]);

        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
            'label'     => "Quizzes",
            'type'      => 'select2_multiple',
            'name'      => 'quizzs', // the method that defines the relationship in your Model
            'entity'    => 'quizzs', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'pivot'     => true,
            'model'     => "App\Models\Quizz", // foreign key model
            'options'   => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }),
            'tab' => 'contents'
        ]);


    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
