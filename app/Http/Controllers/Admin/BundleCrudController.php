<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\BundleRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class BundleCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class BundleCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Bundle::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/bundle');
        CRUD::setEntityNameStrings('bundle', 'bundles');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        #CRUD::setFromDb(); // columns

        $this->crud->addColumns([[
                'label' => "Product container",
                'type' => 'select',
                'name' => 'product_id', // the method that defines the relationship in your Model
                'entity' => 'productOwner', // the method that defines the relationship in your Model
                'attribute' => 'name', // foreign key attribute that is shown to user

            ],
            [
                'label' => "Product included",
                'type' => 'select',
                'name' => 'bundled_product_id', // the method that defines the relationship in your Model
                'entity' => 'productRelated', // the method that defines the relationship in your Model
                'attribute' => 'name', // foreign key attribute that is shown to user

            ],
            [
                'name' => 'description',
                'label' => 'Description'
            ],
            [
                'name'        => 'quantity',
                'label'       => "Quantity",
            ]
        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(BundleRequest::class);

        #CRUD::setFromDb(); // fields

        $options = [];
        for ($i = 0; $i <= 100; $i++) {
            $options[$i] = $i;
        };


        $this->crud->addFields([
            [
                'label' => "Product container",
                'type' => 'select2',
                'name' => 'product_id', // the method that defines the relationship in your Model
                'entity' => 'productOwner', // the method that defines the relationship in your Model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'options'   => (function ($query) {
                    return $query->orderBy('name', 'ASC')->where('type', 'bundle')->get();
                }),
            ],
            [
                'label' => "Product included",
                'type' => 'select2',
                'name' => 'bundled_product_id', // the method that defines the relationship in your Model
                'entity' => 'productRelated', // the method that defines the relationship in your Model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'options'   => (function ($query) {
                    return $query->orderBy('name', 'ASC')->where('type', '!=', 'bundle')->get();
                }),
            ],
            [
                'name' => 'description',
                'label' => 'Description',
                'type' => 'text',
            ],
            [
                'name'        => 'quantity',
                'label'       => "Quantity",
                'type'        => 'select_from_array',
                'options'     => $options,

            ],

        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
