<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\RecipestepRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Http\Requests\DropzoneRequest;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class RecipestepCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class RecipestepCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CloneOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Recipestep');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/recipestep');
        $this->crud->setEntityNameStrings('recipestep', 'recipesteps');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();

        $this->crud->addColumn(
            [
                'name' => 'id',
                'type' => 'text',
                'label' => 'ID'
            ]
        );

        $this->crud->addColumn([  // Select2
            'label' => "Recipe",
            'type' => 'select',
            'name' => 'recipe_id', // the db column for the foreign key
            'entity' => 'recipe', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            // optional
            'model' => "App\Models\Recipe", // foreign key model
            'tab' => 'Info',
        ]);

        $this->crud->addColumn([
            'name' => 'name',
            'label' => "Name",
            'type' => 'text',
            'tab' => 'Info',
        ]);

        $this->crud->addColumn([
            'name' => 'sort_order',
            'label' => "Sort order",
            'type' => 'number',
            'tab' => 'Info',
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(RecipestepRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();
        $this->crud->addField([
            'name' => 'name',
            'label' => "Name",
            'type' => 'text',
            'tab' => 'Info',
        ]);
        $this->crud->addField([
            'name' => 'description',
            'label' => "Description",
            'type' => 'wysiwyg',
            'tab' => 'Info',
        ]);

        $this->crud->addField([  // Select2
            'label' => "Recipe",
            'type' => 'select2',
            'name' => 'recipe_id', // the db column for the foreign key
            'entity' => 'recipe', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            // optional
            'model' => "App\Models\Recipe", // foreign key model
            'tab' => 'Info',
        ]);

        $this->crud->addField([
            'tab' => 'Images',
            'name' => 'photos', // db column name
            'label' => 'Photos', // field caption
            'type' => 'dropzone', // voodoo magic
            'prefix' => '/', // upload folder (should match the driver specified in the upload handler defined below)
            'upload-url' => '/' . config('backpack.base.route_prefix') . '/media-dropzone', // POST route to handle the individual file uploads
        ]);

        $this->crud->addField([
            'name' => 'sort_order',
            'label' => "Sort order",
            'type' => 'number',
            'tab' => 'Info',
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
