<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ReciperowRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ReciperowCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ReciperowCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Reciperow');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/reciperow');
        $this->crud->setEntityNameStrings('reciperow', 'reciperows');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();
        $this->crud->addColumn(
            [
                'name' => 'id',
                'type' => 'text',
                'label' => 'ID'
            ]
        );
        $this->crud->addColumn([  // Select2
            'label' => "Recipe",
            'type' => 'select',
            'name' => 'recipe_id', // the db column for the foreign key
            'entity' => 'recipe', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            // optional

        ]);

        $this->crud->addColumn([  // Select2
            'label' => "Ingredient",
            'type' => 'select',
            'name' => 'ingredient_id', // the db column for the foreign key
            'entity' => 'ingredient', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            // optional
        ]);
        $this->crud->addColumn([
            'name' => 'quantity',
            'label' => "Quantity",
            'type' => 'number',

        ]);

        $this->crud->addColumn([
            'name' => 'measurement_unit',
            'label' => "Unit",
            'type' => 'text',

        ]);

        $this->crud->addColumn([
            'name' => 'notes',
            'label' => "Notes",
            'type' => 'text',
        ]);

        $this->crud->addColumn([  // Select2
            'label' => "Ingredients list",
            'type' => 'select',
            'name' => 'ingredientslist_id', // the db column for the foreign key
            'entity' => 'ingredientslist', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            // optional
            'model' => "App\Models\Incredientslist", // foreign key model
            'tab' => 'info'
        ]);

    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(ReciperowRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();
        $this->crud->addField([  // Select2
            'label' => "Recipe",
            'type' => 'select2',
            'name' => 'recipe_id', // the db column for the foreign key
            'entity' => 'recipe', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            // optional
            'model' => "App\Models\Recipe", // foreign key model
            'default' => 2, // set the default value of the select2
            'options' => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select
            'tab' => 'info'
        ]);

        $this->crud->addField([  // Select2
            'label' => "Ingredient",
            'type' => 'select2',
            'name' => 'ingredient_id', // the db column for the foreign key
            'entity' => 'ingredient', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            // optional
            'model' => "App\Models\Ingredient", // foreign key model
            'default' => 2, // set the default value of the select2
            'options' => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select
            'tab' => 'info'
        ]);

        $this->crud->addField([
            'name' => 'quantity',
            'label' => "Quantity",

            'tab' => 'info'
        ]);

        $this->crud->addField([   // select2_from_array
            'name' => 'measurement_unit',
            'label' => "Unit",
            'type' => 'select2_from_array',
            'options' => [
              'cup' => 'Cup',
              'gram' => 'Gram',
              'tea spoon' => 'Teaspoon',
              'table_spoon' => 'Tablespoon',
              '' => 'None',
              'kilo' => 'Kilo',
              'liter'=> 'Liter',
              'milliliter' => 'Milliliter',
              'pinch' => 'Pinch',
              'as needed' => 'As needed'
            ],
            'allows_null' => false,
            'default' => 'grams',
            'tab' => 'info'
            // 'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;
        ],);

        $this->crud->addField([
            'name' => 'notes',
            'label' => "Notes",
            'type' => 'text',
            'tab' => 'info'
        ]);

        $this->crud->addField([  // Select2
            'label' => "Ingredients list",
            'type' => 'select2',
            'name' => 'ingredientslist_id', // the db column for the foreign key
            'entity' => 'ingredientslist', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            // optional
            'model' => "App\Models\Incredientslist", // foreign key model
            'tab' => 'info'
        ]);

    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
