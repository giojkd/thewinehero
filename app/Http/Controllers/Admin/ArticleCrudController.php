<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ArticleRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\Http\Requests\DropzoneRequest;
use Request;

/**
 * Class ArticleCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ArticleCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CloneOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ReorderOperation;
    use \Backpack\ReviseOperation\ReviseOperation;


    public function setup()
    {
        $this->crud->setModel('App\Models\Article');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/article');
        $this->crud->setEntityNameStrings('article', 'articles');

        $this->crud->addButtonFromModelFunction('line', 'make_url_tag', 'makeUrlTag', 'beginning');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();



        $this->crud->addColumn(
            [
                'name' => 'id',
                'type' => 'text',
                'label' => 'ID'
            ]
        );


        $this->crud->addColumn([
            'name' => 'enabled',
            'label' => "Pusblished",
            'type' => 'boolean',

        ]);

        $this->crud->addColumn([
            'name' => 'is_ntts',
            'label' => "Is Ntts",
            'type' => 'boolean',

        ]);

        $this->crud->addColumn([
            // run a function on the CRUD model and show its return value
            'name' => "photos",
            'label' => "Cover", // Table column heading
            'type' => "model_function",
            'function_name' => 'cover', // the method in your Model
            // 'function_parameters' => [$one, $two], // pass one/more parameters to that method
            'limit' => 10000, // Limit the number of characters shown
        ]);
/*
        $this->crud->addColumn([
            'name' => 'status',
            'label' => "Published",
            'type' => 'boolean',
        ]);
*/
        $this->crud->addColumn([
            'name' => 'name',
            'label' => "Name",
            'type' => 'text',
        ]);

        $this->crud->addColumn([    // Select2Multiple = n-n relationship (with pivot table)
            'label' => "Manufacturers",
            'name' => 'manufacturers',
            'type' => 'select_multiple',
            'entity' => 'manufacturers', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            // optional
            'model' => "App\Models\Manufacturer", // foreign key model

            'limit' => 400
        ]);

        $this->crud->addColumn([    // Select2Multiple = n-n relationship (with pivot table)
            'label' => "Consortium",
            'name' => 'consortiums', // the method that defines the relationship in your Model
            'type' => 'select_multiple',
            'entity' => 'consortiums', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            // optional
            'model' => "App\Models\Consortium", // foreign key model

            'limit' => 400
        ]);

        $this->crud->addColumn([
            'name' => 'published_at',
            'label' => 'Published at',
            'type' => 'text'
        ]);

        $this->crud->addColumn([    // Select2Multiple = n-n relationship (with pivot table)
            'label'     => "Categories",
            'type'      => 'select_multiple',
            'name'      => 'categories', // the method that defines the relationship in your Model
            'entity'    => 'categories', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
            // 'select_all' => true, // show Select All and Clear buttons?

            // optional
            'model'     => "App\Models\Category", // foreign key model
            'options'   => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            })
        ]);

        $this->crud->addColumn([    // Select2Multiple = n-n relationship (with pivot table)
            'label'     => "Article Category",
            'type'      => 'select',
            'name'      => 'articlecategory', // the method that defines the relationship in your Model
            'entity'    => 'articlecategory', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
            // 'select_all' => true, // show Select All and Clear buttons?

            // optional
            'model'     => "App\Models\Articlecategory", // foreign key model

        ]);

        $this->crud->addColumn([    // Select2Multiple = n-n relationship (with pivot table)
            'label'     => "Categories",
            'type'      => 'select_multiple',
            'name'      => 'categories', // the method that defines the relationship in your Model
            'entity'    => 'categories', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
            // 'select_all' => true, // show Select All and Clear buttons?

            // optional
            'model'     => "App\Models\Category", // foreign key model
            'options'   => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            })
        ]);

        $this->crud->addColumn([
            'name' => 'reviews_count',
            'label' => "Reviews",
            'type' => 'text',

        ]);

        $this->crud->addColumn([
            'name' => 'reviews_avg',
            'label' => "Score",
            'type' => 'text',

        ]);




        $this->crud->addColumn([
            'name' => 'is_premium',
            'label' => 'Is Premium',
            'type' => 'boolean',

        ]);

        $this->crud->addColumn([
            'name' => 'url',
            'type'     => 'closure',
            'function' => function ($entry) {
                $langs = array_keys(Config('backpack.crud.locales'));
                $return = '<table>';
                foreach ($langs as $lang) {
                    $return .= '<tr><td>' . $lang . '</td><td>' . parse_url($entry->makeUrl($lang))['path'] . '</td></tr>';
                }
                $return .= '</table>';
                return $return;
            }
        ]);






        /*
  $this->crud->addColumn([
    'name' => 'url',
    'label' => "Link",
    'type' => 'model_function',
    'function_name' => 'makeUrl'
  ]);
*/
    }

    protected function setupCreateOperation()
    {

        $locale = (isset($this->crud->getRequest()->all()['locale'])) ? $this->crud->getRequest()->all()['locale'] : 'en' ;

        $labels = [
            'it' => [
                'is_legacy' => 'E\' un articolo di tipo vecchio ? ',
                'enabled' => 'Pubblicato',
                'name' => 'Titolo dell\'articolo',
                'subtitle' => 'Descrizione di tipo meta',
                'url' => 'Indirizzo da inserire nella barra di navigazione',
                'brief' => 'Versione breve dell\'articolo',
                'content' => 'Contenuto dell\'articolo',
                'published_at' => 'Pubblicato in data',
                'user_id' => 'Scrittore di questo articolo',
                'tags' => 'Etichette applicate a questo articolo',
                'categories' => 'Categorie di questo articolo',
                'articlecategory_id' => 'Categoria degli articolo a cui appartiene questo articolo',
                'photos' => 'Immagini',
                'products' => 'Prodotti raccomandati in questo articolo',
                'price' => 'Prezzo',
                'is_free' => 'La fruizione dell\'articolo è gratuita',
                'is_free_until' => 'E\' gratuita fino a quando?',
                'manufacturers' => 'Produttori raccomandati in questo articolo',
                'consortiums' => 'Consorzi raccomandati in questo articolo',
                'podcasts' => 'Podcastt raccomndati in questo articolo',
                'articlecategories' => 'Categoria degli articolo a cui appartiene questo articolo',
                'image' => 'Immagine',
                'image_alt' => 'Contenuto dell\'attributo alt dell\'immagine',
                'image_link' => 'Link per l\'immagine',
                'cta' => 'Chiamata all\'azione',
                'body' => 'Blocchi',
                'is_premium' => 'E\' un articolo di tipo premium?',
                'credits' => 'Crediti'
            ],
            'en' => [
                'is_legacy' => 'Is legacy',
                'enabled' => 'Published',
                'name' => 'Title',
                'subtitle' => 'Meta description',
                'url' => 'URL',
                'brief' => 'Brief',
                'content' => 'Content',
                'published_at' => 'Published at',
                'user_id' => 'Editor',
                'tags' => 'Tags',
                'categories' => 'Categories',
                'articlecategory_id' => 'Article category',
                'photos' => 'Images',
                'products' => 'Products',
                'price' => 'Price',
                'is_free' => 'Is free',
                'is_free_until' => 'Is free until',
                'manufacturers' => 'Manufacturers',
                'consortiums' => 'Consortiums',
                'podcasts' => 'Podcasts',
                'articlecategories' => 'Article categories',
                'image' => 'Images',
                'image_alt' => 'Alt tag',
                'image_link' => 'Image link',
                'cta' => 'Call to action',
                'body' => 'Body blocks',
                'is_premium' => 'Is premium',
                'credits' => 'Credits'
            ]
        ];

        $l = $labels[$locale];

        $this->crud->setValidation(ArticleRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();

        $this->crud->addField([
            'name' => 'is_legacy',
            'label' => $l['is_legacy'],
            'type' => 'boolean',

        ]);

        $this->crud->addFields([
            [
                'name' => 'is_ntts',
                'label' => "Is Ntts",
                'type' => 'boolean',
                'tab' => 'Ntts'
            ],
            [
                'name' => 'is_ntts_processed',
                'label' => "Is Ntts Processed",
                'type' => 'boolean',
                'tab' => 'Ntts'
            ],
            [
                'name' => 'ntts_content',
                'label' => 'Content for ntts',
                'type' => 'textarea',
                'tab' => 'Ntts'
            ]
        ]);

        $this->crud->addField([
            'name' => 'enabled',
            'label' => $l['enabled'],

            'type' => 'checkbox',
            'tab' => 'Old'
        ]);

/*
        $this->crud->addField([
            'name' => 'status',
            'label' => "Published",
            'type' => 'checkbox',
        ]);
*/
        $this->crud->addField([
            'name' => 'name',
            'label' => $l['name'],

            'type' => 'text',
            'tab' => 'Old',
        ]);
        $this->crud->addField([
            'name' => 'subtitle',
            'label' => $l['subtitle'],

            'type' => 'textarea',
            'tab' => 'Old'
        ]);
        $this->crud->addField([
            'name' => 'url',
            'label' => $l['url'],
            'type' => 'text',

            'tab' => 'Old'
        ]);
        $this->crud->addField([
            'name' => 'brief',
            'label' => $l['brief'],

            'type' => 'textarea',
            'tab' => 'Old'
        ]);
        $this->crud->addField([
            'name' => 'content',
            'label' => $l['content'],

            'type' => 'wysiwyg',
            'tab' => 'Old'
        ]);

        $this->crud->addField([
            'name' => 'published_at',
            'label' => $l['published_at'],

            'type' => 'datetime_picker',
            // optional:
            'datetime_picker_options' => [
                'format' => 'DD/MM/YYYY HH:mm',
                'language' => 'en'
            ],
            'allows_null' => true,
            'tab' => 'Old'
            // 'default' => '2017-05-12 11:59:59',
        ]);

        $this->crud->addField([

            'type' => 'select2',
            'name' => 'user_id', // the db column for the foreign key
            'label' => $l['user_id'], // the db column for the foreign key
            'entity' => 'user', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'tab' => 'Old',
            // optional
            'model' => "App\User", // foreign key model
            'default' => ''
            /* 'options'   => (function ($query) {
    return $query->orderBy('name', 'ASC')->where('depth', 1)->get();
  }) */
        ]);

        /*

$this->crud->addField([   // Upload
'name' => 'photos',
'label' => 'Photos',
'type' => 'upload_multiple',
'upload' => true,
//'disk' => 'public', // if you store files in the /public folder, please ommit this; if you store them in /storage or S3, please specify it;
// optional:
//'temporary' => 10 // if using a service, such as S3, that requires you to make temporary URL's this will make a URL that is valid for the number of minutes specified
]);

*/

        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)

            'type'      => 'select2_multiple',
            'name'      => 'tags', // the method that defines the relationship in your Model
            'label'      => $l['tags'], // the method that defines the relationship in your Model
            'entity'    => 'tags', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
            // 'select_all' => true, // show Select All and Clear buttons?

            // optional
            'model'     => "App\Models\Tag", // foreign key model
            'options'   => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }),
            'tab' => 'Old'
        ]);

        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)

            'type'      => 'select2_multiple',
            'name'      => 'categories', // the method that defines the relationship in your Model
            'label'      => $l['categories'], // the method that defines the relationship in your Model
            'entity'    => 'categories', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
            // 'select_all' => true, // show Select All and Clear buttons?

            // optional
            'model'     => "App\Models\Category", // foreign key model
            'options'   => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }),
            'tab' => 'Old'
        ]);


        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)

            'type'      => 'select2',
            'name'      => 'articlecategory_id', // the method that defines the relationship in your Model
            'label'      => $l['articlecategory_id'], // the method that defines the relationship in your Model
            'entity'    => 'articlecategory', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            'model'     => "App\Models\Articlecategory", // foreign key model
            'tab' => 'Old'
        ]);

        $this->crud->addField([

            'name' => 'photos', // db column name
            'label' => $l['photos'], // db column name

            'type' => 'dropzone', // voodoo magic
            'prefix' => '/', // upload folder (should match the driver specified in the upload handler defined below)
            'upload-url' => '/' . config('backpack.base.route_prefix') . '/media-dropzone', // POST route to handle the individual file uploads
            'tab' => 'Old'
        ]);

        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)

            'type'      => 'select2_multiple',
            'name'      => 'products', // the method that defines the relationship in your Model
            'label'      => $l['products'], // the method that defines the relationship in your Model
            'entity'    => 'products', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
            // 'select_all' => true, // show Select All and Clear buttons?

            // optional
            'model'     => "App\Models\Product", // foreign key model
            'options'   => (function ($query) {
                return $query->whereEnabled(1)->orderBy('name', 'ASC')->get();
            }),
            'tab' => 'Old'
        ]);

        $this->crud->addField([
            'name' => 'price',
            'label' => $l['price'],

            'type' => 'number',
            'tab' => 'Old'
        ]);

        $this->crud->addField([
            'name' => 'is_free',
            'label' => $l['is_free'],

            'type' => 'checkbox',
            'tab' => 'Old'
        ]);

        $this->crud->addField([
            'name' => 'is_free_until',
            'label' => $l['is_free_until'],

            'type' => 'date_picker',
            'tab' => 'Old'
        ]);

        $this->crud->addField([
            'name' => 'is_premium',
            'label' => $l['is_premium'],

            'type' => 'checkbox',
            'tab' => 'Old'
        ]);

        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)

            'name' => 'manufacturers',
            'label' => $l['manufacturers'],
            'type' => 'select2_multiple',
            'entity' => 'manufacturers', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'pivot'     => true,
            // optional
            'model' => "App\Models\Manufacturer", // foreign key model
            'options' => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }), 'tab' => 'Old'

        ]);

        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)

            'name' => 'consortiums', // the method that defines the relationship in your Model
            'label' => $l['consortiums'], // the method that defines the relationship in your Model
            'type' => 'select2_multiple',
            'entity' => 'consortiums', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'pivot'     => true,
            // optional
            'model' => "App\Models\Consortium", // foreign key model
            'options' => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }), 'tab' => 'Old'

        ]);

        $this->crud->addField([

            'type'      => 'select2_multiple',
            'label'      => $l['podcasts'], // the method that defines the relationship in your Model
            'name'      => 'podcasts', // the method that defines the relationship in your Model
            'entity'    => 'podcasts', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
            // 'select_all' => true, // show Select All and Clear buttons?

            // optional
            'model'     => "App\Models\Podcast", // foreign key model
            'tab' => 'Old'

        ]);

        $this->crud->addField([

            'type'      => 'select2_multiple',
            'name'      => 'articlecategories', // the method that defines the relationship in your Model
            'label'      => $l['articlecategories'], // the method that defines the relationship in your Model
            'entity'    => 'articlecategories', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
            // 'select_all' => true, // show Select All and Clear buttons?

            // optional
            'model'     => "App\Models\Articlecategory", // foreign key model
            'tab' => 'Old'

        ]);


        $this->crud->addField([
            'name' => 'last_updated_lang',
            'label' => 'last_updated_lang',

            'type' => 'hidden',
            'value' => (!is_null(request()->locale)) ? request()->locale : 'en',
        ]);

        $this->crud->addField([
            'name'  => 'body',
            'label'  => $l['body'],

            'type'  => 'repeatable',
            'fields' => [
                [
                    'name'    => 'content',
                    'label'    => $l['content'],
                    'type'    => 'wysiwyg',
                    'extra_plugins' => ['html5audio']

                ],
                [

                    'name' => "image",
                    'label' => $l["image"],
                    'type' => 'image',
                    'crop' => true,
                ],
                [
                    'label' => $l['image_alt'],
                    'name' => 'image_alt',

                    'type' => 'text'
                ],
                [
                    'name' => 'cta',
                    'label' => $l['cta'],

                    'type' => 'select_from_array',
                    'options' => ['newsletter_subscription' => 'Newsletter Subscription']
                ],
                [
                    'label' => $l['image_link'],
                    'name' => 'image_link',
                    'type' => 'text'
                ],
                [
                    'label' => $l['credits'],
                    'name' => 'credits',
                    'type' => 'wysiwyg'
                ]
            ],

            // optional
            'new_item_label'  => 'Add Block', // customize the text of the button
            'init_rows' => 0, // number of empty rows to be initialized, by default 1
            #'min_rows' => 2, // minimum rows allowed, when reached the "delete" buttons will be hidden
            #'max_rows' => 2, // max
            'tab' => 'New'
        ]);




    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }


    public function handleDropzoneUpload(DropzoneRequest $request)
    {
        $disk = "custom_public"; //
        $destination_path = "";
        $destination_path_filename = "storage";
        $file = $request->file('file');

        try {
            $image = \Image::make($file);
            $filename = md5($file->getClientOriginalName() . time()) . '.jpg';
            \Storage::disk($disk)->put($destination_path . '/' . $filename, $image->stream());
            return response()->json(['success' => true, 'filename' => $destination_path_filename . '/' . $filename]);
        } catch (\Exception $e) {
            dd($e);
            if (empty($image)) {
                return response('Not a valid image type', 412);
            } else {
                return response('Unknown error', 412);
            }
        }
    }

    protected function setupReorderOperation()
    {
        // define which model attribute will be shown on draggable elements
        $this->crud->set('reorder.label', 'name');
        // define how deep the admin is allowed to nest the items
        // for infinite levels, set it to 0
        $this->crud->set('reorder.max_level', 5);
    }
}
