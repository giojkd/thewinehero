<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PagetypeRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class PagetypeCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PagetypeCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    #use \Backpack\CRUD\app\Http\Controllers\Operations\CloneOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Pagetype');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/pagetype');
        $this->crud->setEntityNameStrings('pagetype', 'pagetypes');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();
        $this->crud->addColumn([
            'name' => 'slug',
            'label' => "Slug",
            'type' => 'text',

        ]);
        $this->crud->addColumn([
            'name' => 'og_type',
            'label' => "og:type",
            'type' => 'text',

        ]);
        $this->crud->addColumn([
            'name' => 'title',
            'label' => "Title",
            'type' => 'text',

        ]);
        $this->crud->addColumn([
            'name' => 'description',
            'label' => "Description",
            'type' => 'textarea',

        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(PagetypeRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();
        $this->crud->addField([
            'name' => 'slug',
            'label' => "Slug",
            'type' => 'text',

        ]);
        $this->crud->addField([
            'name' => 'og_type',
            'label' => "og:type",
            'type' => 'text',

        ]);
        $this->crud->addField([
            'name' => 'title',
            'label' => "Title",
            'type' => 'text',

        ]);
        $this->crud->addField([
            'name' => 'description',
            'label' => "Description",
            'type' => 'textarea',

        ]);


        $this->crud->addField([   // Upload
            'name' => 'cover',
            'label' => 'Cover',
            'type' => 'browse',
            'upload' => true,
            //'disk' => 'custom_public', // if you store files in the /public folder, please ommit this; if you store them in /storage or S3, please specify it;
            // optional:
            //temporary' => 10 // if using a service, such as S3, that requires you to make temporary URL's this will make a URL that is valid for the number of minutes specified
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
