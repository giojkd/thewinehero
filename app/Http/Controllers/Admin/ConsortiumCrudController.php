<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ConsortiumRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ConsortiumCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ConsortiumCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ReorderOperation;


    protected function setupReorderOperation()
    {
        // define which model attribute will be shown on draggable elements
        $this->crud->set('reorder.label', 'name');
        // define how deep the admin is allowed to nest the items
        // for infinite levels, set it to 0
        $this->crud->set('reorder.max_level', 1);
    }

    public function setup()
    {
        $this->crud->setModel('App\Models\Consortium');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/consortium');
        $this->crud->setEntityNameStrings('consortium', 'consortia');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #   $this->crud->setFromDb();
        $this->crud->addColumn(
            [
                'name' => 'id',
                'type' => 'text',
                'label' => 'ID'
            ]
        );

        $this->crud->addColumn([
            'name' => 'enabled',
            'label' => "Enabled",
            'type' => 'boolean',

        ]);
         $this->crud->addColumn([
            'name' => 'name',
            'label' => "Name",
            'type' => 'text',
        ]);

        $this->crud->addColumn([
            'name' => 'is_sponsor',
            'label' => "Is sponsor",
            'type' => 'boolean',
        ]);

        $this->crud->addColumn([
            'name' => 'url',
            'type'     => 'closure',
            'function' => function ($entry) {
                $langs = array_keys(Config('backpack.crud.locales'));
                $return = '<table>';
                foreach ($langs as $lang) {
                    $return .= '<tr><td>' . $lang . '</td><td>' . parse_url($entry->makeUrl($lang))['path'] . '</td></tr>';
                }
                $return .= '</table>';
                return $return;
            }
        ]);
        $this->crud->addColumn(
            [
                'name' => 'customizable_layout', // The db column name
                'label' => "Customizable layout", // Table column heading
                'type' => 'boolean'
            ],
        );
        $this->crud->addColumn([

            'label' => 'Customizable Layout Links',
            'name' => 'customizable_layout_links',
            'type' => 'closure',
            'function' => function ($item) {
                return view('admin_custom.customizableLayoutLinks', ['item' => $item]);
            }

        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(ConsortiumRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();

        $this->crud->addField([
            'name' => 'enabled',
            'label' => "Enabled",
            'type' => 'checkbox',

        ]);


        $this->crud->addField(
            [
                'name' => 'customizable_layout', // The db column name
                'label' => "Customizable layout", // Table column heading
                'type' => 'checkbox'
            ],
        );
        $this->crud->addField([
            'name' => 'name',
            'label' => "Name",
            'type' => 'text',
        ]);

        $this->crud->addField([
            'name' => 'url',
            'label' => "Url",
            'type' => 'text',
        ]);

        $this->crud->addField([
            'name' => 'name_old',
            'label' => "Name OLD",
            'type' => 'text',
        ]);

        $this->crud->addField([   // repeatable
            'name'  => 'custom_icons',
            'label' => 'Custom icons',
            'type'  => 'repeatable',
            'fields' => [
                [
                    'name'    => 'image',
                    'type'    => 'browse',
                    'label'   => 'Image',

                ],
                [
                    'name'    => 'title',
                    'type'    => 'text',
                    'label'   => 'Title',

                ],
                [
                    'name'  => 'content',
                    'type'  => 'ckeditor',
                    'label' => 'Content',
                ],
            ],
            #'tab' => 'info',

            // optional
            'new_item_label'  => 'Add icon', // customize the text of the button
        ],);


        $this->crud->addField([
            'label' => "Logo",
            'name' => "logo",
            'type' => 'image',
            'upload' => true,
            'crop' => true, // set to true to allow cropping, false to disable
            //'aspect_ratio' => 1, // ommit or set to 0 to allow any aspect ratio
            // 'disk' => 's3_bucket', // in case you need to show images from a different disk
            // 'prefix' => 'uploads/images/profile_pictures/' // in case your db value is only the file name (no path), you can use this to prepend your path to the image src (in HTML), before it's shown to the user;
        ]);

        $this->crud->addFields([
            [
                'type' => 'checkbox',
                'name' => 'is_sponsor',
                'label' => 'Is sponsor'
            ],
            [
                'type' => 'textarea',
                'name' => 'short_description',
                'label' => 'Short description'
            ],
            [
                'type' => 'wysiwyg',
                'name' => 'description',
                'label' => 'Description'
            ],
            [
                'type' => 'text',
                'name' => 'funded_at',
                'label' => 'Funded at'
            ],
            [
                'type' => 'text',
                'name' => 'consortium_members',
                'label' => 'Consortium member'
            ],
            [
                'type' => 'text',
                'name' => 'production',
                'label' => 'Production'
            ],
            [
                'type' => 'text',
                'name' => 'surface',
                'label' => 'Surface'
            ],
            [
                'type' => 'email',
                'name' => 'email',
                'label' => 'Email'
            ],
            [
                'type' => 'text',
                'name' => 'telephone',
                'label' => 'Telephone'
            ],
            [
                'type' => 'url',
                'name' => 'website',
                'label' => 'Website'
            ],
            /*
            [
                'type' => 'wysiwyg',
                'name' => 'history_description',
                'label' => 'Historical description'
            ],
            [
                'type' => 'wysiwyg',
                'name' => 'geography_description',
                'label' => 'Geographical description'
            ],
            [
                'type' => 'wysiwyg',
                'name' => 'production_description',
                'label' => 'Production description'
            ],
*/


        ]);

           $this->crud->addField([
            'label' => 'Address',
            'name' => 'address',
            'type' => 'address_algolia',
            'store_as_json' => true
        ]);

        $this->crud->addField([   // repeatable
            'name'  => 'custom_blocks',
            'label' => 'Custom blocks',
            'type'  => 'repeatable',
            'fields' => [
                [
                    'name'    => 'title',
                    'type'    => 'text',
                    'label'   => 'Title',
                    'wrapper' => ['class' => 'form-group col-md-12'],
                ],
                [
                    'name'  => 'content',
                    'type'  => 'ckeditor',
                    'label' => 'Content',
                ],
            ],


            // optional
            'new_item_label'  => 'Add block', // customize the text of the button
        ],);

        $this->crud->addField([
            'name' => 'custom_blocks_aux',
            'label' => 'Custom blocks AUX',
            'type'  => 'repeatable',
            'fields' => [
                [
                    'name'    => 'title',
                    'type'    => 'text',
                    'label'   => 'Title',
                    'wrapper' => ['class' => 'form-group col-md-12'],
                ],
                [
                    'name'  => 'content',
                    'type'  => 'ckeditor',
                    'label' => 'Content',
                ],
            ],


            // optional
            'new_item_label'  => 'Add block', // customize the text of the button
        ]);



        $this->crud->addField([
            'tab' => 'Images',
            'name' => 'photos', // db column name
            'label' => 'Photos', // field caption
            'type' => 'dropzone', // voodoo magic
            'prefix' => '/', // upload folder (should match the driver specified in the upload handler defined below)
            'upload-url' => '/' . config('backpack.base.route_prefix') . '/media-dropzone', // POST route to handle the individual file uploads
        ]);

        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
            'label' => "Videos",
            'type' => 'select2_multiple',
            'name' => 'videos', // the method that defines the relationship in your Model
            'entity' => 'videos', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            'pivot' => true, // on create&update, do you need to add/delete pivot table entries?
            // 'select_all' => true, // show Select All and Clear buttons?

            // optional
            'model' => "App\Models\Video", // foreign key model
            'options' => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            })

        ]);

        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
            'label' => "Articles",
            'type' => 'select2_multiple',
            'name' => 'articles', // the method that defines the relationship in your Model
            'entity' => 'articles', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            'pivot' => true, // on create&update, do you need to add/delete pivot table entries?
            // 'select_all' => true, // show Select All and Clear buttons?

            // optional
            'model' => "App\Models\Article", // foreign key model
            'options' => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            })

        ]);

        $this->crud->addFields([
            [
                'name' => 'h1',
                'type' => 'text',
                'label' => 'H1'
            ],
            [
                'name' => 'metatitle',
                'type' => 'text',
                'label' => 'Meta Title'
            ],
            [
                'name' => 'meta_description',
                'type' => 'text',
                'label' => 'Meta Description'
            ],
        ]);

    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

     public function handleDropzoneUpload(DropzoneRequest $request)
    {
        $disk = "custom_public"; //
        $destination_path = "";
        $destination_path_filename = "storage";
        $file = $request->file('file');

        try {
            $image = \Image::make($file);
            $filename = md5($file->getClientOriginalName() . time()) . '.jpg';
            \Storage::disk($disk)->put($destination_path . '/' . $filename, $image->stream());
            return response()->json(['success' => true, 'filename' => $destination_path_filename . '/' . $filename]);
        } catch (\Exception $e) {
            dd($e);
            if (empty($image)) {
                return response('Not a valid image type', 412);
            } else {
                return response('Unknown error', 412);
            }
        }
    }
}
