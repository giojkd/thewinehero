<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\RecipeRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\Http\Requests\DropzoneRequest;
use App\Models\Recipe;
use App\Models\Product;

/**
 * Class RecipeCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class RecipeCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ReorderOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Recipe');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/recipe');
        $this->crud->setEntityNameStrings('recipe', 'recipes');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();
        $this->crud->addColumn(
            [
                'name' => 'id',
                'type' => 'text',
                'label' => 'ID'
            ]
        );
        $this->crud->addColumn([
            'name' => 'is_locked',
            'label' => "Is locked",
            'type' => 'boolean',

        ]);
        $this->crud->addColumn([
            'name' => 'enabled',
            'label' => "Enabled",
            'type' => 'boolean',

        ]);

        $this->crud->addColumn([
            'name' => 'id',
            'label' => "ID",


        ]);

        $this->crud->addColumn([
            'name' => 'name',
            'label' => "Name",


        ]);
        $this->crud->addField([
            'name' => 'url',
            'label' => "Url",
            'type' => 'text',
        ]);

        $this->crud->addColumn([    // Select2Multiple = n-n relationship (with pivot table)
            'label' => "Manufacturers",
            'name' => 'manufacturers',
            'type' => 'select_multiple',
            'entity' => 'manufacturers', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            // optional
            'model' => "App\Models\Manufacturer", // foreign key model

            'limit' => 400
        ]);

        $this->crud->addColumn([    // Select2Multiple = n-n relationship (with pivot table)
            'label' => "Consortium",
            'name' => 'consortiums', // the method that defines the relationship in your Model
            'type' => 'select_multiple',
            'entity' => 'consortiums', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            // optional
            'model' => "App\Models\Consortium", // foreign key model

            'limit' => 400
        ]);

        $this->crud->addColumn(
            [
                // n-n relationship (with pivot table)
                'label' => "Ingredients", // Table column heading
                'type' => "select_multiple",
                'name' => 'ingredients', // the method that defines the relationship in your Model
                'entity' => 'ingredients', // the method that defines the relationship in your Model
                'attribute' => "name", // foreign key attribute that is shown to user
                'model' => "App\Models\Ingredient", // foreign key model
             ]
        );

        $this->crud->addColumn([
            'name' => 'reviews_count',
            'label' => "Reviews",
            'type' => 'text',

        ]);

        $this->crud->addColumn([
            'name' => 'reviews_avg',
            'label' => "Score",
            'type' => 'text',

        ]);


        $this->crud->addColumn([
            'name' => 'is_premium',
            'label' => 'Is Premium',
            'type' => 'boolean',
            'tab' => 'info'
        ]);

        $this->crud->addColumn([    // Select2Multiple = n-n relationship (with pivot table)
            'label'     => "Categories",
            'type'      => 'select_multiple',
            'name'      => 'categories', // the method that defines the relationship in your Model
            'entity'    => 'categories', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
            // 'select_all' => true, // show Select All and Clear buttons?

            // optional
            'model'     => "App\Models\Category", // foreign key model
            'options'   => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            })
        ]);

        $this->crud->addColumn([
            'name' => 'url',
            'type'     => 'closure',
            'function' => function ($entry) {
                $langs = array_keys(Config('backpack.crud.locales'));
                $return = '<table>';
                foreach ($langs as $lang) {
                    $return .= '<tr><td>' . $lang . '</td><td>' . parse_url($entry->makeUrl($lang))['path'] . '</td></tr>';
                }
                $return .= '</table>';
                return $return;
            }
        ]);



    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(RecipeRequest::class);

        $this->crud->addField([
            'name' => 'is_locked',
            'label' => "Is locked",
            'type' => 'checkbox',
            'tab' => 'info',
        ]);

        $this->crud->addField([
            'name' => 'enabled',
            'label' => "Enabled",
            'type' => 'checkbox',
            'tab' => 'info',
        ]);

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();
        $this->crud->addField([
          'label' => "User",
          'type' => 'select2',
          'name' => 'user_id', // the db column for the foreign key
          'entity' => 'user', // the method that defines the relationship in your Model
          'attribute' => 'name', // foreign key attribute that is shown to user
          'tab' => 'info',
          // optional
          'model' => "App\User", // foreign key model
          'default' => ''
          /* 'options'   => (function ($query) {
          return $query->orderBy('name', 'ASC')->where('depth', 1)->get();
        }) */
      ]);

        $this->crud->addField([
            'name' => 'name',
            'label' => "Name",
            'type' => 'text',
            'tab' => 'info'
        ]);
        $this->crud->addField([
            'name' => 'subtitle',
            'label' => "Subtitle",
            'type' => 'text',
            'tab' => 'info'
        ]);

        $this->crud->addField([
            'name' => 'url',
            'label' => "Url",
            'type' => 'text',
            'tab' => 'info',
        ]);

        $this->crud->addField([
            'name' => 'description',
            'label' => "Description",
            'type' => 'wysiwyg',
            'tab' => 'info'
        ]);
        $this->crud->addField([
            'name' => 'calories',
            'label' => "Calories",
            'type' => 'number',
            'tab' => 'info'
        ]);
        $this->crud->addField([
            'name' => 'keywords',
            'label' => "Keywords",
            'type' => 'text',
            'tab' => 'info',
            'help' => 'Inserire separate da , (virgola) Es: coffe beans, pasta'
        ]);
        $this->crud->addField([
            'name' => 'servings',
            'label' => "Servings",
            'type' => 'number',
            'tab' => 'info'
        ]);
        $this->crud->addField([
            'name' => 'servings_notes',
            'label' => "Servings Notes",
            'type' => 'textarea',
            'tab' => 'info'
        ]);
        $this->crud->addField([
            'name' => 'servings_notes_old',
            'label' => "Servings Notes OLD",
            'type' => 'textarea',
            'tab' => 'info'
        ]);
        $this->crud->addField([
            'name' => 'preparation_time',
            'label' => "Preparation time (minutes)",
            'type' => 'number',
            'tab' => 'info'
        ]);
        $this->crud->addField([
            'name' => 'cooking_time',
            'label' => "Cooking time (minutes)",
            'type' => 'number',
            'tab' => 'info'
        ]);
        $this->crud->addField([
            'name' => 'preparation_time_notes',
            'label' => "Preparation time notes",
            'type' => 'textarea',
            'tab' => 'info'
        ]);
      /*  $this->crud->addField([
            'name' => 'keywords',
            'label' => "Keywords",
            'type' => 'text',
            'hint' => 'Separate multiple entries in a keywords list with commas. Es: winter apple pie, nutmeg crust',
            'tab' => 'info'
        ]); */
        $this->crud->addField([
            'name' => 'preparation_time_notes_old',
            'label' => "Preparation time notes OLD",
            'type' => 'textarea',
            'tab' => 'info'
        ]);

        $this->crud->addField([
            'label' => 'Podcasts',
            'type'      => 'select2_multiple',
            'name'      => 'podcasts', // the method that defines the relationship in your Model
            'entity'    => 'podcasts', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
            // 'select_all' => true, // show Select All and Clear buttons?

            // optional
            'model'     => "App\Models\Podcast", // foreign key model
            'tab' => 'Media'

        ]);


        $this->crud->addField([
            'name' => 'general_notes',
            'label' => "General notes",
            'type' => 'textarea',
            'tab' => 'info'
        ]);

        $this->crud->addField([
            'tab' => 'Media',
            'name' => 'photos', // db column name
            'label' => 'Photos', // field caption
            'type' => 'dropzone', // voodoo magic
            'prefix' => '/', // upload folder (should match the driver specified in the upload handler defined below)
            'upload-url' => '/' . config('backpack.base.route_prefix') . '/media-dropzone', // POST route to handle the individual file uploads
        ]);

        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
            'label' => "Videos",
            'type' => 'select2_multiple',
            'name' => 'videos', // the method that defines the relationship in your Model
            'entity' => 'videos', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            'pivot' => true, // on create&update, do you need to add/delete pivot table entries?
            // 'select_all' => true, // show Select All and Clear buttons?

            // optional
            'model' => "App\Models\Video", // foreign key model
            'options' => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }),
            'tab' => 'Media'

        ]);

        $this->crud->addField([
            'tab' => 'Media',
            'name' => 'video_link', // db column name
            'label' => 'Video Link', // field caption
            'type' => 'text', // voodoo magic
        ]);




        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
            'label' => "Complexity",
            'type' => 'select2',
            'name' => 'complexity_id', // the method that defines the relationship in your Model
            'entity' => 'complexity', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            // optional
            'model' => "App\Models\Complexity", // foreign key model
            'options' => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }),
            'tab' => 'info'
        ]);

        $this->crud->addField([
            'label' => "Course",
            'type' => 'select2',
            'name' => 'dishcourse_id',
            'entity' => 'dishcourse',
            'attribute' => 'name',
            'model' => "App\Models\Dishcourse",
            'options' => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }),
            'tab' => 'info'
        ]);

        $this->crud->addField([
            'label' => "Cuisine",
            'type' => 'select2',
            'name' => 'cuisine_id',
            'entity' => 'cuisine',
            'attribute' => 'name',
            'model' => "App\Models\Cuisine",
            'options' => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }),
            'tab' => 'info'
        ]);

        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
            'label'     => "Categories",
            'type'      => 'select2_multiple',
            'name'      => 'categories', // the method that defines the relationship in your Model
            'entity'    => 'categories', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
            // 'select_all' => true, // show Select All and Clear buttons?

            // optional
            'model'     => "App\Models\Category", // foreign key model
            'options'   => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }),
            'tab' => 'info'
        ]);

        /*

        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
            'label'     => "Products",
            'type'      => 'select2_multiple',
            'name'      => 'products', // the method that defines the relationship in your Model
            'entity'    => 'products', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
            // 'select_all' => true, // show Select All and Clear buttons?
            'tab' => 'Pairings',
            // optional
            'model'     => "App\Models\Product", // foreign key model
            'options'   => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select
        ]);

        */

        $this->crud->addField([   // select_and_order
            'name'  => 'product_pairings',
            'label' => "Pairings",
            'type'  => 'select_and_order',
            'options' => Product::whereEnabled(1)->orderBy('name')->get()->pluck('name','id')->toArray(),
            'tab' => 'Pairings'
        ]);



        $this->crud->addField([
          'label' => 'Recommended recipes',
          'type' => 'select2_from_array',
          'options' => Recipe::all()->pluck('name','id'),
          'allows_multiple' => true,
          #'fake' => true,
          'name' => 'recommendations',
          'tab' => 'info'
        ]);

        $this->crud->addField([
            'name' => 'price',
            'label' => "Price",
            'type' => 'number',
            'tab' => 'info'
        ]);

        $this->crud->addField([
            'name' => 'is_free',
            'label' => "Is Free",
            'type' => 'checkbox',
            'tab' => 'info'
        ]);

        $this->crud->addField([
            'name' => 'is_free_until',
            'label' => "Is free until",
            'type' => 'date_picker',
            'tab' => 'info'
        ]);


        $this->crud->addField([
            'name' => 'is_premium',
            'label' => 'Is Premium',
            'type' => 'checkbox',
            'tab' => 'info'
        ]);

        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
            'label'     => "Ingredients lists",
            'type'      => 'select2_multiple',
            'name'      => 'ingredientslists', // the method that defines the relationship in your Model
            'entity'    => 'ingredientslists', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
            // 'select_all' => true, // show Select All and Clear buttons?
            'tab' => 'info',
            // optional
            'model'     => "App\Models\Incredientslist", // foreign key model
            'options'   => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select
        ]);

        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
            'label' => "Manufacturers",
            'name' => 'manufacturers',
            'type' => 'select2_multiple',
            'entity' => 'manufacturers', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'pivot'     => true,
            // optional
            'model' => "App\Models\Manufacturer", // foreign key model
            'options' => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }),
            'tab' => 'info'
        ]);

        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
            'label' => "Consortium",
            'name' => 'consortiums', // the method that defines the relationship in your Model
            'type' => 'select2_multiple',
            'entity' => 'consortiums', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'pivot'     => true,
            // optional
            'model' => "App\Models\Consortium", // foreign key model
            'options' => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }),
            'tab' => 'info'
        ]);

    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }



    public function handleDropzoneUpload(DropzoneRequest $request)
    {
        $disk = "custom_public"; //
        $destination_path = "";
        $destination_path_filename = "storage";
        $file = $request->file('file');

        try
        {
            $image = \Image::make($file);
            $filename = md5($file->getClientOriginalName().time()).'.jpg';
            \Storage::disk($disk)->put($destination_path.'/'.$filename, $image->stream());
            return response()->json(['success' => true, 'filename' => $destination_path_filename . '/' . $filename]);
        }
        catch (\Exception $e)
        {
            dd($e);
            if (empty ($image)) {
                return response('Not a valid image type', 412);
            } else {
                return response('Unknown error', 412);
            }
        }
    }

    protected function setupReorderOperation()
    {
        // define which model attribute will be shown on draggable elements
        $this->crud->set('reorder.label', 'name');
        // define how deep the admin is allowed to nest the items
        // for infinite levels, set it to 0
        $this->crud->set('reorder.max_level', 1);
    }


    public function saveReorder()
    {
        $this->crud->hasAccessOrFail('reorder');

        $all_entries = \Request::input('tree');



        if (count($all_entries)) {
            $count = $this->crud->updateTreeOrder($all_entries);
        } else {
            return false;
        }

        dd($all_entries);

        return 'success for ' . $count . ' items';
    }

}
