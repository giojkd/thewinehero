<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\LinkclickRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class LinkclickCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class LinkclickCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    #use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    #use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    #use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    #use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Linkclick::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/linkclick');
        CRUD::setEntityNameStrings('link click', 'link clicks');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {

        // daterange filter
        $this->crud->addFilter(
            [
                'type'  => 'date_range',
                'name'  => 'from_to',
                'label' => 'Date range'
            ],
            false,
            function ($value) { // if the filter is active, apply these constraints
                $dates = json_decode($value);
                $this->crud->addClause('where', 'created_at', '>=', $dates->from);
                $this->crud->addClause('where', 'created_at', '<=', $dates->to . ' 23:59:59');
            }
        );

        $this->crud->addColumns([
            [
                'label' => 'Link',
                'name' => 'clicked_link',
                'type' => 'text',
                'limit' => '240'
            ],
            [
                'label' => 'Clicked from',
                'name' => 'clicked_from',
                'type' => 'text',
                'limit' => '240'
            ],
            /*[
                'label' => 'External',
                'name' => 'is_external',
                'type' => 'boolean'
            ],*/
            [
                'label' => 'Remote ip',
                'name' => 'remote_ip',
                'type' => 'text'
            ],
        ]);


        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(LinkclickRequest::class);



        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
