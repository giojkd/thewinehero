<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ArticlecategoryRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ArticlecategoryCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ArticlecategoryCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Articlecategory');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/articlecategory');
        $this->crud->setEntityNameStrings('articlecategory', 'articlecategories');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();
        $this->crud->addColumns([
            [
                'name' => 'id',
                'type' => 'text',
                'label' => 'ID'
            ],
            [
                'name' => 'name',
                'type' => 'text'
            ]
        ]);

        $this->crud->addColumn(
            [
                'name' => 'url',
                'type'     => 'closure',
                'function' => function ($entry) {
                    $langs = array_keys(Config('backpack.crud.locales'));
                    $return = '<table>';
                    foreach ($langs as $lang) {
                        $return .= '<tr><td>' . $lang . '</td><td>'.parse_url($entry->makeUrl($lang))['path'].'</td></tr>';
                    }
                    $return .= '</table>';
                    return $return;
                }
            ],
        );
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(ArticlecategoryRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        #   $this->crud->setFromDb();

        $this->crud->addFields([

            [
                'name' => 'name',
                'type' => 'text',
                'label' => 'Name'
            ],
            [
                'name' => 'description',
                'type' => 'textarea',
                'label' => 'Description'
            ],

            [
                'name' => 'url',
                'type' => 'text',
                'label' => 'Url'
            ],
            [
                'name' => 'h1',
                'type' => 'text',
                'label' => 'H1'
            ],
            [
                'name' => 'metatitle',
                'type' => 'text',
                'label' => 'Meta Title'
            ],
            [
                'name' => 'meta_description',
                'type' => 'text',
                'label' => 'Meta Description'
            ],
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
