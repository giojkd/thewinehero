<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProductRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\Http\Requests\DropzoneRequest;
use App\Models\Product;
use App\Models\Category;

/**
 * Class ProductCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ProductCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Product');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/product');
        $this->crud->setEntityNameStrings('product', 'products');
    }

    protected function setupShowOperation(){
        $this->crud->addColumn([
            'name' => 'articles',
            'type' => 'select_multiple',
            'entity' => 'articles', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to use
            'model' => "App\Models\Article", // foreign key model
            'limit' => '1000'
        ]);
        $this->crud->addColumn([
            'name' => 'recipes',
            'type' => 'select_multiple',
            'entity' => 'recipes', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to use
            'model' => "App\Models\Recipe", // foreign key model
            'limit' => '1000'
        ]);
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();

        $this->crud->addColumn(
            [
                'name' => 'id',
                'type' => 'text',
                'label' => 'ID'
            ]
        );

        $this->crud->addColumn(
            [
                'name' => 'type',
                'type' => 'text',
                'label' => 'Product Type'
            ]
        );

        $this->crud->addColumn([    // Select2Multiple = n-n relationship (with pivot table)
            'label' => "Type",
            'type' => 'select',
            'name' => 'producttype_id', // the method that defines the relationship in your Model
            'entity' => 'producttype', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            // optional
            'model' => "App\Models\Producttype", // foreign key model
            'tab' => 'Info',
        ]);


        $this->crud->addColumn([
            'name' => 'enabled',
            'label' => "Enabled",
            'type' => 'boolean',

        ]);

        $this->crud->addColumn([
            'name' => 'name',
            'label' => "Name",
            'type' => 'text',
        ]);


        $this->crud->addColumn([
            'name' => 'price',
            'label' => "Price",
            'type' => 'text',
        ]);



        $this->crud->addColumn([    // Select2Multiple = n-n relationship (with pivot table)
            'label' => "Manufacturer",
            'type' => 'select',
            'name' => 'manufacturer_id', // the method that defines the relationship in your Model
            'entity' => 'manufacturer', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            // optional
            'model' => "App\Models\Manufacturer", // foreign key model

        ]);

        $this->crud->addColumn([    // Select2Multiple = n-n relationship (with pivot table)
            'label'     => "Categories",
            'type'      => 'select_multiple',
            'name'      => 'categories', // the method that defines the relationship in your Model
            'entity'    => 'categories', // the method that defines the relationship in your Model
            'attribute' => 'identifier', // foreign key attribute that is shown to user

            'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
            // 'select_all' => true, // show Select All and Clear buttons?

            // optional
            'model'     => "App\Models\Category", // foreign key model
            'options'   => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            })
        ]);

        $this->crud->addColumn([
            'name' => 'articles',
            'type' => 'model_function',
            'function_name' => 'getEntityLinkName',
            'function_parameters' => ['articles'],
            'limit' => 1000
        ]);

        $this->crud->addColumn([
            'name' => 'recipes',
            'type' => 'model_function',
            'function_name' => 'getEntityLinkName',
            'function_parameters' => ['recipes'],
            'limit' => 1000
        ]);

        $this->crud->addColumn([
            'name' => 'url',
            'type'     => 'closure',
            'function' => function ($entry) {
                $langs = array_keys(Config('backpack.crud.locales'));
                $return = '<table>';
                foreach ($langs as $lang) {
                    $return .= '<tr><td>' . $lang . '</td><td>' . parse_url($entry->makeUrl($lang))['path'] . '</td></tr>';
                }
                $return .= '</table>';
                return $return;
            }
        ]);


    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(ProductRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();

        $this->crud->addField([
            'name' => 'enabled',
            'label' => "Enabled",
            'type' => 'checkbox',
            'tab' => 'Info',
        ]);

        $this->crud->addField(
            [
                'name' => 'type',
                'type' => 'select_from_array',
                'options' => [
                    'simple' => 'Simple',
                    'bundle' => 'Bundle'
                ],
                'label' => 'Product Type',
                'tab' => 'Info',
            ]
        );

        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
            'label' => "Type",
            'type' => 'select2',
            'name' => 'producttype_id', // the method that defines the relationship in your Model
            'entity' => 'producttype', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            // optional
            'model' => "App\Models\Producttype", // foreign key model
            'tab' => 'Info',
        ]);


        $this->crud->addField([
            'name' => 'name',
            'label' => "Name",
            'type' => 'text',
            'tab' => 'Info',
        ]);

        $this->crud->addField([
            'name' => 'url',
            'label' => "Url",
            'type' => 'text',
            'tab' => 'Info',
        ]);


        $this->crud->addField([
            'name' => 'description_short',
            'label' => "Short Description",
            'type' => 'text',
            'tab' => 'Info',
        ]);

        $this->crud->addField([
            'name' => 'description',
            'label' => "Description",
            'type' => 'wysiwyg',
            'tab' => 'Info',
        ]);

        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
            'label' => "Manufacturer",
            'type' => 'select2',
            'name' => 'manufacturer_id', // the method that defines the relationship in your Model
            'entity' => 'manufacturer', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            // optional
            'model' => "App\Models\Manufacturer", // foreign key model
            'options' => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }),
            'tab' => 'Info',
        ]);

        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
            'label' => "Consortium",
            'type' => 'select2',
            'name' => 'consortium_id', // the method that defines the relationship in your Model
            'entity' => 'consortium', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            // optional
            'model' => "App\Models\Consortium", // foreign key model
            'options' => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }),
            'tab' => 'Info',
        ]);

        $this->crud->addField([
            'name' => 'format_quantity',
            'label' => "Format quantity",
            'type' => 'text',
            'tab' => 'Info',
        ]);

        $this->crud->addField([
            'name' => 'format_measurement_unit',
            'label' => "Format mu",
            'type' => 'select_from_array',
            'options' => [
              'l' => 'l',
              'ml'=>'ml',
              'cl' => 'cl',
              'g' => 'g',
              'kg' => 'kg'
            ],
            'tab' => 'Info',
        ]);



        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
            'label' => "Categories",
            'type' => 'select2_multiple',
            'name' => 'categories', // the method that defines the relationship in your Model
            'entity' => 'categories', // the method that defines the relationship in your Model
            'attribute' => 'identifier', // foreign key attribute that is shown to user

            'pivot' => true, // on create&update, do you need to add/delete pivot table entries?
            // 'select_all' => true, // show Select All and Clear buttons?

            // optional
            'model' => "App\Models\Category", // foreign key model
            'options' => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }),
            'tab' => 'Info',
        ]);

        $this->crud->addField([
            'tab' => 'Images',
            'name' => 'photos', // db column name
            'label' => 'Photos', // field caption
            'type' => 'dropzone', // voodoo magic
            'prefix' => '/', // upload folder (should match the driver specified in the upload handler defined below)
            'upload-url' => '/' . config('backpack.base.route_prefix') . '/media-dropzone', // POST route to handle the individual file uploads
        ]);

        $this->crud->addField([
            'tab' => 'Images',
            'label' => 'Images by Quantity',
            'name' => 'nphotos',
            'type' => 'repeatable',
            'fields' => [
                [
                    'name'    => 'quantity',
                    'type'    => 'number',
                    'label'   => 'Quantity',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
                [
                    'name'  => 'image',
                    'label' => 'Image',
                    'type'  => 'browse'
                ]
            ]
        ]);


        $this->crud->addField([
            'tab' => 'Images',
            'label' => 'Podcasts',
            'type'      => 'select2_multiple',
            'name'      => 'podcasts', // the method that defines the relationship in your Model
            'entity'    => 'podcasts', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
            // 'select_all' => true, // show Select All and Clear buttons?

            // optional
            'model'     => "App\Models\Podcast", // foreign key model


        ]);


        $this->crud->addField([
          'label' => 'Recommended products',
          'type' => 'select2_from_array',
          'options' => Product::all()->pluck('name','id'),
          'allows_multiple' => true,
          'fake' => true,
          'name' => 'recommendations',
          'store_in'=> 'recommendations',
          'tab' => 'Info'
        ]);

        $this->crud->addField([
            'name' => 'price',
            'label' => "Price",
            'type' => 'text',
            'tab' => 'E-Commerce',
        ]);


        $this->crud->addField([
            'name' => 'price_compare',
            'label' => "Compare price with",
            'type' => 'number','attributes' => ["step" => "any"],
            'tab' => 'E-Commerce',
        ]);

        $this->crud->addField([
            'name' => 'quantity',
            'type' => 'number','attributes' => ["step" => "any"],
            'label' => "Quantità",
            'tab' => 'E-Commerce',
        ]);
        $this->crud->addField([
            'name' => 'max_quantity',
            'type' => 'number','attributes' => ["step" => "any"],
            'label' => "Quantità massima per ordine",
            'tab' => 'E-Commerce',
        ]);


        $this->crud->addField([
            'label' => 'Larghezza prodotto',
            'name' => 'width',
            'type' => 'number','attributes' => ["step" => "any"],
            'tab' => 'Dimensions',
            'prefix' => 'cm',
            'tab' => 'E-Commerce',
        ]);

        $this->crud->addField([
            'label' => 'Altezza prodotto',
            'name' => 'height',
            'type' => 'number','attributes' => ["step" => "any"],
            'tab' => 'Dimensions',
            'prefix' => 'cm',
            'tab' => 'E-Commerce',
        ]);


        $this->crud->addField([
            'label' => 'Profondità prodotto',
            'name' => 'depth',
            'type' => 'number','attributes' => ["step" => "any"],
            'tab' => 'Dimensions',
            'prefix' => 'cm',
            'tab' => 'E-Commerce',
        ]);



        $this->crud->addField([
            'label' => 'Peso prodotto',
            'name' => 'weight',
            'type' => 'number','attributes' => ["step" => "any"],
            'tab' => 'E-Commerce',
            'prefix' => 'Kg'
        ]);

        $this->crud->addField([
            'label' => 'Larghezza imballaggio',
            'name' => 'packaging_width',
            'type' => 'number','attributes' => ["step" => "any"],
            'tab' => 'E-Commerce',
            'prefix' => 'cm'
        ]);

        $this->crud->addField([
            'label' => 'Altezza imballaggio',
            'name' => 'packaging_height',
            'type' => 'number','attributes' => ["step" => "any"],
            'tab' => 'Dimensions',
            'prefix' => 'cm',
            'tab' => 'E-Commerce',
        ]);


        $this->crud->addField([
            'label' => 'Profondità imballaggio',
            'name' => 'packaging_depth',
            'type' => 'number','attributes' => ["step" => "any"],
            'tab' => 'Dimensions',
            'prefix' => 'cm',
            'tab' => 'E-Commerce',
        ]);

        $this->crud->addField([
            'label' => 'Peso del prodotto imballato',
            'name' => 'packaging_weight',
            'type' => 'number','attributes' => ["step" => "any"],
            'tab' => 'E-Commerce',
            'prefix' => 'Kg',
            'hint' => 'Used for shipping csts'
        ]);

        $this->crud->addField([
            'name' => 'articles',
            'type' => 'select2_multiple',
            'entity' => 'articles', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to use
            'model' => "App\Models\Article", // foreign key model
            'tab' => 'Relations'
        ]);

        $this->crud->addField([
            'name' => 'recipes',
            'type' => 'select2_multiple',
            'entity' => 'recipes', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to use
            'model' => "App\Models\Recipe", // foreign key model
            'limit' => 25,
            'tab' => 'Relations'
        ]);

        $this->crud->addField([   // select_and_order
            'name'  => 'categories_list',
            'label' => "Features",
            'type'  => 'select_and_order',
            #'options' => Category::where('depth','>=',1)->orderBy('lft','ASC')->get()->pluck('name', 'id')->toArray(),
            'options' => Category::orderBy('lft','ASC')->get()->pluck('identifier', 'id')->toArray(),
            'tab' => 'Features'
        ]);

        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
            'label' => "Manufacturers",
            'name' => 'manufacturers',
            'type' => 'select2_multiple',
            'entity' => 'manufacturers', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'pivot'     => true,
            // optional
            'model' => "App\Models\Manufacturer", // foreign key model
            'options' => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }),
            'tab' => 'Info'
        ]);

        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
            'label' => "Consortium",
            'name' => 'consortiums', // the method that defines the relationship in your Model
            'type' => 'select2_multiple',
            'entity' => 'consortiums', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'pivot'     => true,
            // optional
            'model' => "App\Models\Consortium", // foreign key model
            'options' => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }),
            'tab' => 'Info'
        ]);



    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function handleDropzoneUpload(DropzoneRequest $request)
    {
        $disk = "custom_public"; //
        $destination_path = "";
        $destination_path_filename = "storage";
        $file = $request->file('file');

        try
        {
            $image = \Image::make($file);
            $filename = md5($file->getClientOriginalName().time()).'.jpg';
            \Storage::disk($disk)->put($destination_path.'/'.$filename, $image->stream());
            return response()->json(['success' => true, 'filename' => $destination_path_filename . '/' . $filename]);
        }
        catch (\Exception $e)
        {
            dd($e);
            if (empty ($image)) {
                return response('Not a valid image type', 412);
            } else {
                return response('Unknown error', 412);
            }
        }
    }
}
