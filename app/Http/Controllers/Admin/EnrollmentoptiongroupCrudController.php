<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\EnrollmentoptiongroupRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class EnrollmentoptiongroupCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class EnrollmentoptiongroupCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Enrollmentoptiongroup');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/enrollmentoptiongroup');
        $this->crud->setEntityNameStrings('enrollmentoptiongroup', 'enrollmentoptiongroups');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();
        $this->crud->addColumn(
            [
                'name' => 'id',
                'type' => 'text',
                'label' => 'ID'
            ]
        );
        $this->crud->addColumn([
            'name' => 'name',
            'label' => "Name",
            'type' => 'text',
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(EnrollmentoptiongroupRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();
        $this->crud->addField([
            'name' => 'name',
            'label' => "Name",
            'type' => 'text',
        ]);

        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
            'label' => "Option",
            'type' => 'select2',
            'name' => 'enrollmentoption_id', // the method that defines the relationship in your Model
            'entity' => 'enrollmentoption', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            // optional
            'model' => "App\Models\Enrollmentoption", // foreign key model
            /*'options' => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }),*/
            #'tab' => 'Info',
        ]);


    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
