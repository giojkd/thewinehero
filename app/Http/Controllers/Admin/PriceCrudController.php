<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PriceRequest;
use App\Models\Product;
use App\Models\Country;
use App\Models\Deal;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class PriceCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PriceCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Price::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/price');
        CRUD::setEntityNameStrings('price', 'prices');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::setFromDb(); // columns

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(PriceRequest::class);

        CRUD::setFromDb(); // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function priceManager(PriceRequest $request){
        $data = [];

        $products = Product::whereEnabled(1)->orderBy('name')->get();



#        dd($prices);

        $data['products'] = $products;
        $data['countries'] = Country::whereActive(1)->orderBy('name')->get();

        return view('admin_custom.priceManager',$data);
    }

    public function priceManagerSet(PriceRequest $request)
    {
        $request->validate([
            'quantity' => 'required',
            'country_id' => 'required',
            'product_id' => 'required'
        ]);

        $product = Product::find($request->product_id);
        $price = $product
        ->prices()
        ->where('quantity',$request->quantity)
        ->where('country_id',$request->country_id)
        ->first();

        if(!is_null($price)){
            $type = $request->type;
            $price->$type = $request->value;
            $price->save();
        }else{
            $product->prices()->create([
                'country_id' => $request->country_id,
                'quantity' => $request->quantity,
                $request->type => $request->value
            ]);
        }
    }

    public function priceManagerDeal(PriceRequest $request)
    {
        $data = [];

        $deals = Deal::whereEnabled(1)->orderBy('name')->get();

        $prices = [];

        if($deals->count()>0){
            $deals->each(function ($deal, $keyProduct) use (&$prices) {
                if (!is_null($deal->prices)) {
                    $deal->prices->each(function ($price, $keyPrice) use (&$prices, $deal) {
                        $prices[$deal->id][$price->country_id][$price->quantity]['price'] = $price->price;
                        $prices[$deal->id][$price->country_id][$price->quantity]['price_compare'] = $price->price_compare;
                    });
                }
            });
        }

        #        dd($prices);

        $data['deals'] = $deals;
        $data['prices'] = $prices;
        $data['countries'] = Country::whereActive(1)->orderBy('name')->get();

        return view('admin_custom.priceManagerDeal', $data);
    }

    public function priceManagerSetDeal(PriceRequest $request)
    {
        $deal = Deal::find($request->deal_id);
        $price = $deal
        ->prices()
        ->where('country_id', $request->country_id)
        ->where('quantity', $request->quantity)
        ->first();

        if (!is_null($price)) {
            $type = $request->type;
            $price->$type = $request->value;
            $price->save();
        } else {
            $deal->prices()->create([
                'country_id' => $request->country_id,
                'quantity' => $request->quantity,
                $request->type => $request->value
            ]);
        }
    }

}
