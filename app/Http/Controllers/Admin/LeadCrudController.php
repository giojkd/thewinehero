<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\LeadRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Request;

/**
 * Class LeadCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class LeadCrudController extends CrudController
{

    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;

#   use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
#   use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
#   use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
#   use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Lead');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/lead');
        $this->crud->setEntityNameStrings('lead', 'leads');
    }

    /*


    {
        "name":"Giovanni",
        "email":"giojkd@gmail.com",
        "address":"new york",
        "surname":"Orlandi",
        "doorbell":"GO",
        "telephone":"3281560822",
        "email_confirmation":"giojkd@gmail.com"
    }

    {
        "name": "Vanessa",
        "email": "vanessa@lebaccanti.com",
        "address": "Via della Cernaia 47 Firenze",
        "surname": "Held",
        "telephone": "12345",
        "vat_number": "12345",
        "company_name": "Le Baccanti"
    }


    */

    protected function setupListOperation()
    {

        $request = request();

        #dd($request->all());

        if($request->has('all')){
            $this->crud->addClause('whereNull', 'confirmed_at');
            $this->crud->addClause('whereNotNull', 'shipping_address');
        }else{
            $this->crud->addClause('whereNotNull', 'confirmed_at');
        }

        // TODO: remove setFromDb() and manually define Columns, maybe Filters


#        $this->crud->setFromDb();

        $this->crud->addColumns([
            [
                'label' => 'ID',
                'name' => 'id'
            ],
            [
                'label' => 'Confirmed at',
                'name' => 'confirmed_at',
                'type' => 'datetime'
            ],
            [
                'name' => 'shipping_address',
                'label' => 'Delivery address',
                'type' => 'closure',
                'function' => function($entry){
                    $entry = $entry->shipping_address;
                    $r = [];
                    $r[] = '<small><table class="table table-sm">';
                    if(isset($entry['name']) && isset($entry['surname'])){ $r[] = '<tr><td>'. $entry['name'].' '. $entry['surname'].'</tr></td>'; }
                    if (isset($entry['email'])) { $r[] = '<tr><td>Email: <a href="mailto:' . $entry['email'].'">' . $entry['email'] .'</a></tr></td>'; }
                    if (isset($entry['telephone'])) { $r[] = '<tr><td>Phone: <a href="tel:' . $entry['telephone'] . '">' . $entry['telephone'] . '</a></tr></td>'; }
                    if (isset($entry['address'])) { $r[] = '<tr><td>Address: <a target="_blank" href="https://www.google.com/maps/place/' . $entry['address'] . '">' . $entry['address'] . '</a></tr></td>'; }
                    if (isset($entry['doorbell'])) { $r[] = '<tr><td>Doorbell: ' . $entry['doorbell'] . '</tr></td>'; }
                    $r[] = '</table></small>';
                    return implode(' ',$r);
                }
            ],
            [
                'name' => 'billing_address',
                'label' => 'Billing address',
                'type' => 'closure',
                'function' => function($entry){
                    $entry = $entry->billing_address;
                    $r = [];
                    $r[] = '<small><table class="table table-sm">';
                    if (isset($entry['name']) && isset($entry['surname'])) {
                        $r[] = '<tr><td>' . $entry['name'] . ' ' . $entry['surname'] . '</tr></td>';
                    }
                    if (isset($entry['email'])) {
                        $r[] = '<tr><td>Email: <a href="mailto:' . $entry['email'] . '">' . $entry['email'] . '</a></tr></td>';
                    }
                    if (isset($entry['telephone'])) {
                        $r[] = '<tr><td>Phone: <a href="tel:' . $entry['telephone'] . '">' . $entry['telephone'] . '</a></tr></td>';
                    }
                    if (isset($entry['address'])) {
                        $r[] = '<tr><td>Address: <a target="_blank" href="https://www.google.com/maps/place/' . $entry['address'] . '">' . $entry['address'] . '</a></tr></td>';
                    }
                    if (isset($entry['company_name'])) {
                        $r[] = '<tr><td>Company: ' . $entry['company_name'] . '</tr></td>';
                    }
                    if (isset($entry['vat_number'])) {
                        $r[] = '<tr><td>Vat number: ' . $entry['vat_number'] . '</tr></td>';
                    }
                    $r[] = '</table></small>';
                    return implode(' ', $r);
                }
            ],
            [
                'name' => 'grand_total',
                'label' => 'Grand total',
                'prefix' => '€',
                'type' => 'number',
                'decimals'      => 2,
                'dec_point'     => ',',
                'thousands_sep' => '.',
            ],
            [
                'name' => 'products',
                'label' => 'Products',
                'type' => 'closure',
                'function' => function($entry){
                    $r = [];
                    $r[] = '<small><table class="table table-sm">';

                    foreach($entry->products as $row){
                        if($row->pivot->quantity > 0){
                            if(!is_null($row->photos)){
                                $r[] = '<tr><td><img src="' . Route('ir', ['size' => 'h70', 'filename' => $row->photos[0]]) . '" /> <b>' . $row->name . '</b> x' . $row->pivot->quantity . '</td></tr>';
                            }

                        }
                    }


                    $r[] = '</table></small>';
                    return implode(' ', $r);
                }
            ]
        ]);

    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(LeadRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        $this->crud->setFromDb();
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
