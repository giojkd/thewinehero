<?php

namespace App\Http\Controllers\Admin;

use \Backpack\CRUD\app\Http\Controllers\CrudController;
use \Backpack\PermissionManager\app\Http\Requests\UserStoreCrudRequest as StoreRequest;
use \Backpack\PermissionManager\app\Http\Requests\UserUpdateCrudRequest as UpdateRequest;
use Illuminate\Support\Facades\Hash;

class UserCrudControllerAdmin extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation {
        store as traitStore;
    }
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation {
        update as traitUpdate;
    }
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;

    public function setup()
    {
        $this->crud->setModel(config('backpack.permissionmanager.models.user'));
        $this->crud->setEntityNameStrings(trans('backpack::permissionmanager.user'), trans('backpack::permissionmanager.users'));
        $this->crud->setRoute(backpack_url('user'));
    }

    public function setupListOperation()
    {


        $this->crud->enableExportButtons();

        $this->crud->setColumns(
            [
                [
                    'name' => 'active',
                    'label' => 'Active',
                    'type' => 'boolean',
                ],
                [
                    'name' => 'has_profile',
                    'label' => 'Has profile',
                    'type' => 'boolean'
                ],
                [
                    'name' => 'id',
                    'label' => 'ID',
                    'type' => 'text'
                ],
                [
                    'name' => 'created_at',
                    'label' => 'Created at',
                    'type' => 'date'
                ],
                [
                    'name' => 'avatar', // The db column name
                    'label' => "Avatar", // Table column heading
                    'type' => 'image',
                    // 'prefix' => 'folder/subfolder/',
                    // image from a different disk (like s3 bucket)
                    // 'disk' => 'disk-name',
                    // optional width/height if 25px is not ok with you
                    // 'height' => '30px',
                    // 'width' => '30px',
                ],
                [
                    'name' => 'name',
                    'label' => trans('backpack::permissionmanager.name'),
                    'type' => 'text',
                ],
                [
                    'name' => 'surname',
                    'label' => trans('backpack::permissionmanager.surname'),
                    'type' => 'text',
                ],
                [
                    'name' => 'email',
                    'label' => trans('backpack::permissionmanager.email'),
                    'type' => 'email',
                ],
                [ // n-n relationship (with pivot table)
                    'label' => trans('backpack::permissionmanager.roles'), // Table column heading
                    'type' => 'select_multiple',
                    'name' => 'roles', // the method that defines the relationship in your Model
                    'entity' => 'roles', // the method that defines the relationship in your Model
                    'attribute' => 'name', // foreign key attribute that is shown to user
                    'model' => config('permission.models.role'), // foreign key model
                ],
                [ // n-n relationship (with pivot table)
                    'label' => trans('backpack::permissionmanager.extra_permissions'), // Table column heading
                    'type' => 'select_multiple',
                    'name' => 'permissions', // the method that defines the relationship in your Model
                    'entity' => 'permissions', // the method that defines the relationship in your Model
                    'attribute' => 'name', // foreign key attribute that is shown to user
                    'model' => config('permission.models.permission'), // foreign key model
                ],
                [
                    'name' => 'show_in_home',
                    'label' => 'Show in Homepage Experts Slide',
                    'type' => 'boolean',
                ],
            ]
        );

        // Role Filter
        $this->crud->addFilter(
            [
                'name' => 'role',
                'type' => 'dropdown',
                'label' => trans('backpack::permissionmanager.role'),
            ],
            config('permission.models.role')::all()->pluck('name', 'id')->toArray(),
            function ($value) { // if the filter is active
                $this->crud->addClause('whereHas', 'roles', function ($query) use ($value) {
                    $query->where('role_id', '=', $value);
                });
            }
        );

        // Extra Permission Filter
        $this->crud->addFilter(
            [
                'name' => 'permissions',
                'type' => 'select2',
                'label' => trans('backpack::permissionmanager.extra_permissions'),
            ],
            config('permission.models.permission')::all()->pluck('name', 'id')->toArray(),
            function ($value) { // if the filter is active
                $this->crud->addClause('whereHas', 'permissions', function ($query) use ($value) {
                    $query->where('permission_id', '=', $value);
                });
            }
        );
    }

    public function setupCreateOperation()
    {
        $this->addUserFields();
        $this->crud->setValidation(StoreRequest::class);
    }

    public function setupUpdateOperation()
    {
        $this->addUserFields();
        $this->crud->setValidation(UpdateRequest::class);
    }

    /**
     * Store a newly created resource in the database.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $this->crud->request = $this->crud->validateRequest();
        $this->crud->request = $this->handlePasswordInput($this->crud->request);
        $this->crud->unsetValidation(); // validation has already been run

        return $this->traitStore();
    }

    /**
     * Update the specified resource in the database.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update()
    {
        $this->crud->request = $this->crud->validateRequest();
        $this->crud->request = $this->handlePasswordInput($this->crud->request);
        $this->crud->unsetValidation(); // validation has already been run

        return $this->traitUpdate();
    }

    /**
     * Handle password input fields.
     */
    protected function handlePasswordInput($request)
    {
        // Remove fields not present on the user.
        $request->request->remove('password_confirmation');
        $request->request->remove('roles_show');
        $request->request->remove('permissions_show');

        // Encrypt password if specified.
        if ($request->input('password')) {
            $request->request->set('password', Hash::make($request->input('password')));
        } else {
            $request->request->remove('password');
        }

        return $request;
    }



    protected function addUserFields()
    {
        $this->crud->addFields([
            [
                'name' => 'active',
                'label' => 'Active',
                'type' => 'checkbox',
            ],
            [
                'name' => 'has_profile',
                'label' => 'Has profile',
                'type' => 'checkbox'
            ],
            [
                'label' => "Avatar",
                'name' => "avatar",
                'type' => 'image',
                'upload' => true,
                'crop' => true, // set to true to allow cropping, false to disable
                //'aspect_ratio' => 1, // ommit or set to 0 to allow any aspect ratio
                // 'disk' => 's3_bucket', // in case you need to show images from a different disk
                // 'prefix' => 'uploads/images/profile_pictures/' // in case your db value is only the file name (no path), you can use this to prepend your path to the image src (in HTML), before it's shown to the user;
            ],
            [
                'name' => 'name',
                'label' => trans('backpack::permissionmanager.name'),
                'type' => 'text',
            ],
            [
                'name' => 'surname',
                'label' => trans('backpack::permissionmanager.surname'),
                'type' => 'text',
            ],
            [
                'name' => 'website',
                'label' => trans('backpack::permissionmanager.website'),
                'type' => 'text',
            ],
            [
                'name' => 'social_facebook',
                'label' => trans('backpack::permissionmanager.social_facebook'),
                'type' => 'text',
            ],
            [
                'name' => 'social_twitter',
                'label' => trans('backpack::permissionmanager.social_twitter'),
                'type' => 'text',
            ],
            [
                'name' => 'social_linkedin',
                'label' => trans('backpack::permissionmanager.social_linkedin'),
                'type' => 'text',
            ],
            [
                'name' => 'social_youtube',
                'label' => trans('backpack::permissionmanager.social_youtube'),
                'type' => 'text',
            ],
            [
                'name' => 'heading',
                'label' => trans('backpack::permissionmanager.heading'),
                'type' => 'text',
            ],
            [
                'name' => 'biography',
                'label' => trans('backpack::permissionmanager.biography'),
                'type' => 'textarea',
            ],
            [
                'name' => 'biography_old',
                'label' => 'Biography OLD',
                'type' => 'textarea',
            ],
            [
                'name' => 'h1',
                'type' => 'text',
                'label' => 'H1'
            ],
            [
                'name' => 'metatitle',
                'type' => 'text',
                'label' => 'Meta Title'
            ],
            [
                'name' => 'meta_description',
                'type' => 'text',
                'label' => 'Meta Description'
            ],
            [
                'name' => 'email',
                'label' => trans('backpack::permissionmanager.email'),
                'type' => 'email',
            ],
            [
                'name' => 'password',
                'label' => trans('backpack::permissionmanager.password'),
                'type' => 'password',
            ],
            [
                'name' => 'password_confirmation',
                'label' => trans('backpack::permissionmanager.password_confirmation'),
                'type' => 'password',
            ],
            [
                'name' => 'show_in_home',
                'label' => 'Show in Homepage Experts Slide',
                'type' => 'checkbox',
            ],
            [
                // two interconnected entities
                'label' => trans('backpack::permissionmanager.user_role_permission'),
                'field_unique_name' => 'user_role_permission',
                'type' => 'checklist_dependency',
                'name' => ['roles', 'permissions'],
                'subfields' => [
                    'primary' => [
                        'label' => trans('backpack::permissionmanager.roles'),
                        'name' => 'roles', // the method that defines the relationship in your Model
                        'entity' => 'roles', // the method that defines the relationship in your Model
                        'entity_secondary' => 'permissions', // the method that defines the relationship in your Model
                        'attribute' => 'name', // foreign key attribute that is shown to user
                        'model' => config('permission.models.role'), // foreign key model
                        'pivot' => true, // on create&update, do you need to add/delete pivot table entries?]
                        'number_columns' => 3, //can be 1,2,3,4,6
                    ],
                    'secondary' => [
                        'label' => ucfirst(trans('backpack::permissionmanager.permission_singular')),
                        'name' => 'permissions', // the method that defines the relationship in your Model
                        'entity' => 'permissions', // the method that defines the relationship in your Model
                        'entity_primary' => 'roles', // the method that defines the relationship in your Model
                        'attribute' => 'name', // foreign key attribute that is shown to user
                        'model' => config('permission.models.permission'), // foreign key model
                        'pivot' => true, // on create&update, do you need to add/delete pivot table entries?]
                        'number_columns' => 3, //can be 1,2,3,4,6
                    ],
                ],

            ],
            [
                'name' => 'block_1_title',
                'type' => 'text',
                'label' => 'Block 1 Title'
            ],
            [   // repeatable
                'name'  => 'block_1_content',
                'label' => 'Block 1 Content',
                'type'  => 'repeatable',
                'fields' => [
                    [
                        'name'    => 'content',
                        'type'    => 'text',
                        'label'   => 'Content',

                    ]
                ],
                'new_item_label'  => 'Add content', // customize the text of the button
            ],
            [
                'name' => 'block_2_title',
                'type' => 'text',
                'label' => 'Block 2 Title'
            ],
            [   // repeatable
                'name'  => 'block_2_content',
                'label' => 'Block 2 Content',
                'type'  => 'repeatable',
                'fields' => [
                    [
                        'name'    => 'content',
                        'type'    => 'text',
                        'label'   => 'Content',

                    ]
                ],
                'new_item_label'  => 'Add content', // customize the text of the button
            ],
            [
                'name' => 'block_3_title',
                'type' => 'text',
                'label' => 'Block 3 Title'
            ],
            [   // repeatable
                'name'  => 'block_3_content',
                'label' => 'Block 3 Content',
                'type'  => 'repeatable',
                'fields' => [
                    [
                        'name'    => 'content',
                        'type'    => 'text',
                        'label'   => 'Content',

                    ]
                ],
                'new_item_label'  => 'Add content', // customize the text of the button
            ],
            [
                'name' => 'block_4_title',
                'type' => 'text',
                'label' => 'Block 4 Title'
            ],
            [   // repeatable
                'name'  => 'block_4_content',
                'label' => 'Block 4 Content',
                'type'  => 'repeatable',
                'fields' => [
                    [
                        'name'    => 'content',
                        'type'    => 'text',
                        'label'   => 'Content',

                    ]
                ],
                'new_item_label'  => 'Add content', // customize the text of the button
            ],
            [
                'name' => 'block_5_title',
                'type' => 'text',
                'label' => 'Block 5 Title'
            ],
            [   // repeatable
                'name'  => 'block_5_content',
                'label' => 'Block 5 Content',
                'type'  => 'repeatable',
                'fields' => [
                    [
                        'name'    => 'content',
                        'type'    => 'text',
                        'label'   => 'Content',

                    ]
                ],
                'new_item_label'  => 'Add content', // customize the text of the button
            ],
        ]);
    }
}
