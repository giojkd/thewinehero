<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\QuestionRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class QuestionCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class QuestionCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Question');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/question');
        $this->crud->setEntityNameStrings('question', 'questions');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();
        $this->crud->addColumn([  // Select2
            'label' => "Quizz",
            'type' => 'select',
            'name' => 'quizz_id', // the db column for the foreign key
            'entity' => 'quizz', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            // optional
            'model' => "App\Models\Quizz", // foreign key model

        ]);

        $this->crud->addColumn([
            'name' => 'name',
            'label' => "Name",
            'type' => 'text',
        ]);

        $this->crud->addColumn([
            'name' => 'type_id',
            'label' => "Question type",
            'type' => 'select_from_array',
            'options' => ['1' => 'single', '2' => 'multiple'],
            'allows_null' => false,
            'default' => 'one'
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(QuestionRequest::class);


        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();

        $this->crud->addField([  // Select2
            'label' => "Quizz",
            'type' => 'select2',
            'name' => 'quizz_id', // the db column for the foreign key
            'entity' => 'quizz', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            // optional
            'model' => "App\Models\Quizz", // foreign key model
            'default' => 2, // set the default value of the select2
            'options' => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select
            'tab' => 'info'
        ]);

        $this->crud->addField([
            'name' => 'name',
            'label' => "Question",
            'type' => 'text',
            'tab' => 'info'
        ]);

        $this->crud->addField([
            'name' => 'type_id',
            'label' => "Question type",
            'type' => 'select_from_array',
            'options' => ['1' => 'single', '2' => 'multiple'],
            'allows_null' => false,
            'default' => '1',
            'tab' => 'info'
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
