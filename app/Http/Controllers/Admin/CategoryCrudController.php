<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class CategoryCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class CategoryCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ReorderOperation;
    use \Backpack\ReviseOperation\ReviseOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation {
        update as traitUpdate;
    }
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation {
        store as traitStore;
    }





    public function setup()
    {
        $this->crud->setModel('App\Models\Category');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/category');
        $this->crud->setEntityNameStrings('category', 'categories');
    }

    public function update(){
        #lft,rgt,depth

        $request = $this->crud->getRequest();
        $category = Category::find($request->id);

        $parentCategory = Category::find($request->parent_id);
        if(!is_null($parentCategory)){
            $category->appendToNode($parentCategory)->save();
        }


        $this->crud->getRequest()->request->remove('parent_id');

        $response = $this->traitUpdate();
        // do something after save
        return $response;

    }



    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();
        $this->crud->addColumns([

            [
                'name' => 'id',
                'label' => "ID",
                'type' => 'text',
            ],
            [
                'name' => 'ancestors_breadcrumb',
                'label' => 'Ancestors',
                #'type' => 'model_function',
                #'function_name' => 'printAncestors',

            ],
            [
                'name' => 'name',
                'label' => "Name",
                'type' => 'text',
            ],
            [
                'name' => 'is_filter',
                'label' => "Is Filter",
                'type' => 'boolean',
            ],
            [
                'name' => 'products',
                'type' => 'relationship_count',
                'label' => 'Products'
            ]

        ]);

    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(CategoryRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();

        $this->crud->addField([
            'name' => 'is_filter',
            'label' => "Is Filter",
            'type' => 'checkbox',
        ]);



        $this->crud->addField([
            'name' => 'name',
            'label' => "Name",
            'type' => 'text',
        ]);

        $this->crud->addField([
            'label' => "Icon",
            'name' => "icon",
            'type' => 'image',
            'upload' => true,
            //'crop' => true, // set to true to allow cropping, false to disable
            //'aspect_ratio' => 1, // ommit or set to 0 to allow any aspect ratio
            // 'disk' => 's3_bucket', // in case you need to show images from a different disk
            // 'prefix' => 'uploads/images/profile_pictures/' // in case your db value is only the file name (no path), you can use this to prepend your path to the image src (in HTML), before it's shown to the user;
        ]);

        $this->crud->addField([
            'label' => "Cover",
            'name' => "cover",
            'type' => 'image',
            'upload' => true,
            'crop' => true, // set to true to allow cropping, false to disable
            //'aspect_ratio' => 1, // ommit or set to 0 to allow any aspect ratio
            // 'disk' => 's3_bucket', // in case you need to show images from a different disk
            // 'prefix' => 'uploads/images/profile_pictures/' // in case your db value is only the file name (no path), you can use this to prepend your path to the image src (in HTML), before it's shown to the user;
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();

        $this->crud->addField([
            'name' => 'parent_id',
            #'fake' => true,
            'label' => 'Select parent',
            'type' => 'select2_from_array',
            'options' => Category::orderBy('lft', 'ASC')->orderBy('name', 'ASC')->get()->pluck('ancestors_and_self_breadcrumb', 'id')
        ]);
    }

    protected function setupReorderOperation()
    {
        // define which model attribute will be shown on draggable elements
        $this->crud->set('reorder.label', 'name_with_id');
        // define how deep the admin is allowed to nest the items
        // for infinite levels, set it to 0
        $this->crud->set('reorder.max_level', 5);
    }
}
