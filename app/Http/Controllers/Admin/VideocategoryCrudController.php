<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\VideocategoryRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class VideocategoryCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class VideocategoryCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Videocategory::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/videocategory');
        CRUD::setEntityNameStrings('videocategory', 'videocategories');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        #CRUD::setFromDb(); // columns

        $this->crud->addColumns([
            [
                'name' => 'name',
                'type' => 'text',
                'label' => 'Name'
            ],

        ]);



        $this->crud->addColumn([
            'name' => 'url',
            'type'     => 'closure',
            'function' => function ($entry) {
                $langs = array_keys(Config('backpack.crud.locales'));
                $return = '<table>';
                foreach ($langs as $lang) {
                    $return .= '<tr><td>' . $lang . '</td><td>' . parse_url($entry->makeUrl($lang))['path'] . '</td></tr>';
                }
                $return .= '</table>';
                return $return;
            }
        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(VideocategoryRequest::class);

        #CRUD::setFromDb(); // fields

        $this->crud->addFields([
            [
                'name' => 'name',
                'type' => 'text',
                'label' => 'Name'
            ],




            [
                'name' => 'h1',
                'type' => 'text',
                'label' => 'H1'
            ],
            [
                'name' => 'metatitle',
                'type' => 'text',
                'label' => 'Meta Title'
            ],
            [
                'name' => 'meta_description',
                'type' => 'text',
                'label' => 'Meta Description'
            ],
            [
                'name' => 'url',
                'label' => "Url",
                'type' => 'text',
                'tab' => 'Info',
            ]
        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
