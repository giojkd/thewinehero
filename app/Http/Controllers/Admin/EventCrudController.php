<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\EventRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\Http\Requests\DropzoneRequest;

/**
 * Class EventCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class EventCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CloneOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Event');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/event');
        $this->crud->setEntityNameStrings('event', 'events');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();
        $this->crud->addColumn([
            'name' => 'id',
            'label' => "ID",
            'type' => 'text',

        ]);

        $this->crud->addColumn([
            'name' => 'active',
            'label' => "Active",
            'type' => 'boolean',

        ]);


        $this->crud->addColumn([
            'name' => 'enabled',
            'label' => "Enabled",
            'type' => 'boolean',

        ]);

        $this->crud->addColumn([
            'name' => 'name',
            'label' => "Name",
            'type' => 'text',

        ]);

        $this->crud->addColumn([
            'name' => 'subtitle',
            'label' => "Subtitle",
            'type' => 'text',

        ]);



        $this->crud->addColumn([
            'name' => 'starts_at_day',
            'label' => "Starts",
            'type' => 'date',

        ]);

        $this->crud->addColumn([
            'name' => 'starts_at_time',
            'label' => "At",
            'type' => 'time',

        ]);

        $this->crud->addColumn([
            'name' => 'duration',
            'label' => "Duration",
            'type' => 'number',

        ]);



        $this->crud->addColumn([  // Select2
            'label' => "Host",
            'type' => 'select',
            'name' => 'user_id', // the db column for the foreign key
            'entity' => 'user', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            // optional
            'model' => "App\Models\BackpackUser", // foreign key model

        ]);

        $this->crud->addColumn([    // Select2Multiple = n-n relationship (with pivot table)
            'label' => "Complexity",
            'type' => 'select',
            'name' => 'complexity_id', // the method that defines the relationship in your Model
            'entity' => 'complexity', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            // optional
            'model' => "App\Models\Complexity", // foreign key model

        ]);

        $this->crud->addColumn([
            'name' => 'is_premium',
            'label' => 'Is Premium',
            'type' => 'boolean',
        ]);

        $this->crud->addColumn([
            'name' => 'is_class',
            'label' => "Is a class",
            'type' => 'boolean',
            'tab' => 'info'
        ]);

        $this->crud->addColumn([
            'name' => 'url',
            'type'     => 'closure',
            'function' => function ($entry) {
                $langs = array_keys(Config('backpack.crud.locales'));
                $return = '<table>';
                foreach ($langs as $lang) {
                    $return .= '<tr><td>' . $lang . '</td><td>' . parse_url($entry->makeUrl($lang))['path'] . '</td></tr>';
                }
                $return .= '</table>';
                return $return;
            }
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(EventRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();
        $this->crud->addField([
            'name' => 'active',
            'label' => "Active",
            'type' => 'checkbox',
            'tab' => 'info'
        ]);

        $this->crud->addField([
            'name' => 'enabled',
            'label' => "Enabled",
            'type' => 'checkbox',
            'tab' => 'info',
            'hint' => 'For recommendations'
        ]);

        $this->crud->addField([
            'name' => 'name',
            'label' => "Name",
            'type' => 'text',
            'tab' => 'info'
        ]);

        $this->crud->addField([
            'name' => 'url',
            'label' => "Url",
            'type' => 'text',
            'tab' => 'info'
        ]);

        $this->crud->addField([
            'name' => 'subtitle',
            'label' => "Subtitle",
            'type' => 'text',
            'tab' => 'info'
        ]);

        $this->crud->addField([
            'name' => 'subtitle_old',
            'label' => "Subtitle OLD",
            'type' => 'text',
            'tab' => 'info'
        ]);

        $this->crud->addField([   // repeatable
            'name'  => 'custom_blocks',
            'label' => 'Custom blocks',
            'type'  => 'repeatable',
            'fields' => [
                [
                    'name'    => 'title',
                    'type'    => 'text',
                    'label'   => 'Title',
                    'wrapper' => ['class' => 'form-group col-md-12'],
                ],
                [
                    'name'  => 'content',
                    'type'  => 'ckeditor',
                    'label' => 'Content',
                ],
            ],
            'tab' => 'info',

            // optional
            'new_item_label'  => 'Add block', // customize the text of the button
        ],);

        $this->crud->addField([   // repeatable
            'name'  => 'custom_icons',
            'label' => 'Custom icons',
            'type'  => 'repeatable',
            'fields' => [
                [
                    'name'    => 'image',
                    'type'    => 'browse',
                    'label'   => 'Image',

                ],
                [
                    'name'    => 'title',
                    'type'    => 'text',
                    'label'   => 'Title',

                ],
                [
                    'name'  => 'content',
                    'type'  => 'ckeditor',
                    'label' => 'Content',
                ],
            ],
            'tab' => 'info',

            // optional
            'new_item_label'  => 'Add icon', // customize the text of the button
        ],);



        $this->crud->addField([
            'name' => 'description_short',
            'label' => "Description short",
            'type' => 'textarea',
            'tab' => 'info'
        ]);

        $this->crud->addField([
            'name' => 'description',
            'label' => "Description",
            'type' => 'wysiwyg',
            'tab' => 'info'
        ]);

        $this->crud->addField([
            'name' => 'confirmation_notes',
            'label' => "Confirmation Notes",
            'type' => 'wysiwyg',
            'tab' => 'info'
        ]);

        $this->crud->addField([
            'name' => 'final_confirmation_notes',
            'label' => "Final Confirmation Notes",
            'type' => 'wysiwyg',
            'tab' => 'info'
        ]);


        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
            'label' => "Complexity",
            'type' => 'select2',
            'name' => 'complexity_id', // the method that defines the relationship in your Model
            'entity' => 'complexity', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            // optional
            'model' => "App\Models\Complexity", // foreign key model
            'options' => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }),
            'tab' => 'info'
        ]);

        $this->crud->addField([
            'name' => 'starts_at_day',
            'label' => "Starts",
            'type' => 'date',
            'tab' => 'info'
        ]);

        $this->crud->addField([
            'name' => 'starts_at_time',
            'label' => "At",
            'type' => 'time',
            'tab' => 'info'
        ]);

        $this->crud->addField([
            'name' => 'duration',
            'label' => "Duration",
            'type' => 'number',
            'tab' => 'info'
        ]);

        $this->crud->addField([
            'tab' => 'Images',
            'name' => 'images', // db column name
            'label' => 'Photos', // field caption
            'type' => 'dropzone', // voodoo magic
            'prefix' => '/', // upload folder (should match the driver specified in the upload handler defined below)
            'upload-url' => '/' . config('backpack.base.route_prefix') . '/media-dropzone', // POST route to handle the individual file uploads
        ]);


        $this->crud->addField([  // Select2
            'label' => "Host",
            'type' => 'select2',
            'name' => 'user_id', // the db column for the foreign key
            'entity' => 'user', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            // optional
            'model' => "App\Models\BackpackUser", // foreign key model
            'default' => 2, // set the default value of the select2
            'options' => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select
            'tab' => 'info'
        ]);


        $this->crud->addField([
            'name' => 'enrolment_link',
            'label' => "Enrolment link",
            'type' => 'text',
            'tab' => 'info'
        ]);

        $this->crud->addField([
            'name' => 'is_class',
            'label' => "Is a class",
            'type' => 'checkbox',
            'tab' => 'info'
        ]);


        $this->crud->addField([
            'name' => 'time_conversion_link',
            'label' => "Time conversion link",
            'type' => 'text',
            'tab' => 'info'
        ]);

        $this->crud->addField([
            'name' => 'price',
            'label' => "Price",
            'type' => 'number',
            'tab' => 'info'
        ]);

        $this->crud->addField([
            'name' => 'is_free',
            'label' => "Is Free",
            'type' => 'checkbox',
            'tab' => 'info'
        ]);

        $this->crud->addField([
            'name' => 'is_free_until',
            'label' => "Is free until",
            'type' => 'date_picker',
            'tab' => 'info'
        ]);

        $this->crud->addField([
            'name' => 'is_premium',
            'label' => 'Is Premium',
            'type' => 'checkbox',
            'tab' => 'info'
        ]);

        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
            'label' => "Manufacturers",
            'name' => 'manufacturers',
            'type' => 'select2_multiple',
            'entity' => 'manufacturers', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'pivot'     => true,
            // optional
            'model' => "App\Models\Manufacturer", // foreign key model
            'options' => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }),
            'tab' => 'info'
        ]);

        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
            'label'     => "Products",
            'type'      => 'select2_multiple',
            'name'      => 'products', // the method that defines the relationship in your Model
            'entity'    => 'products', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
            // 'select_all' => true, // show Select All and Clear buttons?

            // optional
            'model'     => "App\Models\Product", // foreign key model
            'options'   => (function ($query) {
                return $query->whereEnabled(1)->orderBy('name', 'ASC')->get();
            }),
            'tab' => 'info'
        ]);

        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
            'label' => "Consortium",
            'name' => 'consortiums', // the method that defines the relationship in your Model
            'type' => 'select2_multiple',
            'entity' => 'consortiums', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'pivot'     => true,
            // optional
            'model' => "App\Models\Consortium", // foreign key model
            'options' => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }),
            'tab' => 'info'
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
