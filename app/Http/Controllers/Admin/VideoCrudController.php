<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\VideoRequest;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Str;

/**
 * Class VideoCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class VideoCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CloneOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ReorderOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Video');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/video');
        $this->crud->setEntityNameStrings('video', 'videos');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();
        $this->crud->addColumn([
            'name' => 'active',
            'label' => "Active",
            'type' => 'boolean',
        ]);

        $this->crud->addColumn([
            'name' => 'enabled',
            'label' => "Enabled",
            'type' => 'boolean',

        ]);


        $this->crud->addColumn(
            [
                'name' => 'id',
                'type' => 'text',
                'label' => 'ID'
            ]
        );
        $this->crud->addColumn([
            'name' => 'id',
            'label' => "ID",
            'type' => 'text',
        ]);

        $this->crud->addColumn([
            'name' => 'name',
            'label' => "Name",
            'type' => 'text',
        ]);

        $this->crud->addColumn([    // Select2Multiple = n-n relationship (with pivot table)
            'label' => "Manufacturers",
            'name' => 'manufacturers',
            'type' => 'select_multiple',
            'entity' => 'manufacturers', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            // optional
            'model' => "App\Models\Manufacturer", // foreign key model

            'limit' => 400
        ]);

        $this->crud->addColumn([    // Select2Multiple = n-n relationship (with pivot table)
            'label' => "Consortium",
            'name' => 'consortiums', // the method that defines the relationship in your Model
            'type' => 'select_multiple',
            'entity' => 'consortiums', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            // optional
            'model' => "App\Models\Consortium", // foreign key model

            'limit' => 400
        ]);




        $this->crud->addColumn([    // Select2Multiple = n-n relationship (with pivot table)
            'label'     => "Categories",
            'type'      => 'select_multiple',
            'name'      => 'categories', // the method that defines the relationship in your Model
            'entity'    => 'categories', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
            // 'select_all' => true, // show Select All and Clear buttons?

            // optional
            'model'     => "App\Models\Category", // foreign key model
            'options'   => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            })
        ]);

        $this->crud->addColumn([
            'name' => 'file',
            'label' => "Video",
            'type' => 'text',
        ]);

        $this->crud->addColumn([
            'name' => 'reviews_count',
            'label' => "Reviews",
            'type' => 'text',

        ]);

        $this->crud->addColumn([
            'name' => 'reviews_avg',
            'label' => "Score",
            'type' => 'text',

        ]);


        $this->crud->addColumn([
            'name' => 'is_premium',
            'label' => 'Is Premium',
            'type' => 'boolean',

        ]);

        $this->crud->addColumn([    // Select2Multiple = n-n relationship (with pivot table)
            'label'     => "Video category",
            'type'      => 'select',
            'name'      => 'videocategory_id', // the method that defines the relationship in your Model
            'entity'    => 'videocategory', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'tab' => 'Old',
            #'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
            // 'select_all' => true, // show Select All and Clear buttons?

            // optional
            'model'     => "App\Models\Videocategory", // foreign key model

        ]);

        $this->crud->addColumn([
            'name' => 'url',
            'type'     => 'closure',
            'function' => function ($entry) {
                $langs = array_keys(Config('backpack.crud.locales'));
                $return = '<table>';
                foreach ($langs as $lang) {
                    $return .= '<tr><td>' . $lang . '</td><td>' . parse_url($entry->makeUrl($lang))['path'] . '</td></tr>';
                }
                $return .= '</table>';
                return $return;
            }
        ]);


    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(VideoRequest::class);

        $locale = (isset($this->crud->getRequest()->all()['locale'])) ? $this->crud->getRequest()->all()['locale'] : 'en';

        $labels = [
            'it' => [
                'is_legacy' => 'E\' un articolo di tipo vecchio ? ',
                'enabled' => 'Pubblicato',
                'name' => 'Titolo dell\'articolo',
                'subtitle' => 'Descrizione di tipo meta',
                'url' => 'Indirizzo da inserire nella barra di navigazione',
                'brief' => 'Versione breve dell\'articolo',
                'content' => 'Contenuto dell\'articolo',
                'published_at' => 'Pubblicato in data',
                'user_id' => 'Scrittore di questo articolo',
                'tags' => 'Etichette applicate a questo articolo',
                'categories' => 'Categorie di questo articolo',
                'articlecategory_id' => 'Categoria degli articolo a cui appartiene questo articolo',
                'photos' => 'Immagini',
                'products' => 'Prodotti raccomandati in questo articolo',
                'price' => 'Prezzo',
                'is_free' => 'La fruizione dell\'articolo è gratuita',
                'is_free_until' => 'E\' gratuita fino a quando?',
                'manufacturers' => 'Produttori raccomandati in questo articolo',
                'consortiums' => 'Consorzi raccomandati in questo articolo',
                'podcasts' => 'Podcastt raccomndati in questo articolo',
                'articlecategories' => 'Categoria degli articolo a cui appartiene questo articolo',
                'image' => 'Immagine',
                'image_alt' => 'Contenuto dell\'attributo alt dell\'immagine',
                'image_link' => 'Link per l\'immagine',
                'cta' => 'Chiamata all\'azione',
                'body' => 'Blocchi',
                'is_premium' => 'E\' un articolo di tipo premium?',

            ],
            'en' => [
                'is_legacy' => 'Is legacy',
                'enabled' => 'Published',
                'name' => 'Title',
                'subtitle' => 'Meta description',
                'url' => 'URL',
                'brief' => 'Brief',
                'content' => 'Content',
                'published_at' => 'Published at',
                'user_id' => 'Editor',
                'tags' => 'Tags',
                'categories' => 'Categories',
                'articlecategory_id' => 'Article category',
                'photos' => 'Images',
                'products' => 'Products',
                'price' => 'Price',
                'is_free' => 'Is free',
                'is_free_until' => 'Is free until',
                'manufacturers' => 'Manufacturers',
                'consortiums' => 'Consortiums',
                'podcasts' => 'Podcasts',
                'articlecategories' => 'Article categories',
                'image' => 'Images',
                'image_alt' => 'Alt tag',
                'image_link' => 'Image link',
                'cta' => 'Call to action',
                'body' => 'Body blocks',
                'is_premium' => 'Is premium'
            ]
        ];

        $l = $labels[$locale];

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();


        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();

        $this->crud->addField([
            'name' => 'active',
            'label' => "Active",
            'type' => 'checkbox',
            'tab' => 'Old'
        ]);


        $this->crud->addField([
            'name' => 'enabled',
            'label' => "Enabled",
            'type' => 'checkbox',
            'tab' => 'Old',
        ]);


        $this->crud->addField([
            'name' => 'name',
            'label' => "Name",
            'type' => 'text',
            'tab' => 'Old'
        ]);

        $this->crud->addField([
            'name' => 'url',
            'type' => 'text',
            'label' => 'Url',
            'tab' => 'Old'
        ]);

        $this->crud->addField([
            'name' => 'subtitle',
            'label' => "Subtitle",
            'type' => 'text',
            'tab' => 'Old'
        ]);
        $this->crud->addField([
            'name' => 'description',
            'label' => "Description",
            'type' => 'wysiwyg',
            'tab' => 'Old'
        ]);

        $this->crud->addField([
            'name' => 'duration',
            'label' => "Duration",
            'type' => 'number',
            'tab' => 'Old'
        ]);
        /*
        $this->crud->addField([
            'label' => "Cover",
            'name' => "cover",
            'type' => 'image',
            'upload' => true,
            'tab' => 'media',
            'crop' => true, // set to true to allow cropping, false to disable
            //'aspect_ratio' => 1, // ommit or set to 0 to allow any aspect ratio
            // 'disk' => 's3_bucket', // in case you need to show images from a different disk
            // 'prefix' => 'uploads/images/profile_pictures/' // in case your db value is only the file name (no path), you can use this to prepend your path to the image src (in HTML), before it's shown to the user;
        ]);
*/

        $this->crud->addField([
            'label' => "Cover",
            'name' => "cover",
            'type' => 'browse',
            'tab' => 'media',
        ]);


        $this->crud->addField([
            'label' => "Homepage Cover",
            'name' => "homepage_cover",
            'type' => 'browse',
            'tab' => 'media',
        ]);





        $this->crud->addField([   // Upload
            'name' => 'file',
            'label' => 'Video',
            'type' => 'browse',
            'tab' => 'media',
            //'disk' => 'uploads', // if you store files in the /public folder, please ommit this; if you store them in /storage or S3, please specify it;
            // optional:
            //temporary' => 10 // if using a service, such as S3, that requires you to make temporary URL's this will make a URL that is valid for the number of minutes specified
        ]);

        $this->crud->addField([   // Upload
            'name' => 'file_teaser',
            'label' => 'Video Teaser',
            'type' => 'browse',
            'tab' => 'media',
            //'disk' => 'uploads', // if you store files in the /public folder, please ommit this; if you store them in /storage or S3, please specify it;
            // optional:
            //temporary' => 10 // if using a service, such as S3, that requires you to make temporary URL's this will make a URL that is valid for the number of minutes specified
        ]);


        $this->crud->addField([
            'name' => 'embed_code',
            'label' => "Embeddable code",
            'type' => 'textarea',
            'tab' => 'media',
        ]);

        $this->crud->addField([
            'name' => 'yt_video_id',
            'label' => "YouTube video ID",
            'type' => 'text',
            'tab' => 'media'
        ]);


        $this->crud->addField([
            'label' => "User",
            'type' => 'select2',
            'name' => 'user_id', // the db column for the foreign key
            'entity' => 'user', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            // optional
            'model' => "App\User", // foreign key model
            'default' => '',
            'tab' => 'Old'
            /* 'options'   => (function ($query) {
                 return $query->orderBy('name', 'ASC')->where('depth', 1)->get();
             }) */
        ]);

        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
            'label'     => "Video category",
            'type'      => 'select2',
            'name'      => 'videocategory_id', // the method that defines the relationship in your Model
            'entity'    => 'videocategory', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'tab' => 'Old',
            #'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
            // 'select_all' => true, // show Select All and Clear buttons?

            // optional
            'model'     => "App\Models\Videocategory", // foreign key model

        ]);

        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
            'label'     => "Categories",
            'type'      => 'select2_multiple',
            'name'      => 'categories', // the method that defines the relationship in your Model
            'entity'    => 'categories', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
            // 'select_all' => true, // show Select All and Clear buttons?

            // optional
            'model'     => "App\Models\Category", // foreign key model
            'options'   => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }),
            'tab' => 'Old'
        ]);

        $this->crud->addField([
            'label' => 'Podcasts',
            'type'      => 'select2_multiple',
            'name'      => 'podcasts', // the method that defines the relationship in your Model
            'entity'    => 'podcasts', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
            // 'select_all' => true, // show Select All and Clear buttons?

            // optional
            'model'     => "App\Models\Podcast", // foreign key model

            'tab' => 'media',
        ]);

        $this->crud->addField([
            'name' => 'is_premium',
            'label' => 'Is Premium',
            'type' => 'checkbox',
            'tab' => 'Old'
        ]);


        $this->crud->addField([
            'name' => 'price',
            'label' => "Price",
            'type' => 'number',
            'tab' => 'Old'
        ]);

        $this->crud->addField([
            'name' => 'is_free',
            'label' => "Is Free",
            'type' => 'checkbox',
            'tab' => 'Old'
        ]);

        $this->crud->addField([
            'name' => 'is_free_until',
            'label' => "Is free until",
            'type' => 'date_picker',
            'tab' => 'Old'
        ]);


        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
            'label'     => "Products",
            'type'      => 'select2_multiple',
            'name'      => 'products', // the method that defines the relationship in your Model
            'entity'    => 'products', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user

            'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
            // 'select_all' => true, // show Select All and Clear buttons?

            // optional
            'model'     => "App\Models\Product", // foreign key model
            'options'   => (function ($query) {
                return $query->whereEnabled(1)->orderBy('name', 'ASC')->get();
            }),
            'tab' => 'Old'
        ]);

        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
            'label' => "Manufacturers",
            'name' => 'manufacturers',
            'type' => 'select2_multiple',
            'entity' => 'manufacturers', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'pivot'     => true,
            // optional
            'model' => "App\Models\Manufacturer", // foreign key model
            'options' => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }),
            'tab' => 'Old'
        ]);

        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
            'label' => "Consortium",
            'name' => 'consortiums', // the method that defines the relationship in your Model
            'type' => 'select2_multiple',
            'entity' => 'consortiums', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'pivot'     => true,
            // optional
            'model' => "App\Models\Consortium", // foreign key model
            'options' => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }),
            'tab' => 'Old'
        ]);

        $this->crud->addField([
            'name' => 'is_legacy',
            'label' => 'Is legacy',
            'type' => 'boolean',

        ]);

        $this->crud->addField([
            'name'  => 'body',
            'label'  => $l['body'],

            'type'  => 'repeatable',
            'fields' => [
                [
                    'name'    => 'content',
                    'label'    => $l['content'],
                    'type'    => 'wysiwyg',
                    'extra_plugins' => ['html5audio']

                ],
                [

                    'name' => "image",
                    'label' => $l["image"],
                    'type' => 'image',
                    'crop' => true,
                ],
                [
                    'label' => $l['image_alt'],
                    'name' => 'image_alt',

                    'type' => 'text'
                ],
                [
                    'name' => 'cta',
                    'label' => $l['cta'],

                    'type' => 'select_from_array',
                    'options' => ['newsletter_subscription' => 'Newsletter Subscription']
                ],
                [
                    'label' => $l['image_link'],
                    'name' => 'image_link',
                    'type' => 'text'
                ],
            ],

            // optional
            'new_item_label'  => 'Add Block', // customize the text of the button
            'init_rows' => 0, // number of empty rows to be initialized, by default 1
            #'min_rows' => 2, // minimum rows allowed, when reached the "delete" buttons will be hidden
            #'max_rows' => 2, // max
            'tab' => 'New'
        ]);

    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }


    protected function setupReorderOperation()
    {
        // define which model attribute will be shown on draggable elements
        $this->crud->set('reorder.label', 'name');
        // define how deep the admin is allowed to nest the items
        // for infinite levels, set it to 0
        $this->crud->set('reorder.max_level', 5);
    }
}
