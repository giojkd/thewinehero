<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\TrainingRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\Http\Requests\DropzoneRequest;
use Str;


/**
* Class TrainingCrudController
* @package App\Http\Controllers\Admin
* @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
*/
class TrainingCrudController extends CrudController
{
  use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
  use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation { update as traitUpdate; }


  public function update()
  {
    // do something before validation, before save, before everything; for example:
    // $this->crud->request->request->add(['author_id'=> backpack_user()->id]);
    // $this->crud->addField(['type' => 'hidden', 'name' => 'author_id']);
    // $this->crud->request->request->remove('password_confirmation');
    // $this->crud->removeField('password_confirmation');


    $this->crud->update(\Request::get($this->crud->model->getKeyName()), ['photos' => '[]']);


    $response = $this->traitUpdate();
    // do something after save
    return $response;
  }

  public function setup()
  {
    $this->crud->setModel('App\Models\Training');
    $this->crud->setRoute(config('backpack.base.route_prefix') . '/training');
    $this->crud->setEntityNameStrings('training', 'trainings');
  }

  protected function setupListOperation()
  {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();
        $this->crud->addColumn(
            [
                'name' => 'id',
                'type' => 'text',
                'label' => 'ID'
            ]
        );
    $this->crud->addColumn([  // Select2
      'label' => "Writer",
      'type' => 'select',
      'name' => 'user_id', // the db column for the foreign key
      'entity' => 'user', // the method that defines the relationship in your Model
      'attribute' => 'name', // foreign key attribute that is shown to user

      // optional
      'model' => "App\Models\BackpackUser", // foreign key model
    ]);

    $this->crud->addColumn([
      'name' => 'name',
      'label' => "Name",
      'type' => 'text',

    ]);

    $this->crud->addColumn([
      'name' => 'subtitle',
      'label' => "Subtitle",
      'type' => 'text',

    ]);

        $this->crud->addColumn([
            'name' => 'is_premium',
            'label' => 'Is Premium',
            'type' => 'boolean',
            'tab' => 'info'
        ]);
  }

  protected function setupCreateOperation()
  {
    $this->crud->setValidation(TrainingRequest::class);

    // TODO: remove setFromDb() and manually define Fields
    #$this->crud->setFromDb();

    $this->crud->addField([  // Select2
      'label' => "Professor",
      'type' => 'select2',
      'name' => 'user_id', // the db column for the foreign key
      'entity' => 'user', // the method that defines the relationship in your Model
      'attribute' => 'name', // foreign key attribute that is shown to user

      // optional
      'model' => "App\Models\BackpackUser", // foreign key model
      'default' => 2, // set the default value of the select2
      'options' => (function ($query) {
        return $query->orderBy('name', 'ASC')->get();
      }), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select
      'tab' => 'info'
    ]);

    $this->crud->addField([
      'name' => 'name',
      'label' => "Name",
      'type' => 'text',
      'tab' => 'info'
    ]);
    $this->crud->addField([
      'name' => 'subtitle',
      'label' => "Subtitle",
      'type' => 'text',
      'tab' => 'info'
    ]);
    $this->crud->addField([
      'name' => 'description',
      'label' => "Description",
      'type' => 'wysiwyg',
      'tab' => 'info'
    ]);

    /*
    $this->crud->addField([
      'name' => 'what_you_will_learn',
      'label' => "What you will learn",
      'type' => 'wysiwyg',
      'tab' => 'info'
    ]);
    */


    $this->crud->addField([   // Table
      'name' => 'what_you_will_learn',
      'label' => 'What you will learn',
      'type' => 'table',
      'entity_singular' => 'option', // used on the "Add X" button
      'columns' => [
        'title' => 'Title',
        'description' => 'Description',
        #'price' => 'Price'
      ],
      #'max' => 10, // maximum rows allowed in the table
      'min' => 0, // minimum rows allowed in the table
          'tab' => 'info'
    ]);


    $this->crud->addField([
      'name' => 'requirements',
      'label' => "Requirements",
      'type' => 'wysiwyg',
      'tab' => 'info'
    ]);


    $this->crud->addField([
      'name' => 'welcome_message',
      'label' => "Welcome message",
      'type' => 'wysiwyg',
      'tab' => 'Messages'
    ]);
    $this->crud->addField([
      'name' => 'congratulations_message',
      'label' => "Congratulations message",
      'type' => 'wysiwyg',
      'tab' => 'Messages'
    ]);


    $this->crud->addField([
      'name' => 'price',
      'label' => "Price",
      'type' => 'number',
      'tab' => 'Price'
    ]);

    $this->crud->addField([
      'name' => 'compare_price_with',
      'label' => "Compaer price with",
      'type' => 'number',
      'tab' => 'Price'
    ]);

    $this->crud->addField([
      'tab' => 'Images',
      'name' => 'photos', // db column name
      'label' => 'Photos', // field caption
      'type' => 'dropzone', // voodoo magic
      'prefix' => '/', // upload folder (should match the driver specified in the upload handler defined below)
      'upload-url' => '/' . config('backpack.base.route_prefix') . '/media-dropzone', // POST route to handle the individual file uploads
    ]);


        $this->crud->addField([
            'name' => 'price',
            'label' => "Price",
            'type' => 'number',

        ]);

        $this->crud->addField([
            'name' => 'is_free',
            'label' => "Is Free",
            'type' => 'checkbox',

        ]);

        $this->crud->addField([
            'name' => 'is_free_until',
            'label' => "Is free until",
            'type' => 'date_picker',

        ]);

        $this->crud->addField([
            'name' => 'is_premium',
            'label' => 'Is Premium',
            'type' => 'checkbox',

        ]);


  }

  protected function setupUpdateOperation()
  {

    $this->setupCreateOperation();
  }

  public function handleDropzoneUpload(DropzoneRequest $request)
  {
    $disk = "custom_public"; //
    $destination_path = "";
    $destination_path_filename = "storage";
    $file = $request->file('file');

    $fileInfo = pathinfo($file->getClientOriginalName());
    $filename = $fileInfo['filename'];
    $extension = $fileInfo['extension'];

    try
    {
      $image = \Image::make($file);
      $filename = $filename.'_'. rand(0,1000).time().'.'. $extension;
      \Storage::disk($disk)->put($destination_path.'/'.$filename, $image->stream());
      return response()->json(['success' => true, 'filename' => $destination_path_filename . '/' . $filename]);
    }
    catch (\Exception $e)
    {
      dd($e);
      if (empty ($image)) {
        return response('Not a valid image type', 412);
      } else {
        return response('Unknown error', 412);
      }
    }
  }
}
