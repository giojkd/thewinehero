<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

#use Request;
use App\Models\Article;
use App\Models\BackpackUser;
use App\Models\Event;
use App\Models\Ingredient;
use App\Models\Product;
use App\Models\Manufacturer;
use App\Models\Recipe;
use App\Models\Training;
use App\Models\Trainingmodule;
use App\Models\Question;
use App\Models\Lead;
use App\Models\Deal;
use App\Models\Enrollmentoption;
use App\Models\Enrollmentoptiongroup;
use App\Models\Video;
use App\Models\Consortium;
use App\Models\Favorite;
use App\Models\Country;
use App\Models\Purchase;
use App\Models\Pagetype;
use App\Models\Plan;
use App\Models\Recommend;
use Illuminate\Support\Carbon;
use Stripe;
use Backpack\Settings\app\Models\Setting;
use Illuminate\Support\Facades\Cookie;
use Jorenvh\Share\Share;
use Illuminate\Support\Facades\Validator;




use App\Models\Quizz;
use App;
use App\Console\Commands\ExportCommand;
use App\Models\Articlecategory;
use App\Models\Banner;
use App\Models\Cms;
use App\Models\Dealsubscription;
use App\Models\Dishcourse;
use App\Models\Experience;
use App\Models\Guest;
use App\Models\Landingpage;
use App\Models\Linkclick;
use App\Models\MailgunWebhook;
use App\Models\Producttype;
use App\Models\Videocategory;
use Artisan;
use Auth;
use Storage;
use DB;
use DOMDocument;
use Exception;
use Jenssegers\Agent\Agent;

use URL;
use Str;
use Route;

class FrontController extends Controller
{
    var $defaultLocale;
    var $viewData;
    var $lead;
    var $session;

    public function __construct(Request $request)
    {

        $agent = new Agent();

        $this->defaultLocale = 'en';
        $this->viewData = [];

        $this->viewData['currentPageUrl'] = 'https://www.mamablip.com/'.request()->path();#URL::current();
        $this->viewData['canPurchase'] = false;
        $this->viewData['enrollmentoptions'] = Enrollmentoption::get();
        $this->viewData['enrollmentoptiongroups'] = Enrollmentoptiongroup::with(['enrollmentoption', 'enrollmentoptions'])->get();
        $this->viewData['agent'] = $agent;
        $this->viewData['sponsors'] = [];
        $this->viewData['social_share'] = [];
        $this->viewData['isMobile'] = $agent->isMobile();
        $this->viewData['producttypes'] = Producttype::orderBy('name','ASC')->get();

    }

    public function isoDate($date){
        return date('Y-m-d', strtotime($date)) . 'T' . date('h:i:s', strtotime($date)) . '+01:00';
    }


    public function getPagetypeMeta(&$data){

        $pageType = Pagetype::where('slug', Route::currentRouteName())->first();

        if (!is_null($pageType)) {
            $data['meta'] = [
                'title' => $pageType-> title . ' | MaMaBlip',
                'description' => $pageType->description,
                'cover' => $pageType->cover,
                'og_type' => $pageType->og_type,
                'canonical' => $this->viewData['currentPageUrl']
            ];
        } else {
            $pageTypeDefault = Pagetype::where('slug', 'default')->first();
            $data['meta'] = [
                'title' => $pageTypeDefault->title.' | MaMaBlip',
                'description' => $pageTypeDefault->description,
                'cover' => $pageTypeDefault->cover,
                'og_type' => $pageTypeDefault->og_type,
                'canonical' => $this->viewData['currentPageUrl']
            ];
        }

        $data['guest'] = $this->getGuest();


        if (isset(request()->ipinfo) && request()->ipinfo->country_name != null && request()->ipinfo->country_name == 'Italy' && App::getLocale() == 'en') {
            $data['showLangSwitchBlock'] = ['url' => url('it')];
        }

        $data['availableCountries'] = Country::whereActive(1)->orderBy('name')->get();



        $data['banners'] = Banner::all()->filter(function ($banner, $key) {
            $links = collect($banner->show_only_in);


            if($links->count() > 0){
                if(in_array(URL::current(),$links->pluck('link')->toArray())){
                    return true;
                }else{
                    return false;
                }
            }else{
                return true;
            }
        })->keyBy('slug');


    }

    public function confirmPurchaseInvoice(Request $request){

        $this->getPagetypeMeta($this->viewData);

        $request->validate([
            'id' => 'required',
            'billing_address.email' => 'email|required',
            'billing_address.name' => 'required',
            'billing_address.surname' => 'required',
            'billing_address.telephone' => 'required',
            'billing_address.address' => 'required',
            'billing_address.vat_number' => 'required',
            'billing_address.company_name' => 'required',
        ]);
        $purchase = Purchase::whereNull('billing_address')->findOrFail($request->id);

        $purchase->billing_address = $request->billing_address;
        #$lead->save();

        return view('front.thank-you-page-invoice',$this->viewData);
    }

    public function requestPurchaseInvoice(Request $request,$locale,$id){

        $this->getPagetypeMeta($this->viewData);

        $purchase = Purchase::findOrFail($id);
        $billingAddress = [
            'email' => '',
            'name' => '',
            'surname' => '',
            'telephone' => '',
            'address' => '',
            'vat_number' => '',
            'company_name' => '',
        ];
        $this->viewData['meta'] = [
            'title' => __('all.request purchase invoice title'),
            'description' => __('all.request purchase invoice  description'),
            'canonical' => $this->viewData['currentPageUrl']
            #'cover' => $video->cover,
        ];
        $this->viewData['purchase_id'] = $id;
        $this->viewData['billing_address'] = $billingAddress;
        $lead = $this->getLead();
        $this->viewData['lead'] = $lead;
        return view('front.request-purchase-invoice',$this->viewData);

    }

    public function requestInvoice($locale, $id)
    {

        $this->getPagetypeMeta($this->viewData);

        $billingAddress = [
            'email' => '',
            'name' => '',
            'surname' => '',
            'telephone' => '',
            'address' => '',
            'vat_number' => '',
            'company_name' => '',
        ];
        $this->viewData['meta'] = [
            'title' => __('all.request ecommerce invoice title'),
            'description' => __('all.request ecommerce invoice  description'),
            'canonical' => $this->viewData['currentPageUrl']
            #'cover' => $video->cover,
        ];
        $this->viewData['lead_id'] = $id;
        $this->viewData['billing_address'] = $billingAddress;
        $lead = $this->getLead();
        $this->viewData['lead'] = $lead;
        return view('front.request-invoice', $this->viewData);
    }

    public function confirmInvoice(Request $request)
    {

        #dd($request->all());

        $this->getPagetypeMeta($this->viewData);

        $request->validate([
            'id' => 'required',
            'billing_address.email' => 'email|required',
            'billing_address.name' => 'required',
            'billing_address.surname' => 'required',
            'billing_address.telephone' => 'required',
            'billing_address.address' => 'required',
            'billing_address.vat_number' => 'required',
            'billing_address.company_name' => 'required',
        ]);
        $this->viewData['meta'] = [
            'title' => __('all.confirm ecommerce invoice title'),
            'description' => __('all.confirm ecommerce invoice  description'),
            'canonical' => $this->viewData['currentPageUrl']
            #'cover' => $video->cover,
        ];
        $lead = Lead::whereNull('billing_address')->findOrFail($request->id);
        $lead->billing_address = $request->billing_address;
        $lead->save();

        $this->viewData['lead'] = $lead;

        return view('front.thank-you-page-invoice',$this->viewData);
    }

    public function pages($locale, $name, $id)
    {
        echo 'ok';
    }

    public function publishTranslations()
    {
        Artisan::call('translations:export', ['group' => 'all']);
    }


    public function getGuest(){

        $request = request();


        if(Cookie::has('guest_id')){
            $guest = Guest::find(Cookie::get('guest_id'));
        }else{
            $guest = Guest::create(['country_id' => 840]);
            Cookie::queue('guest_id', $guest->id, 525600);
        }

        if ($request->has('disableLangSwitch') && $request->disableLangSwitch) {
            $guest->hide_lang_switch_block_until = date('Y-m-d',strtotime('+30days'));
            $guest->save();
        }

        return $guest;
    }

    public function getLead()
    {
        #dd(session()->all());
        $guest = $this->getGuest();
        if (session()->has('lead_id')) {
            $lead_id = session('lead_id');
            $lead = Lead::find($lead_id);
            if (is_null($lead)) {
                echo 'is null';
                $lead = Lead::create(['leadstatus_id' => 1, 'guest_id' => $guest->id]);
                session(['lead_id' => $lead->id]);
            }else{
                if(is_null($lead->guest_id)){
                    $lead->guest_id = $guest->id;
                    $lead->save();
                }
            }
        } else {
            $lead = Lead::create(['leadstatus_id' => 1, 'guest_id' => $guest->id]);
            session(['lead_id' => $lead->id]);
        }

        return $lead;
    }

    public function thankYouPageEcommerceCart(Request $request)
    {

        $this->getPagetypeMeta($this->viewData);

        $request->validate([
            'id' => 'required'
        ]);
        $this->viewData['meta'] = [
            'title' => __('all.thank you page ecommerce title'),
            'description' => __('all.thank you page ecommerce description'),
            'canonical' => $this->viewData['currentPageUrl']
            #'cover' => $video->cover,
        ];
        $this->viewData['invoice_url'] = Route('requestInvoice', ['id' => $request->id]);
        $lead = $this->getLead();
        $this->viewData['lead'] = $lead;
        return view('front.thank-you-page-commerce-cart', $this->viewData);
    }

    public function setCartField(Request $request)
    {

        $lead = Lead::find($request->lead_id);

        $field = $request->name;
        $value = $request->value;
        $address = $lead->shipping_address;
        $address[$field] = $value;
        $lead->shipping_address = $address;
        $lead->save();

        $refresh = 0;

        if ($field == 'address') {
            $lead->calculateShippingCost();
            $refresh = 1;
        }

        return ['status' => 1, 'refresh' => $refresh];
    }



    public function cart($locale)
    {

        $this->viewData['page_type'] = 'cart';

        #if (Auth::check()) {

            $this->getPagetypeMeta($this->viewData);

            $lead = $this->getLead();



            if(Auth::check()){
                $user = Auth::user();

                $lead->user_id = $user->id;
                $lead->save();

                $this->viewData['client_secret_intent'] = $user->createSetupIntent();
                $this->viewData['stripe_customer_id'] = $user->createOrGetStripeCustomer();
                $this->viewData['user'] = $user;
                $this->viewData['payment_methods'] = collect($user->paymentMethods());

                $shipping_address = [
                    'name' => (isset($lead->shipping_address['name'])) ? $lead->shipping_address['name'] : $user->name,
                    'surname' => (isset($lead->shipping_address['surname'])) ? $lead->shipping_address['surname'] : $user->surname,
                    'email' => (isset($lead->shipping_address['email'])) ? $lead->shipping_address['email'] : $user->email,
                    'email_confirmation' => (isset($lead->shipping_address['email_confirmation'])) ? $lead->shipping_address['email_confirmation'] : $user->email,
                    'doorbell' => (isset($lead->shipping_address['doorbell'])) ? $lead->shipping_address['doorbell'] : '',
                    'notes' => (isset($lead->shipping_address['notes'])) ? $lead->shipping_address['notes'] : '',
                    'telephone' => (isset($lead->shipping_address['telephone'])) ? $lead->shipping_address['telephone'] : $user->telephone,
                    'address' => (isset($lead->shipping_address['address'])) ? $lead->shipping_address['address'] : '',
                ];

                $billing_address = [
                    'name' => (isset($lead->billing_address['name'])) ? $lead->billing_address['name'] : $user->name,
                    'surname' => (isset($lead->billing_address['surname'])) ? $lead->billing_address['surname'] : $user->surname,
                    'email' => (isset($lead->billing_address['email'])) ? $lead->billing_address['email'] : $user->email,
                    'telephone' => (isset($lead->billing_address['telephone'])) ? $lead->billing_address['telephone'] : $user->telephone,
                    'address' => (isset($lead->billing_address['address'])) ? $lead->billing_address['address'] : '',
                ];

            }else{

                $user = new  BackpackUser();

                $this->viewData['client_secret_intent'] = $user->createSetupIntent();

                $shipping_address = [
                    'name' => (isset($lead->shipping_address['name'])) ? $lead->shipping_address['name'] : '',
                    'surname' => (isset($lead->shipping_address['surname'])) ? $lead->shipping_address['surname'] : '',
                    'email' => (isset($lead->shipping_address['email'])) ? $lead->shipping_address['email'] : '',
                    'email_confirmation' => (isset($lead->shipping_address['email_confirmation'])) ? $lead->shipping_address['email_confirmation'] : '',
                    'doorbell' => (isset($lead->shipping_address['doorbell'])) ? $lead->shipping_address['doorbell'] : '',
                    'notes' => (isset($lead->shipping_address['notes'])) ? $lead->shipping_address['notes'] : '',
                    'telephone' => (isset($lead->shipping_address['telephone'])) ? $lead->shipping_address['telephone'] : '',
                    'address' => (isset($lead->shipping_address['address'])) ? $lead->shipping_address['address'] : '',
                ];

                $billing_address = [
                    'name' => (isset($lead->billing_address['name'])) ? $lead->billing_address['name'] : '',
                    'surname' => (isset($lead->billing_address['surname'])) ? $lead->billing_address['surname'] : '',
                    'email' => (isset($lead->billing_address['email'])) ? $lead->billing_address['email'] : '',
                    'telephone' => (isset($lead->billing_address['telephone'])) ? $lead->billing_address['telephone'] : '',
                    'address' => (isset($lead->billing_address['address'])) ? $lead->billing_address['address'] : '',
                ];

            }




            $this->viewData['products'] = $lead->products;

            $this->viewData['lead'] = $lead;

            $this->viewData['guest'] = $this->getGuest();



            $this->viewData['shipping_address'] = $shipping_address;
            $this->viewData['billing_address'] = $billing_address;

            $this->viewData['meta'] = [
                'title' => __('all.cart checkout title'),
                'description' => __('all.cart checkout description'),
                'canonical' => $this->viewData['currentPageUrl']
                #'cover' => $video->cover,
            ];

            #charge front end in order to show in 3d secure the amount BEGIN

            if($lead->grand_total > 0){

                $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET'));
                $this->viewData['client_secret_intent'] = $stripe->paymentIntents->create([
                    'currency' => 'eur',
                    'amount' => (int)($lead->grand_total * 100),
                    'metadata' => ['order_id' => $lead->id],
                    'description' => 'Order ' . $lead->id,
                    'payment_method_types' => ['card'],
                ]);

            }

            #charge front end in order to show in 3d secure the amount END




            return view('front.cart', $this->viewData);
        #} else {
        #    redirect()->setIntendedUrl(url()->current());
        #    return redirect(Route('register'));
        #}
    }

    public function confirmCart(Request $request)
    {



        $user = Auth::user();
        $lead = $this->getLead();

#        dd($lead);

        $lead->dumps()->create([
            'content' => $request->all()
        ]);

        /*
        $validator = Validator::make($request->all(), [
            'shipping_address.email' => 'email|required',
            'shipping_address.name' => 'required',
            'shipping_address.surname' => 'required',
            'shipping_address.telephone' => 'required',
            'shipping_address.address' => 'required',
            'payment_method' => 'required',
            'address_json' => 'required'
        ]);

        #dd($validator->errors());

        if ($validator->fails()) {
            return back()
            ->withErrors($validator)
            ->withInput();
        }
        */




        /*

        $amount = $lead->grand_total;
        $user->addPaymentMethod($request->payment_method);

        try {

            $stripeCharge = $user->charge($amount * 100, $request->payment_method, ['off_session' => true]);

            if ($request->has('has_requested_invoice')) {
                $lead->has_requested_invoice = $request->has_requested_invoice;
            }
            $lead->shipping_address = $request->shipping_address;
            $lead->shipping_address_json = $request->address_json;
            $lead->confirmed_at = date('Y-m-d H:i:s');
            $lead->save();
        } catch (Exception $e) {
            return $e->getMessage();
        }
        */

        if ($request->has('has_requested_invoice')) {
            $lead->has_requested_invoice = $request->has_requested_invoice;
        }
        $lead->shipping_address = $request->shipping_address;
        $lead->shipping_address_json = $request->address_json;
        $lead->confirmed_at = date('Y-m-d H:i:s');
        $lead->save();



        session()->forget('lead_id'); #start a new lead

        return redirect(Route('thankYouPageEcommerceCart') . '?id=' . $lead->id);
    }

    public function addToCart(Request $request)
    {



        $guest = $this->getGuest();
        $lead = $this->getLead();

        $product = Product::findOrFail($request->product_id);



        $subTotal = $product->getPrice($guest->country_id, 'price',$request->quantity);



        $lead->products()->syncWithoutDetaching([
            $product->id =>
            [
                'quantity' => $request->quantity,
                'sub_total' => $subTotal
            ]
        ]);

        $lead->calculateTotals();

        return redirect(Route('cart'));
    }

    public function removeFromCart(Request $request){

        $request->validate([
            'lead_product_id' => 'required|numeric'
        ]);

        $lead = $this->getLead();

        $product = $lead->products()->wherePivot('id', $request->lead_product_id)->detach();

        $lead->calculateTotals();

        return back();


    }


    public function buySingleContentCheckout(Request $request, $locale, $model, $id)
    {

        #dd(func_get_args());

        $lead = $this->getLead();
        $this->viewData['lead'] = $lead;

        if (Auth::check()) {

            $this->getPagetypeMeta($this->viewData);

            $modelAux = '\App\Models\\' . Str::ucfirst($model);
            $content = $modelAux::find($id);

            $this->viewData['meta'] = [
                'title' => '',
                'description' => '',
                'cover' => '',
                'canonical' => $this->viewData['currentPageUrl']
            ];

            $user = Auth::user();
            $this->viewData['modelType'] = $model;
            $this->viewData['model'] = $content;
            $this->viewData['client_secret_intent'] = $user->createSetupIntent();
            $this->viewData['stripe_customer_id'] = $user->createOrGetStripeCustomer();
            $this->viewData['user'] = $user;
            $this->viewData['payment_methods'] = collect($user->paymentMethods());

            if ($user->hasDefaultPaymentMethod()) {
                $this->viewData['default_payment_method'] = $user->defaultPaymentMethod();
            }

            return view('front.buy-single-content-checkout', $this->viewData);

        } else {


            #flash where the user was in order to bring him back where he was after login or registration
            redirect()->setIntendedUrl(url()->current());

            return redirect(Route('register'));
        }
    }

    public function buySingleContent(Request $request)
    {

        $user = Auth::user();
        $stripe = Stripe::make(config('services.stripe')['secret']);
        #$customers = $stripe->customers()->all();

        $model = 'App\Models\\' . Str::ucfirst($request->model);
        $item = $model::find($request->model_id);

        $price = $item->applyPlanDiscount($request->model, $item->price); #apply plan discount

        if ($price > 0) {
            if (!$request->has('old_card')) {
                $stripe->paymentMethods()->attach($request->payment_method, $user->stripe_id);
            } else {
            }
            $user->updateDefaultPaymentMethod($request->payment_method);
        }





        $lead = $this->getLead();
        $this->viewData['lead'] = $lead;

        $paymentIntent = '';

        if ($price > 0) {

            $captureMethod = ($item->preauthOnly()) ? 'manual' : 'automatic';

            $paymentIntent = $stripe->paymentIntents()->create([
                'amount' => $price / 100,
                'currency' => 'EUR',
                #'receipt_email' => 'giojkd@gmail.com',
                'payment_method' => $request->payment_method,
                'confirm' => true,
                'capture_method' => $captureMethod, #'manual', #or 'automatic' which is default
                'customer' =>  $user->stripe_id,
            ]);

                #######################################
                #######################################
      #-------->#must check for payment intent success#<---------#
                #######################################
                #######################################
        }

        $item->sendContentPurchaseNotification($user); #send notification

        $purchase = Purchase::create([
            'user_id' => $user->id,
            'purchaseable_type' =>  $model,
            'purchaseable_id' => $item->id,
            'grand_total' => $price/100,
            'purchase_type' => 'on_demand',
            'payment_intent' => $paymentIntent,
            'paid_at' => ($item->preauthOnly()) ? null : date('Y-m-d H:i:s')
        ]);

        #$user->purchase_without_subscription($model, $item->id, $price); #purchase without subscription doesnt check if user has subscription enabled or anything

        if ($request->ajax()) {
            return ['status' => 1];
        } else {
            return redirect($item->makeUrl());
        }
    }


    public function activatePlan(Request $request)
    {

        $option = Enrollmentoption::findOrFail($request->enrollmentOptionId);
        $user = Auth::user();

        $user->addPaymentMethod($request->payment_method);
        $user->updateDefaultPaymentMethod($request->payment_method);

        $user->invoiceFor('MamaBlip plan: ' . $option->name, $option->price * 100);

        $plan = Plan::create([
            'user_id' => $user->id,
            'enrollmentoption_id'  => $option->id
        ]);

        $user->plan_id = $plan->id;



        if ($request->ajax()) {
            return ['status' => 1];
        } else {
            return redirect(Route('thankYouPagePlanActivation'));
        }
    }

    public function thankYouPagePlanActivation(Request $request)
    {
        $lead = $this->getLead();
        $this->viewData['lead'] = $lead;
        return view('front.thank-you-page-plan-activation', $this->viewData);
    }

    public function enrollmentOptions(Request $request)
    {

        $this->getPagetypeMeta($this->viewData);

        $enrollmentOptionGroupId = ($request->has('enrollmentoptiongroup_id')) ? $request->enrollmentoptiongroup_id : 0;

        $lead = $this->getLead();
        $this->viewData['lead'] = $lead;

        if ($enrollmentOptionGroupId > 0) {
            $group = Enrollmentoptiongroup::with(['enrollmentoption', 'enrollmentoptions'])->findOrFail($enrollmentOptionGroupId);
            $this->viewData['group'] = $group;
            $this->viewData['enrollmentoptions'] = $group->enrollmentoptions;
        } else {
            abort(404);
        }

        if (Auth::check()) {
            $user = Auth::user();
            $this->viewData['client_secret_intent'] = $user->createSetupIntent();
            $this->viewData['stripe_customer_id'] = $user->createOrGetStripeCustomer();
            $this->viewData['user'] = $user;
            $this->viewData['payment_methods'] = collect($user->paymentMethods());
            return view('front.enrollment-options', $this->viewData);
        } else {
            return redirect(Route('register'));
        }
    }

    public function availablePlans($lang)
    {

        $this->getPagetypeMeta($this->viewData);

        $this->viewData['plans'] = Plan::get();
        $lead = $this->getLead();
        $this->viewData['lead'] = $lead;

        return view('front.available-plans', $this->viewData);
    }



    public function contentDownload($model, $id)
    {

        $request = Request();
        if (!$request->hasValidSignature()) {
            abort(401);
        }

        $file = new $model();
        $file = $file->find($id);
        return Storage::disk('file_browser_public')->download($file->file); #, $file->name, $headers);
    }

    public function toggleFavorite($model, $id)
    {
        $model = Str::ucfirst($model);
        $model = 'App\Models\\' . $model;

        $favorite = Favorite::firstOrCreate(['user_id' => Auth::id(), 'favoriteable_type' => $model, 'favoriteable_id' => $id]);


        if (!$favorite->wasRecentlyCreated) {

            $favorite->delete();
            $favorite->favoriteable->recommend->sendRecombeeInteraction(['user_id' => $this->getGuest()->id, 'action' => 'RemoveFavorite']);

        }else{

            $favorite->favoriteable->recommend->sendRecombeeInteraction(['user_id' => $this->getGuest()->id, 'action' => 'AddFavorite']);

        }

        return back();
    }

    public function logout()
    {

        Auth::logout();
        return redirect(Route('home'));
    }

    public function login($locale = 'en')
    {

        $this->getPagetypeMeta($this->viewData);

        $lead = $this->getLead();
        $this->viewData['lead'] = $lead;
        $this->viewData['meta'] = [
            'title' => '',
            'description' => '',
            'cover' => '',
            'canonical' => $this->viewData['currentPageUrl']
        ];
        if (Auth::check()) {
            return redirect(Route('user'));
        } else {
            return view('front.login', $this->viewData);
        }
    }

    public function register($locale)
    {

        $this->getPagetypeMeta($this->viewData);

        $lead = $this->getLead();
        $this->viewData['lead'] = $lead;
        $this->viewData['meta'] = [
            'title' => '',
            'description' => '',
            'cover' => '',
            'canonical' => $this->viewData['currentPageUrl']
        ];

        if (Auth::check()) {
            return redirect(Route('user'));
        } else {
            return view('front.register', $this->viewData);
        }
    }

    public function purchaseableModels($withItems = true)
    {
        return [
            [
                'name' => 'Articles',
                'modelName' => 'App\Models\Article',
                'items' => ($withItems) ? Auth::user()->purchaseType('App\Models\Article') : [],
                'item_view' => 'front.components.article-result',
                'item_name' => 'article',
                'relationship' => 'articles',
                'col_width' => 6
            ],
            [
                'name' => 'Recipes',
                'modelName' => 'App\Models\Recipe',
                'items' => ($withItems) ? Auth::user()->purchaseType('App\Models\Recipe') : [],
                'item_view' => 'front.components.recipe-result',
                'item_name' => 'recipe',
                'relationship' => 'recipes',
                'col_width' => 6
            ],
            [
                'name' => 'Events',
                'modelName' => 'App\Models\Event',
                'items' => ($withItems) ? Auth::user()->purchaseType('App\Models\Event') : [],
                'item_view' => 'front.components.event-result',
                'item_name' => 'event',
                'relationship' => 'events',
                'col_width' => 6

            ],
            [
                'name' => 'Videos',
                'modelName' => 'App\Models\Video',
                'items' => ($withItems) ?  Auth::user()->purchaseType('App\Models\Video') : [],
                'item_view' => 'front.components.video-result',
                'item_name' => 'video',
                'relationship' => 'videos',
                'col_width' => 6
            ],
            [
                'name' => 'Products',
                'modelName' => 'App\Models\Product',
                'items' => ($withItems) ?  Auth::user()->purchaseType('App\Models\Product') : [],
                'item_view' => 'front.components.product-result',
                'item_name' => 'product',
                'relationship' => 'products',
                'col_width' => 4
            ]
        ];
    }

    public function favoriteableModels($withItems = true)
    {
        return [
            [
                'name' => 'Articles',
                'modelName' => 'App\Models\Article',
                'items' => ($withItems) ? Auth::user()->favoriteType('App\Models\Article') : [],
                'item_view' => 'front.components.article-result',
                'item_name' => 'article',
                'relationship' => 'articles',
                'col_width' => 6
            ],
            [
                'name' => 'Recipes',
                'modelName' => 'App\Models\Recipe',
                'items' => ($withItems) ? Auth::user()->favoriteType('App\Models\Recipe') : [],
                'item_view' => 'front.components.recipe-result',
                'item_name' => 'recipe',
                'relationship' => 'recipes',
                'col_width' => 6
            ],
            [
                'name' => 'Events',
                'modelName' => 'App\Models\Event',
                'items' => ($withItems) ? Auth::user()->favoriteType('App\Models\Event') : [],
                'item_view' => 'front.components.event-result',
                'item_name' => 'event',
                'relationship' => 'events',
                'col_width' => 6

            ],
            [
                'name' => 'Videos',
                'modelName' => 'App\Models\Video',
                'items' => ($withItems) ?  Auth::user()->favoriteType('App\Models\Video') : [],
                'item_view' => 'front.components.video-result',
                'item_name' => 'video',
                'relationship' => 'videos',
                'col_width' => 6
            ],
            [
                'name' => 'Products',
                'modelName' => 'App\Models\Product',
                'items' => ($withItems) ?  Auth::user()->favoriteType('App\Models\Product') : [],
                'item_view' => 'front.components.product-result',
                'item_name' => 'product',
                'relationship' => 'products',
                'col_width' => 4
            ]
        ];
    }

    public function user()
    {

        $this->getPagetypeMeta($this->viewData);

        $this->viewData['page_type'] = 'user';

        $lead = $this->getLead();
        $this->viewData['lead'] = $lead;
        if (Auth::check()) {
            $user = Auth::user();
            $type = 'App\Models\Video';

            #dd($type::find($user->purchases()->where('purchaseable_type',$type)->get()->pluck('purchaseable_id')));
            #dd(Auth::user()->purchaseType('App\\Models\\Video'));

            $favoriteableModels =  $this->favoriteableModels();


            $purchaseableModels =  $this->purchaseableModels();

            $purchases = $user->purchases()->with(['purchaseable'])->get()->groupBy('purchaseable_type');


            $countFavorites = collect($favoriteableModels)->reduce(function ($carry, $item) {
                return $carry += collect($item['items'])->count();
            }, 0);


            $countPurchases = $purchases->count();

            $this->viewData['meta'] = [
                'title' => $user->full_name,
                'description' => '',
                'cover' => '',
                'canonical' => $this->viewData['currentPageUrl']
            ];

            $this->viewData['user'] = $user;
            $this->viewData['userHasActivePlan'] = $user->hasActivePlan();
            $this->viewData['plan'] = $user->plan;
            $this->viewData['countFavorites'] = $countFavorites;
            $this->viewData['countPurchases'] = $countPurchases;
            $this->viewData['favoriteableModels'] = $favoriteableModels;
            #$this->viewData['purchaseableModels'] = $purchaseableModels;
            $this->viewData['purchaseableModels'] = $purchases;
            $this->viewData['payment_methods'] = collect($user->paymentMethods());


            $this->viewData['altLangs'] = [
                'urls' => ['it' => url((isset($user->getTranslations()['url']['it']) ? $user->getTranslations()['url']['it'] : '')), 'en' => url(isset($user->getTranslations()['url']['en']) ? $user->getTranslations()['url']['en'] : '')],
                'langMap' => ['en' => 'x-default', 'it' => 'it-it']
            ];

            #dd($this->viewData);



            return view('front.user-area', $this->viewData);
        } else {
            redirect(Route('login'));
        }
    }

    public function enrollTraining(Request $request)
    {

        Auth::user()->trainings()->attach($request->input('training_id'), ['created_at' => date('Y-m-d H:i:s')]);
        return back();
    }

    public function videos(Request $request, $locale)
    {
        $this->getPagetypeMeta($this->viewData);

        $orderBy = ($request->has('orderBy')) ? $request->orderBy : 'release_date';

        $this->viewData['page_type'] = 'videos';

        switch($orderBy){

            case 'release_date':

                $this->viewData['videos'] =
                Video::whereNotNull('videocategory_id')
                ->whereActive(1)
                ->whereEnabled(1)
                ->whereNotNull('cover')
                ->orderBy('lft', 'ASC')
                ->get();

                break;

            case 'recommended_for_you':

                $this->viewData['videos'] =

                Recommend::find(
                    collect(
                        $this
                            ->getGuest()
                            ->getRecommendations([
                                'n' => 100,
                                'model' => 'video'
                            ])['recomms']
                    )
                        ->pluck('id')
                )
                ->pluck('recommendable')
                #->whereNotNull('cover')
                ->where('enabled', 1)
                ->where('active', 1);


                break;

        }

        $lead = $this->getLead();
        $this->viewData['lead'] = $lead;
        $this->viewData['videocategories'] = Videocategory::withCount('videos')->get();
        #dd($this->viewData['videos'][3]);
        $this->viewData['orderBy'] = $orderBy;

        $this->viewData['altLangs'] = [
            'urls' => [
                'it' => url('it/videos'),
                'en' => url('en/videos'),
                'en-us' => url('en/videos'),
                'x-default' => url('en/videos'),
            ],
            'langMap' => ['en' => 'x-default', 'it' => 'it-it']
        ];


        return view('front.videos', $this->viewData);
    }

    public function videocategory($locale,$name,$id=null){


        $this->getPagetypeMeta($this->viewData);



        $legacyUrl = 1;

        if (is_null($id)) {
            $legacyUrl = 0;
            $id = collect(explode('-', $name))->last();
        }



        if ($legacyUrl) {
            $videocategory = Videocategory::findOrFail($id);
            return redirect($videocategory->makeUrl(), 301);
        } else {

            $videocategory = Videocategory::where('url->' . $locale, $name)->firstOrFail();
        }

        #$videocategory->checkUrl($name);

        $this->viewData['videos'] =
        $videocategory->videos()->whereNotNull('videocategory_id')
            ->whereActive(1)
            ->whereEnabled(1)
            ->orderBy('lft', 'ASC')
            ->get();
        $this->viewData['videocategories'] = Videocategory::withCount('videos')->get();
        $this->viewData['selectedVideoCategory'] = $videocategory;
        #dd($this->viewData['videos'][3]);
        $lead = $this->getLead();
        $this->viewData['lead'] = $lead;

        /*
        $this->viewData['altLangs'] = [
            'urls' => ['it' => url($videocategory->getTranslations()['url']['it']), 'en' => url($videocategory->getTranslations()['url']['en'])],
            'langMap' => ['en' => 'x-default', 'it' => 'it-it']
        ];
        */

        $this->viewData['altLangs'] = $videocategory->getAlternateLangUrls();

        $this->viewData['meta']['title'] = $videocategory->metatitle . ' | MaMaBlip';
        $this->viewData['meta']['description'] = $videocategory->meta_description;
        $this->viewData['meta']['h1'] = $videocategory->h1;
        $this->viewData['meta']['x-default-hreflang'] = $videocategory->makeUrl('en');


        $this->viewData['page_type'] = 'videos';

        #dd($this->viewData['altLangs']);

        return view('front.videos', $this->viewData);
    }

    public function video($locale, $name, $id=null)
    {

        $this->getPagetypeMeta($this->viewData);

        $video = Video::whereEnabled(1)->whereActive(1)->with(['reviews'=>function($query){$query->where('active', 1);}]);

        $legacyUrl = 1;

        if (is_null($id)) {
            $legacyUrl = 0;
            $id = collect(explode('-', $name))->last();
        }

        if ($legacyUrl) {
            $video = $video->findOrFail($id);
            return redirect($video->makeUrl(), 301);
        } else {
            $video = $video->where('url->' . $locale, $name)->firstOrFail();
        }

        $this->viewData['meta'] = [
            'title' => (Str::length($video->name) > 60) ? $video->name :  $video->name. ' | MaMaBlip',
            'description' => $video->subtitle,
            'cover' => $video->cover,
            'canonical' => $video->makeUrl(),
            'x-default-hreflang' => $video->makeUrl('en')
        ];



        $video->recommend->sendRecombeeInteraction(['user_id' => $this->getGuest()->id]);

        $sharer = new Share();
        $this->viewData['social_share'] =
        $sharer->page($video->makeUrl())
        ->facebook()
        ->twitter()
        ->linkedin()
        ->whatsapp()
        ->getRawLinks();

        $video->addView();

        $lead = $this->getLead();
        $this->viewData['lead'] = $lead;

        $this->viewData['yt_playlist'] = collect([$video->yt_video_id]);
/*
        $this->viewData['yt_playlist']->merge(
        Video::whereActive(1)
        ->where('id','!=', $video->id)
        ->whereNotNull('yt_video_id')
        ->get()
        ->pluck('yt_video_id')->shuffle()); */

        #dd($this->viewData['yt_playlist']);

        $this->viewData['video'] = $video;

        $this->viewData['richResult'] = [
            '@context'=> 'https://schema.org',
            '@type' => 'VideoObject',
            'name' => $video->name,
            'description' => strip_tags($video->description),
            'thumbnailUrl' => [
                url($video->cover)
            ],
            'contentUrl' => $video->makeUrl(),
            'embedUrl' => $video->makeUrl(),
            'uploadDate' => $this->isoDate($video->created_at),
            'duration' => ($video->duration) ? 'PT' . $video->duration . 'M' : 'PT3M',
            'interactionStatistic' => [
                '@type' => 'InteractionCounter',
                'interactionType' => [
                    '@type' => 'http://schema.org/WatchAction',
                ],
                'userInteractionCount' => $video->views
            ]
        ];

        $this->viewData['altLangs'] = $video->getAlternateLangUrls();
        $this->viewData['sponsors'] = $video->sponsors()->pluck('name')->toArray();
        $this->viewData['page_type'] = 'video';

        $video->views++;
        $video->saveWithoutEvents();


        if ($video->is_free) {
            return view('front.video', $this->viewData);
        } else {
            if (Auth::check()) {
                $user = Auth::user();
                if ($user->purchased('video', $video->id)) {
                    return view('front.video', $this->viewData);
                } else {
                    if ($user->canPurchase('videos')) {
                        $this->viewData['canPurchase'] = true;
                    }
                }
            }
            return view('front.video_teaser', $this->viewData);
        }
    }

    public function events($locale)
    {
        $lead = $this->getLead();
        $this->viewData['lead'] = $lead;

        $this->getPagetypeMeta($this->viewData);

        $this->viewData['dates'] = Event::where('starts_at_day', '>=', date('Y-m-d'))->orderBy('starts_at_day', 'ASC')->orderBy('starts_at_time', 'ASC')->get()->groupBy('starts_at_day');

        $this->viewData['altLangs'] = [
            'urls' => ['it' => url('it/events'), 'en' => url('it/events')],
            'langMap' => ['en' => 'x-default', 'it' => 'it-it']
        ];

        $this->viewData['altLangs'] = [
            'urls' => [
                'it' => url('it/events') ,
                'en' => url('en/events'),
                'en-us' => url('en/events') ,
                'x-default' => url('en/events'),

            ],
            'langMap' => ['en' => 'x-default', 'it' => 'it-it']
        ];

        $this->viewData['page_type'] = 'events';

        return view('front.events', $this->viewData);
    }

    public function event($locale, $name, $id=null)
    {

        $this->getPagetypeMeta($this->viewData);

        $lead = $this->getLead();
        $this->viewData['lead'] = $lead;
        $event = Event::with([
            'reviews' =>function($query){$query->where('active', 1);}
        ]);

        $legacyUrl = 1;
        if (is_null($id)) {
            $legacyUrl = 0;
            $id = collect(explode('-', $name))->last();
        }

        if ($legacyUrl) {
            $event = $event->findOrFail($id);
            return redirect($event->makeUrl(), 301);
        } else {
            $event = $event->where('url->' . $locale, $name)->firstOrFail();
        }


        #$event->checkUrl($name);

        $event->recommend->sendRecombeeInteraction(['user_id' => $this->getGuest()->id]);

        $this->viewData['event'] = $event;

        $this->viewData['meta'] = [
            'title' => (Str::length($event->name) > 60) ? $event->name : $event->name. ' | MaMaBlip',
            'description' => $event->subtitle,
            'cover' => $event->cover,
            'canonical' => $event->makeUrl(),
            'x-default-hreflang' => $event->makeUrl('en')
        ];

        $sharer = new Share();
        $this->viewData['social_share'] =
        $sharer->page($event->makeUrl())
        ->facebook()
        ->twitter()
        ->linkedin()
        ->whatsapp()
        ->getRawLinks();

        $eventImages = [];

        if(!is_null($event->images)){
            foreach($event->images as $image){
                $eventImages[] = url($image);
            }
        }

        $this->viewData['richResult'] = [

            '@context' => 'https://schema.org/',
            '@type' => 'Event',
            'name' => $event->name,
            'startDate' => $this->isoDate($event->starts_at_day.' '.$event->starts_at_time),
            'endDate' => $this->isoDate($event->starts_at_day.' '.date('H:i:s',strtotime($event->starts_at_time.'+'.$event->duration.'minutes'))),
            'eventStatus' => 'https://schema.org/EventScheduled',
            'eventAttendanceMode' => 'https://schema.org/OnlineEventAttendanceMode',
            'location' => [
                '@type' => 'VirtualLocation',
                'url' => $event->makeUrl()
            ],
            'image' => $eventImages,
            'description' => strip_tags($event->description),
            'offers' => [
                '@type' => 'Offer',
                'url' => $event->makeUrl(),
                'price' => $event->price/100,
                'priceCurrency' => 'EUR',
                'availability' => 'https://schema.org/InStock',
                'validFrom' => str_replace(' ','T',$event->created_at)
            ],

            'performer' => [
                '@type' => 'PerformingGroup',
                'name' => Str::ucfirst($event->user->name . ' ' . $event->user->surname)
            ],
            'organizer' => [
                '@type' => 'Organization',
                'name' => 'MaMaBlip',
                'url' => url(App::getLocale())
            ]

        ];

        $this->viewData['sponsors'] = $event->sponsors()->pluck('name')->toArray();
        $this->viewData['altLangs'] = $event->getAlternateLangUrls();

        $this->viewData['page_type'] = 'event';

        if ($event->is_free) {
            return view('front.event', $this->viewData);
        } else {
            if (Auth::check()) {
                $user = Auth::user();
                if ($user->purchased('event', $event->id)) {
                    $this->viewData['purchase'] = $user->getPurchase('event', $event->id);
                    return view('front.event', $this->viewData);
                } else {
                    if ($user->canPurchase('events')) {
                        $this->viewData['canPurchase'] = true;
                    }
                }
            }
            return view('front.event_teaser', $this->viewData);
        }




        return view('front.event', $this->viewData);
    }

    public function enrollEvent($locale, $id)
    {

        $this->getPagetypeMeta($this->viewData);

        $lead = $this->getLead();
        $this->viewData['lead'] = $lead;

        $event = Event::findOrFail($id);
        if (Auth::check() && Auth::user()->purchased('video', $id)) {
            $this->viewData['event'] = $event;
            return view('front.event-enrollment', $this->viewData);
        } else {
            return redirect($event->makeUrl());
        }
    }

    public function iMama($lang){

        #echo 'puppa';

        $this->getPagetypeMeta($this->viewData);

        $lead = $this->getLead();
        $this->viewData['lead'] = $lead;

        $this->viewData['meta'] = [
            'title' => 'iMama',
            'description' => 'iMama',
            #'cover' => collect($article->photos)->first(),
            'canonical' => Route('iMama')
        ];

        $models = ['video', 'product', 'recipe','article','event'];
        $items = [];
        foreach($models as $model){
            $items[] = Recommend::
            find(
                collect(
                        $this
                        ->getGuest()
                        ->getRecommendations([
                                'n' => 10,
                                'model' => $model
                            ])
                        ['recomms'])
                        ->pluck('id')
                    )
                ->pluck('recommendable');
        }

        $this->viewData['recommendedItems'] = collect($items)->flatten()->shuffle();

        return view('front.i-mama', $this->viewData);
    }

    public function article($lang, $name, $id = null)
    {

        $this->getPagetypeMeta($this->viewData);
        $lead = $this->getLead();
        $this->viewData['lead'] = $lead;
        $this->viewData['page_type'] = 'article';

        $legacyUrl = 1;
        if(is_null($id)){
            $legacyUrl = 0;
            $id = collect(explode('-',$name))->last();
        }

        $article = Article::with(['categories', 'tags', 'products', 'user','reviews'=>function($query){$query->where('active', 1);}]);

        if($legacyUrl){
            $article = $article->findOrFail($id);
            return redirect($article->makeUrl(), 301);
        }else{
            $article = $article->where('url->'.$lang,$name)->firstOrFail();
        }

        if(!$article->enabled){
            abort(404);
        }

        $article->views++;
        $article->saveWithoutEvents();

        $article->recommend->sendRecombeeInteraction(['user_id' => $this->getGuest()->id]);

        $this->viewData['article'] = $article;
        $this->viewData['meta'] = [
            'title' => (Str::length($article->name) > 60) ? $article-> name  : $article->name.' | MaMaBlip',
            'description' => $article->subtitle,
            'cover' => collect($article->photos)->first(),
            'canonical' => $article->makeUrl(),
            'x-default-hreflang' => $article->makeUrl('en')
        ];

        $sharer = new Share();
        $this->viewData['social_share'] =
        $sharer->page($article->makeUrl())
        ->facebook()
        ->twitter()
        ->linkedin()
        ->whatsapp()
        ->getRawLinks();


        $articleImages = [];

        if(!is_null($article->photos)){
            foreach($article->photos as $photo){
                $articleImages[] = url($photo);
            }
        }

        /*

            "mainEntityOfPage":{
      "@type":"WebPage",
      "@id":"https://google.com/article"
   },

   "publisher":{
      "@type":"Organization",
      "name":"CNN",
      "logo":{
         "@type":"ImageObject",
         "url":"https://dynaimage.cdn.cnn.com/cnn/q_auto,h_60/https%3A%2F%2Fi2.cdn.turner.com%2Fcnn%2F2017%2Fimages%2F09%2F08%2Flogo-cnntravel.png"
      }
   },
        */

         $this->viewData['richResult'] = [

            '@context' => 'https://schema.org/',
            '@type' => 'NewsArticle',
            'mainEntityOfPage' => [
                '@type' => 'WebPage',
                '@id' => 'https://google.com/article'
            ],
            'headline' => $article->name,
            'image' => $articleImages,
            'datePublished' => $this->isoDate($article->published_at),
            'dateModified' => $this->isoDate($article->updated_at),
            'url' => $article->makeUrl(),
            'publisher' => [
                '@type' => 'Organization',
                'name' => 'Mamablip',
                'logo' => [
                    '@type' => 'ImageObject',
                    'url' => 'https://aalsdnxkwq.cloudimg.io/v7/www.mamablip.com/front/images/logo.png?width=250'
                ]
            ],
            'description' => $article->brief,
            'author' => [
                '@type' => 'person',
                'name' => $article->user->name.' '. $article->user->surname
            ]
         ];

        $this->viewData['sponsors'] = $article->sponsors()->pluck('name')->toArray();
        $this->viewData['altLangs'] = $article->getAlternateLangUrls();

        if ($article->is_free) {
            return view('front.article', $this->viewData);
        } else {
            if (Auth::check()) {
                $user = Auth::user();
                if ($user->purchased('article', $article->id)) {
                    return view('front.article', $this->viewData);
                } else {
                    if ($user->canPurchase('articles')) {
                        $this->viewData['canPurchase'] = true;
                    }
                }
            }
            return view('front.article_teaser', $this->viewData);
        }




        return view('front.article', $this->viewData);
    }

    public function recipes(Request $request, $locale){
        $this->getPagetypeMeta($this->viewData);

        $facets = [];

        $this->viewData['facets'] = $facets;

        $this->viewData['keyword'] = ($request->has('keyword')) ? $request->keyword : '';
        $this->viewData['onlyOnSale'] = ($request->has('only_on_sale')) ? $request->only_on_sale : 0;
        $this->viewData['locale'] = $locale;
        $lead = $this->getLead();
        $this->viewData['lead'] = $lead;

        $this->viewData['altLangs'] = [
            'urls' => [
                'it' => url('it/recipes'),
                'en' => url('en/recipes'),
                'en-us' => url('en/recipes'),
                'x-default' => url('en/recipes'),
            ],
            'langMap' => ['en' => 'x-default', 'it' => 'it-it']
        ];

        $this->viewData['page_type'] = 'recipes';

        return view('front.recipes-algolia', $this->viewData);
    }


    public function recipesOLD(Request $request, $locale)
    {
        $lead = $this->getLead();
        $this->viewData['lead'] = $lead;

        $this->getPagetypeMeta($this->viewData);

        $filters = ($request->has('filters')) ? $request->filters : ['order_by' => 'release_date'];


        switch ($filters['order_by']) {


            case 'release_date':

                $recipes = Recipe::with(['recipeingredients', 'dishcourse', 'complexity', 'cuisine'])
                ->whereEnabled(1);

                if(isset($filters['dishcourse_id']) && $filters['dishcourse_id'] != ''){
                    $recipes = $recipes->where('dishcourse_id', $filters['dishcourse_id']);
                }

                $recipes = $recipes
                ->get()
                ->sortBy(function ($recipe, $key) {
                    return (is_null($recipe->lft)) ? 1000000 : $recipe->lft;
                });
                break;

            case 'recommended_for_you':

                $recipes = Recommend::find(
                    collect(
                        $this
                            ->getGuest()
                            ->getRecommendations([
                                'n' => 100,
                                'model' => 'recipe'
                            ])['recomms']
                    )
                        ->pluck('id')
                )
                    ->pluck('recommendable')
                    ->where('enabled', 1);

                if (isset($filters['dishcourse_id']) && $filters['dishcourse_id'] != '') {
                    $recipes = $recipes->where('dishcourse_id', $filters['dishcourse_id']);
                }

                $recipes = $recipes->whereNotNull('dishcourse');


                break;
        }



        $this->viewData['dishcourses'] = Dishcourse::get()->sortByDesc('recipes_count');

        $this->viewData['recipes'] = $recipes;
        $this->viewData['filters'] = $filters;

        return view('front.recipes', $this->viewData);

    }


    public function recipe($lang, $name, $id=null)
    {

        $this->getPagetypeMeta($this->viewData);
        $this->viewData['page_type'] = 'recipe';

        $lead = $this->getLead();
        $this->viewData['lead'] = $lead;

        $legacyUrl = 1;
        if (is_null($id)) {
            $legacyUrl = 0;
            $id = collect(explode('-', $name))->last();
        }


        $recipe = Recipe::with([
            'steps'=>function($query){
                $query->orderBy('sort_order','ASC');
            },
            'reviews' => function($query){
                $query->where('active', 1);
            },
            'recipeingredients',
            'products' => function ($query) {
                $query->whereEnabled(1)->orderBy('pivot_id', 'ASC');
            },
            'ingredientslists'
        ])
        ->whereEnabled(1);


        if ($legacyUrl) {
            $recipe = $recipe->findOrFail($id);
            return redirect($recipe->makeUrl(), 301);
        } else {
            $recipe = $recipe->where('url->' . $lang, $name)->firstOrFail();
        }

        $recipe->recommend->sendRecombeeInteraction(['user_id' => $this->getGuest()->id]);

        #$recipe->checkUrl($name);

        $this->viewData['recipe'] = $recipe;

        $this->viewData['meta'] = [
            'title' => (Str::length($recipe->name) > 60) ? $recipe->name :  $recipe->name. ' | MaMaBlip',
            'description' => $recipe->subtitle,
            'cover' => collect($recipe->photos)->first(),
            'canonical' => $recipe->makeUrl(),
            'x-default-hreflang' => $recipe->makeUrl('en')
        ];

        $sharer = new Share();
        $this->viewData['social_share'] =
        $sharer->page($recipe->makeUrl())
        ->facebook()
        ->twitter()
        ->linkedin()
        ->whatsapp()
        ->getRawLinks();



        $recipeIngredients = [];
        if($recipe->ingredients->count() > 0){
            foreach($recipe->recipeingredients as $ingredient){
                if(is_object($ingredient->ingredient)){
                    $recipeIngredients[] = $ingredient->quantity . $ingredient->measurement_unit . ' ' . $ingredient->ingredient->name;
                }
            }
        }

        $recipeInstructions = [];
        if($recipe->steps->count() > 0){
            foreach($recipe->steps as $stepIndex => $step){
                $newStep =
                [
                    '@type' => 'HowToStep',
                    'name' => $step->name,
                    'text' => strip_tags($step->description),
                    'url' => $recipe->makeUrl() . '#preparation-step-' . $stepIndex,
                ];
                if(!is_null($step->photos)){
                    $newStep['image'] = url(collect($step->photos)->first());
                }
                $recipeInstructions[] = $newStep;
            }
        }

        $recipeVideo = [];

        if($recipe->videos->count() > 0){
            $recipeVideo_ = $recipe->videos->first();
            $recipeVideo = [
                '@type' => 'VideoObject',
                'name' => $recipeVideo_->name,
                'description' => strip_tags($recipeVideo_->description),
                'thumbnailUrl' => [
                    url($recipeVideo_->cover)
                ],
                'contentUrl' => $recipeVideo_->makeUrl(),
                'embedUrl' => $recipeVideo_->makeUrl(),
                'uploadDate' => $recipeVideo_->created_at,
                'duration' => 'PT'. $recipeVideo_->duration.'M',
                'interactionStatistic' => [
                    '@type' => 'InteractionCounter',
                    'interactionType' => [
                        '@type' => 'http://schema.org/WatchAction',
                    ],
                    'userInteractionCount' => $recipeVideo_->views
                ]
            ];
        }




        $this->viewData['richResult'] = [
            '@context' => 'https://schema.org/',
            '@type' => 'Recipe',
            'name' => $recipe->name,
            'image' => collect($recipe->photos)->transform(function($item,$key){
                return url($item);
            }),
            'author' => [
                '@type' => 'Person',
                'name' => Str::ucfirst($recipe->user->name . ' ' . $recipe->user->surname)
            ],
            'datePublished' => $this->isoDate($recipe->created_at),
            'description' => $recipe->subtitle,
            'prepTime' => 'PT'.$recipe->preparation_time.'M',
            'cookTime' => 'PT'.$recipe->cooking_time.'M',
            'totalTime' => 'PT'.($recipe->preparation_time + $recipe->cooking_time).'M',
            'keywords' => $recipe->keywords,
            'recipeYield' => $recipe->servings,
            'recipeCategory' => $recipe->dishcourse->name,
            'recipeCuisine' => $recipe->cuisine->name,
            'nutrition' => [
                '@type' => 'NutritionInformation',
                'calories' => $recipe->calories.' calories'
            ],
            'aggregateRating' => [
                '@type' => 'AggregateRating',
                'ratingValue' => 5,
                'ratingCount' => $recipe->reviews->count()
            ],
            'recipeIngredient' => $recipeIngredients,
            'recipeInstructions' => $recipeInstructions,
            'video' => $recipeVideo
        ];



        $this->viewData['sponsors'] = $recipe->sponsors()->pluck('name')->toArray();


        $this->viewData['altLangs'] = $recipe->getAlternateLangUrls();

        $recipe->views++;
        $recipe->saveWithoutEvents();

        if ($recipe->is_free) {
            return view('front.recipe', $this->viewData);
        } else {
            if (Auth::check()) {
                $user = Auth::user();
                if ($user->purchased('recipe', $recipe->id)) {
                    return view('front.recipe', $this->viewData);
                } else {
                    if ($user->canPurchase('recipes')) {
                        $this->viewData['canPurchase'] = true;
                    }
                }
            }
            return view('front.recipe_teaser', $this->viewData);
        }
    }

    public function blog($lang)
    {

        $this->getPagetypeMeta($this->viewData);

        $lead = $this->getLead();

        $this->viewData['lead'] = $lead;
        $blog = Article::whereNotNull('published_at')
        ->where('published_at', '<=', date('Y-m-d'))
        #->orderBy('lft', 'ASC')
        ->orderBy('published_at', 'DESC')
        ->whereEnabled(1)
        ->get();
        /*
        ->groupBy(function ($item, $key) {
            return $item->published_at->format('F \'y');
        });;*/
        #dd($blog);
        $this->viewData['articleCategories'] = Articlecategory::orderBy('name', 'ASC')->get();
        $this->viewData['blog'] = $blog;

        $this->viewData['altLangs'] = [
            'urls' => [
                'it' => url('it/blog'),
                'en' => url('en/blog'),
                'en-us' => url('en/blog'),
                'x-default' => url('en/blog'),

            ],
            'langMap' => ['en' => 'x-default', 'it' => 'it-it']
        ];

        $this->viewData['page_type'] = 'blog';

        return view('front.blog', $this->viewData);
    }

    public function blog_NEW(Request $request, $lang)
    {

        $this->getPagetypeMeta($this->viewData);
        $lead = $this->getLead();
        $this->viewData['lead'] = $lead;

        $filters = ($request->has('filters')) ? $request->filters : ['order_by' => 'release_date'];

        $orderBy = '';

        switch ($filters['order_by']) {
            case 'release_date':
                $blog = Article::whereNotNull('published_at')
                ->where('published_at', '<=', date('Y-m-d'))
                ->orderBy('published_at', 'DESC')
                ->whereEnabled(1);

                if(isset($filters['articlecategory_id'])){
                    $blog = $blog->where('articlecategory_id',$filters['articlecategory_id']);
                }

                $blog = $blog->get();
                break;

            case 'recommended_for_you':
                $blog = Recommend::find(
                    collect(
                        $this
                            ->getGuest()
                            ->getRecommendations([
                                'n' => 1000,
                                'model' => 'article'
                            ])['recomms']
                    )
                        ->pluck('id')
                )
                    ->pluck('recommendable')
                    ->where('enabled', 1)
                    ->where('published_at', '<=', date('Y-m-d'))
                    ->whereNotNull('published_at');

                if (isset($filters['articlecategory_id'])) {
                    $blog = $blog->where('articlecategory_id', $filters['articlecategory_id']);
                }
                break;
        }

        $this->viewData['filters'] = $filters;
        $this->viewData['articleCategories'] = Articlecategory::orderBy('name', 'ASC')->get()->sortByDesc('articles_count')->where('articles_count','>',0);

        $this->viewData['blog'] = $blog;

        return view('front.blog', $this->viewData);

    }



    public function home(Request $request, $locale = '')
    {



        $guest = $this->getGuest();
        $agent = new Agent();
        $lead = $this->getLead();

        if ($request->has('country_id')) { #country changing
            if($guest->country_id != $request->country_id){
                $guest->country_id = $request->country_id;
                $guest->save();
                $lead->empty();
            }
        }

        $this->viewData['page_type'] = 'home';

        $this->getPagetypeMeta($this->viewData);


        if ($locale == '') {
            return redirect(route('home', ['locale' => $this->defaultLocale]),301);
        }

        \Session::put('locale', $locale);
        App::setLocale($locale);

        \Carbon\Carbon::setLocale($locale);

        $logos = [];

        $numbers = [
            [
                'name' => 'Classes',
                'label' => 'Live Classes',
                'quantity' => Trainingmodule::count(),
                'description' => 'With final certification',
                'frequency' => rand(1, 10),
                'img' => 'thw-number-img.png',
                'url' => Route('events')
            ],
            [
                'name' => 'Recipes',
                'label' => 'Recipes',
                'quantity' => Recipe::count(),
                'description' => 'Step by step',
                'frequency' => rand(1, 10),
                'img' => 'thw-number-img.png',
                'url' => Route('recipes')
            ],
            [
                'name' => 'Videos',
                'label' => 'Videos',
                'quantity' => Video::count(),
                'description' => 'In depth videos',
                'frequency' => rand(1, 10),
                'img' => 'thw-number-img.png',
                'url' => Route('videos')
            ],
            [
                'name' => 'Articles',
                'label' => 'Articles',
                'quantity' => Article::count(),
                'description' => 'By our editorial team',
                'frequency' => rand(1, 10),
                'img' => 'thw-number-img.png',
                'url' => Route('blog')
            ],
            [
                'name' => 'Events',
                'label' => 'Events',
                'quantity' => Event::count(),
                'description' => 'Online and interactive',
                'frequency' => rand(1, 10),
                'img' => 'thw-number-img.png',
                'url' => Route('events')
            ],
            [
                'name' => 'Learning paths',
                'label' => 'Learning paths',
                'quantity' => Training::count(),
                'description' => 'With the best gurus',
                'frequency' => rand(1, 10),
                'img' => 'thw-number-img.png',
                'url' => Route('courses')
            ],
        ];

        #dd($numbers);

        $limitRecipes =  ($agent->isMobile()) ? 4 : 6;
        $limitBlog =  ($agent->isMobile()) ? 5 : 5;
        $limitVideos =  ($agent->isMobile()) ? 4 : 4;
        $limitEvents = ($agent->isMobile()) ? 2 : 2;

        $this->viewData['meta'] = [
            'title' => Setting::get('homepage_title_'.$locale),
            'description' => Setting::get('homepage_description_'.$locale),
            'canonical' => url($locale),
            'x-default-hreflang' => url('en')
        ];

        $ingredients = Ingredient::all()->pluck('name');
        $activeIngredients = ['garlic', 'carrots'];
        $ingredients = $ingredients->shuffle();
        $recipes = Recipe::orderBy('created_at', 'DESC')->whereEnabled(1)->limit($limitRecipes)->get();
        $blog = Article::orderBy('published_at', 'DESC')->whereEnabled(1)->limit($limitBlog)->get();
        $this->viewData['blog'] = $blog;
        $this->viewData['mainArticles'][0] = $blog->shift();
        #$this->viewData['mainArticles'][1] = $blog->shift();
        $this->viewData['dates'] = Event::where('starts_at_day', '>=', date('Y-m-d'))->orderBy('starts_at_day', 'ASC')->orderBy('starts_at_time', 'ASC')->limit($limitEvents)->get()->groupBy('starts_at_day');
        $this->viewData['recipes'] = $recipes;
        $this->viewData['ingredients'] = $ingredients;
        $this->viewData['activeIngredients'] = $activeIngredients;
        $this->viewData['videos'] = Video::whereActive(1)->orderBy('lft', 'ASC')->limit($limitVideos)->get();
        $this->viewData['videos_right_column'] = Video::whereActive(1)->orderBy('views', 'desc')->limit($limitVideos)->get();
        $this->viewData['numbers'] = $numbers;
        $this->viewData['sponsor_manufacturers'] = Manufacturer::where('is_sponsor', 1)->orderBy('lft', 'ASC')->get();
        $this->viewData['sponsor_consortiums'] = Consortium::where('is_sponsor', 1)->orderBy('lft', 'ASC')->get();
        $this->viewData['homepage_users'] = BackpackUser::ShownInHome()->get();
        $this->viewData['articleCategories'] = Articlecategory::orderBy('name', 'ASC')->get();
        $this->viewData['lead'] = $lead;
        $this->viewData['altLangs'] = [
            'urls' => [
                'it' => url('it'),
                'en' => url('en'),
                'en-us' => url('en'),
            ],
            'langMap' => ['en' => 'x-default', 'it' => 'it-it',]
        ];



        $homepageDeals = Deal::where('is_featured',1)->orderBy('starts_at','ASC')->get();



        if($homepageDeals->count() > 0){
            $this->viewData['homepageDeals'] = $homepageDeals;

        }



        return view('front.home', $this->viewData);
    }

    public function search($locale, $keyword)
    {


        $this->viewData['page_type'] = 'search';

        $this->getPagetypeMeta($this->viewData);

        $this->viewData['keyword'] = $keyword;

        $lead = $this->getLead();

        $this->viewData['meta'] = [
            'title' => (Str::length($keyword) > 60) ? $keyword :  $keyword. ' | MaMaBlip',
            'description' => $keyword,
            'canonical' => $this->viewData['currentPageUrl']
        ];



        $this->viewData['results'] =  collect([
            /*[
                'name' => 'Events',
                'items' => Event::search($keyword)->get(),
                'item_view' => 'front.components.event-result',
                'item_name' => 'event',
                'col_width' => 4

            ],*/
            [
                'name' => 'Product',
                'items' => Product::search($keyword)->get()->where('enabled',1)->where('type','simple'),
                'item_view' => 'front.components.product-result',
                'item_name' => 'product',
                'col_width' => 3
            ],
            [
                'name' => 'Recipes',
                'items' => Recipe::search($keyword)->get(),
                'item_view' => 'front.components.recipe-result',
                'item_name' => 'recipe',
                'col_width' => 3
            ],
            [
                'name' => 'Articles',
                'items' => Article::search($keyword)->get(),
                'item_view' => 'front.components.article-result',
                'item_name' => 'article',
                'col_width' => 3
            ],
            [
                'name' => 'Videos',
                'items' => Video::search($keyword)->get(),
                'item_view' => 'front.components.video-result',
                'item_name' => 'video',
                'col_width' => 3
            ],
            [
                'name' => 'Manufacturers',
                'items' => Manufacturer::search($keyword)->get(),
                'item_view' => 'front.components.manufacturer-result',
                'item_name' => 'manufacturer',
                'col_width' => 4
            ],
            [
                'name' => 'Consortia',
                'items' => Consortium::search($keyword)->get(),
                'item_view' => 'front.components.consortium-result',
                'item_name' => 'consortium',
                'col_width' => 4
            ],
            [
                'name' => 'Deals',
                'items' => Deal::search($keyword)->get(),
                'item_view' => 'front.components.deal-result',
                'item_name' => 'deal',
                'col_width' => 4
            ]

        ]);

        $countResults = $this->viewData['results']->reduce(function ($carry, $item) {
            return $carry + $item['items']->count();
        });

        $this->viewData['countResults'] = $countResults;
        $this->viewData['lead'] = $lead;

        return view('front.search', $this->viewData);
    }

    public function searchGateway(Request $request, $locale)
    {

        $routeParams = ['locale' => $locale, 'keyword' => $request->keyword];

        return redirect()->to(Route('search.' . $locale, $routeParams));
    }


    public function teamMembers($locale)
    {
    }

    public function teamMember($locale, $name, $id=null)
    {

        $legacyUrl = 1;
        if (is_null($id)) {
            $legacyUrl = 0;
            $id = collect(explode('-', $name))->last();
        }

        $user = BackpackUser::with([
            'articles' => function ($query) {
                $query->where('enabled',1)->orderBy('published_at', 'DESC');
            },
            'videos' => function ($query) {
                $query->where('enabled',1)->orderBy('created_at', 'DESC');
            },
            'events' => function ($query) {
                $query->where('starts_at_day','>', date('Y-m-d'));
            },
            'recipes'  => function ($query) {
                $query->where('enabled',1);
            },
        ]);

        if ($legacyUrl) {
            $user = $user->findOrFail($id);
            return redirect($user->makeUrl(), 301);
        } else {
            $user = $user->where('url->' . $locale, $name)->firstOrFail();
        }



        $this->getPagetypeMeta($this->viewData);

        $this->viewData['page_type'] = 'team-member';
        $this->viewData['user'] = $user;
        $this->viewData['meta'] = [
            'canonical' => $user->makeUrl(),
            'title' => $user->metatitle . ' | MaMaBlip',
            'description' => $user->meta_description,
            'h1' => $user->h1,
            'x-default-hreflang' => $user->makeUrl('en')
        ];
        $this->viewData['favoriteableModels'] = $this->favoriteableModels(false);
        $lead = $this->getLead();
        $this->viewData['lead'] = $lead;
        $this->viewData['altLangs'] = $user->getAlternateLangUrls();
        return view('front.team-member', $this->viewData);
    }

    public function products(Request $request, $locale, $producttype = '')
    {
        $this->getPagetypeMeta($this->viewData);

        $this->viewData['page_type'] = 'products';

        $products = Product::whereEnabled(1)->orderBy('updated_at', 'DESC')->get();
        $categories = collect([]);

        $products->each(function ($product, $key) use (&$categories) {
            if (!is_null($product->categories)) {
                $product->categories->each(function ($category, $key) use (&$categories) {
                    $categories->push($category);
                });
            }
        });




        if ($categories->count() > 0) {
            $categories = $categories->where('is_filter', 1)->unique('id');
            $categories  = $categories->groupBy('parent_id');
        }




        /*
        $categories->each(function($cat,$key){
            echo $cat->name.' parent: '.$cat->parent_id;
            echo '<br>';
        });

        exit;
 */

        $facets = [];

        #$this->viewData['facets'] = ($request->has('facets')) ? $request->facets : [];

        if($producttype != ''){
            $facets['producttype_'.$locale][] = $producttype;
        }
        $this->viewData['facets'] = $facets;

        $this->viewData['keyword'] = ($request->has('keyword')) ? $request->keyword : '';
        $this->viewData['onlyOnSale'] = ($request->has('only_on_sale')) ? $request->only_on_sale : 0;
        $this->viewData['locale'] = $locale;
        $this->viewData['categories'] = $categories;
        #        dd($categories);
        $this->viewData['products'] = $products;
        $lead = $this->getLead();
        $this->viewData['lead'] = $lead;

        $producttypeUrlComponent = ($producttype != '') ? '/'. $producttype : '';

        $this->viewData['altLangs'] = [
            'urls' => [
                'it' => url('it/products') . $producttypeUrlComponent,
                'en' => url('en/products') . $producttypeUrlComponent,
                'en-us' => url('en/products') . $producttypeUrlComponent,

            ],
            'langMap' => ['en' => 'x-default', 'it' => 'it-it']
        ];

        $this->viewData['meta']['x-default-hreflang'] = 'en/products';


        #return view('front.products', $this->viewData);
        return view('front.products-algolia', $this->viewData);
    }

    public function product($locale, $name, $id=null)
    {

        $this->viewData['page_type'] = 'product';

        $this->getPagetypeMeta($this->viewData);

        $product =
            Product::with([
                'categories' => function ($query) {
                        $query->orderBy('pivot_id', 'ASC');
                    },
                'categories.parent',
                'recipes',
                'prices',
                'reviews' => function($query){$query->where('active', 1);}
            ])
            ->where('enabled', 1);

        $legacyUrl = 1;
        if (is_null($id)) {
            $legacyUrl = 0;
            $id = collect(explode('-', $name))->last();
        }

        if ($legacyUrl) {
            $product = $product->findOrFail($id);
            return redirect($product->makeUrl(),301);
        } else {
            $product = $product->where('url->' . $locale, $name)->firstOrFail();
            #dd($product->photos);
            #dd($product);
        }

        $this->viewData['product'] = $product;

        #$product->checkUrl($name);

        $product->recommend->sendRecombeeInteraction([ 'user_id' => $this->getGuest()->id ]);

        $this->viewData['meta'] = [
            'title' => (Str::length($product->name) > 60) ? $product->name : $product->name. ' | MaMaBlip',
            'description' => $product->description,
            'cover' => collect($product->photos)->first(),
            'canonical' => $product->makeUrl(),
            'x-default-hreflang' => $product->makeUrl('en')
        ];

        $sharer = new Share();
        $this->viewData['social_share'] =
        $sharer->page($product->makeUrl())
        ->facebook()
        ->twitter()
        ->linkedin()
        ->whatsapp()
        ->getRawLinks();

        $eventImages = [];

        $productImages = [];
        if (!is_null($product->photos)) {
            foreach ($product->photos as $photo) {
                $productImages[] = url($photo);
            }
        }

        $productReview = [];

        if($product->reviews->count() > 0){
            $firstReview = $product->reviews->sortByDesc('score')->first();
            $productReview = [
                '@type' => 'Review',
                'reviewRating' => [
                    '@type' => 'Rating',
                    'ratingValue' => $firstReview->score,
                    'bestRating' => 5
                ],
                'author' => [
                    '@type' => 'Person',
                    'name' => Str::ucfirst($firstReview->user->name . ' ' . $firstReview->user->surname)
                ]
            ];
        }

        $aggregateRating = [];

        if($product->reviews->count() > 0){
            $aggregateRating = [
                '@type' => 'AggregateRating',
                'ratingValue' => 5,
                'ratingCount' => $product->reviews->count()
            ];
        }

        $guest = $this->getGuest();
        $this->viewData['guest'] = $guest;


        $this->viewData['richResult'] = [

            '@context' => 'https://schema.org/',
            '@type' => 'Product',
            'name' => $product->name,
            'image' => $productImages,
            'description' => strip_tags($product->description),
            'offers' => [
                '@type' => 'Offer',
                'url' => $product->makeUrl(),
                'price' => $product->getPrice($guest->country_id,'price'),
                'priceCurrency' => 'EUR',
                'itemCondition' => 'https://schema.org/NewCondition',
                'availability' => 'https://schema.org/InStock',
                'validFrom' => $this->isoDate($product->created_at)
            ],
            'sku' => $product->sku,
            'brand' => [
                '@type' => 'Brand',
                'name' => (is_object($product->manufacturer)) ? $product->manufacturer->name : $product->consortium->name
            ],
            'aggregateRating' => $aggregateRating,
            'review' => $productReview

        ];
        $lead = $this->getLead();
        $this->viewData['lead'] = $lead;
        $this->viewData['sponsors'] = $product->sponsors()->pluck('name')->toArray();
        $this->viewData['altLangs'] = $product->getAlternateLangUrls();

        $product->views++;
        $product->saveWithoutEvents();

        return view('front.product', $this->viewData);
    }

    public function experience(Request $request, $locale, $name, $id=null)
    {

        $this->getPagetypeMeta($this->viewData);

        $this->viewData['page_type'] = 'experience';

        $legacyUrl = 1;
        if (is_null($id)) {
            $legacyUrl = 0;
            $id = collect(explode('-', $name))->last();
        }



        $experience = Experience::with([
            'availabilities' => function ($query) {
                $query->where('day', '>=', date('Y-m-d'))->orderBy('day', 'asc')->orderBy('time', 'asc');
            }
        ]);

        if ($legacyUrl) {
            $experience = $experience->findOrFail($id);
            return redirect($experience->makeUrl(), 301);
        } else {
            $experience = $experience->where('url->' . $locale, $name)->firstOrFail();
        }


        #$experience->checkUrl($name);


        $this->viewData['meta'] = [
            'title' => (Str::length($experience->name) > 60) ? $experience->name :  $experience->name. ' | MaMaBlip',
            'description' => $experience->subtitle,
            'cover' => collect($experience->photos)->first(),
            'canonical' => $experience->makeUrl(),
            'x-default-hreflang' => $experience->makeUrl('en')
        ];
        $this->viewData['experience'] = $experience;
        $lead = $this->getLead();
        $this->viewData['altLangs'] = $experience->getAlternateLangUrls();
        $this->viewData['lead'] = $lead;
        return view('front.experience', $this->viewData);
    }

    public function experienceCheckOut(Request $request, $locale)
    {
        if (Auth::check()) {

            $this->getPagetypeMeta($this->viewData);

            $experience = Experience::findOrFail($request->experience_id);
            $this->viewData['meta'] = [
                'title' => (Str::length($experience->name) > 60) ? $experience->name : $experience->name. ' | MaMaBlip',
                'description' => $experience->subtitle,
                'cover' => collect($experience->photos)->first(),
                'canonical' => $experience->makeUrl(),
                'x-default-hreflang' => $experience->makeUrl('en')
            ];
            $this->viewData['experience'] = $experience;
            $this->viewData['experience_config'] = $request->all();
            $day = Carbon::createFromFormat('m/d/Y', $request->date);
            $availability = $experience->availabilities()->where('day', $day->format('Y-m-d'))->where('time', $request->time . ':00')->first();
            $total = $availability->price_per_adult * $request->adults + $availability->price_per_child * $request->children;
            $this->viewData['total'] = $total * 100;
            $user = Auth::user();
            $this->viewData['client_secret_intent'] = $user->createSetupIntent();
            $this->viewData['stripe_customer_id'] = $user->createOrGetStripeCustomer();
            $this->viewData['user'] = $user;
            $this->viewData['payment_methods'] = collect($user->paymentMethods());
            $this->viewData['locale'] = $locale;
            $lead = $this->getLead();
            $this->viewData['lead'] = $lead;
            return view('front.experience-checkout', $this->viewData);
        } else {
            return redirect(Route('register'));
        }
    }

    public function thankYouPageExperience(Request $request, $locale)
    {

        $this->getPagetypeMeta($this->viewData);

        $this->viewData['meta'] = [
            'title' => __('all.thank your for your purchase title'),
            'description' => __('all.thank your for your purchase description'),
            'canonical' => $this->viewData['currentPageUrl']
            #'cover' => collect($experience->photos)->first(),
        ];
        $lead = $this->getLead();
        $this->viewData['lead'] = $lead;
        $this->viewData['locale'] = $locale;
        return view('front.thank-you-page-experience', $this->viewData);
    }

    public function buyExperience(Request $request)
    {



        $experience = Experience::findOrFail($request->experience_id);
        $user = Auth::user();

        if (!$request->has('old_card')) {
            $user->addPaymentMethod($request->payment_method);
        }

        $user->updateDefaultPaymentMethod($request->payment_method);

        $day = Carbon::createFromFormat('m/d/Y', $request->date);
        $availability = $experience->availabilities()->where('day', $day->format('Y-m-d'))->where('time', $request->time . ':00')->first();



        $total = $availability->price_per_adult * $request->adults + $availability->price_per_child * $request->children;

        #$price = $item->applyPlanDiscount($request->model, $total); #apply plan discount

        $price = $total;

        if ($price > 0) {
            $user->invoiceFor('MamaBlip Experience :' . $experience->name, $price);
        }

        $lead = $this->getLead();
        $this->viewData['lead'] = $lead;

        if ($request->ajax()) {
            return ['status' => 1];
        } else {
            return redirect(Route('thankYouPageExperience', ['locale' => $request->locale]));
        }
    }

    public function course($locale, $name, $id)
    {
        #

        $this->viewData['page_type'] = 'course';

        $this->getPagetypeMeta($this->viewData);

        $training = Training::with([
            'levels', 'levels.blocks', 'levels.blocks.modules'
        ])->findOrFail($id);

        $this->viewData['meta'] = [
            'title' => (Str::length($training->name) > 60) ? $training->name : $training->name.' | MaMaBlip' ,
            'description' => $training->subtitle,
            'cover' => collect($training->photos)->first(),
            'canonical' => $training->makeUrl(),
            'x-default-hreflang' => $training->makeUrl('en')
        ];

        $this->viewData['training'] = $training;

        $this->viewData['no_index'] = 1;
        $lead = $this->getLead();
        $this->viewData['lead'] = $lead;
        $this->viewData['altLangs'] = $training->getAlternateLangUrls();

        if ($training->is_free) {
            return view(
                'front.training',
                $this->viewData
            );
        } else {
            if (Auth::check()) {
                $user = Auth::user();
                if ($user->purchased('training', $training->id)) {
                    return view('front.training', $this->viewData);
                } else {
                    if ($user->canPurchase('trainings')) {
                        $this->viewData['canPurchase'] = true;
                    }
                }
            }
            return view('front.training_teaser', $this->viewData);
        }
    }

    public function courses($locale)
    {
        $this->getPagetypeMeta($this->viewData);
        $this->viewData['no_index'] = 1;
        $this->viewData['trainings'] = Training::all();
        $lead = $this->getLead();
        $this->viewData['lead'] = $lead;
        return view('front.trainings', $this->viewData);
    }

    public function quizz($locale, $name, $id)
    {

        $this->getPagetypeMeta($this->viewData);
        $this->viewData['page_type'] = 'quizz';

        $quizz = Quizz::findOrFail($id);
        $this->viewData['quizz'] = $quizz;
        $lead = $this->getLead();
        $this->viewData['altLangs'] = $quizz->getAlternateLangUrls();
        $this->viewData['lead'] = $lead;
        return view('front.quizz', $this->viewData);
    }

    public function quizzSubmit(Request $request)
    {


        $quizz = Quizz::find($request->input('quizz_id'));
        $pointsPerQuestion = 100 / $quizz->questions->count();
        $replies = $request->input('question');
        $results = [];
        $correctAnswers = 0;
        foreach ($quizz->questions as $question) {
            if ($question->type_id == 1) {
                $result = (collect(collect((array)$replies[$question->id])->intersect($question->correctAnswer->pluck('id'))->all())->count() > 0) ? true : false;
            }
            if ($question->type_id == 2) {
                $result = (collect(collect((array)$replies[$question->id])->intersect($question->correctAnswer->pluck('id'))->all())->count() == $question->correctAnswer->count() && $question->correctAnswer->count() == collect((array)$replies[$question->id])->count()) ? true : false;
            }
            if ($result) {
                $correctAnswers++;
            }
            $results[$question->id] = ['status' => $result, 'correct_answers' => $question->correctAnswer->pluck('id')->toArray(), 'old' => $replies[$question->id]];
        }

        $exam_passed = ($correctAnswers > ($quizz->questions->count() * 0.6)) ? true : false;

        $score = 100 * $correctAnswers / $quizz->questions->count();

        $flash = ['questions' => $results, 'exam_passed' => $exam_passed, 'score' => $score];

        Auth::user()->quizzs()->attach($quizz->id, ['score' => $score, 'result' => json_encode($results), 'created_at' => date('Y-m-d H:i:s')]);
        $lead = $this->getLead();
        $this->viewData['lead'] = $lead;
        $request->session()->flash('results', $flash);
        return back();
    }


    public function purchaseContent(Request $request, $model, $id)
    {



        $user = Auth::user();
        $content = Str::plural($model);
        $model = Str::ucfirst($model);

        if ($user->canPurchase($content, $id)) {

            $user->purchase($model, $id);

            $model = 'App\Models\\' . $model;
            $item = $model::findOrFail($id);
            return redirect($item->makeUrl());
        } else {
            return 'error';
        }
    }

    public function manufacturer(Request $request, $locale, $name, $id=null)
    {

        #dd(func_get_args());

        $this->getPagetypeMeta($this->viewData);



        $legacyUrl = 1;
        if (is_null($id)) {
            $legacyUrl = 0;
            $id = collect(explode('-', $name))->last();
        }

        $manufacturer = Manufacturer::with([
            'products' => function ($query) {
                $query->whereEnabled(1);
            },
            'consortiums',
            'articles',
            'videos'
        ])
        ->whereEnabled(1);

        if ($legacyUrl) {
            $manufacturer = $manufacturer->findOrFail($id);
            return redirect($manufacturer->makeUrl(), 301);
        } else {
            $manufacturer = $manufacturer->where('url->' . $locale, $name)->firstOrFail();
        }

        #$manufacturer->checkUrl($name);

        $this->viewData['meta'] = [


            'cover' => collect($manufacturer->photos)->first(),
            'canonical' => $manufacturer->makeUrl(),
            'title' => $manufacturer->metatitle . ' | MaMaBlip',
            'description' => $manufacturer->meta_description,
            'h1' => $manufacturer->h1,
            'x-default-hreflang' => $manufacturer->makeUrl('en')
        ];

        $this->viewData['experiences'] = $manufacturer->experiences()->whereEnabled(1)->get();

        $this->viewData['manufacturer'] = $manufacturer;

        $this->viewData['sponsors'] = [$manufacturer->name];
        $this->viewData['altLangs'] = $manufacturer->getAlternateLangUrls();

        $lead = $this->getLead();
        $this->viewData['lead'] = $lead;
        $this->viewData['page_type'] = 'manufacturer';

        $manufacturer->views++;
        $manufacturer->saveWithoutEvents();

        if($manufacturer->customizable_layout){
            $filePath = 'js/vvveb/my-pages/'.Str::slug($manufacturer->name).'-'.$locale;
            #$filePath = 'js/vvveb/'.ucfirst(Str::slug($manufacturer->name)).'-'.$locale;
            $page = file_get_contents(public_path($filePath));

            $page = str_replace(['<section','/section>'],['<div','/div>'],$page);

            $dom = new \DOMDocument();
            $dom->loadHTML($page);
            $el = collect($dom->getElementsByTagName('body'));
            $node = $el->first();
            $style = $node->ownerDocument->saveHTML($dom->getElementById('vvvebjs-styles'));
            $this->viewData['pageContent'] = $node->ownerDocument->saveHTML($node).$style;


            #exit;
        }

        return view('front.manufacturer', $this->viewData);
    }

    public function consortium(Request $request, $locale, $name, $id=null)
    {

        $this->viewData['page_type'] = 'consortium';

        $this->getPagetypeMeta($this->viewData);

        $legacyUrl = 1;
        if (is_null($id)) {
            $legacyUrl = 0;
            $id = collect(explode('-', $name))->last();
        }

        $consortium = Consortium::with([
            'manufacturers',
            'products' => function ($query) {
                $query->whereEnabled(1);
            }
        ]);

        if ($legacyUrl) {
            $consortium = $consortium->findOrFail($id);
            return redirect($consortium->makeUrl(), 301);
        } else {
            $consortium = $consortium->where('url->' . $locale, $name)->firstOrFail();
        }

        #$consortium->checkUrl($name);

        $this->viewData['meta'] = [
            'title' => $consortium->metatitle.' | MaMaBlip',
            'description' => $consortium->meta_description,
            'h1' => $consortium->h1,
            'cover' => collect($consortium->photos)->first(),
            'canonical' => $consortium->makeUrl(),
            'x-default-hreflang' => $consortium->makeUrl('en')
        ];

        $this->viewData['consortium'] = $consortium;

        $this->viewData['sponsors'] = [$consortium->name];
        $this->viewData['altLangs'] = $consortium->getAlternateLangUrls();

        $lead = $this->getLead();
        $this->viewData['lead'] = $lead;

        $consortium->views++;
        $consortium->saveWithoutEvents();

        if ($consortium->customizable_layout) {
            $filePath = 'js/vvveb/my-pages/' . Str::slug($consortium->name) . '-' . $locale;
            #$filePath = 'js/vvveb/'.ucfirst(Str::slug($manufacturer->name)).'-'.$locale;
            $page = file_get_contents(public_path($filePath));

            $page = str_replace(['<section', '/section>'], ['<div', '/div>'], $page);

            $dom = new \DOMDocument();
            $dom->loadHTML($page);
            $el = collect($dom->getElementsByTagName('body'));
            $node = $el->first();
            $style = $node->ownerDocument->saveHTML($dom->getElementById('vvvebjs-styles'));
            $this->viewData['pageContent'] = $node->ownerDocument->saveHTML($node) . $style;


            #exit;
        }


        return view('front.consortium', $this->viewData);

    }

    public function articlecategory($locale, $name, $id=null)
    {
        $this->getPagetypeMeta($this->viewData);

        $this->viewData['page_type'] = 'articlecategory';


        if (is_null($id)) {
            $articlecategory = Articlecategory::where('url->' . $locale, $name)->firstOrFail();
        }else{
            $articlecategory = Articlecategory::findOrFail($id);
            return redirect($articlecategory->makeUrl(), 301);
        }

        #$articlecategory->checkUrl($name);

        $blog = $articlecategory->articles()->whereNotNull('published_at')->where('enabled',1)->where('published_at', '<=', date('Y-m-d'))->orderBy('published_at', 'DESC')->get()->groupBy(function ($item, $key) {
            return $item->published_at->format('F \'y');
        });;
        #dd($blog);
        $this->viewData['articlecategory'] = $articlecategory;
        $this->viewData['blog'] = $blog;
        $this->viewData['meta'] = [
            'canonical' => $articlecategory->makeUrl(),
            'title' => $articlecategory->metatitle . ' | MaMaBlip',
            'description' => $articlecategory->meta_description,
            'h1' => $articlecategory->h1,
            'x-default-hreflang' => $articlecategory->makeUrl('en')
        ];
        $lead = $this->getLead();
        $this->viewData['altLangs'] = $articlecategory->getAlternateLangUrls();
        $this->viewData['lead'] = $lead;
        return view('front.articlecategory', $this->viewData);
    }

    public function removePaymentMethod(Request $request)
    {
        $request->validate([
            'id' => 'required'
        ]);
        if (Auth::check()) {
            $paymentMethod = Auth::user()->findPaymentMethod($request->id);
            $paymentMethod->delete();
            return back();
        }
        return back(); #with errors
    }

    public function deleteUserData(Request $request)
    {
        if (Auth::check()) {
            $request->validate([
                'locale' => 'required'
            ]);
            if ($request->passphrase != 'DELETE MY DATA') {
                return back();
            } else {
                $user = BackpackUser::findOrFail(Auth::id());
                $user->delete();
                Auth::logout();
                return redirect(url('/' . $request->locale));
            }
        }
        return back(); #with errors
    }


    public function scoutImportProducts()
    {
        #$status = shell_exec("php artisan scout:import App\Models\Product");
        $status = Artisan::call('scout:import', ["model" => "App\\Models\\Product"]);
        return ['status' => $status];
    }

    public function leaveAReview(Request $request){

        $request->validate([
            'rating' => 'numeric|required',
        ]);

        Auth::user()->reviews()->create(
            collect($request->all())->except('_token')->toArray()
        );

        if(!$request->ajax()){
            return back();
        }else{
            return ['message' => __('all.review success')];
        }

    }

    public function landingpage(Request $request,$locale,$name,$id){

        $this->getPagetypeMeta($this->viewData);

        $lp = Landingpage::findOrFail($id);

        $view = (!is_null($lp->view)) ? 'front.landingpages.'. $lp->view : 'front.landingpages.default';
        $lead = $this->getLead();


        $this->viewData['lp'] = $lp;
        $this->viewData['lead'] = $lead;

        return view($view, $this->viewData);
    }

    public function deals(Request $request, $locale)
    {
        $lead = $this->getLead();
        $this->viewData['lead'] = $lead;

        $this->getPagetypeMeta($this->viewData);
        $now = date('Y-m-d H:i:s');
        $aWeekAgo = date('Y-m-d H:i:s',strtotime('-7days'));



        $activeDeals = Deal::where('active',1)
        ->where('starts_at','<=', $now)
        ->where('ends_at','>=', $now)
        ->where('active',1)
        ->orderBy('starts_at','DESC')
        ->get();

        $futureDeals = Deal::where('active',1)
        ->where('starts_at','>', $now)
        ->where('active',1)
        ->orderBy('starts_at','DESC')
        ->get();

        $expiredDeals = Deal::where('active',1)
        ->where('ends_at','<', $now)
        ->where('active',1)
        ->orderBy('starts_at','DESC')
        ->get();

        $this->viewData['deals'] = collect([
            'active' => $activeDeals,
            'future' => $futureDeals,
            'expired' => $expiredDeals
        ]);

        $this->viewData['page_type'] = 'home';

        $this->viewData['altLangs'] = [
            'urls' => [
                'it' => url('it/deals') ,
                'en' => url('en/deals') ,
                'en-us' => url('en/deals'),

            ],
            'langMap' => ['en' => 'x-default', 'it' => 'it-it'],
        ];

        $this->viewData['meta'] = [
            'title' => Setting::get('deals_title_' . $locale),
            'description' => Setting::get('deals_description_' . $locale),
            'canonical' => url($locale.'/deals'),
            'x-default-hreflang' => url('en')
        ];





        return view('front.deals', $this->viewData);
    }

    public function deal(Request $request, $locale,$name,$id=null){

        $guest = $this->getGuest();
        $lead = $this->getLead();

        if ($request->has('country_id')) { #country changing
            if ($guest->country_id != $request->country_id) {
                $guest->country_id = $request->country_id;
                $guest->save();
                $lead->empty();
            }
        }



        $this->viewData['lead'] = $lead;

        $this->getPagetypeMeta($this->viewData);

        #dd($this->viewData);

        $guest = $this->getGuest();

        $deal = Deal::with([
            'products',
            'products.prices' => function($query) use($guest){
                $query
                ->where('country_id', $guest->country_id)
                ->where('price','>',0);
            },
            'user',
            'products.products'
        ]);

        $legacyUrl = 1;

        if (is_null($id)) {
            $legacyUrl = 0;
            $id = collect(explode('-', $name))->last();
        }

        if ($legacyUrl) {
            $deal = $deal->findOrFail($id);
            return redirect($deal->makeUrl(), 301);
        } else {

            $deal = $deal->where('url->' . $locale, $name)->firstOrFail();

            #dd($product->photos);
            #dd($product);
        }


        #$deal->checkUrl($name);

        #dd($deal);

        $this->viewData['altLangs'] = $deal->getAlternateLangUrls();
        $this->viewData['deal'] = $deal;
        $this->viewData['lastedPercentage'] = $deal->lastedPercentage();
        $this->viewData['productionMonthEnd'] = date('Y/m/d', strtotime($deal->ends_at));
        $this->viewData['meta'] = [
            'title' => (Str::length($deal->name) > 60) ? $deal->name  : $deal->name . ' | MaMaBlip',
            'description' => $deal->short_description,
            'cover' => collect($deal->photos)->first(),
            'canonical' => $deal->makeUrl(),
            'x-default-hreflang' => $deal->makeUrl('en')
        ];

        try{
            $this->viewData['altLangs'] = [
                'urls' => [
                    'it' => url($deal->getTranslations()['url']['it']),
                    'en' => url($deal->getTranslations()['url']['en'])
                ],
                'langMap' => ['en' => 'x-default', 'it' => 'it-it']
            ];
        }catch(Exception $e){

        }




        return view('front.deal', $this->viewData);
    }

    public function viewsDashboard(Request $request){
        $locale = 'en';
        if($request->has('locale')){
            #dd($request->all());
            App::setLocale($request->locale);
            $locale = $request->locale;
        }
        $data = [
            'stats' => [
                'Products' => Product::select('url', 'id', 'views', 'name')->orderBy('views', 'desc')->get(),
                'Videos' => Video::select('url', 'id', 'views', 'name')->orderBy('views', 'desc')->get(),
                'Recipes' => Recipe::select('url', 'id', 'views', 'name')->orderBy('views', 'desc')->get(),
                'Manufacturers' => Manufacturer::select('url', 'id', 'views', 'name')->orderBy('views', 'desc')->get(),
                'Consortiums' => Consortium::select('url', 'id', 'views', 'name')->orderBy('views', 'desc')->get(),
                'Articles' => Article::select('url', 'id', 'views', 'name')->orderBy('views', 'desc')->get(),
            ],
            'locale' => $locale
        ];

        return view('admin_custom.viewsDashboard',$data);
    }

    public function dealSubscription(Request $request,$locale){
        $lead = $this->getLead();

        $this->viewData['lead'] = $lead;
        $dealSubscription = Dealsubscription::create($request->all());
        $this->getPagetypeMeta($this->viewData);
        $this->viewData['deal'] = Deal::find($request->deal_id);
        $this->viewData['return_url'] = Route('deals');
        return view('front.thank-you-page-deal-subscription',$this->viewData);
    }


    public function MailgunWebhooks(Request $request){
        MailgunWebhook::create(['body' => $request->all()]);
    }

    public function cms($lang, $name, $id){

        $cms = Cms::findOrFail($id);
        $lead = $this->getLead();

        $this->getPagetypeMeta($this->viewData);

        $this->viewData['lead'] = $lead;
        $this->viewData['page_type'] = 'article';
        $this->viewData['cms'] = $cms;

        return view('front.cms', $this->viewData);
    }

    public function trackLinkClick(Request $request){

        #$request->validate();

        $is_external = 1;

        Linkclick::create([
            'clicked_link'  => $request->clicked_link,
            'clicked_from'  => $request->clicked_from,
            'is_external' => $is_external,
            'remote_ip' => request()->ip(),

        ]);
    }

    public function vvveb(){
        return view('vvveb');
    }

    public function vvvebPreview(Request $request, $locale)
    {
        $this->getPagetypeMeta($this->viewData);
        $lead = $this->getLead();
        $this->viewData['lead'] = $lead;

        $filePath = 'js/vvveb/my-pages/'.$request->filename;
        #$filePath = 'js/vvveb/' . $request->filename;
        $page = file_get_contents(public_path($filePath));

        $page = str_replace(['<section', '/section>'], ['<div', '/div>'], $page);

        $dom = new \DOMDocument();
        $dom->loadHTML($page);
        $el = collect($dom->getElementsByTagName('body'));
        $node = $el->first();
        $style = $node->ownerDocument->saveHTML($dom->getElementById('vvvebjs-styles'));

        $this->viewData['pageContent'] = $node->ownerDocument->saveHTML($node). $style;



        return view('vvveb-preview',$this->viewData);
    }



}
