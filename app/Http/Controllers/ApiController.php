<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    //
    public function articles(Request $request, $lang){
        return Article::whereNotNull('published_at')
            ->where('published_at', '<=', date('Y-m-d'))
            #->orderBy('lft', 'ASC')
            ->orderBy('published_at', 'DESC')
            ->whereEnabled(1)
            ->get(['name','subtitle','photos','brief']);

    }
}
