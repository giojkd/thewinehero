<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use OpenTok\OpenTok;
use OpenTok\MediaMode;
use OpenTok\ArchiveMode;

class TokBoxController extends Controller
{
    public function createSession(){
        $opentok = new OpenTok(env('TOKBOXAPIKEY'), '124c798ff0a2cbbeb4dacd5e0e9a7455e0e59a2f');
        $session = $opentok->createSession();
        $sessionId = $session->getSessionId();






// Start a live streaming broadcast of a session, using broadcast options

        $data = ['sessionId' => $sessionId];
        return view('tokbox.broadcast',$data);
/*

        $broadcast = $opentok->startBroadcast($sessionId);
        $options = array(
            'layout' => Layout::getBestFit(),
            'maxDuration' => 5400,
            'resolution' => '1280x720'
        );
        $broadcast = $opentok->startBroadcast($sessionId, $options);

// Store the broadcast ID in the database for later use

        $broadcastId = $broadcast->id;

        dd($broadcastId); */
    }

    public function subscribeSession(){
        $opentok = new OpenTok(env('TOKBOXAPIKEY'), '124c798ff0a2cbbeb4dacd5e0e9a7455e0e59a2f');
        $sessionId = '2_MX40NjU3MDkxMn5-MTU4NDYzMjcwNTM3NX44RTdwMFVkeVdWYmN1dW5iQVZieGtWNTl-UH4';
        $token = $opentok->generateToken($sessionId);
        $data = ['sessionId' => $sessionId,'token' => $token,'apiKey' => env('TOKBOXAPIKEY')];
        return view('tokbox.subscribe',$data);
    }
}
