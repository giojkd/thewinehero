<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;
use Laravel\Scout\Searchable;

class Event extends Model
{
    use CrudTrait;
    use HasTranslations;
    use \App\Traits\Helpers;
    use Searchable;

    protected $preauth = true;

    public function preauthOnly(){
        return (isset($this->preauth)) ? $this->preauth : false;
    }

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'events';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    protected $translatable = ['url','name','description_short','description','subtitle','custom_blocks','confirmation_notes', 'final_confirmation_notes', 'custom_icons'];
    protected $casts = [
      'images' => 'array',
      'custom_blocks' => 'array',
      'custom_icons' => 'array',
      #'starts_at_day' => 'date',
      #'starts_at_time' => 'date'

    ];
    protected $with = ['complexity'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */



    public function toSearchableArray(){


      $array = [ 'id' => $this->id ];

      $fieldsToImport = ['name','description_short','description'];
      foreach($fieldsToImport as $field){
        foreach(config('locales') as $locale){
          $array[$field.'_'.$locale] = $this->getTranslation($field,$locale);
        }
      }

      $array['user'] =  $this->user->name.' '.$this->user->surname;

      // Customize array...

      return $array;

    }

    function endsAtUnixTimestamp(){
        return date('YmdHis',strtotime($this->starts_at_day.' '.$this->starts_at_time.'+1'.$this->duration.'minutes'));
    }

    public function shouldBeSearchable(){
        return ($this->active && date('YmdHis',strtotime($this->starts_at_day.' '.$this->starts_at_time) >= date('YmdHis')));
    }

    public function sendFinalConfirmation(){

    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    function complexity(){
      return $this->belongsTo('App\Models\Complexity');
    }

    public function user(){
        return $this->belongsTo('App\Models\BackpackUser');
    }

    public function manufacturers()
    {
        return $this->belongsToMany('App\Models\Manufacturer');
    }
    public function consortiums()
    {
        return $this->belongsToMany('App\Models\Consortium');
    }

    public function products()
    {
        return $this->belongsToMany('App\Models\Product');
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    public function getNameWithDatetimeAttribute(){
        return $this->name.' '.date('d/m/Y',strtotime($this->starts_at_day)).' '.date('H:i',strtotime($this->starts_at_time));
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
