<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;
use Str;

class Training extends Model
{
    use CrudTrait;
    use HasTranslations;
    use \App\Traits\Helpers;
    use \Staudenmeir\EloquentHasManyDeep\HasRelationships;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'trainings';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    protected $translatable = ['name','subtitle','description','what_you_will_learn','requirements','welcome_message','congratulations_message'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];
    protected $casts = ['photos' => 'array','what_you_will_learn' => 'array'];
    protected $modelNameOverride = 'course';
    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function user(){
        return $this->belongsTo('App\Models\BackpackUser','user_id');
    }

    public function users(){
        return $this->belongsToMany('App\User','training_user');
    }

    public function levels(){
      return $this->hasMany('App\Models\Traininglevel');
    }

    public function videos(){
      return $this->hasManyDeep('App\Models\Video',['App\Models\Traininglevel','App\Models\Trainingblock','App\Models\Trainingmodule','trainingmodule_video']);
    }

    public function files(){
      return $this->hasManyDeep('App\Models\File',['App\Models\Traininglevel','App\Models\Trainingblock','App\Models\Trainingmodule','file_trainingmodule']);
    }

    public function quizzs(){
      return $this->hasManyDeep('App\Models\Quizz',['App\Models\Traininglevel','App\Models\Trainingblock','App\Models\Trainingmodule','quizz_trainingmodule']);
    }

    public function modules(){
      return $this->hasManyDeep('App\Models\Trainingmodule',['App\Models\Traininglevel','App\Models\Trainingblock']);
    }


    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
