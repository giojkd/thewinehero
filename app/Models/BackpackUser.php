<?php

namespace App\Models;

use App\User;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Backpack\CRUD\app\Models\Traits\InheritsRelationsFromParentModel;
use Backpack\CRUD\app\Notifications\ResetPasswordNotification as ResetPasswordNotification;
use Illuminate\Notifications\Notifiable;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;
use Spatie\Permission\Traits\HasRoles;
use Str;


class BackpackUser extends User {


    #use InheritsRelationsFromParentModel;
    use Notifiable;
    use CrudTrait; // <----- this
    use HasRoles; // <------ and this
    use HasTranslations;

    public $translatable = [

        'biography',
        'meta_title',
        'url',
        'h1',
        'metatitle',
        'meta_description',

        'block_1_title',
        'block_2_title',
        'block_3_title',
        'block_4_title',
        'block_5_title',

        'block_1_content',
        'block_2_content',
        'block_3_content',
        'block_4_content',
        'block_5_content'

    ];


    protected $table = 'users';

    public static $laracombee = ['name' => 'string','surname'=>'string','email'=>'string'];

    /**
     * Send the password reset notification.
     *
     * @param string $token
     *
     * @return void
     */
    public function sendPasswordResetNotification($token) {
        $this->notify(new ResetPasswordNotification($token));
    }

    /**
     * Get the e-mail address where password reset links are sent.
     *
     * @return string
     */
    public function getEmailForPasswordReset() {
        return $this->email;
    }

    public function setAvatarAttribute($value) {
        $attribute_name = "avatar";
        $disk = config('backpack.base.root_disk_name'); // or use your own disk, defined in config/filesystems.php
        $destination_path = "public/storage"; // path relative to the disk above
        // if the image was erased
        if ($value == null) {
            // delete the image from disk
            \Storage::disk($disk)->delete($this->{$attribute_name});

            // set null in the database column
            $this->attributes[$attribute_name] = null;
        }

        // if a base64 was sent, store it in the db
        if (Str::startsWith($value, 'data:image')) {
            // 0. Make the image
            $image = \Image::make($value)->encode('jpg', 90);
            // 1. Generate a filename.
            $filename = md5($value . time()) . '.jpg';
            // 2. Store the image on disk.
            \Storage::disk($disk)->put($destination_path . '/' . $filename, $image->stream());
            // 3. Save the public path to the database
            // but first, remove "public/" from the path, since we're pointing to it from the root folder
            // that way, what gets saved in the database is the user-accesible URL
            $public_destination_path = Str::replaceFirst('public/', '', $destination_path);
            $this->attributes[$attribute_name] = $public_destination_path . '/' . $filename;
        }
    }

    public function printFullName(){
      return $this->name.' '.$this->surname;
    }


    public function videos() {
        return $this->hasMany('\App\Models\Video', 'user_id');
    }

    public function articles() {
        return $this->hasMany('\App\Models\Article','user_id');
    }

    public function events() {
        return $this->hasMany('\App\Models\Event', 'user_id');
    }

    public function recipes() {
        return $this->hasMany('\App\Models\Recipe', 'user_id');
    }

    public function quizzs(){
      return $this->belongsToMany('\App\Models\Quizz', 'user_id');
    }

    public function trainings(){
      return $this->belongsToMany('\App\Models\Training','user_id');
    }

    public function purchases(){
        return $this->hasMany('App\Models\Purchase','user_id');
    }

    public function enrolled($training){
      return !is_null($this->trainings()->where('training_id',$training->id)->first());
    }

    public function getFullNameAttribute(){
        return Str::ucfirst($this->name.' '.$this->surname);
    }

    public function getFullNameWithContactsAttribute(){
        return Str::ucfirst($this->name . ' ' . $this->surname.' '.$this->telephone.' '.$this->email);
    }

    public function reviews(){
        return $this->hasMany(Review::class,'user_id');
    }

}
