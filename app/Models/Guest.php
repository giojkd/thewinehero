<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

use Recombee\RecommApi\Client;
use Recombee\RecommApi\Requests as Reqs;
use Recombee\RecommApi\Exceptions as Ex;

class Guest extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'guests';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function showLangSwitchBlock(){
        return (is_null($this->hide_lang_switch_block_until) || strtotime($this->hide_lang_switch_block_until) < time());
    }

    public function sendToRecombee()
    {

        $client = new Client(env('RECOMBEE_DB_ID'), env('RECOMBEE_DB_KEY'));
        $client->send(new Reqs\AddUser('user-' . $this->id));
    }

    public function getRecommendations($cfg){

        $filters = [];

        $n = isset($cfg['n']) ? $cfg['n'] : 5;
        $model = isset($cfg['model']) ? $cfg['model'] : [];

        $filters = ['filter' => "('model' == \"" . $model . "\") and ('enabled') and ('recommend_id' != null)"];

        $client = new Client(env('RECOMBEE_DB_ID'), env('RECOMBEE_DB_KEY'));

        $response = $client->send(new Reqs\RecommendItemsToUser('user-'.$this->id, $n, $filters));
        return $response;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function country(){
        return $this->belongsTo(Country::class);
    }

    public function leads(){
        return $this->hasMany(Lead::class);
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
