<?php

namespace App\Models;
use App;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;
use Str;
use Laravel\Scout\Searchable;
use App\Traits\Helpers;

class Consortium extends Model
{
    use CrudTrait;
    use HasTranslations;
    use Helpers;
    use Searchable;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'consortiums';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    protected $translatable = ['h1','metatitle','meta_description','url','name','custom_blocks', 'custom_icons','short_description', 'description', 'funded_at', 'surface', 'production', 'consortium_member', 'history_description', 'geography_description', 'production_description'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];
    protected $casts = ['address' => 'json','photos' => 'json', 'custom_blocks' => 'array', 'custom_icons' => 'array'];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function shouldBeSearchable()
    {
        return ($this->enabled);
    }

    public function toSearchableArray()
    {

        App::setLocale('it');


        $locales = array_keys(config('backpack.crud.locales'));

        $array['website'] = $this->website;

        foreach ($locales as $locale) {

            $array['name_' . $locale] = $this->getTranslation('name', $locale);
            $array['description_' . $locale] = $this->getTranslation('description', $locale);
            $array['short_description_' . $locale] = $this->getTranslation('short_description', $locale);

            $array['url_' . $locale] = $this->makeUrl($locale);
        }


        $cover = (!is_null($this->photos)) ? collect($this->photos)->first() : null;
        if (!is_null($cover)) {
            $cover =  Route('ir', ['size' => 'h480', 'filename' => $cover], false);
        }
        $array['cover'] = $cover;
        return $array;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function manufacturers(){
        return $this->belongsToMany('App\Models\Manufacturer');
    }


    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }

    public function articles()
    {
        return $this->belongsToMany(Article::class);
    }

    public function videos()
    {
        return $this->belongsToMany(Video::class);
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

        public function setLogoAttribute($value)
    {
        $attribute_name = "logo";
        $disk = config('backpack.base.root_disk_name'); // or use your own disk, defined in config/filesystems.php
        $destination_path = "public/storage"; // path relative to the disk above

        // if the image was erased
        if ($value==null) {
            // delete the image from disk
            \Storage::disk($disk)->delete($this->{$attribute_name});

            // set null in the database column
            $this->attributes[$attribute_name] = null;
        }

        // if a base64 was sent, store it in the db
        if (Str::startsWith($value, 'data:image'))
        {
            // 0. Make the image
            $image = \Image::make($value)->encode('png', 100);
            // 1. Generate a filename.
            $filename = md5($value.time()).'.png';
            // 2. Store the image on disk.
            \Storage::disk($disk)->put($destination_path.'/'.$filename, $image->stream());
            // 3. Save the public path to the database
            // but first, remove "public/" from the path, since we're pointing to it from the root folder
            // that way, what gets saved in the database is the user-accesible URL
            $public_destination_path = Str::replaceFirst('public/', '', $destination_path);
            $this->attributes[$attribute_name] = $public_destination_path.'/'.$filename;
        }
    }
}
