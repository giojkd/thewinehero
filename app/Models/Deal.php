<?php

namespace App\Models;

use App\User;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;
use Laravel\Scout\Searchable;


class Deal extends Model
{
    use CrudTrait;
    use \App\Traits\Helpers;
    use HasTranslations;
    use Searchable;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'deals';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    protected $with = ['products'];
    protected $translatable = ['name','short_description','description','url', 'custom_icons', 'boxes'];
    protected $casts = ['images' => 'json', 'custom_icons'=> 'json'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function products(){
        return $this->belongsToMany(Product::class);
    }

    public function user(){
        return $this->belongsTo(BackpackUser::class,'user_id');
    }

    public function video(){
        return $this->belongsTo(Video::class);
    }

    public function lastedPercentage(){
        $now = time();
        $begin = strtotime($this->starts_at);
        $end = strtotime($this->ends_at);
        $lasted = $now-$begin;
        $total = $end-$begin;
        $perc = $lasted*100/$total;
        return round($perc);
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function shouldBeSearchable()
    {
        return ($this->active);
    }

    public function searchableAs()
    {

        $indexName = $this->table;

        if (env('APP_ENV') == 'local') {
            $indexName .= '_local';
        }

        return $indexName;
    }



    public function hasStarted(){
        return (strtotime($this->starts_at) < time());
    }

    public function hasExpired(){
        $now = date('Y-m-d H:i:s');
        if(strtotime($this->ends_at) < time()){
            return true;
        }
        return false;

    }

    public function toSearchableArray(){
        $locales = array_keys(config('backpack.crud.locales'));

        foreach ($locales as $locale) {
            $array['name_' . $locale] = $this->getTranslation('name', $locale);
            $array['description_' . $locale] = $this->getTranslation('description', $locale);
            $array['short_description_' . $locale] = $this->getTranslation('short_description', $locale);
        }

        $bundles = $this->products;
        $productsArray = [];

        $productsInBundles = [];

        if($bundles->count() > 0){
            foreach($bundles as $bundle){
                $productsArray[] = $bundle->toSearchableArrayLight();
                foreach($bundle->products as $product){
                    $productsInBundles[] = $product->toSearchableArrayLight();
                }
            }
        }

        #$array['bundledProducts'] = $productsArray;
        $array['productsInBundles'] = $productsInBundles;


        return $array;
    }


    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
