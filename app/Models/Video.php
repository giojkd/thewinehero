<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;
use App\Traits\Helpers;
use Laravel\Scout\Searchable;
use Str;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
class Video extends Model
{
  use Searchable;
  use CrudTrait;
  use HasTranslations;
  use Helpers;
  /*
  |--------------------------------------------------------------------------
  | GLOBAL VARIABLES
  |--------------------------------------------------------------------------
  */

  protected $table = 'videos';
  // protected $primaryKey = 'id';
  // public $timestamps = false;
  protected $guarded = ['id'];
  protected $with = ['user'];
  protected $casts = ['created_at' => 'datetime'];
  protected $translatable = ['url','name','subtitle','description','body'];
  // protected $fillable = [];
  // protected $hidden = [];
  // protected $dates = [];

  /*
  |--------------------------------------------------------------------------
  | FUNCTIONS
  |--------------------------------------------------------------------------
  */

    public function addView(){
        $this->views++;
        $this->saveWithoutEvents();
    }

    public function searchableAs()
    {
      return 'videos';
    }

    public function toSearchableArray()
    {

        $indexableLocales = array_keys(Config('backpack.crud')['locales']);
        $fieldsToImport = ['name', 'subtitle', 'description'];
        $categoriesReturn = [];
        $array = [];

        $categories = $this->categories;

        if (!\is_null($categories)) {
            foreach ($categories as $category) {
                foreach ($indexableLocales as $locale) {
                    $categoriesReturn['category_' . $locale][] = $category->getTranslation('name', $locale);
                }
            }
        }

        foreach($indexableLocales as $locale){
            foreach ($fieldsToImport as $field) {
                    $array[$field . '_' . $locale] = trim(preg_replace('/\s\s+/', ' ', strip_tags($this->getTranslation($field, $locale))));
            }
        }

        if (isset($categoriesReturn)) {
            $array = array_merge($array, $categoriesReturn);
        }

        return $array;

    }

    public function shouldBeSearchable(){
       #return ( $this->enabled && !is_null($this->video));
       return ( $this->enabled);
    }

    /*
  |--------------------------------------------------------------------------
  | RELATIONS
  |--------------------------------------------------------------------------
  */

    public function podcasts(){
        return $this->morphToMany(Podcast::class, 'podcastable');
    }

    public function manufacturers()
    {
        return $this->belongsToMany('App\Models\Manufacturer');
    }
    public function consortiums()
    {
        return $this->belongsToMany('App\Models\Consortium');
    }

    public function videocategory(){
        return $this->belongsTo(Videocategory::class);
    }

    public function user(){
        return $this->belongsTo('App\User');
    }


    public function products()
    {
        return $this->belongsToMany('App\Models\Product');
    }


    public function setBodyAttribute($value)
    {

        $attributes = $this->attributes;

        $disk = config('backpack.base.root_disk_name');
        $destination_path = "public/storage";
        $value = JSON_DECODE($value, 1);

        #$body = (isset($attributes['body'])) ? JSON_DECODE($attributes['body'],1) : [];
        $body = [];


        $lang = isset($attributes['last_updated_lang']) ? $attributes['last_updated_lang'] : 'en';
        #$body[$lang] = [];



        if (count($value) > 0) {

            foreach ($value as $item) {

                // if the image was erased
                if ($item['image'] == null) {
                    Storage::disk($disk)->delete($item['image']);
                    $item['image'] = null;
                }

                if (Str::startsWith($item['image'], 'data:image')) {
                    $image = Image::make($item['image'])->encode('jpg', 90);
                    $filename = Str::slug($item['image_alt']) . '.jpg';
                    Storage::disk($disk)->put($destination_path . '/' . $filename, $image->stream());
                    $public_destination_path = Str::replaceFirst('public/', '', $destination_path);
                    $item['image'] = $public_destination_path . '/' . $filename;
                }

                $body[] = $item;
            }
        }



        #$this->setTranslation('body', $lang, json_encode($body));

        $this->attributes['body'] = json_encode($body);

        #dd($this->attributes);


    }

  /*
  |--------------------------------------------------------------------------
  | SCOPES
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | ACCESSORS
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | MUTATORS
  |--------------------------------------------------------------------------
  */
}
