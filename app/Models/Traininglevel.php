<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Traininglevel extends Model
{
  use CrudTrait;
  use HasTranslations;
  use \Staudenmeir\EloquentHasManyDeep\HasRelationships;
  /*
  |--------------------------------------------------------------------------
  | GLOBAL VARIABLES
  |--------------------------------------------------------------------------
  */

  protected $table = 'traininglevels';
  // protected $primaryKey = 'id';
  // public $timestamps = false;
  protected $guarded = ['id'];
  protected $translatable = ['name','subtitle','description'];
  // protected $fillable = [];
  // protected $hidden = [];
  // protected $dates = [];
  protected $casts = ['photos' => 'array'];

  /*
  |--------------------------------------------------------------------------
  | FUNCTIONS
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | RELATIONS
  |--------------------------------------------------------------------------
  */

  public function children(){
    return $this->belongsTo('App\Models\Category','id','parent_id');
  }
  public function training(){
    return $this->belongsTo('App\Models\Training');
  }

  public function blocks(){
    return $this->belongsTo('App\Models\Trainingblock');
  }


  public function modules(){
    return $this->hasManyDeep('App\Models\Trainingmodule',['App\Models\Trainingblock']);
  }


  public function videos(){
    return $this->hasManyDeep('App\Models\Video',['App\Models\Trainingblock','App\Models\Trainingmodule','trainingmodule_video']);
  }

  public function files(){
    return $this->hasManyDeep('App\Models\File',['App\Models\Trainingblock','App\Models\Trainingmodule','file_trainingmodule']);
  }

  public function quizzs(){
    return $this->hasManyDeep('App\Models\Quizz',['App\Models\Trainingblock','App\Models\Trainingmodule','quizz_trainingmodule']);
  }


  /*
  |--------------------------------------------------------------------------
  | SCOPES
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | ACCESSORS
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | MUTATORS
  |--------------------------------------------------------------------------
  */
}
