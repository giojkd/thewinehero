<?php

namespace App\Models;

use App\Traits\Helpers;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Str;

class Purchase extends Model
{
    use CrudTrait;
    use Helpers;
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'purchases';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];
    protected $casts = ['billing_address' => 'object', 'payment_intent' => 'object'];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    protected static function boot()
    {
        parent::boot();
        Purchase::updating(function ($model) {
            $model->final_confirmation_sent_at = null;
        });
    }

    public function getPurchaseType(){
        return collect(Str::of($this->purchaseable_type)->explode('\\'))->last();
    }

    public function purchaseable(){
        return $this->morphTo();
    }

    public function getPurchaseable(){
        $item = $this->purchaseable;
        return $item;
    }

    public function getUser(){
        $item = $this->user;
        return $item;
    }

    public function user(){
        return $this->belongsTo(BackpackUser::class);
    }


/*
    public function sendPurchaseConfirmation()
    {

        $qrCodeName = 'ticket_' . $this->id . '.png';
        $path = Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix() . 'tickets/' . $qrCodeName;
        (new QRCode)->render($this->id, $path);

        $base = $this->getNotificationStandardData(1);

        $base['event'] = $this->event;
        $base['ticket'] = $this;
        #        $base['qr_code'] = url('storage/tickets/'.$qrCodeName);
        $base['qr_code'] = config('app.url') . '/storage/tickets/' . $qrCodeName;


        $data['subject'] = 'Ecco il tuo biglietto!';
        $data['content'] = $base;

        #dd($data);

        $recipient = $this->user->email;

        $result = Mail::to($recipient)->send(new TicketPurchaseConfirmation($data));



        return $result;
    }
*/
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
