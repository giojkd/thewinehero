<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;
use Laravel\Scout\Searchable;
use App;
use Str;


class Product extends Model
{
    use CrudTrait;
    use HasTranslations;
    use \App\Traits\Helpers;
    use Searchable;
    use \Staudenmeir\EloquentHasManyDeep\HasRelationships;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'products';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    protected $casts = ['photos' => 'array', 'recommendations' => 'array', 'categories_list' => 'array','nphotos'=>'json'];
    protected $translatable = ['url','name', 'description', 'description_short'];
    protected $with = ['manufacturer', 'categories','prices'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function getIndexedPricesAttribute(){

        $prices = [];

        $product = $this;

        if (!is_null($product->prices)) {
            $product->prices->each(function ($price, $keyPrice) use (&$prices, $product) {
                $prices[$price->country_id][$price->quantity]['price'] = $price->price;
                $prices[$price->country_id][$price->quantity]['price_compare'] = $price->price_compare;
            });
        }

        return collect($prices);

    }

    public function shouldBeSearchable()
    {
        return ($this->enabled);
    }

    public function searchableAs()
    {

        $indexName = $this->table;

        if(env('APP_ENV') == 'local'){
            $indexName.='_local';
        }

        return $indexName;
    }

    ##############
    ### PRICES ###
    ##############

    public function hasPricesSet(){

        return ($this->prices->count() > 0);
    }

    public function getPrice($country_id,$type = 'price',$quantity = 1){

        $prices = $this->prices()->where('country_id', $country_id)->where('price','>',0)->get();




        if($prices->count() > 0){
            $prices = $prices->keyBy('quantity');

            if (isset($prices[$quantity])) {

                return $prices[$quantity][$type];

            }else{
                if(isset($prices[1][$type])){
                    return $prices[1][$type] * $quantity;
                }
                return 0;

            }

        }
        return 0;
    }

    ##############
    ##############
    ##############


    public function toSearchableArray()
    {

        App::setLocale('en');
        $attributes = [];
        $attrs = $this->categories;
        $locales = array_keys(config('backpack.crud.locales'));
        foreach ($locales as $locale) {
            $array['name_' . $locale] = $this->getTranslation('name', $locale);
            $array['description_' . $locale] = $this->getTranslation('description', $locale);
            $array['short_description_' . $locale] = $this->getTranslation('description_short', $locale);

            $array['url_' . $locale] = $this->makeUrl($locale);
            if ($attrs->count() > 0) {
                foreach ($attrs as $attr) {
                    if(is_object($attr->parent)){
                        $array[Str::snake($attr->parent->name . '_' . $locale)] = $attr->getTranslation('name', $locale);
                    }

                }
            }

            if(isset($this->producttype)){
                $array['producttype_'.$locale] = $this->producttype->getTranslation('name', $locale);
            }

            $array['brand_link_' . $locale] = '';

            if(!is_null($this->manufacturer)){
                $array['brand_link_'.$locale] = $this->manufacturer->makeUrl($locale);
                $array['brand_'.$locale] = $this->manufacturer->name;
            }
            if(!is_null($this->consortium)){
                $array['brand_link_'.$locale] = $this->consortium->makeUrl($locale);
                $array['brand_' . $locale] = $this->consortium->getTranslation('name', $locale);
            }

        }

        #$array['price'] = $this->price;
        #$array['price_compare'] = $this->price_compare;

        if($this->prices->count() > 0){
            foreach($this->prices as $price){
                $array['price_'.$price->country_id] = $price->price;
                $array['price_compare_'.$price->country_id] = $price->price_compare;
            }
        }else{
            $countries = Country::whereActive(1)->get();
            foreach($countries as $country){
                $array['price_'.$country->id] = 0;
                $array['price_compare_'.$country->id] = 0;
            }
        }

        $cover = (!is_null($this->photos)) ? collect($this->photos)->first() : null;
        if (!is_null($cover)) {
            $cover =  Route('ir', ['size' => 'h480', 'filename' => $cover], false);
        }
        $array['cover'] = $cover;

        $array['type'] = $this->type;

        $array['isOnDeal'] = ($this->isOnDeal()) ? 1 : 0;
        $array['currentDealUrl'] = ($this->isOnDeal()) ? $this->activeDeals->first()->makeUrl() : null;

        return $array;
    }

    public function toSearchableArrayLight(){

        App::setLocale('it');
        $array = [];
        $attrs = $this->categories;
        $locales = array_keys(config('backpack.crud.locales'));


        $array['isOnDeal'] = ($this->isOnDeal()) ? 1 : 0;
        $array['currentDealUrl'] = ($this->isOnDeal()) ? $this->activeDeals->first()->makeUrl() : null;

        foreach ($locales as $locale) {

            $array['name_' . $locale] = $this->getTranslation('name', $locale);
            $array['description_' . $locale] = $this->getTranslation('description', $locale);
            $array['short_description_' . $locale] = $this->getTranslation('description_short', $locale);



            if ($attrs->count() > 0) {
                foreach ($attrs as $attr) {
                    if (is_object($attr->parent)) {
                        $array[Str::snake($attr->parent->name . '_' . $locale)] = $attr->getTranslation('name', $locale);
                    }
                }
            }

            if (isset($this->producttype)) {
                $array['producttype_' . $locale] = $this->producttype->getTranslation('name', $locale);
            }

            if (!is_null($this->manufacturer)) {
                $array['brand_' . $locale] = $this->manufacturer->name;
            }
            if (!is_null($this->consortium)) {
                $array['brand_' . $locale] = $this->consortium->getTranslation('name', $locale);
            }

        }


        return $array;


    }

    public function getRecommendations()
    {
        if (!is_null($this->recommendations))
            return Product::whereIn('id', $this->recommendations)->get();
        return null;
    }

    public function getRecommendationsAttribute($value){
        if(!is_null($value)){
            $data = json_decode($value,1);
            if(isset($data['recommendations'])){
                return $data['recommendations'];
            }else{
                return [];
            }

        }
        return null;

    }

    public function getMaxQuantity(){
        return (!is_null($this->max_quantity) && $this->max_quantity > env('PRODUCT_MAX_QUANTITY_PER_ORDER')) ? $this->max_quantity : env('PRODUCT_MAX_QUANTITY_PER_ORDER');
    }

    public function isOnDeal(){
        return ($this->activeDeals()->get()->count() > 0) ? true : false;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */


    public function activeDeals(){
        return $this->deals()->where('deals.ends_at', '>', date('Y-m-d H:i:s'));
    }

    public function deals(){
        return $this->hasManyDeepFromRelations($this->bundles(), (new Product)->directDeals());

    }

    public function directDeals(){
        return $this->belongsToMany(Deal::class,'deal_product','product_id');
    }

    public function bundles(){
        return $this->belongsToMany(Product::class, 'bundles', 'bundled_product_id', 'product_id')->withPivot('quantity');
    }

    public function podcasts()
    {
        return $this->morphToMany(Podcast::class, 'podcastable');
    }

    public function getBundleQuantityOptions($country_id){
        return $this->prices()->where('country_id',$country_id)->where('price','>',0)->get()->keyBy('quantity')->keys()->sort()->toArray();
    }

    public function prices(){
        return $this->morphMany(Price::class,'priceable');
    }

    public function producttype(){
        return $this->belongsTo(Producttype::class);
    }

    public function manufacturers()
    {
        return $this->belongsToMany('App\Models\Manufacturer');
    }
    public function consortiums()
    {
        return $this->belongsToMany('App\Models\Consortium');
    }

    public function manufacturer()
    {
        return $this->belongsTo('App\Models\Manufacturer');
    }
    public function categories()
    {
        return $this->belongsToMany('App\Models\Category')->withPivot('id');
    }

    public function consortium()
    {
        return $this->belongsTo('App\Models\Consortium');
    }


    public function events()
    {
        return $this->belongsToMany('App\Models\Event');
    }

    public function articles(){
        return $this->belongsToMany('App\Models\Article');
    }

    public function videos(){
        return $this->belongsToMany('App\Models\Video');
    }

    public function recipes()
    {
        return $this->belongsToMany('App\Models\Recipe');
    }

    public function products(){
        return $this->belongsToMany(Product::class,'bundles','product_id','bundled_product_id')->withPivot('quantity','description');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
