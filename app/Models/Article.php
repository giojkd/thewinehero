<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;
use Laravel\Scout\Searchable;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Setting;
use Cion\TextToSpeech\Facades\TextToSpeech;

class Article extends Model
{
  use \Backpack\CRUD\app\Models\Traits\CrudTrait, \Venturecraft\Revisionable\RevisionableTrait;
  use Searchable;
  use CrudTrait;
  use HasTranslations;
  use \App\Traits\Helpers;
  /*
  |--------------------------------------------------------------------------
  | GLOBAL VARIABLES
  |--------------------------------------------------------------------------
  */

  protected $table = 'articles';
  // protected $primaryKey = 'id';
  // public $timestamps = false;
  protected $guarded = ['id'];
  protected $translatable = ['name','subtitle','content','brief','url','body','ntts_path','ntts_content'];
  protected $modelNameOverride = 'blog';

    public function identifiableName()
    {
        return $this->id;
    }

    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
  |--------------------------------------------------------------------------
  | FUNCTIONS
  |--------------------------------------------------------------------------
  */

    public function ntts(){

        $contents = $this->getTranslations();

        $ntts = $contents['ntts_content'];
        $title = $contents['name'];

        foreach($ntts as $lang => $content){

            $mapLang = [
                'en' => 'en-US',
                'it' => 'it-IT'
            ];

            $mapVoices = [
                'en' => 'Joey',
                'it' => 'Bianca'
            ];

            $options = [
                'voice' => $mapVoices[$lang],
                'engine' => 'neural'
            ];

            $titleSlug = Str::slug($title[$lang]);

            $path = $titleSlug . '.mp3';

            TextToSpeech::language($mapLang[$lang])
            ->disk('public')
            ->saveTo($path)
            ->convert($content,$options);

            $this
            ->setTranslation('ntts_path', $lang, '/storage/'.$path);

        }

        $this->is_ntts_processed = 1;
        $this->saveWithoutEvents();

    }

    public function searchableAs()
    {
        $indexName = $this->table;

        if (env('APP_ENV') == 'local') {
            $indexName .= '_local';
        }

        return $indexName;
    }

    public function splitBody($value)
    {
        return explode('; ', $value);
    }

    public function toSearchableArray()
    {

        #$indexableLocales = config('locales');
        #$indexableLocales = ['en'];
        $array = ['id' => $this->id];

        $indexableLocales = array_keys(Config('backpack.crud')['locales']);

        /*
    $categories = $this->categories;

    if(!\is_null($categories)){
      $categoriesReturn = [];
      foreach($categories as $category){
        foreach($indexableLocales as $locale){
          $categoriesReturn['category_'.$locale][] = $category->getTranslation('name', $locale );
        }
      }
    }
*/

        $locales = array_keys(config('backpack.crud.locales'));
        $attrs = $this->categories;

        foreach ($locales as $locale) {
            if ($attrs->count() > 0) {
                foreach ($attrs as $attr) {
                    if (is_object($attr->parent)) {
                        $array[Str::snake($attr->parent->name . '_' . $locale)] = $attr->getTranslation('name', $locale);
                    }
                }
            }
            $array['published_at_' . $locale] = date(Setting::get('date_format_' . $locale), strtotime($this->published_at));
        }

        $fieldsToImport = ['name', 'subtitle', 'brief', 'content'];
        foreach ($fieldsToImport as $field) {
            foreach ($indexableLocales as $locale) {
                $array[$field . '_' . $locale] = substr(trim(preg_replace('/\s\s+/', ' ', strip_tags($this->getTranslation($field, $locale)))), 0, 16000);
            }
        }

        /*
        foreach (config('locales') as $locale) {
            $contentExploded = explode('; ', trim(preg_replace('/\s\s+/', ' ', strip_tags($this->getTranslation('content', $locale)))));
            foreach ($contentExploded as $contentIndex => $content) {
                $array['content_part_' . $contentIndex . '_' . $locale] = $content;
            }
        }


    if(isset($categoriesReturn)){
      $array = array_merge($array, $categoriesReturn);
    }

    */

        foreach ($array as $index => $item) {
            if (!is_array($item)) {
                if (!mb_detect_encoding($item, ['UTF-8'], true)) {
                    $array[$index] = \utf8_encode($item);
                }
            }
        }

        $array['cover'] = url($this->cover(400));

        $array['editor'] = $this->user->name.' '.$this->user->surname;

        foreach (config('locales') as $locale) {
            $array['url_' . $locale] = $this->makeUrl($locale);
        }

        $array['published_at'] = date('Ymd', strtotime($this->published_at));

        return $array;
    }

  public function shouldBeSearchable(){
    return ($this->enabled && date('YmdHis',strtotime($this->published_at) >= date('YmdHis')));
  }


  public function cover(){
  if(!is_null($this->photos)){
      if(isset($this->photos[0])){
        if($this->photos[0] != ''){
            $path = parse_url(Route('ir', ['size' => 50, 'filename' => $this->photos[0]]));

            return $path['path'];
        }
      }
  }
  return null;
}
    /*
  |--------------------------------------------------------------------------
  | RELATIONS
  |--------------------------------------------------------------------------
  */


    public function podcasts()
    {
        return $this->morphToMany(Podcast::class, 'podcastable');
    }


    public function manufacturers()
    {
        return $this->belongsToMany('App\Models\Manufacturer');
    }
    public function consortiums()
    {
        return $this->belongsToMany('App\Models\Consortium');
    }

  public function tags(){
    return $this->belongsToMany('App\Models\Tag');
  }

  public function products(){
    return $this->belongsToMany('App\Models\Product');
  }

  public function user(){
    return $this->belongsTo('App\Models\BackpackUser');
  }

  public function sameCategoryArticles(){
    return $this->sameCategoryItems(9);
  }

  public function articleCategory(){
      return $this->belongsTo('App\Models\Articlecategory','articlecategory_id');
  }

    public function articlecategories()
    {
        return $this->belongsToMany('App\Models\Articlecategory');
    }

    public function setBodyAttribute($value){

        $attributes = $this->attributes;

        $disk = config('backpack.base.root_disk_name');
        $destination_path = "public/storage";
        $value = JSON_DECODE($value,1);

        #$body = (isset($attributes['body'])) ? JSON_DECODE($attributes['body'],1) : [];
        $body = [];


        $lang = isset($attributes['last_updated_lang']) ? $attributes['last_updated_lang'] : 'en';
        #$body[$lang] = [];



        if(count($value) > 0){

            foreach($value as $item){

                // if the image was erased
                if ($item['image'] == null) {
                    Storage::disk($disk)->delete($item['image']);
                    $item['image'] = null;
                }

                if (Str::startsWith($item['image'], 'data:image')) {
                    $image = Image::make($item['image'])->encode('jpg', 90);
                    $filename = Str::slug($item['image_alt']) . '.jpg';
                    Storage::disk($disk)->put($destination_path . '/' . $filename, $image->stream());
                    $public_destination_path = Str::replaceFirst('public/', '', $destination_path);
                    $item['image'] = $public_destination_path . '/' . $filename;
                }

                $body[] = $item;
            }

        }



        #$this->setTranslation('body', $lang, json_encode($body));

        $this->attributes['body'] = json_encode($body);

        #dd($this->attributes);


    }




  /*
  |--------------------------------------------------------------------------
  | SCOPES
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | ACCESSORS
  |--------------------------------------------------------------------------
  */
  /*
  public function getPhotosAttribute($value){

  $value = json_decode($value);

  return json_encode(collect($value)->map(function($photo){
  return 'storage/'.str_replace('storage/','',$photo);
})->toArray());

}

*/

    public function getCoverAttribute(){
        return $this->cover();
    }


/*
|--------------------------------------------------------------------------
| MUTATORS
|--------------------------------------------------------------------------
*/

/*

public function setPhotosAttribute($value)
{
$attribute_name = "photos";
$disk = "custom_public";
$destination_path = "";

$this->uploadMultipleFilesToDisk($value, $attribute_name, $disk, $destination_path);
}

*/

protected static function boot(){
  parent::boot();
  /*
  if(!backpack_user()->hasRole('Admin')){
  static::addGlobalScope('onlymine',function(Builder $query){
  $query -> where('user_id',backpack_user()->id);
});
}
*/
}

protected $casts = [
  'photos' => 'array',
  'published_at' => 'date',
  'created_at' => 'date',
  'updated_at' => 'date',
];
}
