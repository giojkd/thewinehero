<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Recombee\RecommApi\Client;
use Recombee\RecommApi\Requests as Reqs;
use Recombee\RecommApi\Exceptions as Ex;
use Illuminate\Support\Str;

class Recommend extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'recommends';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    function sendToRecombee(){

        $requests = array();
        $client = new Client(env('RECOMBEE_DB_ID'), env('RECOMBEE_DB_KEY'));

        $data = $this->getRecombeeData();

        $r = new Reqs\SetItemValues(
            $this->recommendable_id,
            $data,
            ['cascadeCreate' => true]
        );

        array_push($requests, $r);

        $result =  $client->send(new Reqs\Batch($requests));
        #dd($result);
    }

    function getRecombeeData(){
        $item = $this->recommendable;
        if(!is_null($item)){
            $model = $item->getModelName();
            $method = 'getRecombeeData'.Str::ucfirst($model);

            $data = $this->$method($item);

            unset($data['id']);

            if(isset($data['photos'])){
                $data['photos'] = collect($data['photos'])->transform(function($photo,$key){
                    return url($photo);
                })->toArray();
            }
            if(isset($data['description_it'])){
                $data['description_it'] = $this->cleanString($data['description_it']);
            }
            if (isset($data['description_en'])) {
                $data['description_en'] = $this->cleanString($data['description_en']);
            }

            $data['created_at'] = $this->isoDate($this->created_at);

            if(isset($data['user'])){
                unset($data['user']);
            }

            $data['model'] = $model;
            $data['enabled'] = $item->enabled;

            $data['recommend_id'] = $item->id;

            foreach($data as $index => $item){
                if(!is_array($item)){
                    if(!mb_detect_encoding($item, ['UTF-8'], true)){
                        $data[$index] = \utf8_encode($item);
                    }
                }
            }

            return $data;
        }
        return false;
    }

    function getRecombeeDataProduct($item){
        $data = $item->toSearchableArray();

        return $data;
    }

    function getRecombeeDataVideo($item){
        $data = $item->toSearchableArray();

        $data['reviews_avg'] = $item->reviews_avg;
        $data['reviews_count'] = $item->reviews_count;
        $data['views'] = $item->views;
        $data['is_premium'] = $item->is_premium;

        $data['reviews_avg'] = $item->reviews_avg;
        return $data;
    }

    function getRecombeeDataArticle($item){
        $data = $item->toSearchableArray();
        $data['photos'] = $item->photos;
        return $data;
    }

    function cleanString($string){
      return strip_tags(trim(preg_replace('/\s\s+/', ' ', $string)));
    }

    public function isoDate($date)
    {
        return date('Y-m-d', strtotime($date)) . 'T' . date('h:i:s', strtotime($date)) . '+01:00';
    }

    function getRecombeeDataEvent($item){

        $data = $item->toSearchableArray();
        $beginTimestamp = $item->starts_at_day . ' ' . $item->starts_at_time;
        $data['begins_at'] = $this->isoDate($beginTimestamp);
        $data['ends_at'] = $this->isoDate(date('Y-m-d H:i:s',strtotime($beginTimestamp.' +'.$item->duration.'minutes')));
        $data['duration'] = $item->duration;

        return $data;

    }

    function getRecombeeDataRecipe($item){

        $data = $item->toSearchableArray();



        return $data;
    }

    public function sendRecombeeInteraction($cfg){

        $cfg['user_id'] = 'user-'.$cfg['user_id'];
        $cfg['action'] = (isset($cfg['action'])) ? $cfg['action'] : 'ItemDetailView';

        $action = $cfg['action'];

        $method = 'ri'.$action;
        $this->$method($cfg);

    }

    public function riAddToCart($cfg){
        $client = new Client(env('RECOMBEE_DB_ID'), env('RECOMBEE_DB_KEY'));
        $r = new Reqs\AddCartAddition($cfg['user_id'], $this->id);

        try {
            $status = $client->send($r);
        } catch (\Recombee\RecommApi\Exceptions\ResponseException $e) {
        }
    }

    public function riItemDetailView($cfg){
        $client = new Client(env('RECOMBEE_DB_ID'), env('RECOMBEE_DB_KEY'));
        $r = new Reqs\AddDetailView($cfg['user_id'], $this->id);

        try {
            $status = $client->send($r);
        } catch (\Recombee\RecommApi\Exceptions\ResponseException $e) {
        }

    }

    public function riAddFavorite($cfg){
        $client = new Client(env('RECOMBEE_DB_ID'), env('RECOMBEE_DB_KEY'));
        $r = new Reqs\AddBookmark($cfg['user_id'], $this->id);

        try {
            $status = $client->send($r);
        } catch (\Recombee\RecommApi\Exceptions\ResponseException $e) {
        }

    }

    public function riRemoveFavorite($cfg)
    {

        $client = new Client(env('RECOMBEE_DB_ID'), env('RECOMBEE_DB_KEY'));
        $r = new Reqs\DeleteBookmark($cfg['user_id'], $this->id);

        try{
            $status = $client->send($r);
        }catch(\Recombee\RecommApi\Exceptions\ResponseException $e){

        }


    }

    public static function feedRecombee($console = false){

    }




    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    function recommendable(){
        return $this->morphTo();
    }



    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
