<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Recipe extends Model
{
  use CrudTrait;
  use Searchable;
  use HasTranslations;
  use \App\Traits\Helpers;
  /*
  |--------------------------------------------------------------------------
  | GLOBAL VARIABLES
  |--------------------------------------------------------------------------
  */

  protected $table = 'recipes';
  // protected $primaryKey = 'id';
  // public $timestamps = false;
  protected $translatable = ['url','video_link','name','subtitle','description', 'general_notes', 'preparation_time_notes', 'servings_notes','keywords'];
  protected $guarded = ['id'];
  protected $casts = ['photos' => 'array','recommendations' => 'array', 'product_pairings' => 'array'];
  protected $with = ['ingredients','complexity'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
  |--------------------------------------------------------------------------
  | FUNCTIONS
  |--------------------------------------------------------------------------
  */

    public function searchableAs()
    {

        $indexName = $this->table;

        if (env('APP_ENV') == 'local'
        ) {
            $indexName .= '_local';
        }

        return $indexName;
    }

    public function getRecommendations()
    {
        if (!is_null($this->recommendations))
            return Recipe::whereIn('id', $this->recommendations)->get();
        return null;
    }

    public function shouldBeSearchable(){
        return ($this->enabled);
    }

    /*
  |--------------------------------------------------------------------------
  | RELATIONS
  |--------------------------------------------------------------------------
  */

    public function podcasts()
    {
        return $this->morphToMany(Podcast::class, 'podcastable');
    }


    public function manufacturers()
    {
        return $this->belongsToMany('App\Models\Manufacturer');
    }
    public function consortiums()
    {
        return $this->belongsToMany('App\Models\Consortium');
    }

    public function videos()
    {
        return $this->belongsToMany(Video::class);
    }

  public function toSearchableArray(){
    $array = [ 'id' => $this->id ];

    $fieldsToImport = ['name','subtitle','description','servings_notes', 'preparation_time_notes', 'general_notes'];
    foreach($fieldsToImport as $field){
      foreach(config('locales') as $locale){
        $array[$field.'_'.$locale] = $this->getTranslation($field,$locale);
      }
    }
    foreach (config('locales') as $locale) {
        $array['ingredients_' . $locale][] = '';
    }

    $ingredients = $this->ingredients;
    if(!is_null($ingredients)){
      foreach ($ingredients as $key => $ingredient) {
        foreach(config('locales') as $locale){
          $array['ingredients_'.$locale][] = $ingredient->getTranslation('name',$locale);
        }
      }
    }

    $dishCourse = $this->dishcourse;
    if(!is_null($dishCourse)){


        foreach (config('locales') as $locale) {
            $array['dishcourse_' . $locale] = $dishCourse->getTranslation('name', $locale);
        }
    }

    $cuisine = $this->cuisine;
    if (!is_null($cuisine)) {
        foreach (config('locales') as $locale) {
            $array['cuisine_' . $locale] = $cuisine->getTranslation('name', $locale);
        }
    }

    $complexity = $this->complexity;
    if (!is_null($complexity)) {
        foreach (config('locales') as $locale) {
            $array['complexity_' . $locale] = $complexity->getTranslation('name', $locale);
        }
    }

    $array['servings'] = $this->servings;
    $array['calories'] = $this->calories;
    $array['preparation_time'] = $this->preparation_time;
    $array['cooking_time'] = $this->cooking_time;

    $array['cover'] = Route('ir', ['size' => 240, 'filename' => $this->photos[0]]);


    foreach (config('locales') as $locale) {
        $array['url_' . $locale] = $this->makeUrl($locale);
    }

    $array['updated_at'] = (int)date('Ymd',strtotime($this->updated_at));


    return $array;
  }

  function ingredients(){
    return $this->belongsToMany('App\Models\Ingredient','reciperows');
  }

  function recipeingredients(){
    return $this->hasMany('App\Models\Reciperow');
  }

  function complexity(){
    return $this->belongsTo('App\Models\Complexity');
  }
  function dishcourse(){
    return $this->belongsTo('App\Models\Dishcourse');
  }
  function cuisine(){
    return $this->belongsTo('App\Models\Cuisine');
  }
  function steps(){
    return $this->hasMany('App\Models\Recipestep');
  }

  function products(){
    return $this->belongsToMany('App\Models\Product')->withPivot('id');
  }

  public function user(){
    return $this->belongsTo('App\User');
  }

  public function ingredientslists(){
      return $this->belongsToMany('App\Models\Incredientslist', 'ingredientslist_recipe','recipe_id', 'ingredientslist_id');
  }


  /*
  |--------------------------------------------------------------------------
  | SCOPES
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | ACCESSORS
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | MUTATORS
  |--------------------------------------------------------------------------
  */
}
