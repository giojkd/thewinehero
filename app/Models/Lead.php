<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Mail;
use App\Mail\LeadConfirmation;

use App\Traits\Helpers;
use Mailgun\Mailgun;
use Exception;

class Lead extends Model
{
    use CrudTrait;
    use Helpers;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'leads';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];
    protected $casts = ['shipping_address' => 'array', 'billing_address' => 'array', 'shipping_address_json' => 'array'];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */



    public function sendConfirmationEmails(){

        try{

            $data['content'] = $this->getMailNotificationsDefaultData();
            $data['content']['order_id'] = $this->id;
            $data['content']['user_full_name'] = $this->shipping_address['name'] . ' ' . $this->shipping_address['surname'];
            $data['content']['lead'] = $this;
            $data['content']['rows'] = $this->products;
            $data['subject'] = 'Great!';

            $html = view('notifications.lead_confirmation', $data['content'])->render();

            $to = explode(',', env('ADMIN_ORDER_NOTIFICATION_EMAILS'));
            $to[] = $this->shipping_address['email'];

            $mg = Mailgun::create('693d18e720e98b4fc07a5ff497d30d33-d2cc48bc-b28a3f54');

            $status = $mg->messages()->send('mamablip.com', [
                'from'    => 'blip@mamablip.com',
                'to'      => $to,
                'subject' => $data['subject'],
                'html'    => $html
            ]);


            $statusArray =  [
                'id' => $status->getId(),
                'message' => $status->getMessage()
            ];

            $this->notification_sending_attempts++;

            if($statusArray['message'] == 'Queued. Thank you.'){
                $this->notification_sending_status = 1;
            }else{
                $this->notification_sending_status = -1;
            }

            $this->saveWithoutEvents();


            /*
            $this->notification_sending_attempts++;
            $this->notification_sending_status = 1;
            $this->saveWithoutEvents();
            return Mail::to($to)->send(new LeadConfirmation($data));
            */


        }catch(Exception $e){
            $this->notification_sending_attempts++;
            $this->notification_sending_status = -1;
            $this->saveWithoutEvents();
        }

#        dd($this->shipping_address);


    }

    public function calculateShippingCost(){

        /* Calculate shipping cost logic */

        ###################################

        $this->calculateTotals();
    }

    public function empty(){
        $this->products()->detach();
        $this->calculateTotals();
    }

    public function calculateTotals(){

        $products = $this->products;
        $productsTotal = 0;
        $totalWeight = 0;
        $country = $this->guest->country;

        if(!is_null($products)){
            foreach($products as $product){
                $totalWeight+=$product->pivot->quantity * $product->packaging_weight;
                $productsTotal+= $product->pivot->sub_total;
            }
        }


        $this->products_total = $productsTotal;
        $shippingTotal = $this->getShippingQuotationByWeight($country->iso_3166_2, $totalWeight)->first()['value'];

        $discountTotal = (!is_null($this->discount_total)) ? $this->discount_total : 0;
        $this->grand_total = $productsTotal + $shippingTotal - $discountTotal;
        $this->shipping_total = $shippingTotal;

        $this->save();

    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function guest(){
        return $this->belongsTo(Guest::class);
    }

    public function dumps(){
        return $this->hasMany('App\Models\Dump');
    }

    public function products(){
      return $this->belongsToMany('App\Models\Product')->withPivot([
            'created_at',
            'updated_at',
            'quantity',
            'merchant_id',
            'sub_total',
            'id'
        ]);
    }

    public function user(){
      return $this->belongsTo('App\Models\Backpackuser');
    }

    public function merchants(){
      return $this->belongsToMany('App\Models\Merchant');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
