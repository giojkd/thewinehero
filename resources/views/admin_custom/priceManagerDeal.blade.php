@extends('admin_custom.top_left')
@section('content')

<h2>Price manager</h2>

<table class="table table-hover table-striped">
    <thead>
        <tr>
            <th></th>
            @foreach ($countries as $country)
                <th>{{ $country->name }}</th>
            @endforeach

        </tr>
    </thead>
    <tbody>
        @foreach ($products as $product)
            <tr>
                <td>{{ $product->name }}</td>
                @foreach ($countries as $country)
                    <td>
                        <input
                        type="text"
                        class="form-control price-input"
                        data-type="price"
                        data-country_id="{{ $country->id }}"
                        data-product_id="{{ $product->id }}"
                        placeholder="Price"
                        value="@if (isset($prices[$product->id][$country->id]['price'])){{ $prices[$product->id][$country->id]['price'] }}@endif"
                        >
                        <input
                        type="text"
                        class="form-control price-input"
                        data-type="price_compare"
                        data-country_id="{{ $country->id }}"
                        data-product_id="{{ $product->id }}"
                        placeholder="Price compare"
                        value="@if (isset($prices[$product->id][$country->id]['price_compare'])){{ $prices[$product->id][$country->id]['price_compare'] }}@endif"
                        >
                    </td>
                @endforeach
            </tr>
        @endforeach
    </tbody>
</table>

@endsection

@push('after_scripts')

    <script>
        var currentVal = 0;
        $(function(){
            $('.price-input').focus(function(){
                currentVal = $(this).val();
            });
            $('.price-input').blur(function(){
                if($(this).val() != '' && $(this).val() != currentVal){
                    var data = $(this).data();
                    data._token = '{{ csrf_token() }}';
                    data.value = $(this).val();
                    $.post('{{ backpack_url('price-manager') }}',data,function(r){
                        if(r.status == 1){

                        }else{

                        }
                    });
                }
            })
        })
    </script>

@endpush

