@extends('admin_custom.top_left')
@section('content')

<a href="{{ Route('viewsDashboard') }}?locale=it">Italiano</a>
<a href="{{ Route('viewsDashboard') }}?locale=en">English</a>

<hr>

<h2>Views Dashboard</h2>
<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
    @foreach ($stats as $type => $metrics)
        <li class="nav-item" role="presentation">
            <a class="nav-link @if(Str::slug($type) == 'products') active @endif" id="pills-{{Str::slug($type)}}-tab" data-toggle="pill" href="#pills-{{Str::slug($type)}}" role="tab" aria-controls="pills-{{Str::slug($type)}}" aria-selected="true">{{$type}}</a>
        </li>
    @endforeach
</ul>
<div class="tab-content" id="pills-tabContent">


@foreach ($stats as $type => $metrics)
    <div class="tab-pane fade show @if(Str::slug($type) == 'products') active @endif " id="pills-{{Str::slug($type)}}" role="tabpanel" aria-labelledby="pills-{{Str::slug($type)}}-tab">
        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Url</th>
                    <th>Views</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($metrics as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->url }}</td>
                    <td>{{ $item->views }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endforeach
</div>


@endsection
