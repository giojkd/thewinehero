@extends('admin_custom.top_left')
@section('content')

<h2>Price manager</h2>

<table class="table table-hover table-striped">
    <thead>
        <tr>
            <th></th>
            @foreach ($countries as $country)
                <th>{{ $country->name }}</th>
            @endforeach

        </tr>
    </thead>
    <tbody>
        @foreach ($products as $product)
            <tr>
                <td>{{ $product->name }}</td>
                @foreach ($countries as $country)
                    <td class="productPricesContainer"
                        data-product_id="{{ $product->id }}"
                        data-country_id="{{ $country->id }}"
                    >


                            @for($quantity = 1; $quantity<= 10; $quantity++)
                                <div class="row mb-1">
                                    <div class="col">
                                        <input
                                        value="{{ $quantity }}"
                                        disabled
                                        class="form-control"
                                        type="text">
                                    </div>
                                    <div class="col">
                                        <input
                                        type="text"
                                        class="form-control price-input"
                                        data-type="price"
                                        data-country_id="{{ $country->id }}"
                                        data-product_id="{{ $product->id }}"
                                        data-quantity="{{ $quantity }}"
                                        placeholder="Price"
                                        value="@if(isset($product->indexed_prices[$country->id][$quantity]) && $product->indexed_prices[$country->id][$quantity] > 0) {{ $product->indexed_prices[$country->id][$quantity]['price'] }}@endif"
                                        >
                                    </div>
                                    <div class="col">
                                        <input
                                        type="text"
                                        class="form-control price-input"
                                        data-type="price_compare"
                                        data-country_id="{{ $country->id }}"
                                        data-product_id="{{ $product->id }}"
                                        data-quantity="{{ $quantity }}"
                                        placeholder="Price compare"
                                        value="@if(isset($product->indexed_prices[$country->id][$quantity]) && $product->indexed_prices[$country->id][$quantity] > 0) {{ $product->indexed_prices[$country->id][$quantity]['price_compare'] }}@endif"
                                        >
                                    </div>
                                </div>
                            @endfor




                    </td>
                @endforeach
            </tr>
        @endforeach
    </tbody>
</table>

@endsection

@push('after_scripts')

    <script>


        var currentVal = 0;
        $(function(){
            $('.price-input').focus(function(){
                currentVal = $(this).val();
            });
            $('.price-input').blur(function(){
                //if($(this).val() != '' && $(this).val() != currentVal){
                if($(this).val() != currentVal){
                    var data = $(this).data();
                    data._token = '{{ csrf_token() }}';
                    data.value = $(this).val();
                    $.post('{{ backpack_url('price-manager') }}',data,function(r){
                        if(r.status == 1){

                        }else{

                        }
                    });
                }
            })
        })
    </script>

@endpush

