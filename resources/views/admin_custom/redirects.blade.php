@extends('admin_custom.top_left')
@section('content')

    <form action="/redirects" method="POST">
        @csrf
        <div class="form-group">
            <textarea name="redirects" rows="100" class="form-control w-100">{{$redirects}}</textarea>
        </div>
        <button type="submit" class="btn btn-success btn-block mt-2">Save</button>
    </form>

@endsection
