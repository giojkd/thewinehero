<table class="table table-sm">
    <tr>
        <td><a target="_blank" href="{{ url('/en/vvveb-preview/?filename='.ucfirst(Str::slug($item->getTranslations()['name']['en'])))}}-en">En</a></td>
    </tr>
    <tr>
        <td><a target="_blank" href="{{ url('/it/vvveb-preview/?filename='.ucfirst(Str::slug($item->getTranslations()['name']['it'])))}}-it">It</a></td>
    </tr>
</table>

