@if(is_null($entry->final_confirmation_sent_at))
    <a href="javascript:void(0)"
    onclick="sendFinalConfirmation(this)"
    data-id="{{ $entry->getKey() }}"
    >
        Send Final Confirmation
    </a>
@else
    <small>Final Confirmation Sent At {{ $entry->final_confirmation_sent_at }}</small>
@endif

