@if(is_null($entry->paid_at))
    <a href="javascript:void(0)"
    onclick="chargePurchase(this)"
    data-id="{{ $entry->getKey() }}"
    >
        Charge now
    </a>
@else
    <small>Paid at {{ $entry->paid_at }}</small>
@endif

