<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->

{{-- <li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i
  class="fa fa-dashboard nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li> --}}
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon fa fa-edit"></i> Organizer</a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('language') }}'><i
                    class='nav-icon fa fa-language'></i> Languages</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('category') }}'><i
                    class='nav-icon fa fa-list-alt'></i>
                Categories</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('tag') }}'><i class='nav-icon fa fa-tags'></i>
                Tags</a>
        </li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('complexity') }}'><i
                    class='nav-icon fa fa-question'></i> Complexities</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('dishcourse') }}'><i
                    class='nav-icon fa fa-question'></i> Dishcourses</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('cuisine') }}'><i
                    class='nav-icon fa fa-question'></i> Cuisines</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('articlecategory') }}'><i
                    class='nav-icon fa fa-question'></i> Article Categories</a></li>
        <li class=nav-item><a class=nav-link href="{{ backpack_url('elfinder') }}"><i
                    class="nav-icon fa fa-files-o"></i><span>{{ trans('backpack::crud.file_manager') }}</span></a>
        </li>
        <li class='nav-item'><a class='nav-link' href='/translations' target="_blank"><i
                    class='nav-icon fa fa-question'></i> Supertool</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('setting') }}'><i
                    class='nav-icon la la-cog'></i> <span>Settings</span></a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('pagetype') }}'><i
                    class='nav-icon la la-list'></i> <span>Page types</span></a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('videocategory') }}'><i
                    class='nav-icon la la-question'></i> Video Categories</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('producttype') }}'><i
                    class='nav-icon la la-question'></i> Product types</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('country') }}'><i
                    class='nav-icon la la-question'></i> Countries</a></li>
            <li class='nav-item'><a class='nav-link' href='{{ backpack_url('landingpage') }}'><i
            class='nav-icon la la-question'></i> Landing Pages</a></li>
            <li class='nav-item'><a class='nav-link' href='{{ backpack_url('release') }}'><i class='nav-icon la la-question'></i>
        Releases</a></li>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('review') }}'><i class='nav-icon fa fa-question'></i>
        Reviews</a></li>
<li class='nav-item'><a class='nav-link' href='{{ Route('viewsDashboard') }}'><i class='nav-icon fa fa-question'></i>
        Views Dashboard</a></li>
    </ul>
</li>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('purchase') }}'><i class='nav-icon fa fa-dollar'></i>
        <span>Purchases</span></a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('landingpage') }}'><i
            class='nav-icon la la-question'></i> Landing Pages</a></li>
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon fa fa-group"></i> Authentication</a>
    <ul class="nav-dropdown-items">
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('user') }}"><i
                    class="nav-icon fa fa-user"></i>
                <span>Users</span></a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('role') }}"><i
                    class="nav-icon fa fa-group"></i>
                <span>Roles</span></a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('permission') }}"><i
                    class="nav-icon fa fa-key"></i> <span>Permissions</span></a></li>
    </ul>
</li>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('event') }}'><i
            class='nav-icon fa fa-graduation-cap'></i> Live Classes & Events</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('article') }}'><i
            class='nav-icon fa fa-file-text'></i>
        Articles</a></li>


<!-- Users, Roles, Permissions -->



<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon fa fa-group"></i> Training</a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('training') }}'><i
                    class='nav-icon fa fa-question'></i> Trainings</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('traininglevel') }}'><i
                    class='nav-icon fa fa-question'></i> Levels</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('trainingblock') }}'><i
                    class='nav-icon fa fa-question'></i> Blocks</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('trainingmodule') }}'><i
                    class='nav-icon fa fa-question'></i> Modules</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('file') }}'><i
                    class='nav-icon fa fa-question'></i> Files</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('video') }}'><i
                    class='nav-icon fa fa-question'></i> Videos</a></li>
        <li class="nav-item nav-dropdown">
            <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon fa fa-group"></i> Tests</a>
            <ul class="nav-dropdown-items">
                <li class='nav-item'><a class='nav-link' href='{{ backpack_url('quizz') }}'><i
                            class='nav-icon fa fa-question'></i> Quizzs</a></li>
                <li class='nav-item'><a class='nav-link' href='{{ backpack_url('question') }}'><i
                            class='nav-icon fa fa-question'></i> Questions</a></li>
                <li class='nav-item'><a class='nav-link' href='{{ backpack_url('availableanswer') }}'><i
                            class='nav-icon fa fa-question'></i> Available answers</a></li>
            </ul>
        </li>
    </ul>
</li>

<li class="nav-item nav-dropdown">
    <a href="#" class="nav-link nav-dropdown-toggle"><i class="nav-icon fa fa-book"></i> Cookings</a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('recipe') }}'><i
                    class='nav-icon fa fa-question'></i> Recipes</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('ingredient') }}'><i
                    class='nav-icon fa fa-question'></i> All the Ingredients</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('incredientslist') }}'><i
                    class='nav-icon fa fa-question'></i> Lists of ingredients</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('reciperow') }}'><i
                    class='nav-icon fa fa-question'></i> Recipe Ingredients</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('recipestep') }}'><i
                    class='nav-icon fa fa-question'></i> Recipe Steps</a></li>
    </ul>
</li>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('consortium') }}'><i
            class='nav-icon fa fa-question'></i> Consortia</a></li>












<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon fa fa-truck"></i> Manufacturers</a>

    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('manufacturer') }}'><i
                    class='nav-icon fa fa-question'></i> Manufacturers</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('product') }}'><i
                    class='nav-icon fa fa-question'></i> Products</a></li>
        <li class="nav-item nav-dropdown">
            <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon fa fa-motorcycle"></i> Experiences</a>
            <ul class="nav-dropdown-items">
                <li class='nav-item'><a class='nav-link' href='{{ backpack_url('experience') }}'><i
                            class='nav-icon fa fa-question'></i> Experiences</a></li>
                <li class='nav-item'><a class='nav-link' href='{{ backpack_url('availability') }}'><i
                            class='nav-icon fa fa-question'></i> Availabilities</a></li>
                <li class='nav-item'><a class='nav-link' href='{{ backpack_url('pricerule') }}'><i
                            class='nav-icon fa fa-question'></i> Price Rules</a></li>
                <li class='nav-item'><a class='nav-link' href='{{ route('availabilityBulkGet') }}'><i
                            class='nav-icon fa fa-question'></i> Availability Bulk</a></li>
            </ul>

        </li>
    </ul>
</li>


<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon fa fa-truck"></i> eCommerce</a>
    <ul class="nav-dropdown-items">
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('bundle') }}'><i
                    class='nav-icon fa fa-question'></i> Bundles</a></li>
<li class='nav-item'><a target="_blank" class='nav-link' href='{{ Route('artisanScoutImportProducts') }}'><i
            class='nav-icon la la-question'></i> Importa prodotti su algolia</a></li>

         <li class='nav-item'><a class='nav-link' href='{{ backpack_url('lead') }}'><i
                    class='nav-icon fa fa-question'></i> Orders</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('lead') }}?all=true'><i
                    class='nav-icon fa fa-question'></i> Leads</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('deal') }}'><i class='nav-icon la la-question'></i>
        Deals</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('price-manager') }}'><i
            class='nav-icon la la-question'></i> Price Manager</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('merchant') }}'><i
                    class='nav-icon fa fa-question'></i> Merchants</a></li>
        <li class="nav-item nav-dropdown">
            <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon fa fa-group"></i> Subscriptions</a>
            <ul class="nav-dropdown-items">
                <li class='nav-item'><a class='nav-link' href='{{ backpack_url('plan') }}'><i
                            class='nav-icon fa fa-question'></i> Plans</a></li>
                <li class='nav-item'><a class='nav-link' href='{{ backpack_url('enrollmentoption') }}'><i
                            class='nav-icon fa fa-question'></i> Enrollmentoptions</a></li>
                <li class='nav-item'><a class='nav-link' href='{{ backpack_url('enrollmentoptiongroup') }}'><i
                            class='nav-icon fa fa-question'></i> Enrollment Option Groups</a></li>

            </ul>
        </li>

    </ul>
</li>












<li class='nav-item'><a class='nav-link' href='{{ backpack_url('podcast') }}'><i class='nav-icon la la-question'></i> Podcasts</a></li>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('dealsubscription') }}'><i class='nav-icon la la-question'></i> Dealsubscriptions</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('cms') }}'><i class='nav-icon la la-question'></i> Cms</a></li>
<li class='nav-item'><a class='nav-link' href='/redirects'><i class='nav-icon la la-question'></i> Redirects</a></li>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('linkclick') }}'><i class='nav-icon la la-question'></i> Linkclicks</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('banner') }}'><i class='nav-icon la la-question'></i> Banners</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('vvveb') }}'><i class='nav-icon la la-edit'></i> Landing Pages Builder</a></li>
