@extends(backpack_view('layouts.top_left'))

@php
$defaultBreadcrumbs = [
  trans('backpack::crud.admin') => url(config('backpack.base.route_prefix'), 'dashboard'),
  $crud->entity_name_plural => url($crud->route),
  trans('backpack::crud.add') => false,
];

// if breadcrumbs aren't defined in the CrudController, use the default breadcrumbs
$breadcrumbs = $breadcrumbs ?? $defaultBreadcrumbs;
@endphp

@section('header')
  <section class="container-fluid">
    <h2>
      <span class="text-capitalize">Bulk Availabilities</span>
      <small>Set a bunch of availability options all at once.</small>

      @if ($crud->hasAccess('list'))
        <small><a href="{{ url($crud->route) }}" class="hidden-print font-sm"><i class="fa fa-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }} <span>{{ $crud->entity_name_plural }}</span></a></small>
      @endif
    </h2>
  </section>
@endsection

@section('content')

  <div class="row">
    <div class="{{ $crud->getCreateContentClass() }}">
      <!-- Default box -->

      @include('crud::inc.grouped_errors')

      <form method="post"
      action="{{ Route('availabilityBulkCreate') }}"
      @if ($crud->hasUploadFields('create'))
        enctype="multipart/form-data"
      @endif
      >
      {!! csrf_field() !!}

      <!-- load the view from the application if it exists, otherwise load the one in the package -->
      <!-- load the view from the application if it exists, otherwise load the one in the package -->
      @if(view()->exists('vendor.backpack.crud.form_content'))
        @include('vendor.backpack.crud.form_content', [ 'fields' => $crud->fields(), 'action' => 'create' ])
      @else
        @include('crud::form_content', [ 'fields' => $crud->fields(), 'action' => 'create' ])
      @endif

      <div id="saveActions" class="form-group">
        <input type="hidden" name="save_action" value="save_and_back">
        <button type="submit" class="btn btn-success">
          <span class="fa fa-save" role="presentation" aria-hidden="true"></span> &nbsp;
          <span data-value="save_and_back">Save and back</span>
        </button>
        <a href="http://localhost:8000/admin/availability" class="btn btn-default"><span class="fa fa-ban"></span> &nbsp;Cancel</a>
      </div>

      {{--  @include('crud::inc.form_save_buttons') --}}
    </form>
  </div>
</div>

@endsection
