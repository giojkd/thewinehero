<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body>

    <div id="iframe-wrapper"><iframe src="/js/vvveb/editor.html" style="width: 100vw; height: 100vh"></iframe></div>

    <!-- Option 2: jQuery, Popper.js, and Bootstrap JS
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    -->
    <script src="/js/vvveb/js/jquery.min.js"></script>

    <script src="/js/vvveb/js/jquery.hotkeys.js"></script>


<!-- bootstrap-->
<script src="/js/vvveb/js/popper.min.js"></script>


<!-- builder code-->
<!-- This is the main editor code -->
<script src="/js/vvveb/libs/builder/builder.js"></script>

<!-- undo manager-->
<script src="/js/vvveb/libs/builder/undo.js"></script>

<!-- inputs-->
<!-- The inputs library, here is the code for inputs such as text, select etc used for component properties -->
<script src="/js/vvveb/libs/builder/inputs.js"></script>

<!-- components-->
<!-- Components for Bootstrap 4 group -->
<script src="/js/vvveb/libs/builder/components-bootstrap4.js"></script>
<!-- Components for Widgets group -->
<script src="/js/vvveb/libs/builder/components-widgets.js"></script>


<!-- plugins -->

<!-- code mirror libraries - code editor syntax highlighting for html editor -->
<link href="/js/vvveb/libs/codemirror/lib/codemirror.css" rel="stylesheet"/>
<link href="/js/vvveb/libs/codemirror/theme/material.css" rel="stylesheet"/>
<script src="/js/vvveb/libs/codemirror/lib/codemirror.js"></script>
<script src="/js/vvveb/libs/codemirror/lib/xml.js"></script>
<script src="/js/vvveb/libs/codemirror/lib/formatting.js"></script>

<!-- code mirror vvveb plugin -->
<!-- replaces default textarea as html code editor with codemirror-->
<script src="/js/vvveb/libs/builder/plugin-codemirror.js"></script>
<script>
    $(document).ready(function(){
        Vvveb.Builder.init('/js/vvveb/editor.php', function() {
            //load code after page is loaded here
            Vvveb.Gui.init();
        });
    });
</script>
  </body>
</html>
