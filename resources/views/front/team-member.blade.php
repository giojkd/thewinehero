@extends('front.main')

@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-3 text-center">
        <div class="sticky-top" style="top: 90px;">
            @include('front.components.team-member-bio')
        </div>
      </div>
      <div class="col-md-9">

          <h1 class="text-yellow my-4">{{ $meta['h1'] }}</h1>
        @foreach($favoriteableModels as $result)
          @if(!is_null($user[$result['relationship']]) && $user[$result['relationship']]->count() > 0)
              <div class="row">
                <div class="col-md-3">
                  <span class="sticky-index sticky-top">{{$result['name']}}</span>
                </div>
                <div class="col-md-9 pt-4">
                  @foreach($user[$result['relationship']]->chunk(12/$result['col_width']) as $items)
                    <div class="row">
                      @foreach($items as $item)
                        <div class="col-md-{{$result['col_width']}}">
                          @include($result['item_view'],[$result['item_name'] => $item])
                        </div>
                      @endforeach
                    </div>
                  @endforeach
                </div>
              </div>
            @hss('50')
          @endif
        @endforeach
      </div>
    </div>
  </div>

{{--
<div class="glider-carousel vw-100">
  <div><img src="https://www.mamablip.com/ir/h120/storage/5e83cba45f295291e646eb9d2bb40fae.png" alt=""></div>
  <div><img src="https://www.mamablip.com/ir/h120/storage/713ad5e091b9011292f0603ece51c7be.png" alt=""></div>
  <div><img src="https://www.mamablip.com/ir/h120/storage/5bc720840bfde4ae5da525e88341493e.png" alt=""></div>
  <div><img src="https://www.mamablip.com/ir/h120/storage/fe099f74477f47d7c280249ff5668f67.png" alt=""></div>
  <div><img src="https://www.mamablip.com/ir/h120/storage/5e83cba45f295291e646eb9d2bb40fae.png" alt=""></div>
  <div><img src="https://www.mamablip.com/ir/h120/storage/713ad5e091b9011292f0603ece51c7be.png" alt=""></div>
  <div><img src="https://www.mamablip.com/ir/h120/storage/5bc720840bfde4ae5da525e88341493e.png" alt=""></div>
  <div><img src="https://www.mamablip.com/ir/h120/storage/fe099f74477f47d7c280249ff5668f67.png" alt=""></div>
  <div><img src="https://www.mamablip.com/ir/h120/storage/5e83cba45f295291e646eb9d2bb40fae.png" alt=""></div>
  <div><img src="https://www.mamablip.com/ir/h120/storage/713ad5e091b9011292f0603ece51c7be.png" alt=""></div>
  <div><img src="https://www.mamablip.com/ir/h120/storage/5bc720840bfde4ae5da525e88341493e.png" alt=""></div>
  <div><img src="https://www.mamablip.com/ir/h120/storage/fe099f74477f47d7c280249ff5668f67.png" alt=""></div>

</div>
 --}}
@endsection

