@extends('front.main')


@section('content')
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <div class="mt-4">
                        <h1 class="text-warning">@lang('all.Thank you!')</h1>
                        <p>
                            @lang('all.Now your plan is active! Check your private area to see how much content you have left to access!') <br>
                            <a href="{{ Route('user') }}" class="btn btn-warning text-white">@lang('all.Go now!')</a>
                        </p>
                    </div>
                </div>
            </div>


        </div>
@endsection
