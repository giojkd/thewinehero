@extends('front.main')

@if (!$manufacturer->customizable_layout)


@section('content')

  <div class="new-curry overview">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-12">
          <div class="overview-inner">

            <nav class="navbar navbar-expand-md navbar-dark">
              <button class="navbar-toggler" type="button" data-toggle="collapse"
              data-target="#collapsibleNavbar">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="collapsibleNavbar">
              <ul class="navbar-nav">
                <li class="nav-item active">
                  <a class="nav-link" href="#overview" toggleActiveLink scrollOffset="-125">@lang('all.Overview')</a>
                </li>
                @if (!is_null($manufacturer->consortiums) && $manufacturer->consortiums->count() > 0)
                    <li class="nav-item">
                        <a class="nav-link" href="#consortium" toggleActiveLink  scrollOffset="-125">@lang('all.Consortia')</a>
                    </li>
                @endif

                @if($manufacturer->custom_blocks != '')
                    @foreach (json_decode($manufacturer->custom_blocks,1) as $block)
                        <li class="nav-item">
                        <a class="nav-link" href="#{{ Str::slug($block['title'].' block') }}" toggleActiveLink  scrollOffset="-125">{{ $block['title'] }}</a>
                        </li>
                    @endforeach
                @endif

                @if ($manufacturer->products)

                <li class="nav-item">
                  <a class="nav-link" href="#our_products" toggleActiveLink  scrollOffset="-125">@lang('all.Our products')</a>
                </li>

                @endif

                <li class="nav-item">
                  <a class="nav-link" href="#contacts" toggleActiveLink  scrollOffset="-125">@lang('all.Contacts')</a>
                </li>

                @if($manufacturer->experiences)
                    <li class="nav-item">
                    <a class="nav-link" href="#our_experiences" toggleActiveLink  scrollOffset="-125">@lang('all.Our experiences')</a>
                    </li>
                @endif

                <li class="nav-item" style="padding-top: 5px;">
                  @include('front.components.favorite-toggler',['item' => $manufacturer])
                </li>

              </ul>
            </div>
          </nav>

        </div>
      </div>
    </div>
  </div>
</div>
<div class="course-cover background-cover justify-content-center d-flex" id="overview">
  <div class="">
    <img class="d-flex img-fluid background-cover-img" src="{{Route('ir',['size' => '2560','filename' => $manufacturer->photos[0]])}}" alt="@altTagMl($manufacturer->photos[0])">
  </div>
  <div class="course-cover-content-wrapper">
    <div class="d-flex h-100 w-100 justify-content-center">
      <div class="d-flex align-self-center">
        <div class="text-center">
          <h1>{{$meta['h1']}}</h1>


            <img style="height: 120px;" src="{{ url($manufacturer->logo) }}" alt="{{ $manufacturer->name }}">

          <h2 class="traning-professor">{{$manufacturer->subtitle}}</h2>
        </div>
      </div>
    </div>
  </div>

</div>

<div class="manufacturer">

@if ($manufacturer->custom_icons != '')


<section class="calander">
    <div class="container">
        <ul class="row bg-white features-list list-unstyled">
            @foreach (json_decode($manufacturer->custom_icons,1) as $icon)
                <li class="d-block col-6 col-md-3 text-center py-3 border-right">
                    <img class="feature-icon" src="{{ url($icon['image']) }}" alt="@altTagMl($icon['image'])"><br>
                    <h3>{{ $icon['title'] }}</h3>
                </li>
            @endforeach
        </ul>
    </div>
</section>

@endif
@hss('100')

<div class="text-center">
    <a href="#our_experiences" scrollOffset="-125" style="padding: 20px 50px; border: 4px solid " class="btn btn-outline-warning btn-lg rounded-0  shadow">@lang('all.Book your experience now!')</a>
</div>

@hss('100')

@if(!is_null($manufacturer->photos))
    <div class="slick-carousel carousel-stripe">
      @foreach($manufacturer->photos as $image)
          <img src="{{Route('ir',['size' => 'h320','filename' => $image])}}" alt="@altTagMl($image)">
      @endforeach
    </div>
@endif



<div class="manufacturer-custom-blocks">
    @if($manufacturer->custom_blocks !='')
        @foreach (json_decode($manufacturer->custom_blocks,1) as $block)
            <div class="container" id="{{ Str::slug($block['title'].' block') }}">
                <div class="row">
                    <div class="col text-center">
                        <h2>{{ $block['title'] }}</h2>
                        <div class="card shadow d-inline-block p-2 w-100">
                            <div class="card-body text-justify best-readable">
                                {!! $block['content'] !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    @endif
</div>


@if(!is_null($manufacturer->consortiums))
        @if($manufacturer->consortiums->count() > 0)
    <div class="container" id="consortium">
            <div class="row">
                <div class="col text-center">
                    <h2>@lang('all.Member of')</h2>
                    <div class="card shadow d-inline-block p-2 w-100">
                      <div class="card-body">
                        @foreach ($manufacturer->consortiums as $consortium)
                            <a @if($consortium->enabled) href="{{ $consortium->makeUrl() }}" @else href="javascript:" @endif>
                                <img class="consortium-logo" src="{{ url($consortium->logo) }}" alt="{{$consortium->name}}">
                            </a>
                        @endforeach
                      </div>
                    </div>

                </div>
            </div>
    </div>
    @hss('50')
@endif
@endif


@if($manufacturer->products && $manufacturer->products->count() > 0)
   <div class="container" id="our_products">
       <div class="row">
           <div class="col-md-12 text-center">
               <h3 class="h2">@lang('all.Our products')</h3>
           </div>
       </div>
   </div>
    <div class="container">
        <div class="row">
            @foreach ($manufacturer->products as $product)
                <div class="col-md-4 col-6">
                    @include('front.components.product-result')
                </div>
            @endforeach
        </div>

    </div>
@endif

@if($manufacturer->articles && $manufacturer->articles->count() > 0)
   <div class="container" id="articles">
       <div class="row">
           <div class="col-md-12 text-center">
               <h2>@lang('all.articles') @lang('all.on') {{ $manufacturer->name }}</h2>
           </div>
       </div>
   </div>
    <div class="container">
        <div class="row">
            @foreach ($manufacturer->articles as $article)
                <div class="col-md-4 col-6">
                    @include('front.components.article-result')
                </div>
            @endforeach
        </div>

    </div>
@endif


@if($manufacturer->videos && $manufacturer->videos->count() > 0)
   <div class="container" id="videos">
       <div class="row">
           <div class="col-md-12 text-center">
               <h2>@lang('all.videos') @lang('all.on') {{ $manufacturer->name }}</h2>
           </div>
       </div>
   </div>
    <div class="container">
        <div class="row">
            @foreach ($manufacturer->videos as $video)
                <div class="col-md-4 col-6">
                    @include('front.components.video-result')
                </div>
            @endforeach
        </div>

    </div>
@endif



@if(!is_null($experiences) && $experiences->count() > 0)
   <div class="container" id="our_experiences">
       <div class="row">
           <div class="col-md-12 text-center">
               <h2>@lang('all.Our experiences')</h2>
           </div>
       </div>
   </div>
    <div class="container">
        <div class="row">
            @foreach ($experiences as $experience)
                <div class="col-md-4 col-6">
                    @include('front.components.experience-result',['experience' => $experience])
                </div>
            @endforeach
        </div>
    </div>
@endif

<div class="text-center" id="contacts">
        <h2>@lang('all.Contact') {{ $manufacturer->name }} </h2>
      <div class="container">
          <div class="row">
        <div class="col-md-6">
          <div id="manufacturerMap">

          </div>
        </div>
        <div class="col-md-6">

            <div class="card mt-4 mt-md-0">
                <div class="card-body text-center text-md-right">
                    <p class="text-warning h4"><i class="fa fa-phone"></i> @lang('all.Phone')</p> <a class="text-dark" href="tel:{{ $manufacturer->telephone }}">{{ $manufacturer->telephone }}</a>
                    <hr>
                    <p class="text-warning h4"><i class="fa fa-globe"></i> Web</p> <a class="text-dark" href="{{ $manufacturer->website }}" target="_blank" rel="nofollow"> {{ $manufacturer->website }}</a>
                    <hr>
                    <p class="text-warning h4"><i class="fa fa-envelope"></i> Email</p> <a class="text-dark" href="mailto:{{ $manufacturer->email }}">  {{ $manufacturer->email }}</a>
                    <hr>
                    <p class="text-warning h4"><i  class="fa fa-map-marker"></i> @lang('all.address') </p> {{ $manufacturer->address['value'] }}</h4>

                </div>
            </div>

        </div>


      </div>
      </div>
</div>

</div>

@endsection

@push('scripts')
  <style>
    #manufacturerMap{
      height: 480px;
      width: 100%;
    }

    .best-readable p{
        width: 100%;
    }

  </style>
    @if(isset($manufacturer->address))
  <script>



       function initMap() {
        map = new google.maps.Map(
            document.getElementById('manufacturerMap'),
            {center: new google.maps.LatLng({{$manufacturer->address['latlng']['lat']}}, {{$manufacturer->address['latlng']['lng']}}), zoom: 16});

        var iconBase = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/';

        var features = [
          {
            position: new google.maps.LatLng({{$manufacturer->address['latlng']['lat']}}, {{$manufacturer->address['latlng']['lng']}}),
          }
        ];

        // Create markers.
        for (var i = 0; i < features.length; i++) {
          var marker = new google.maps.Marker({
            position: features[i].position,
            map: map
          });
        };
      }


  </script>



  <script defer
    src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_API_KEY') }}&callback=initMap">
    </script>
@endif
    <script>
        $(function(){
            $('.slick-carousel').slick({
                 slidesToShow: 3,
                slidesToScroll: 1,
                autoplay: true,
                infinite: true,
                centerMode: true,
                variableWidth: true,
                dots: true,
                autoplaySpeed: 2000,
                responsive: [
                    {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                    },
                    {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                    },
                    {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            });
        })
    </script>
@endpush

@else

    @section('content')
        {!! $pageContent !!}
    @endsection

@endif
