@extends('front.main')

@section('content')
    <div class="event-section bg-warning mb-4">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="py-4">
                        <h1 class="text-center">@lang("all.deals section")</h1>
                        {{-- <p>Upcoming events you can't miss</p> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if ($deals->count() > 0)
        <div class="container">
            <div class="row">
                @foreach ($deals as $dealType => $dealsOfType)
                    @if ($dealsOfType->count() > 0)
                        @foreach ($dealsOfType as $deal)
                            <div class="col-md-6">
                                @include('front.components.deal-result')
                            </div>
                        @endforeach
                    @endif
                @endforeach
            </div>
        </div>
    @else
       <div class="container">
            <div class="row">
                <div class="col">
                    <div class="alert alert-secondary">@lang('all.no deals in schedule')</div>
                </div>
            </div>
        </div>

    @endif

    <style>
        .deal-cover{
            position: relative;
        }

        .deal-cover-info{
            position: absolute;
            top: 15px;
            left: 15px;
        }
    </style>

@endsection
