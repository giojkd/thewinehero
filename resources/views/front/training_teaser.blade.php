@extends('front.main')

@section('content')

  <div class="new-curry overview">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-12">
          <div class="overview-inner">

            <nav class="navbar navbar-expand-md navbar-dark">
              <button class="navbar-toggler" type="button" data-toggle="collapse"
              data-target="#collapsibleNavbar">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="collapsibleNavbar">
              <ul class="navbar-nav">
                <li class="nav-item active">
                  <a class="nav-link" href="#overview" toggleActiveLink scrollOffset="-125">@lang('all.Overview')</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#what_you_will_learn" toggleActiveLink  scrollOffset="-125">@lang('all.What you will learn')</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#who_is_the_course_for" toggleActiveLink  scrollOffset="-125">@lang('all.Who is the course for')</a>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="#syllabus" toggleActiveLink  scrollOffset="-125">@lang('all.Syllabus')</a>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="#who_will_you_learn_with" toggleActiveLink  scrollOffset="-125">@lang('all.Who will you learn with')</a>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="#enrollment_options" toggleActiveLink  scrollOffset="-125">@lang('all.Enrollment Options')</a>
                </li>

                <li class="nav-item" style="padding-top: 5px;">
                  @include('front.components.favorite-toggler',['item' => $training])
                </li>
              </ul>
            </div>
          </nav>

        </div>
      </div>
    </div>
  </div>
</div>
<div class="course-cover background-cover justify-content-center d-flex" id="overview">
  <div class="">
    <img class="d-flex img-fluid background-cover-img" src="{{Route('ir',['size' => '2560','filename' => $training->photos[0]])}}" alt="@altTagMl($training->photos[0])">
  </div>
  <div class="course-cover-content-wrapper">
    <div class="d-flex h-100 w-100 justify-content-center">
      <div class="d-flex align-self-center">
        <div class="text-center">
          <h1>{{$training->name}}</h1>
          <span class="text-white">with</span><br>
          <div class="course-cover-avatar my-2">
            <img src="{{Route('ir',['size' => '120','filename' => $training->user->avatar])}}" alt="{{ $training->user->full_name }}">
          </div>
          <h2 class="traning-professor">{{$training->user->name}} {{$training->user->surname}}</h2>
        </div>
      </div>
    </div>
  </div>

</div>




    <section class="calander">
  <div class="container">
    <div class="row bg-white features-list">
        <div class="col-6 col-md-3 text-center py-3 border-right">
        <i class="fa fa-clock fa-3x"></i><br>
         <b></b>
        <h3>{{$training->videos->unique()->sum('duration')}} @lang('all.hours of video')</h3>
      </div>
      <div class="col-6 col-md-3 text-center py-3 border-right">
          <i class="fas fa-arrow-alt-circle-down fa-3x"></i>
          <h3>{{$training->files->count()}} @lang('all.downloadable readings')</h3>
      </div>

      <div class="col-6 col-md-3 text-center py-3 border-right">
          <i class="fas fa-globe fa-3x"></i><br>

            <h3>{{$training->modules->count()}} @lang('all.online Classes')</h3>
      </div>
      <div class="col-6 col-md-3 text-center py-3 border-right">
          <i class="fa fa-running fa-3x"></i>
          <h3>{{$training->levels->count()}} @lang('all.levels to complete the preparation')</h3>
      </div>
    </div>
  </div>
</section>




@hss('50')
<div class="text-center">
  <a class="btn btn-outline-warning py-2 text-warning px-4 rounded-0">@lang('Enrolled by n students',['n' => $training->users->count()])</a>
</div>



@if(!is_null($training->what_you_will_learn))
  <div class="training-section" id="what_you_will_learn">
    <h1>@lang('all.What will you learn')</h1>
    <div class="container">
      @foreach(collect(json_decode($training->what_you_will_learn,1))->chunk(2) as $chunks)
        <div class="row">
          @foreach($chunks as $wywl)
            <div class="col-md-6">
              <div class="card shadow mt-4 mt-md-0">
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-2 text-center text-success">
                      <div class="d-inline-block mt-3">
                        <i class="fa-2x far fa-check-circle"></i>
                      </div>
                    </div>
                    <div class="col-md-10 pl-md-0">
                      <h5 class="card-title">{{$wywl['title']}}</h5>
                      <p class="card-text">{{$wywl['description']}}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          @endforeach
        </div>
      @endforeach
    </div>
  </div>
@endif

<div class="training-section" id="who_is_the_course_for">
  <h1>@lang('all.Who is the course for')</h1>
<div class="container-fluid">
  <div class="row">
    <div class="col-md-6 offset-md-3 text-justify">
        <div class="p-2">
            {!!$training->requirements!!}
      </div>
    </div>
  </div>
  </div>

</div>


<div class="training-section" id="syllabus">
  <h1>@lang('all.Syllabus')</h1>
  <div id="syllabus" class="container">
    @foreach($training->levels as $indexLevel =>  $level)

        @include('front.components.course-card')

    @endforeach
  </div>
</div>

<div class="who-you-will-learn-with-section bg-warning" id="who_will_you_learn_with">
  <div class="container">
    <div class="row">
      <div class="col-md-4 justify-content-center d-flex">
        <div class="d-flex align-self-center">

            <img class="training-user-avatar" src="{{Route('ir',['size' => 'h480','filename' => $training->user->avatar])}}" alt="{{ $training->user->full_name }}">

        </div>
      </div>
      <div class="col-md-8">
        <h1>@lang('all.Who you will learn with')</h1>
        <h2>{{$training->user->name}} {{$training->user->surname}}</h2>
        <p>
          {{$training->user->biography}}
        </p>
        <a class="shadow btn-lg btn btn-outline-warning text-white" target="_blank" href="{{$training->user->makeUrl()}}">@lang('all.Know more about') {{$training->user->name}} {{$training->user->surname}}</a>
      </div>
    </div>
  </div>

</div>
@hss('50')
    <div class="container-fluid">
        <div class="row">
            <div class="col text-center">
                <h2>@lang('all.How to enroll this course?')</h2>
                @if(!$training->is_premium)
                    @auth
                        @if(Auth::user()->hasActivePlan())
                            @if($canPurchase)

                                @lang('all.More enrollable courses',['n' => Auth::user()->plan->trainings])

                                <br>
                                <div class="mt-2">
                                    <form method="POST" action="{{ Route('purchaseContent',['model' => 'training','id' => $training->id]) }}">
                                        @csrf
                                        <button type="submit" class="btn btn-warning btn-large text-white btn-lg">@lang('all.Enroll now!')</button>
                                    </form>
                                </div>

                            @else
                                <div class="alert alert-danger">
                                    @lang('all.The trainings included in your plan for this month are over, but you can still upgrade to another plan')
                                </div>
                                @include('front.components.enrollment-options',['content' => $training])

                            @endif
                        @else
                            @include('front.components.enrollment-options',['content' => $training])
                        @endif
                    @endauth
                    @guest
                        @include('front.components.enrollment-options',['content' => $training])
                    @endguest
                @endif
                  @include('front.components.buy-single-content',
            [
              'content' => [
                  'name' => $training->getModelName(),
                  'price_compare' => ($training->applyPlanDiscount($training->getModelName(),$training->price) != $training->price) ? $training->price : 0,
                  'price' => $training->applyPlanDiscount($training->getModelName(),$training->price),
                  'id' => $training->id,
                  'is_premium' => $training->is_premium
                  ]
            ]
        )
            </div>
        </div>
    </div>

@push('scripts')


@endpush



@endsection
