@extends('front.main')

@section('content')

    <div class="deal-cover">
        @if ($isMobile)
            <img src="https://www.mamablip.com{{ Route('ir',['size' => 1280,'filename' => $deal->images[1]],false) }}" alt="@altTagMl($deal->images[0])" class="w-100">
        @else
            <img src="https://www.mamablip.com{{ Route('ir',['size' => 1920,'filename' => $deal->images[0]],false) }}" alt="@altTagMl($deal->images[0])" class="w-100">
        @endif

        <div class="deal-cover-title-wrapper">
            <h1>{{ $deal->name }}</h1>
            <h2>{{ $deal->short_description }}</h2>

        </div>
        <div class="deal-cover-info">
            <div class="perc-bar" style="width: {{ $lastedPercentage }}%"></div>
            <div class="deal-cover-info-content">
                <p id="deal-lasted-time" class="d-none d-md-block">
                    @lang('all.more than <span>:percentage</span> of the stock has already been sold',['percentage' => $lastedPercentage.'%'])

                </p>
                <div id="production-month-countdown"></div>
            </div>
        </div>
        <div class="deal-cover-shop-now-button-wrapper">
            <a scrolloffset="-100" href="#bundle-options" class="btn btn-secondary text-white btn-lg mt-3 mb-2 btn-megalodon">@lang('all.shop now')</a>
        </div>
    </div>

    <style>

        .price-cut{
            font-size: 15px;
            text-decoration: line-through;
            color: #999;
            font-weight: 500;
            display: block;
            margin-top: -5px;
        }

        .btn-megalodon{
            font-size: 36px;
        }

        .deal-cover-shop-now-button-wrapper{
            position: absolute;
            bottom: 125px;
            right: 25px;
        }

        .countdown-time-component{
            display: inline-block;
            text-align: center;
            margin-left: 10px;
        }

        .countdown-time-component h2{
            font-size: 36px;
            font-weight: 600;
        }

        .countdown-time-component small{
               font-size: 13px;
            font-weight: 300;
        }

        #production-month-countdown{
            position: absolute;
            top: 15px;
            right: 15px;
            color: #fff;
        }

        #deal-lasted-time{
            color:#fff;
            font-size: 2rem;
            position: absolute;
            top: 15px;
            left: 15px;
            font-weight: 300;
        }

        #deal-lasted-time span{
            font-weight: 600;
            font-size: 36px;
        }

        .deal-cover .deal-cover-info{
            position: absolute;
            bottom: 0px;
            left: 0px;
            width: 100%;
            background: rgba(0,0,0,0.15);
            height: 80px;
        }

        .deal-cover .deal-cover-info .perc-bar{
            position: absolute;
            top: 0px;
            left: 0px;
            height: 80px;
            background-color: #e6a005;

        }

        .deal-cover .deal-cover-info .deal-cover-info-content{
            position: relative;

        }


    </style>





    @hss('70')

    <div class="container" id="deal-content">
        <div class="row my-3 d-none">
                <div class="col-md-8">
                    @include('front.components.favorite-toggler',['item' => $deal])
                </div>
                <div class="col-md-4">

                </div>
            </div>
        <div class="row">
            <div class="col-md-12">
                {{-- @cim(Route('ir',['size' => 1280,'filename' => $deal->images[0]])) --}}

                <div class="article mb-4">
                    <h1 class="text-warning">{{ $deal->name }}</h1>
                    <h2 style="font-size: 1.8rem; font-weight: 300;">{{ $deal->short_description }}</h2>
                </div>
                <div class="deal-descritpion best-readable">
                    {!! $deal->description !!}
                </div>
                @hss('40')
            </div>

            @if(!is_null($deal->boxes) && $deal->boxes != '')

                <ul class="d-flex row w-100">
                    @foreach (json_decode($deal->boxes,1) as $block)
                        <li class="d-flex col-md-{{12/count(json_decode($deal->boxes,1))}} text-center">
                           <div class="d-block w-100">
                                <img style="height: 90px; filter: invert(87%) sepia(20%) saturate(6220%) hue-rotate(346deg) brightness(100%) contrast(97%)" src="{{url($block['image'])}}">
                                @hss('10')
                                <h5 class="mb-0">{{ $block['title'] }}</h5>
                                @hss('7')
                                {!!$block['content']!!}
                           </div>
                        </li>
                    @endforeach
                </ul>
            @endif

    </div>
    </div>

    @hss('20')

    @if ($deal->custom_icons != '')

        <section class="calander">
            <div class="container">
                <div class="row bg-white features-list">
                    @foreach (json_decode($deal->custom_icons,1) as $icon)
                        <div class="col-6 col-md-3 text-center py-3 border-right">
                            <img class="feature-icon" src="{{ url($icon['image']) }}" alt="@altTagMl($icon['image'])"><br>
                            <h3>{{ $icon['title'] }}</h3>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>

    @endif
    <div id="bundle-options" ></div>
    <div class="container mt-3" >
        <div class="row">
            <div class="col text-center">
                @foreach ($deal->products as $product)
                    @include('front.components.product-result-bundle',['bundle'=>$product,'deal' => $deal])
                @endforeach
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @if (!is_null($deal->video))
                    <div class="video-player">
                        @if (!is_null($deal->video->file))
                            <video class="w-100" src="{{ url($deal->video->file) }}" controls controlsList="nodownload">
                        @endif
                    </div>
                @endif
            </div>
        </div>
    </div>


        @if ($deal->hasExpired())
            <div id="expiredDealModal" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">@lang('all.expired deal modal title')</h5>
                            {{--<button type="button" class="close" data-dismiss="modal">&times;</button>--}}
                        </div>
                        <form action="{{Route('dealSubscription')}}">
                            <div class="modal-body">
                                <p>@lang('all.expired deal modal content l1')</p>
                                <p class="text-warning"><small>@lang('all.expired deal modal content l2')</small></p>
                                <input type="hidden" name="deal_id" value="{{$deal->id}}">
                                <input type="hidden" name="locale" value="{{App::getLocale()}}">
                                <input type="email"  name="email" class="form-control" required placeholder="@lang('youremail@example.com')">
                                <p class="text-muted"><small>@lang('all.expired deal modal content l3')</p>
                            </div>
                            <div class="modal-footer">
                                {{--<a class="btn btn-secondary" href="javascript:" data-dismiss="modal">@lang('all.close')</a>--}}
                                <button type="submit" class="btn btn-warning text-white">@lang('all.subscribe')</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            @push('scripts')
                <script>
                    function openExpiredDealModal(){
                        $(()=>{
                            $('#expiredDealModal').modal({
                                backdrop: 'static',
                                keyboard: false
                            });
                        })
                    }
                    openExpiredDealModal();

                </script>
            @endpush
        @endif

        @if (!$deal->hasStarted())
            <div id="notStartedYetDealModal" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">@lang('all.not started yet deal modal title')</h5>
                            {{--<button type="button" class="close" data-dismiss="modal">&times;</button>--}}
                        </div>
                        <form action="{{Route('dealSubscription')}}">
                            <div class="modal-body">
                                <p>@lang('all.not started yet deal modal content l1')</p>
                                <p class="text-warning"><small>@lang('all.not started yet deal modal content l2')</small></p>
                                <input type="hidden" name="deal_id" value="{{$deal->id}}">
                                <input type="hidden" name="locale" value="{{App::getLocale()}}">
                                <input type="email"  name="email" class="form-control" required placeholder="@lang('youremail@example.com')">
                                <p class="text-muted"><small>@lang('all.not started yet deal modal content l3')</p>
                            </div>
                            <div class="modal-footer">
                                {{--<a class="btn btn-secondary" href="javascript:" data-dismiss="modal">@lang('all.close')</a>--}}
                                <button type="submit" class="btn btn-warning text-white">@lang('all.subscribe')</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            @push('scripts')
                <script>
                    function openNotStartedYetDealModal(){
                        $(()=>{
                            $('#notStartedYetDealModal').modal({
                                backdrop: 'static',
                                keyboard: false
                            });
                        })
                    }
                    openNotStartedYetDealModal();

                </script>
            @endpush
        @endif

@endsection


<style>

    .deal-descritpion{

    }

    .deal-descritpion ul{
        list-style: none;
        padding: 0px;
    }

    .deal-short-description{
            font-size: 2.5rem;
            font-weight: bold;
            line-height: 2.3rem;
            background-color: #ababab;
            padding: 25px;
            color: #fff;
    }

    .yellow-background{
        background:#e6a005;
    }

    .deal-remaining-time{
        background-color: rgba(255,255,255,0.75);
    }



    .product-bundle-price-option{
        background: #fff;

        transition: 0.3s all;
    }

    .product-bundle-price-option:hover{
/*        box-shadow: 0px 0px 14px rgba(0,0,0,0.45); */
    }

    .deal-cover{
        position: relative;
        width: 100vw;
        height: 100vh;
        overflow: hidden;
        padding-top: 72px;
        margin-top: -70px;
    }



    .deal-cover-title-wrapper{
        position: absolute;
        top: 90px;
        right: 0px;
        padding: 15px 20px;
        background: rgba(255,255,255,0.8);
    }

    .deal-cover-title-wrapper h1{
        font-size: 1.8rem;
        display: inline-block;
        margin-bottom: 0px;
        font-weight: bold;
        color: #000;
    }

    .deal-cover-title-wrapper h2{
        font-size: 1rem;
        font-weight: 300;
    }

    .product-bundle-price-option{

        padding: 15px;

    }

    .product-bundle-price-option .product-bundle-quantity{
        font-weight: bold;
        font-size: 24px;
        display: block;
        text-align: center;
        color: #555;
    }

    .product-bundle-price-option .product-bundle-quantity b{
        font-size: 36px;
        color: #fab417;
    }

     .product-bundle-price-option .product-bundle-price{
        font-weight: bold;
        font-size: 24px;
        display: block;
        text-align: center;
        color: #555;
    }

    .product-bundle-price-option .product-bundle-price b{
        font-size: 36px;

    }

    .contained-quantity{
        font-size: 42px;
        font-weight: bold;
        color: #fab417;
        margin-right: 10px;
        margin-left: 10px;
    }
    body.modal-open{
        overflow-y: scroll!important;
    }

</style>


@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.countdown/2.2.0/jquery.countdown.min.js"></script>
<script>

    var translations = {
        'giorni':'@lang('all.giorni')',
        'ore':'@lang('all.ore')',
        'minuti':'@lang('all.minuti')',
        'secondi':'@lang('all.secondi')',
    }

    $(function(){
        $('#production-month-countdown').countdown('{{ date($productionMonthEnd) }}', function(event) {
        var $this = $(this).html(event.strftime(''
            + '<div class="countdown-time-component"><h2 class="mb-0">%D</h2><small>'+translations.giorni+'</small></div>'
            + '<div class="countdown-time-component"><h2 class="mb-0">%H</h2><small>'+translations.ore+'</small></div>'
            + '<div class="countdown-time-component"><h2 class="mb-0">%M</h2><small>'+translations.minuti+'</small></div>'
            + '<div class="countdown-time-component"><h2 class="mb-0">%S</h2><small>'+translations.secondi+'</small></div>'
            ));
        });
    })

</script>
@if (!is_null($deal->video->yt_video_id))
        <script async src="https://www.youtube.com/iframe_api"></script>
        <script>
            function onYouTubeIframeAPIReady() {
                var player;
                player = new YT.Player('YouTubeVideoPlayer', {
                    videoId: '{{ $deal->video->yt_video_id }}', // YouTube Video ID
                    width: '100%', // Player width (in px)
                    height: '100%', // Player height (in px)
                    playerVars: {
                        {{-- playlist: '{{ collect($yt_playlist)->implode(',') }}',  --}}
                        autoplay: 1, // Auto-play the video on load
                        autohide: 1,
                        disablekb: 1,
                        controls: 1, // Hide pause/play buttons in player
                        showinfo: 1, // Hide the video title
                        modestbranding: 1, // Hide the Youtube Logo
                        loop: 1, // Run the video in a loop
                        fs: 0, // Hide the full screen button
                        autohide: 0, // Hide video controls when playing
                        rel: 0,
                        enablejsapi: 1,
                        //color: '#fab417' only white and red
                    },
                    events: {
                        onReady: function(e) {
                            e.target.mute();
                            e.target.setPlaybackQuality('hd1080');
                            e.target.playVideo();

                        },
                        onStateChange: function(e) {
                            /*
                        if (e && e.data === 1) {
                            var videoHolder = document.getElementById('home-banner-box');
                            if (videoHolder && videoHolder.id) {
                                videoHolder.classList.remove('loading');
                            }
                        } else if (e && e.data === 0) {
                            e.target.playVideo()
                        }
                        */
                        }
                    }
                });

            }

        </script>
    @endif



        <!-- Hotjar Tracking Code for https://www.mamablip.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:2131932,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>

@endpush
