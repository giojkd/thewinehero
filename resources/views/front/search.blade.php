@extends('front.main')

@section('content')
<div class="search-results">

<h1 class="page-title">@lang('all.Results for') "{{$keyword}}"</h1>
@if($countResults > 0)
  @foreach($results as $result)
    @if(count($result['items']) > 0)
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">
            <span class="sticky-index sticky-top">{{$result['name']}}</span>
          </div>
          <div class="col-md-9">

              @foreach($result['items']->chunk(12/$result['col_width']) as $items)
                <div class="row">
                  @foreach($items as $item)
                    <div class="col-md-{{$result['col_width']}}">
                      @include($result['item_view'],[$result['item_name'] => $item])
                    </div>
                  @endforeach
                </div>
              @endforeach

          </div>
        </div>

      </div>
      @hss('50')

    @endif
  @endforeach
  @else

      <div class="container">
          <div class="row">
              <div class="col text-center">
                 @lang('all.No search results message',['keyword'=> $keyword])<br>
                <a href="javascript:  $zopim.livechat.window.show();" class="mt-4 btn btn-warning btn-lg text-white">@lang('all.Ask Mama!')</a>
              </div>
          </div>
      </div>



  @endif

</div>
@endsection

@push('headtags')
    <meta name="robots" content="noindex">
@endpush
