@extends('front.main')


@section('content')
    @csrf
    <div class="enrollment-options" id="enrollment_options">
    <h1>@lang('all.Select your favorite option')</h1>
    <div class="container">
        <div class="row justify-content-center">
        @foreach($enrollmentoptions as $index => $option)
            <div class="col-md-4 mb-4">
            <div class="enrollment-option bg-white shadow p-4 text-center">
                <div class="option-price text-warning ">
                {{money($option->price,'EUR')->format()}}
                </div>
                <div class="option-price-info">
                @if($option->is_subscription)
                    @lang('messages.Every n days',['n' => $option->frequency])
                @else
                    @lang('all.Pay once')
                @endif
                </div>
                <!--<span class="text-warning">Free until June 30th 2020!</span>-->
                <hr>


                    <label class="btn btn-lg btn-outline-warning btn-block" >
                            <input onchange="$('.enrollmentOptionIdInput').val($(this).val())" type="radio" name="data[enrollmentoption_id]" value="{{ $option->id }}" @if($index == 0) checked @endif> @lang('all.Select this option')
                    </label>



                {{--
                <form class="" action="{{Route('enrollTraining')}}" method="post">
                @csrf
                <input type="hidden" name="training_id" value="{{$training->id}}">
                <button type="submit" class="btn btn-lg btn-outline-warning btn-block">Enroll now</button>
                </form>
                --}}
            </div>
            </div>
        @endforeach
        </div>
    </div>
    </div>
    <div class="enrollment-options">
        @if($payment_methods->count() > 0)
        <h1>@lang('all.Choose a payment method')</h1>
        <div class="row">
            <div class="col col-md-4 offset-md-4">


            @foreach($payment_methods as $payment_method)
                <div class="card mb-3">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-8">
                                @lang('all.Credit card:') <b>{{ Str::ucfirst($payment_method->card->brand) }} ({{ $payment_method->card->last4 }})</b>
                                @lang('all.Exp:') <b>{{ $payment_method->card->exp_month }}/{{ $payment_method->card->exp_year }}</b>
                            </div>
                            <div class="col-md-4 col-12 text-right">
                                <form action="{{ Route('activatePlan') }}" method="POST">
                                    @csrf
                                    <input type="hidden" name="payment_method" value="{{ $payment_method->id }}">
                                    <input class="enrollmentOptionIdInput" type="hidden" name="enrollmentOptionId" value="{{ $enrollmentoptions->first()->id }}">
                                    <button type="submit" class="mt-2 mt-md-0 btn btn-warning text-white btn-block">@lang('all.Use this card')</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
               </div>
        </div>
        @endif
        @if($payment_methods->count() > 0)
            <h1>@lang('all.Or set a new one')</h1>
        @else
            <h1>@lang('all.Payment method')</h1>
        @endif

        <div class="row">
            <div class="col-md-4 offset-md-4">


                        <div class="form-group">
                            <label for="">@lang("Cardholder's name")</label>
                            <input id="card-holder-name" type="text" class="form-control" required>
                        </div>
                        <label for="">@lang('all.Credit card number')</label>
                        <div id="new-card-form-wrapper" class="form-control-stripe"></div>


                <button data-secret="{{ $client_secret_intent->client_secret }}" id="card-button" type="button" class="btn btn-warning mt-4 btn-block text-whit btn-lg">@lang('all.Confirm')</button>
            </div>
        </div>
    </div>


@push('scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <script src="https://js.stripe.com/v3/"></script>
    <script>
        $(() => {
            const stripe = Stripe('{{ env('STRIPE_KEY') }}');
            const elements = stripe.elements();
            const cardElement = elements.create('card');
            cardElement.mount('#new-card-form-wrapper');
            const cardHolderName = document.getElementById('card-holder-name');
            const cardButton = document.getElementById('card-button');
            const clientSecret = cardButton.dataset.secret;
            cardButton.addEventListener('click', async (e) => {
                const { setupIntent, error } = await stripe.confirmCardSetup(
                    clientSecret, {
                        payment_method: {
                            card: cardElement,
                            billing_details: { name: cardHolderName.value }
                        }
                    }
                );
                if (error) {
                    console.log(error);
                    alert(error.message);
                    //alert('unsuccess')
                } else {
                    console.log(setupIntent);
                    setupIntent.enrollmentOptionId = $('input[name="data[enrollmentoption_id]"]').val();
                    setupIntent._token = '{{ csrf_token() }}';
                    $.post('{{ Route('activatePlan') }}',setupIntent,function(r){
                        if(r.status == 1){
                            location.reload();
                        }
                    },'json')
                }
            });
        })
    </script>



@endsection

