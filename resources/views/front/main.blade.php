<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    @if(isset($no_index))
        <meta name="robots" content="noindex" />
    @endif

    @if (env('NOINDEX') == true)
        <meta name="robots" content="noindex">
    @endif

    <link rel="apple-touch-icon" sizes="180x180" href="/front/images/favicon/apple-touch-icon.png?v=3ewj5K3wqo">
    <link rel="icon" type="image/png" sizes="32x32" href="/front/images/favicon/favicon-32x32.png?v=3ewj5K3wqo">
    <link rel="icon" type="image/png" sizes="16x16" href="/front/images/favicon/favicon-16x16.png?v=3ewj5K3wqo">
    <link rel="manifest" href="/front/images/favicon/site.webmanifest?v=3ewj5K3wqo">
    <link rel="mask-icon" href="/front/images/favicon/safari-pinned-tab.svg?v=3ewj5K3wqo" color="#ffb515">
    <link rel="shortcut icon" href="/front/images/favicon/favicon.ico?v=3ewj5K3wqo">


    <meta name="msapplication-TileColor" content="#777777">
    <meta name="msapplication-config" content="/front/images/favicon/browserconfig.xml?v=3ewj5K3wqo">
    <meta name="theme-color" content="#ffffff">

    <meta name="ga_sponsor" value="{{ implode(',',$sponsors) }}" >

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-KLTX6R5');</script>
    <!-- End Google Tag Manager -->



    <meta charset="UTF-8">
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="cache-control" content="no-store" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    {{--
    <link rel="stylesheet" href="{{mix('/css/app.css')}}">
     --}}

    @if (isset($page_type))
         <link rel="stylesheet" href="{{ mix('/css/' . $page_type . '.css') }}">
        {{-- <link rel="stylesheet" href="/css/{{ $page_type }}.css">  --}}
    @else
        <link rel="stylesheet" href="/css/app.css?v=1.3">
    @endif

    {{--
    <link rel="preload" href="{{mix('/css/fontawesome.css')}}" as="style">
     --}}




    @if(isset($meta['title']))
        <title>{{ $meta['title'] }}</title>
    @endif

    @if (isset($meta['description']))
        <meta name="description" content="{{ strip_tags($meta['description']) }}">
    @endif

    @if(isset($meta['og_type']))
        <meta property="og:type" content="{{ $meta['og_type'] }}">
    @endif

    @if(isset($currentPageUrl))
        <meta property="og:url" content="{{ $currentPageUrl }}">
    @endif

    @if(isset($meta['title']))
        <meta property="og:title" content="{{ $meta['title'] }}">
    @endif

    @if(isset($meta['cover']))
        <meta property="og:image" content="{{ url($meta['cover']) }}">
    @endif

    @if (isset($meta['description']))
        <meta property="og:description" content="{{ strip_tags($meta['description']) }}">
    @endif

    <meta name="twitter:site" content="@mamablip">

    @if(isset($currentPageUrl))
        <meta name="twitter:url" content="{{ $currentPageUrl }}">
    @endif

    @if(isset($meta['title']))
        <meta name="twitter:title" content="{{ $meta['title'] }}">
    @endif

    @if (isset($meta['description']))
        <meta name="twitter:description" content="{{ strip_tags($meta['description']) }}">
    @endif

    @if(isset($meta['cover']))
        <meta name="twitter:image" content="{{ url($meta['cover']) }}">
    @endif

    <meta property="og:locale" content="{{ App::getLocale() }}_{{ Str::upper(App::getLocale()) }}" />
    <meta property="og:site_name" content="mamablip" />

    @if(isset($currentPageUrl))
        <link rel="canonical" href="{{ $meta['canonical'] }}">
    @endif

    @if(isset($altLangs))
        @foreach ($altLangs['urls'] as $langLinkIndex => $langLink)
            <link rel="alternate" href="{{ url($langLink) }}" hreflang="{{ $langLinkIndex }}" />
        @endforeach
    @endif
    @if(isset($meta['x-default-hreflang']))
        <link rel="alternate" href="{{ url($meta['x-default-hreflang']) }}" hreflang="x-default" />
    @endif

    @stack('headtags')

</head>
<body>

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KLTX6R5"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->



  @include('front.components.navbar')
  @yield('content')
  @include('front.components.footer')
  @stack('scripts_before_app')

  <script>
    var postToken = '{{ csrf_token() }}';
    var currentPageUrl = '{{ Request::url() }}';
  </script>

  <script type="text/javascript" src="{{mix('/js/app.js')}}?v=1.0"></script>
  {{-- <script type="text/javascript" src="/js/app.js?v=1"></script>  --}}
  @if(isset($addthis))
    <script defer type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=vanessaheld"></script>
  @endif
  {{-- @if(isset($zdassets))  --}}



 {{-- @endif --}}

<!-- Cookie Consent by https://www.CookieConsent.com -->
<script defer type="text/javascript" src="//www.cookieconsent.com/releases/3.1.0/cookie-consent.js"></script>
<script defer type="text/javascript" >
document.addEventListener('DOMContentLoaded', function () {

    setInterval(function(){
        cookieconsent.run({"notice_banner_type":"simple","consent_type":"express","palette":"light","language":"en","change_preferences_selector":"#changeCookiePreferencesBtn","cookies_policy_url":"https://www.mamablip.com/en/pages/terms-and-conditions/1"});
    },10000);


    $('#sendinblueIframe').attr('src','https://my.sendinblue.com/users/subscribe/js_id/3lg15/id/1');

});
</script>
@if(isset($richResult))
    <script data-cfasync="false" type="application/ld+json" >
        @json($richResult)
    </script>
@endif

<noscript>ePrivacy and GPDR Cookie Consent by <a href="https://www.CookieConsent.com/" rel="nofollow noopener">Cookie Consent</a></noscript>
<!-- End Cookie Consent by https://www.CookieConsent.com -->
@stack('scripts')
{{-- <link rel="stylesheet" href="{{mix('/css/fontawesome.css')}}">  --}}
{{-- <link rel="preload" crossorigin href="{{mix('/css/fontawesome.css')}}" as="font" onload="this.rel='stylesheet'">  --}}
<script src="//code.tidio.co/jyxim86wqb0l4w6y97mm0wbkidd6alnb.js" async></script>
</body>
</html>
