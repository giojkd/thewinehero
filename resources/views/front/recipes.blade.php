@extends('front.main')
@section('content')

<div class="recipes">



  <h1 class="page-title text-left">@lang('all.Our favorite recipes')</h1>
  <div class="container-fluid">
        <div class="grid-filter-top-sticky row">
        <div class="py-2 text-left my-4 col-md-10">
            @foreach($dishcourses as $dishcourse)

                <a class="btn btn-sm @if(isset($filters['dishcourse_id']) && $filters['dishcourse_id'] == $dishcourse->id ) btn-warning text-white @else btn-outline-warning @endif   btn-round  mb-2" href="{{ Route('recipes') }}?{{ http_build_query(['filters'=>array_merge($filters,['dishcourse_id' => $dishcourse->id])]) }}">{{ $dishcourse->name }} ({{ $dishcourse->recipes_count }})</a>

            @endforeach
        </div>
        <div class="col-md-2 py-4">

                <form action="{{ Route('recipes') }}">
                    @foreach ($filters as $filter => $value)
                        @if ($filter != 'order_by')
                            <input type="hidden" name="filters[{{ $filter }}]" value="{{ $value }}">
                        @endif
                    @endforeach
                    <select name="filters[order_by]" id="" class="form-control submit-on-change">
                        <option @if($filters['order_by'] == 'release_date') selected @endif value="release_date">@lang('all.sort by release date')</option>
                        <option @if($filters['order_by'] == 'recommended_for_you') selected @endif value="recommended_for_you">@lang('all.sort by recommended for you')</option>
                    </select>
                </form>

        </div>
    </div>
  </div>

  <div class="container-fluid" >

    <div class="row">

        @foreach($recipes as $recipe )
            <div class="col-md-2">
                @include('front.components.recipe-result')
            </div>
        @endforeach

    </div>

  </div>

</div>




@endsection
