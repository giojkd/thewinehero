@extends('front.main')
@if (!$consortium->customizable_layout)
@section('content')
<div class="consortium">
  <div class="new-curry overview">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-12">
          <div class="overview-inner">

            <nav class="navbar navbar-expand-md navbar-dark">
              <button class="navbar-toggler" type="button" data-toggle="collapse"
              data-target="#collapsibleNavbar">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="collapsibleNavbar">
              <ul class="navbar-nav">
                <li class="nav-item active">
                  <a class="nav-link" href="#overview" toggleActiveLink scrollOffset="-125">@lang('all.Overview')</a>
                </li>
                @if ($consortium->manufacturers && $consortium->manufacturers->count() > 0)
                    <li class="nav-item">
                    <a class="nav-link" href="#manufacturers" toggleActiveLink  scrollOffset="-125">@lang('all.Manufacturers')</a>
                    </li>
                @endif

                @if(!is_null($consortium->custom_blocks) && $consortium->custom_blocks != '')
                    @foreach (json_decode($consortium->custom_blocks,1) as $block)
                        <li class="nav-item">
                        <a class="nav-link" href="#{{ Str::slug($block['title'].' block') }}" toggleActiveLink  scrollOffset="-125">{{ $block['title'] }}</a>
                        </li>
                    @endforeach
                @endif

                @if ($consortium->products)

                <li class="nav-item">
                  <a class="nav-link" href="#our_products" toggleActiveLink  scrollOffset="-125">@lang('all.Our products')</a>
                </li>

                @endif

                <li class="nav-item">
                  <a class="nav-link" href="#contacts" toggleActiveLink  scrollOffset="-125">@lang('all.Contacts')</a>
                </li>

                @if($consortium->experiences)
                    <li class="nav-item">
                    <a class="nav-link" href="#our_experiences" toggleActiveLink  scrollOffset="-125">@lang('all.Our experiences')</a>
                    </li>
                @endif

                <li class="nav-item" style="padding-top: 5px;">
                  @include('front.components.favorite-toggler',['item' => $consortium])
                </li>

              </ul>
            </div>
          </nav>

        </div>
      </div>
    </div>
  </div>
</div>
<div class="course-cover background-cover justify-content-center d-flex" id="overview">
  <div class="">
    <img class="d-flex img-fluid background-cover-img" src="{{Route('ir',['size' => '2560','filename' => $consortium->photos[0]])}}" alt="@altTagMl($consortium->photos[0])">
  </div>
  <div class="course-cover-content-wrapper">
    <div class="d-flex h-100 w-100 justify-content-center">
      <div class="d-flex align-self-center">
        <div class="text-center">
          <h1>{{$meta['h1']}}</h1>
          {{-- <span class="text-white">with</span><br>  --}}
            <img style="height:80px" src="{{ url($consortium->logo) }}" alt="{{ $consortium->name }}">

        </div>
      </div>
    </div>
  </div>

</div>


@if ($consortium->custom_icons != '')

<section class="calander">
    <div class="container">
        <ul class="row bg-white features-list list-unstyled">
            @foreach (json_decode($consortium->custom_icons,1) as $icon)
                <li class="d-block col-6 col-md-3 text-center py-3 border-right">
                    <img class="feature-icon" src="{{ url($icon['image']) }}" alt="@altTagMl($icon['image'])"><br>
                    <h3>{{ $icon['title'] }}</h3>
                </li>
            @endforeach
        </ul>
    </div>
</section>

@endif
@hss('50')
@if(!is_null($consortium->photos))
    <div class="slick-carousel carousel-stripe">
      @foreach($consortium->photos as $image)
          <img src="{{Route('ir',['size' => 'h320','filename' => $image])}}" alt="@altTagMl($image)">
      @endforeach
    </div>
@endif
@hss('50')

<div class="manufacturer-custom-blocks">
    @if(!is_null($consortium->custom_blocks) && $consortium->custom_blocks != '')
        @foreach (json_decode($consortium->custom_blocks,1) as $block)
            <div class="container" id="{{ Str::slug($block['title'].' block') }}">
                <div class="row">
                    <div class="col text-center">
                        <h2>{{ $block['title'] }}</h2>
                        <div class="card shadow d-inline-block p-2 w-100">
                            <div class="card-body best-readable text-justify">
                                {!! $block['content'] !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    @endif
</div>

@if($consortium->manufacturers && $consortium->manufacturers->count() > 0)
    <div class="container" id="manufacturers">
            <div class="row">
                <div class="col text-center">
                    <h2>@lang('all.Consortium members')</h2>
                    <div class="card shadow d-inline-block p-2 w-100">
                      <div class="card-body text-justify best-readable">
                        @foreach ($consortium->manufacturers as $manufacturer)
                            <a href="{{ $manufacturer->makeUrl() }}"><img class="consortium-logo" src="{{ url($manufacturer->logo) }}" alt="{{$manufacturer->name}}"></a>
                        @endforeach
                      </div>
                    </div>

                </div>
            </div>
    </div>
    @hss('50')
@endif

@if($consortium->products && $consortium->products->count() > 0)
   <div class="container" id="our_products">
       <div class="row">
           <div class="col-md-12 text-center">
               <h2>@lang('all.Our products')</h2>
           </div>
       </div>
   </div>
    <div class="container">
        <div class="row">
            @foreach ($consortium->products as $product)
                <div class="col-md-4 col-6">
                    @include('front.components.product-result')
                </div>
            @endforeach
        </div>

    </div>
@endif




@if($consortium->articles && $consortium->articles->count() > 0)
   <div class="container" id="articles">
       <div class="row">
           <div class="col-md-12 text-center">
               <h2>@lang('all.articles') @lang('all.on') {{ $consortium->name }}</h2>
           </div>
       </div>
   </div>
    <div class="container">
        <div class="row">
            @foreach ($consortium->articles as $article)
                <div class="col-md-4 col-6">
                    @include('front.components.article-result')
                </div>
            @endforeach
        </div>

    </div>
@endif


@if($consortium->videos && $consortium->videos->count() > 0)
   <div class="container" id="videos">
       <div class="row">
           <div class="col-md-12 text-center">
               <h2>@lang('all.videos') @lang('all.on') {{ $consortium->name }}</h2>
           </div>
       </div>
   </div>
    <div class="container">
        <div class="row">
            @foreach ($consortium->videos as $video)
                <div class="col-md-4 col-6">
                    @include('front.components.video-result')
                </div>
            @endforeach
        </div>

    </div>
@endif

<div class="text-center" id="contacts">
    <h2>@lang('all.Contacts') {{ $consortium->name }}</h2>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
            <div id="consortiumMap">

            </div>
            </div>
            <div class="col-md-6 ">

                <div class="card mt-4 mt-md-0">
                    <div class="card-body text-center text-md-right">
                    <p class="text-warning h4"><i class="fa fa-phone"></i> @lang('all.Phone')</p> <a class="text-dark" href="tel:{{ $consortium->telephone }}">{{ $consortium->telephone }}</a>
                    <hr>
                    <p class="text-warning h4"><i class="fa fa-globe"></i> Web</p> <a class="text-dark" href="{{ $consortium->website }}" target="_blank" rel="nofollow"> {{ $consortium->website }}</a>
                    <hr>
                    <p class="text-warning h4"><i class="fa fa-envelope"></i> Email</p> <a class="text-dark" href="mailto:{{ $consortium->email }}">  {{ $consortium->email }}</a>

                </div>
            </div>

            </div>
        </div>
    </div>
</div>

</div>

@endsection

@push('scripts')
  <style>
    #consortiumMap{
      height: 480px;
      width: 100%;
    }

  </style>
    @if(isset($consortium->address))
    <script>



        function initMap() {
            map = new google.maps.Map(
                document.getElementById('consortiumMap'),
                {center: new google.maps.LatLng({{$consortium->address['latlng']['lat']}}, {{$consortium->address['latlng']['lng']}}), zoom: 16});

            var iconBase = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/';

            var features = [
            {
                position: new google.maps.LatLng({{$consortium->address['latlng']['lat']}}, {{$consortium->address['latlng']['lng']}}),
            }
            ];

            // Create markers.
            for (var i = 0; i < features.length; i++) {
            var marker = new google.maps.Marker({
                position: features[i].position,
                map: map
            });
            };
        }


    </script>



    <script defer src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_API_KEY') }}&callback=initMap"> </script>
    @endif


    <script>
        $(function(){
            $('.slick-carousel').slick({
                 slidesToShow: 3,
                slidesToScroll: 1,
                autoplay: true,
                infinite: true,
                centerMode: true,
                variableWidth: true,
                dots: true,
                autoplaySpeed: 2000,
                responsive: [
                    {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                    },
                    {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                    },
                    {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            });
        })
    </script>
@endpush

@else

    @section('content')
        {!! $pageContent !!}
    @endsection

@endif
