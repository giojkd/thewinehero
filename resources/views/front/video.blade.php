@extends('front.main')
@section('content')
    <div class="video-player">
        @if (is_null($video->yt_video_id))
            @if (is_null($video->embed_code))
                <video src="{{ url($video->file) }}" controls poster="posterimage.jpg" controlsList="nodownload">

                </video>
            @else
                {!! $video->embed_code !!}
            @endif
        @else
                <div class="h-100 vw-100" id="YouTubeVideoPlayer" ></div>
        @endif
    </div>
    <div class="video-spechs">
        <div class="container">
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mt-4">
                            <li class="breadcrumb-item"><a class="text-warning" href="{{ url(App::getLocale()) }}">Home</a></li>
                            <li class="breadcrumb-item"><a class="text-warning" href="{{ $video->videocategory->makeUrl() }}">{{ $video->videocategory->name }}</a></li>
                        </ol>
                    </nav>
                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="d-inline-block">{{$video->name}}</h1><br>
                        </div>
                    </div>

                    <div class="video-infos">
                        <div class="row">
                            <div class="col-md-5">
                                {{-- @lang('all.views'): {{ $video->views }} • @lang('all.released')  --}}
                                {{ $video->created_at->formatLocalized('%b %d,  %Y') }}
                                @include('front.components.favorite-toggler',['item' => $video])
                                @if (!is_null($video->user))
                                    @hss('20')
                                    <span class="text-warning">
                                        <b>
                                            @if ($video->user->has_profile)
                                                @lang('all.by') <a class="text-warning" href="{{ $video->user->makeUrl() }}">{{ $video->user->name }} {{ $video->user->surname }}</a>
                                            @else
                                                @lang('all.by') <span class="text-warning">{{ $video->user->name }} {{ $video->user->surname }}</span>
                                            @endif

                                        </b>
                                    </span>
                                    <br>
                                @endif
                            </div>
                            <div class="col-md-7">
                                 <div class="row">
                                    <div class="col-md-12 text-right">
                                        @include('front.components.social-share')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <p class="video-subtitle">
                        {{ $video->subtitle }}
                    </p>
                    @hss('50')
                    @if($video->podcasts->count() > 0)
                        @foreach ($video->podcasts as $podcast)
                            {!! $podcast->spotify_embeddable_code	 !!}
                        @endforeach
                        @hss('70')
                    @endif





                    @if ($video->is_legacy)
                        <div class="video-description best-readable style-first-letter">
                            {!! $video->description !!}
                        </div>
                    @else
                        @foreach (json_decode($video->body,1) as $bodyBlock)
                            <div class="video-description best-readable style-first-letter">
                                {!!$bodyBlock['content']!!}
                            </div>
                            @if (isset($bodyBlock['image_link']) && $bodyBlock['image_link'] != '')
                                <a href="{{ $bodyBlock['image_link'] }}">
                                    <img class="w-100" alt="{{ $bodyBlock['image_alt'] }}" src="@cim(Route('ir',['size' => 'h480','filename' => $bodyBlock['image']]))">
                                </a>
                            @else
                                @if($bodyBlock['image_alt'] != '')
                                    <img class="w-100" alt="{{ $bodyBlock['image_alt'] }}" src="@cim(Route('ir',['size' => 'h480','filename' => $bodyBlock['image']]))">
                                @endif
                            @endif
                            @if ($bodyBlock['cta'] != '')
                                @include('front.includible-articles-ctas.'.$bodyBlock['cta'])
                            @endif
                        @endforeach
                    @endif



                    <hr>
                    @include('front.components.newsletter-subscription-form')
                    <hr>
                    @hss('70')
                    @include('front.components.reviews-container',['reviews' => $video->reviews,'id' => $video->id,'reviewable_type' => $video->getModelNameFull()])
                </div>
                {{--
                <div class="col-md-3 p-4">
                    @foreach ($video->sameCategoryItems() as $item)
                        @if ($item->active)
                            @include('front.components.video-result',['video' => $item])
                        @endif

                    @endforeach
                </div>
                 --}}
            </div>
        </div>
    </div>

    <div class="container mt-4">
        <div class="row">
            <div class="col-md-8 offset-md-2">

                <div class="row">
                    @foreach ($video->sameCategoryItems() as $item)
                        @if ($item->active)
                            <div class="col-md-4 p-4">

                                    @include('front.components.video-result',['video' => $item])

                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection


@push('scripts')

    @if (!is_null($video->yt_video_id))
        <script>

            var tag = document.createElement('script');
            tag.src = "https://www.youtube.com/iframe_api";
            var firstScriptTag = document.getElementsByTagName('script')[0];
            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

            var player;
            function onYouTubeIframeAPIReady() {

                player = new YT.Player('YouTubeVideoPlayer', {
                    videoId: '{{ $video->yt_video_id }}', // YouTube Video ID
                    width: '100%', // Player width (in px)
                    height: '100%', // Player height (in px)
                    playerVars: {
                        playlist: '{{ collect($yt_playlist)->implode(',') }}',
                        autoplay: 0, // Auto-play the video on load
                        autohide: 1,
                        disablekb: 1,
                        controls: 1, // Hide pause/play buttons in player
                        showinfo: 1, // Hide the video title
                        modestbranding: 1, // Hide the Youtube Logo
                        loop: 0, // Run the video in a loop
                        fs: 0, // Hide the full screen button
                        autohide: 0, // Hide video controls when playing
                        rel: 0,
                        enablejsapi: 1,
                        //color: '#fab417' only white and red
                    },
                    events: {
                        onReady: function(e) {
                            e.target.mute();
                            e.target.setPlaybackQuality('hd1080');
                            //e.target.playVideo();

                        },
                        onStateChange: function(e) {
                            /*
                        if (e && e.data === 1) {
                            var videoHolder = document.getElementById('home-banner-box');
                            if (videoHolder && videoHolder.id) {
                                videoHolder.classList.remove('loading');
                            }
                        } else if (e && e.data === 0) {
                            e.target.playVideo()
                        }
                        */
                        }
                    }
                });

            }

        </script>
    @endif

    <style>
        .video-player iframe {
            width: 100% !important;
            height: 80vh !important;
        }

        .best-readable a{
            color: #ffc107;
        }

    </style>


@endpush
