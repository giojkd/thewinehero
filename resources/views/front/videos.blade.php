@extends('front.main')
@section('content')
    @hss('50')

    <div class="container-fluid">
        <div class="row">
        <div class="col-md-12 text-center">

                @if (isset($selectedVideoCategory))
                    <h1 class="text-yellow">
                        {{ $meta['h1'] }}
                    </h1>
                    <h2 class="text-center small">{{ $selectedVideoCategory->subtitle }}</h2>
                @else
                    <h1 class="text-yellow">
                        @lang('all.Recommended')
                    </h1>
                    <h2 class="text-center small">{{ Setting::get('videos_subtitle_'.App::getLocale()) }}</h2>

                @endif


        </div>
    </div>
    @hss('50')
    <div class="row">
        <div class="py-2 text-center col-md-6 offset-md-3">
            @foreach ($videocategories->keyBy('id') as $videocategory_id => $videocategory)

                <a class="btn btn-sm btn-warning btn-round text-white mb-2" href="{{ $videocategory->makeUrl() }}">
                    {{ $videocategory->name }}
                </a>

            @endforeach
        </div>
    </div>

    <div class="row">
        @if (isset($orderBy))
            <div class="col-md-2 text-center offset-md-5">
                <div class="p-2">
                    <form action="{{ Route('videos') }}">
                        <select name="orderBy" id="" class="form-control submit-on-change">
                            <option @if ($orderBy == 'release_date') selected @endif value="release_date">@lang('all.sort by release date')</option>
                            <option @if ($orderBy == 'recommended_for_you') selected @endif value="recommended_for_you">@lang('all.sort by recommended for
                                you')</option>
                        </select>
                    </form>
                </div>
            </div>
        @endif
    </div>
    @hss('50')
    </div>
    <div class="videos">
        <div class="container">

            <div class="row">
                <div class="col-md-8 col-md-2 offset-md-2">
                    @if (!$agent->isMobile())
                        <div class="row">
                            <div class="col-md-12">
                                @include('front.components.video-result',[
                                    'video' => $videos->first(),
                                    'coverImageHeight' => 1600,
                                    'coverHeight'=> 480
                                ])
                            </div>
                        </div>
                    @endif
                    @foreach ($videos->chunk(4) as $chunks)
                        <div class="row">
                            @foreach ($chunks as $key => $video)
                                @if (get_class($video) == 'App\Models\Video')
                                    <div class="col-md-6">
                                        @include('front.components.video-result')
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection


@push('scripts')
    <script type="text/javascript">
        var video_id;
        var video;
        $('.video-item').hover(function() {
            video_id = $(this).data('video-id');
            video = $('#video-' + video_id).get(0);
            video.play();
        }, function() {
            //video.pause();
            //video.load();
            video.currentTime = 0;
            video.pause();
        })
    </script>
@endpush
