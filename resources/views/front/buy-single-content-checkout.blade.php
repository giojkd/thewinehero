@extends('front.main')
@section('content')

    <div class="container mt-4">
        <div class="row">
            <div class="col">
                <h1>@lang("all.You're buying")</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5">
                @include("front.components.{$modelType}-result",[$modelType => $model])
                @hss('30')
                <div class="text-center">
                    <h3>
                        @if ($model->price > 0)
                            @lang('all.for'): {{ money($model->applyPlanDiscount($modelType,$model->price),'EUR') }}
                        @else
                            @lang('all.for free checkout single content')
                        @endif

                    </h3>
                </div>
            </div>
            <div class="col-md-7">
                    @if ($model->price > 0)
                         @if($payment_methods->count() > 0)
                            <h3>@lang('all.Choose a payment method')</h3>
                            @foreach($payment_methods as $payment_method)
                                <div class="card mb-3">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-8">
                                                @lang('all.Credit card:') <b>{{ Str::ucfirst($payment_method->card->brand) }} ({{ $payment_method->card->last4 }})</b>
                                                @lang('all.Exp:') <b>{{ $payment_method->card->exp_month }}/{{ $payment_method->card->exp_year }}</b>
                                            </div>
                                            <div class="col-md-4 col-12 text-right">
                                                <form action="{{ Route('buySingleContent') }}" method="POST">
                                                    @csrf
                                                    <input type="hidden" name="payment_method" value="{{ $payment_method->id }}">
                                                    <input type="hidden" name="model" value="{{ $modelType }}">
                                                    <input type="hidden" name="old_card" value="1">
                                                    <input type="hidden" name="model_id" value={{ $model->id  }}>
                                                    <button type="submit" class="mt-2 mt-md-0 btn btn-warning text-white btn-block">@lang('all.Use this card')</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                            <div>
                                @if($payment_methods->count() > 0)
                                    <h3>@lang('all.Or set a new one')</h3>
                                @endif
                                <div class="form-group">
                                    <label for="">@lang("all.Cardholder's name")</label>
                                    <input id="card-holder-name" type="text" class="form-control" required value="{{ $user->full_name() }}">
                                </div>
                                <label for="">@lang('all.Credit card number')</label>
                                <div id="new-card-form-wrapper" class="form-control-stripe"></div>
                                @include('front.components.safe-payment')
                            </div>
                    @endif
                    <div style="height: 70px; overflow: scroll; padding: 15px; font-size: 12px; background: white;">@lang('all.terms and conditions for classes')</div>

                    @if ($model->price > 0)
                             @hss('20')
                                <label class="mb-0"><input required type="checkbox" name="" id=""> @lang('all.accept terms and conditions')</label>
                            @hss('20')
                            <button data-secret="{{ $client_secret_intent->client_secret }}" id="card-button" type="button" class="btn btn-warning mt-0 btn-block text-white btn-lg">@lang('all.Confirm')</button>
                    @else
                        <form action="{{ Route('buySingleContent') }}" method="POST">
                            @csrf
                             @hss('20')
                                <label class="mb-0"><input required type="checkbox" name="" id=""> @lang('all.accept terms and conditions')</label>
                            @hss('20')
                            <input type="hidden" name="model" value="{{ $modelType }}">
                            <input type="hidden" name="model_id" value={{ $model->id  }}>
                            <button type="submit" class="btn btn-warning mt-0 btn-block text-white btn-lg">@lang('all.Confirm')</button>
                        </form>
                    @endif

            </div>
        </div>
    </div>

@endsection
@push('scripts')


    <script src="https://js.stripe.com/v3/"></script>
    <script>
        $(() => {
            const stripe = Stripe('{{ env('STRIPE_KEY') }}');
            const elements = stripe.elements();
            const cardElement = elements.create('card');
            cardElement.mount('#new-card-form-wrapper');
            const cardHolderName = document.getElementById('card-holder-name');
            const cardButton = document.getElementById('card-button');
            const clientSecret = cardButton.dataset.secret;
            cardButton.addEventListener('click', async (e) => {
                const { setupIntent, error } = await stripe.confirmCardSetup(
                    clientSecret, {
                        payment_method: {
                            card: cardElement,
                            billing_details: { name: cardHolderName.value }
                        }
                    }
                );
                if (error) {
                    console.log(error);
                    alert(error.message);
                    //alert('unsuccess')
                } else {
                    console.log(setupIntent);
                    setupIntent.model = '{{ $modelType }}';
                    setupIntent.model_id = {{ $model->id }};
                    setupIntent._token = '{{ csrf_token() }}';
                    $.post('{{ Route('buySingleContent') }}',setupIntent,function(r){
                        if(r.status == 1){
                            location.href= '{{ $model->makeUrl() }}';
                        }
                    },'json')
                }
            });
        })
    </script>


@endpush
