@extends('front.main')


@section('content')
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <div class="mt-4">
                        <h1 class="text-warning">@lang('all.Thank you!')</h1>
                        <p>
                            @lang('all.Thank you! Check your email for your purchase confirmation!')<br>
                            <a href="/" class="btn btn-warning text-white">@lang('all.Shop more!')</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
@endsection
