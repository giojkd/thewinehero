@extends('front.main')

@section('content')

    <div class="container-fluid article">
        <div class="row">
            <div class="col-md-12">
                <div class="text-center py-4">
                    <h1 class="display-4">@lang('all.aimama')</h1>
                    <p class="lead">@lang('all.aimama subtitle')</p>
                    <hr class="my-4">
                    <p>@lang('all.aimama hint')</p>
                </div>
                <div class="search-results">
                      <div class="container-fluid">
                            <div class="row">
                    @foreach ($recommendedItems as $item)



                                                <div class="col-md-3">
                                                    @include('front.components.'.$item->getModelName().'-result',[$item->getModelName() => $item])
                                                </div>


                        @hss('50')
                    @endforeach
                     </div>
                        </div>
                </div>
            </div>
        </div>
    </div>

@endsection
