@extends('front.main')

@section('content')


  <div class="new-curry overview">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-12">
          <div class="overview-inner">

            <nav class="navbar navbar-expand-md navbar-dark">
              <button class="navbar-toggler" type="button" data-toggle="collapse"
              data-target="#collapsibleNavbar">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="collapsibleNavbar">
              <ul class="navbar-nav">
                <li class="nav-item active">
                  <a class="nav-link" href="#overview" toggleActiveLink scrollOffset="-75">@lang('all.Overview')</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#ingredients" toggleActiveLink  scrollOffset="-75">@lang('all.Ingredients')</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#preparation" toggleActiveLink  scrollOffset="-75">@lang('all.Preparation')</a>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="#winepairing" toggleActiveLink  scrollOffset="-75">@lang('all.Wine Pairing')</a>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="#reviews" toggleActiveLink  scrollOffset="-75">@lang('all.Reviews')</a>
                </li>

                <li class="nav-item" style="padding-top: 5px;">
                  @include('front.components.favorite-toggler',['item' => $recipe])
                </li>
                <!--
                <li class="nav-item">
                <a class="nav-link" href="#">Enrollment options</a>
              </li>
            -->
          </ul>
        </div>
      </nav>

    </div>
  </div>
</div>
</div>

</div>
<div id="overview">
  <section class="curry">
    <div class="container">
      <div class="row">
        <div class="col-12 col-md-6 remove-padding">
          <div class="curry-left">
            <h1>{{$recipe->name}}</h1>
            <h2>{{$recipe->subtitle}}</h2>

            <ul class="review-score-stars">
              @for($i = 0; $i < (int)$recipe->reviews_avg; $i++)
                <li>
                  <i class="fas fa-star"></i>
                </li>
              @endfor
              @for( $i = (int)$recipe->reviews_avg; $i < 5;  $i++)
                <li>
                  <i class="far fa-star"></i>
                </li>
              @endfor
              <li>
                ( @lang('all.Scored x on n reviews',['x' => $recipe->reviews_avg, 'n' => $recipe->reviews->count()]) )
              </li>
            </ul>


            <div class="delicious">
              <div class="delicious-inner row">
                <div class="col text-right">
                  <p class="new-color">{{$recipe->reviews->first()->user->name}}:</p>
                </div>
                <div class="col">
                  <p>{{Str::limit($recipe->reviews->first()->description,150,'...')}}</p>
                  <p><a href="#reviews">@lang('all.read all reviews...')</a></p>
                </div>
                <div class="col">
                  <p><img src="@img('new-play-btn.png')" ></p>
                </div>
              </div>
            </div>

          </div>
        </div>

        @include('front.components.social-share')

        <div class="col-12 col-md-6 remove-padding">
          <div class="curry-right">
            <img src="{{Route('ir',['size' => 1200,'filename'=>$recipe->photos[0]])}}" alt="@altTagMl($recipe->photos[0])">
            <a class="curry-play-btn" href="#preparation" scrolloffset="-75">
              <img src="@img('play-btn.png')">
            </a>
          </div>
        </div>


      </div>
      @hss('50')
      <div class="row">
        <div class="col-md-12">

          <div class="best-readable recipe-description style-first-letter">
            {!!$recipe->description!!}
          </div>
        </div>
      </div>
    </div>
  </section>
</div>


<section class="calander">
  <div class="container">
    <div class="row bg-white features-list">
      <div class="col-6 col-md-3 text-center py-3 border-right">
          <i class="fa fa-hourglass-2 fa-3x"></i>
          <h3>{{$recipe->preparation_time}} @lang('all.minutes to prepare')</h3>
            @if(!is_null($recipe->preparation_time_notes))
                      {{$recipe->preparation_time_notes}}
            @endif
      </div>
      <div class="col-6 col-md-3 text-center py-3 border-right">
        <i class="fa fa-list fa-3x"></i><br>
         <b></b>
        <h3>{{$recipe->ingredients->count()}} @lang('all.ingredients')</h3>
      </div>
      <div class="col-6 col-md-3 text-center py-3 border-right">
          <i class="fa fa-running fa-3x"></i><br>

            <h3>{{$recipe->calories}} kCal</h3>
      </div>
      <div class="col-6 col-md-3 text-center py-3 border-right">
            <i class="fa fa-list-ol fa-3x"></i><br>
            <h3>{{$recipe->steps->count()}} @lang('all.steps to complete the preparation')</h3>

      </div>

    </div>
    <div class="button enroll">
        <a href="#">@lang('all.Cooked hundreds of times!')</a>
      </div>
  </div>
</section>


<section class="ingredients-section" id="ingredients">
     @if($recipe->podcasts->count() > 0)
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <div class="mb-4">
                        <div class="ingredients-section-inner">
                        <h1>Podcast</h1>
                        </div>
                        @foreach ($recipe->podcasts as $podcast)
                            {!! $podcast->spotify_embeddable_code	 !!}
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        @hss('70')
    @endif
  <div class="container">
    <div class="row">

      <div class="col-12 col-md-12">
        <div class="ingredients-section-inner">
          <h1><img src="@img('ingrediant.png')"> @lang('all.Ingredients')</h1>
            <h2>
                @lang('all.for n servings',['n' => $recipe->servings])
            </h2>
          @if(!is_null($recipe->servings_notes))
            <h3>{{$recipe->servings_notes}}</h3>
          @endif
          <!--<p>Check the ingredients you have and thos you still need to buy as a shopping list and send it to your email or sms</p>-->
        </div>
      </div>
    </div>
  </div>

  <div class="ingredients">
    <div class="container">
      <div class="row">
        <div class="col-12 col-md-6">
          <div class="ingredients-section-inner-left">
            @if(is_null($recipe->ingredientslists))
            <ul>

              @foreach($recipe->recipeingredients as $ingredientIndex =>  $ingredient)
                @if($ingredientIndex < 3)
                    <li>
                    <img src="@img('circle.png')">
                    <!--<img src="@img('check.jpg')">-->
                    <span class="new-color">{{$ingredient->ingredient->name}}</span>
                    {{$ingredient->quantity}} @all('all.'.$ingredient->measurement_unit)

                    @if(!is_null($ingredient->notes))
                        <span class="new-size">({{$ingredient->notes}})</span>
                    @endif
                    </li>
                @endif

              @endforeach
                <div class="alert alert-secondary">
                    @lang("Do you want to see all the recipe's ingredients?")
                    <a href="#enrollment-options" scrolloffset="-140" class="btn btn-block btn-warning text-white">@lang('all.Yes!')</a>
                </div>
            </ul>
            @else

                @foreach($recipe->ingredientslists as $list)
                    <h1>{{ $list->name }}</h1>
                    <ul>
                    @foreach($list->recipeingredients as $ingredientIndex =>  $ingredient)
                        @if($ingredientIndex < 3)
                            <li>
                                <img src="@img('circle.png')">
                                <!--<img src="@img('check.jpg')">-->
                                <span class="new-color">{{$ingredient->ingredient->name}}</span>
                                {{$ingredient->quantity}} @lang('all.'.$ingredient->measurement_unit)

                                @if(!is_null($ingredient->notes))
                                    <span class="new-size">({{$ingredient->notes}})</span>
                                @endif
                            </li>
                        @endif

                    @endforeach
                        </ul>


                @endforeach

                 <div class="alert alert-secondary">
                    @lang("Do you want to see all the recipe's ingredients?")
                    <a href="#enrollment-options" scrolloffset="-140" class="btn btn-block btn-warning text-white">@lang('all.Yes!')</a>
                </div>
            @endif

          </div>

          <!--
          <div class="ingredients-btn">
          <a href="#"><img src="images/add-cart.png"> Add the ingredients to your shopping list</a>
        </div>
      -->
    </div>

    <div class="col-12 col-md-6">

      <div class="ingredients-section-inner-right">
        <div class="ingredients-section-inner-left-inner">
          <div class="ingredients-section-inner-left">
            <h1>@lang('all.Perfect Wine Match')</h1>
            <p></p>
            <p><a href="#winepairing" scrollOffset="-75">@lang('all.more pairings...')</a></p>
          </div>
          <div class="ingredients-section-inner-right">
            <h1><img src="@img('flippo.png')" alt="Filippo Bartolotta"></h1>
            <p class="new-color">Filippo Bartolotta</p>
          </div>
        </div>
        <section class=" slider" id="filippo-slider">
          <div class="repeater">
            <div class="slippo-slider">
              <div class="Filippo Bartolotta-inner">
                @include('front.components.product-result',['product' => $recipe->products->first()])
              </div>
            </div>
          </div>
        </section>
      </div>

    </div>
  </div>
</div>
</div>
</section>

<div id="preparation">
  <section class="ingredients-section">
    <div class="container">
      <div class="row">

        <div class="col-12 col-md-12">
          <div class="ingredients-section-inner">
            <h1><img src="@img('ingrediant.png')">
              @lang('all.Preparation')
              <!--
              <span class="new-design">
              <img src="@img('full-screen.png')"> Go Full Screen
            </span>
          -->
        </h1>
        <h2>
            @lang('for n servings',['n' => $recipe->servings])
        </h2>
        @if(!is_null($recipe->servings_notes))
          <h3>{{$recipe->servings_notes}}</h3>
        @endif
        <!--<p>Check the ingredients you have and thos you still need to buy as a shopping list and send it to your email or sms</p>-->
      </div>
    </div>
  </div>
</div>
</section>

@foreach([$recipe->steps->first()] as $index => $step)
  <section class="curry new-border">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12 col-md-2">
          <div class="step">
            <p>@lang('all.Step')</p>
            <p><b>{{$index+1}}</b></p>
            <p class="new-color">@lang('all.Of') {{$recipe->steps->count()}}</p>
          </div>
        </div>

        <div class="col-12 col-md-10">
          <div class="transfer">
            <div class="transfer-inner-left">
              <p>{{$step->name}}</p>
              <div class="more-text">
                {!! $step->description !!}
              </div>
              @isset($step->photos[0])


                @foreach($step->photos as $photo)
                  <div class="recipe-step-photo">
                    <img src="{{Route('ir',['size' => 'h240','filename' => $photo])}}" alt="@altTagMl($photo)">
                  </div>
                @endforeach


              @endif
            </div>


          </div>
          <!-- <p class="btn"><a href="#">Done</a></p> -->
        </div>
        <div class="col-12 col-md-2">
        </div>
      </div>
    </div>
  </section>
@endforeach
@hss('40')
<div class="row">
    <div class="col-md-4 offset-md-4">
            <div class="alert alert-secondary">
                    @lang("Do you want to see all the recipe's ingredients?")
                    <a href="#enrollment-options" scrolloffset="-140" class="btn btn-block btn-warning text-white">@lang('all.Yes!')</a>
                </div>
    </div>
</div>
@hss('40')

</div>

<div id="winepairing">


  <section class="ingredients-section">
    <div class="container">
      <div class="row">

        <div class="col-12 col-md-6">
          <div class="ingredients-section-inner">
            <h1><img src="@img('cup.png')" style="height: 60px; width: auto;">
              @lang('all.Wine pairing')
              <!--
              <span class="new-design">
              <img src="@img('full-screen.png')"> Go Full Screen
            </span>
          -->
        </h1>
      </div>
    </div>
    <div class="col-12 col-md-6">

    </div>
  </div>
</div>
</section>


<section class="wine-text">
  <div class="owl-carousel paired-wines d-none d-md-block">
    @foreach($recipe->products as $product)
      @include('front.components.product-result')
    @endforeach

  </div>
   <div class="paired-wines d-md-none">
       <div class="row">


            @foreach($recipe->products as $product)
                <div class="col-6">
                    @include('front.components.product-result')
                </div>
            @endforeach
        </div>
  </div>
</section>
</div>

<div id="reviews">

    <div class="container">
      <div class="row">
        <div class="col-12 col-md-6">
          <div class="ingredients-section-inner">
            <h1>
              @lang('all.Reviews')
            </h1>
          </div>
        </div>
      </section>
      <div class="container">
        <div class="row">
          <div class="col-12 col-md-12">
            @include('front.components.reviews-container',['reviews' => $recipe->reviews,'id' => $recipe->id,'reviewable_type' => $recipe->getModelNameFull()])
          </div>
        </div>
      </div>

    </div>
  </div>
</div>

<div class="container-fluid" id="enrollment-options">
        <div class="row">
            <div class="col text-center">
                <h2>@lang('all.How to read the entire recipe?')</h2>
                @if(!$recipe->is_premium)
                    @auth
                        @if(Auth::user()->hasActivePlan())
                            @if($canPurchase)

                                @lang('all.More readable recipes',['n' => Auth::user()->plan->recipes])

                                <br>
                                <div class="mt-2">
                                    <form method="POST" action="{{ Route('purchaseContent',['model' => 'recipe','id' => $recipe->id]) }}">
                                        @csrf
                                        <button type="submit" class="btn btn-warning btn-large text-white btn-lg">@lang('all.Read now!')</button>
                                    </form>
                                </div>

                            @else
                                <div class="alert alert-danger">
                                    @lang('all.The recipes included in your plan for this month are over, but you can still upgrade to another plan')
                                </div>
                                @include('front.components.enrollment-options',['content' => $recipe])
                            @endif
                        @else
                            @include('front.components.enrollment-options',['content' => $recipe])
                        @endif
                    @endauth
                    @guest
                        @include('front.components.enrollment-options',['content' => $recipe])
                    @endguest
                @endif
 @include('front.components.buy-single-content',
            [
              'content' => [
                  'name' => $recipe->getModelName(),
                  'price_compare' => ($recipe->applyPlanDiscount($recipe->getModelName(),$recipe->price) != $recipe->price) ? $recipe->price : 0,
                  'price' => $recipe->applyPlanDiscount($recipe->getModelName(),$recipe->price),
                  'id' => $recipe->id,
                  'is_premium' => $recipe->is_premium
                  ]
            ]
        )
            </div>
        </div>
    </div>


@if(!is_null($recipe->getRecommendations()))
  <div id="recommendations">
    <section class="ingredients-section">
      <div class="container">
        <div class="row">
          <div class="col-12 col-md-6">
            <div class="ingredients-section-inner">
              <h1>
                @lang('all.You may like')
              </h1>
            </div>
          </div>
        </section>
        <div class="container">
          <div class="row">
            @foreach($recipe->getRecommendations() as $recipe)
              <div class="col-12 col-md-3">
                @include('front.components.recipe-result')
              </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
@endif






@endsection
