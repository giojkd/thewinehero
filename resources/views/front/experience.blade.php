@extends('front.main')


@section('content')

     @hss('50')

        <div class="slick-carousel carousel-stripe">
        @foreach($experience->photos as $image)

            <img src="{{Route('ir',['size' => 'h320','filename' => $image])}}" alt="@altTagMl($image)">

        @endforeach
        </div>

        @hss('50')
    <div class="container">

        <div class="row">
            <div class="col-md-7">

                <div class="experience-page">

                    <div class="my-4">
                        <h2>Description</h2>
                        <div class="best-readable text-left">
                            {!! $experience->description !!}
                        </div>
                    </div>
                    <hr>
                    <div class="my-4">
                        <h2>What is included</h2>
                        <div class="best-readable">
                            {!! $experience->whats_included !!}
                        </div>
                    </div>
                    <hr>
                    <div class="my-4">
                        <h2>What is not included</h2>
                        <div class="best-readable">
                            {!! $experience->whats_not_included !!}
                        </div>

                    </div>
                    @if (!is_null($experience->address))
                        <div class="my-4">
                            <h2>Address</h2>
                            <div class="best-readable mb-4">
                                <i class="fa fa-map-marker"></i>   {{ $experience->address['value'] }}
                            </div>
                            <div id="experienceMap"></div>
                        </div>
                    @endif
                </div>

            </div>
            <div class="col-md-4 offset-md-1 pt-4">
                <div class="right-main product-page">
                    <div class="calendar_product sticky-top">
                        <div class="calendar-top-area ">
                        <div class="calendar-top-area-left  col-sm-12">
                            <h1 class="mt-2">{{$experience->name}}</h1>
                            <p>{{$experience->subtitle}}</p>
                        </div><!-- calendar-top-area-left -->

                        <div class="calendar-top-area-right  col-sm-5">
                           {{--  @if($product->reviews->count()>0)
                            <span>{{number_format($product->reviews->avg('evaluation'),1)}}</span>
                            {{__('all.on')}} {{$product->reviews->count()}} {{__('all.reviews')}}
                            @endif  --}}
                        </div><!-- calendar-top-area-right -->
                        </div><!-- calendar-top-area -->

                        <div class="calendar-main-area">

                        <div class="calendar calendar-first" id="calendar_first">

                        </div>

                        </div><!-- calendar-main-area -->

                        <div class="calendar-time-area" style="margin-bottom: 15px;">


                        <div class="p-2">
                               <div class="time-zone">
                            <div class="select-time-zone">
                                <div class="row">
                                    <div class="col">
                                        What time?
                                    </div>
                                    <div class="col">
                                         <select class="form-control" id="available_times">
                                <option>0:00</option>
                            </select>
                                    </div>
                                </div>
                            </div>
                        </div><!-- time-zone -->
                        <div class="time-zone adult-zone my-2">
                            <div class="row">
                                <div class="col"> <span>
                                 {{ucfirst(__('all.adults'))}} <small id="price_per_adult_show"> (0,00 {{__('all.per adult')}})</small>
                                    </span>
                                </div>
                                <div class="col"><div class="input-group">
                                <span class="input-group-btn">
                                    <button type="button" class="btn-number red-btn-round plus-minus"  data-type="minus" data-field="quant[1]">
                                    <span class="glyphicon glyphicon-minus">-</span>
                                    </button>
                                </span>
                                <input id="adults_num" type="text" name="quant[1]" class="form-control input-number" value="2" min="1" max="100">
                                <span class="input-group-btn">
                                    <button type="button" class=" btn-number red-btn-round plus-minus" data-type="plus" data-field="quant[1]">
                                    <span class="glyphicon glyphicon-plus">+</span>
                                    </button>
                                </span>
                            </div><!-- input-group --></div>
                            </div>




                        </div><!-- adult-zone -->


                        <div class="time-zone child-zone">
                            <div class="row">
                                <div class="col"><span>{{ucfirst(__('all.children'))}} <small id="price_per_child_show"> (0,00 {{__('all.per child')}})</small></span></div>
                                <div class="col"><div class="input-group">
                                <span class="input-group-btn">
                                    <button type="button" class="btn-number red-btn-round plus-minus"  data-type="minus" data-field="quant[2]">
                                    <span class="glyphicon glyphicon-minus">-</span>
                                    </button>
                                </span>
                                <input id="children_num" type="text" name="quant[2]" class="form-control input-number" value="0" min="0" max="100">
                                <span class="input-group-btn">
                                    <button type="button" class=" btn-number red-btn-round plus-minus" data-type="plus" data-field="quant[2]">
                                    <span class="glyphicon glyphicon-plus">+</span>
                                    </button>
                                </span>
                            </div><!-- input-group --></div>
                            </div>
                        </div>





                        </div><!-- child-zone -->

                        </div><!-- calendar-main-area -->

                        <div class="p-2">
                            <table cellpadding="0" cellspacing="0" align="center" width="100%">
                                <tbody>
                                <tr>
                                    <td class="left-align">{{ucfirst(__('all.adults'))}} </td>
                                    <td id="adults_price_summary">€0,00 (€0,00 x 0 {{__('all.adults')}}) </td>
                                </tr>
                                <tr>
                                    <td class="left-align">{{ucfirst(__('all.children'))}}  </td>
                                    <td id="children_price_summary">€0,00 (€0,00 x 0 {{__('all.children')}})</td>
                                </tr>
                                <tr>
                                    <td class="left-align">{{ucfirst(__('all.total'))}} </td>
                                    <td class="grand-total">€ 0,00 </td>
                                </tr>
                                </tbody>
                            </table>
                            <form action="{{route('addToCartExperience')}}" method="get">

                                <input type="hidden" name="experience_id" value="{{$experience->id}}">
                                <input id="input_adults" type="hidden" name="adults" value="">
                                <input id="input_children" type="hidden" name="children" value="">
                                <input id="input_date" type="hidden" name="date" value="">
                                <input id="input_time" type="hidden" name="time" value="">
                                <input id="input_addons" type="hidden" name="input_addons" value="">
                                <button class="place-order-btn btn btn-warning btn-block text-white mt-4 btn-lg" type="submit">{{ucfirst(__('all.book now'))}}</button>
                            </form>
                        </div>


                    </div>
                    </div>
            </div>
        </div>
    </div>
@endsection


@push('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.0/moment.min.js"></script>

    <style>
        .grid-item { width: 200px; }
        .grid-item--width2 { width: 400px; }
    </style>
    <script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
    <script>
        $(function(){
            $('.grid').masonry({
                itemSelector: '.grid-item',
                columnWidth: 200
            });

        })

</script>
@if (!is_null($experience->address))
    <script>

       function initMap() {
        map = new google.maps.Map(
            document.getElementById('experienceMap'),
            {center: new google.maps.LatLng({{$experience->address['latlng']['lat']}}, {{$experience->address['latlng']['lng']}}), zoom: 16});

        var iconBase = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/';

        var features = [
          {
            position: new google.maps.LatLng({{$experience->address['latlng']['lat']}}, {{$experience->address['latlng']['lng']}}),
          }
        ];

        // Create markers.
        for (var i = 0; i < features.length; i++) {
          var marker = new google.maps.Marker({
            position: features[i].position,
            map: map
          });
        };
      }

    </script>
@endif

 <script defer
    src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_API_KEY') }}&callback=initMap">
    </script>

    <style>
        #experienceMap{
            height: 480px;
            width: 100%;
        }

.calendar_product {
  background: #fff;
    /* border: 1px solid #b9b9b9; */
    padding: 25px;
    border-radius: 8px;
    box-shadow: 0px 0px 12px rgba(0,0,0,0.15);
}
.calendar-top-area.col-sm-12 {
  padding: 10px;
}

.calendar-time-area img {
  width: 20px;
  position: absolute;
  left: 0;
  top: 8px;
}
.calendar-main-area.col-sm-12 {
  padding-top: 30px;
}
.time-zone.child-zone img {
  top: 5px;
}
.time-zone input.form-control.input-number {
  padding: 0;
  width: 40px !IMPORTANT;
  max-width: 40px;
  text-align: center;
  border: 0;
  outline: none;
  box-shadow: none;
  height: 25px;
  margin: 0;
}



button.btn-number.red-btn-round.plus-minus {
  background: #fab417;
  color: #fff;
  text-align: center;
  border: 0;
  width: 25px;
  height: 25px;
  padding: 0;
  border-radius: 100%;
  text-indent: unset;
  font-weight: bold;
  outline: none;
}
button.btn-number.red-btn-round.plus-minus span {
  padding-left: 0;
  height: auto;
  width: auto;
  padding: 0;
  line-height: normal;
  font-size: 24px;
  position: relative;
  top: -3px;
  float: none;
}
.select-time-zone select {
  font-size: 18px;
  border: 0;
  outline: none;
  box-shadow: none;
  text-align: right;
  width: auto;

  height: auto;
  padding-left: 5px;
  padding-right: 50px !important;
}
.calendar-time-area.col-sm-12:after {
  border-bottom: 1px solid #fab417;
  content: "";
  display: block;
  width: 100%;

  height: 1px;
  background: #fab417;
  margin: 10px auto 20px;
}


.calendar-price-table.col-sm-12 table td {
  color: #595959;
  padding-right: 10px;
  padding-bottom: 5px;
}
.calendar-price-table button.place-order-btn {
  padding: 20px 10px;
  margin-bottom: 20px;
}
    </style>


        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/js/tempusdominus-bootstrap-4.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/css/tempusdominus-bootstrap-4.min.css" />

        <style media="screen">
        td.day.active{
        background: #fab417 !important;
        }
        </style>
        <script type="text/javascript">

        var product = @json($experience);
        var availabilities = product.availabilities;
        var free_transfer = product.free_transfer;
        var price_rules = product.pricerules;
        var free_transfer_up_to = product.free_transfer_up_to;
        var enabledDates = @json(array_values($experience->availabilities->pluck('day')->unique()->toArray()));
        var selectedDate;
        var selectedTime;
        var availableTimes;
        var total = 0;
        var maxPersons = 10;
        var minPersons = 2;
        var children = 0;
        var adults = 0;

        var currencyFormatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'EUR',
        });

        var countChanges = 0;
        $('.checkbox-addon').change(function(){
            //$(this).find('label').hasClass('active');
            countChanges++;
            if(countChanges == 3){
                calculateTotals();
                countChanges = 0;
            }
        })

        function calculateTotals(){

            total = 0;

            children = parseInt($('#children_num').val());
            adults = parseInt($('#adults_num').val());

            var participants = children+adults;

            var transferred_soil = Math.ceil(participants/free_transfer_up_to);
            var free_transfer_in_math = free_transfer*transferred_soil;

            var adultsTransferWeight = adults/participants*free_transfer_in_math;
            var childrenTransferWeight = children/participants*free_transfer_in_math;
            var price_per_adult = 0;
            var price_per_child = 0;
            var adults_price = 0;
            var children_price = 0;
            var discount = 0;
            selectedTime = $('#available_times').val();
            //console.log(selectedTime);
            for(var i in availabilities){
                var av = availabilities[i];
                //console.log('comoparing '+moment(av.day).format('L')+' with '+moment(selectedDate).format('L')+' and '+moment(selectedTime,'HH:mm:ss').format('HH:mm')+ ' with '+moment(av.time,'HH:mm:ss').format('HH:mm'));
                if(moment(av.day).format('L') == moment(selectedDate).format('L') && moment(selectedTime,'HH:mm:ss').format('HH:mm') == moment(av.time,'HH:mm:ss').format('HH:mm')){

                    price_per_adult = parseFloat(av.price_per_adult);
                    price_per_child = parseFloat(av.price_per_child);

                    console.log(av);

                    maxPersons = av.max_persons;
                    minPersons = av.min_persons;

                    //edit price per adult and child due to price rules

                        if(typeof(price_rules) != 'undefined'){
                            for(var j in price_rules){
                                var rule = price_rules[j];
                                if(participants >= rule.from_participants && participants <= rule.to_participants){
                                    discount = rule.discount;
                                    price_per_adult*= 1 - rule.discount/100;
                                    price_per_child*= 1 - rule.discount/100;
                                }
                            }
                        }


                        //end price rules

                    adults_price = price_per_adult * adults;
                    children_price = price_per_child * children;
                    total += adults_price + children_price + free_transfer_in_math;
                    var price_per_adult_to_show = price_per_adult+(adultsTransferWeight/adults);
                    var price_per_child_to_show = price_per_child+(childrenTransferWeight/children);
                    if(isNaN(price_per_child_to_show)){
                        price_per_child_to_show = 0.00;
                    }

                    var addOnTotal = 0;
                    var addons = [];

                    $('.checkbox-addon').each(function(){
                        if($(this).is(':checked')){

                            //console.log('price per adults '+parseFloat($(this).data('price_per_adult'))+' price per child '+parseFloat($(this).data('price_per_child')));
                            var addonAdults = parseFloat($(this).data('price_per_adult'))*adults;
                            var addonChildren = parseFloat($(this).data('price_per_child'))*children;
                            addOnTotal+= addonAdults + addonChildren;
                            addons.push($(this).data('id'));
                            var selectorAdults = '#tot_add_on_'+$(this).data('id')+'_adults';
                            var selectorChildren = '#tot_add_on_'+$(this).data('id')+'_children';

                            $(selectorAdults).html(currencyFormatter.format(addonAdults));
                            $(selectorChildren).html(currencyFormatter.format(addonChildren));

                        }else{

                            var selectorAdults = '#tot_add_on_'+$(this).data('id')+'_adults';
                            var selectorChildren = '#tot_add_on_'+$(this).data('id')+'_children';
                            $(selectorAdults).html('€0,00');
                            $(selectorChildren).html('€0,00');

                        }
                    })

                    $('#input_addons').val(addons.join(','));

                    //console.log(addOnTotal);

                    total+=addOnTotal;

                    $('#price_per_adult_show').html('('+currencyFormatter.format(price_per_adult_to_show)+' per adult)');
                    $('#price_per_child_show').html('('+currencyFormatter.format(price_per_child_to_show)+' per child)');
                    $('.grand-total').html(currencyFormatter.format(total));
                    $('#adults_price_summary').html(currencyFormatter.format(adults_price+adultsTransferWeight)+' ('+currencyFormatter.format(price_per_adult_to_show)+' x '+adults+' adults)');
                    $('#children_price_summary').html(currencyFormatter.format(children_price+childrenTransferWeight)+' ('+currencyFormatter.format(price_per_child_to_show)+' x '+children+' children)');
                }
            }
            $('#input_adults').val(adults);
            $('#input_children').val(children);
            $('#input_date').val(moment(selectedDate).format('L'));
            $('#input_time').val(moment(selectedTime,'HH:mm:ss').format('HH:mm'));
            console.log({
                adults:adults,
                children:children,
                adults_price:adults_price,
                children_price:children_price,
                transferred_soil:transferred_soil,
                free_transfer_in_math:free_transfer_in_math,
                total:total,
                adultsTransferWeight:adultsTransferWeight,
                childrenTransferWeight:childrenTransferWeight,
                price_per_adult_to_show:price_per_adult_to_show,
                price_per_child_to_show:price_per_child_to_show,
                discount:discount
            });
        }

        function updateAvailableTimes(){

            console.log('Begin update available times');
            console.log('selected date '+selectedDate);
            availableTimes = [];
            $('#available_times').html('');
            $('td.day.today').removeClass('today');//shitty method

            for(var i in availabilities){
                var av = availabilities[i];
                if(moment(av.day).format('L') == moment(selectedDate).format('L')){
                availableTimes.push(av.time);
                }
            }
            for(var i in availableTimes){
                $('#available_times').append('<option value="'+availableTimes[i]+'">'+availableTimes[i].substr(0,5)+'</option>')
            }
            calculateTotals();
            console.log(availableTimes);

        }

        $(function(){
        selectedDate = enabledDates[0]
        availableTimes = [];
        $('#calendar_first').datetimepicker({
            inline: true,
            sideBySide: true,
            format: 'L',
            enabledDates:enabledDates,
            defaultDate:enabledDates[0],
        });
        $('#calendar_first').on("change.datetimepicker",function(e){
            selectedDate = e.date;

            updateAvailableTimes();

        })

        $('td.day.today').removeClass('today');//shitty method
            updateAvailableTimes();
        })

        $('.btn-number').click(function(e){

            e.preventDefault();


            children = parseInt($('#children_num').val());
            adults = parseInt($('#adults_num').val());

            var totParticipants = children + adults;

            fieldName = $(this).attr('data-field');
            type      = $(this).attr('data-type');
            var input = $("input[name='"+fieldName+"']");
            var currentVal = parseInt(input.val());
            if (!isNaN(currentVal)) {

                if(type == 'minus') {

                    if((totParticipants - 1) >= minPersons){
                        if(currentVal > input.attr('min')) {
                            input.val(currentVal - 1).change();
                        }
                        if(parseInt(input.val()) == input.attr('min')) {
                            $(this).attr('disabled', true);
                        }
                    }

                }

                if(type == 'plus') {

                    if((totParticipants + 1) <= maxPersons){

                        if(currentVal < input.attr('max')) {
                            input.val(currentVal + 1).change();
                        }
                        if(parseInt(input.val()) == input.attr('max')) {
                            $(this).attr('disabled', true);
                        }

                    }
                }

            } else {
                input.val(0);
            }

            calculateTotals();

        });

        $('.input-number').focusin(function(){
            $(this).data('oldValue', $(this).val());
        });

        $('.input-number').change(function() {

            minValue =  parseInt($(this).attr('min'));
            maxValue =  parseInt($(this).attr('max'));
            valueCurrent = parseInt($(this).val());


            name = $(this).attr('name');
            if(valueCurrent >= minValue) {
                $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
            } else {
                alert('Sorry, the minimum value was reached');
                $(this).val($(this).data('oldValue'));
            }
            if(valueCurrent <= maxValue) {
                $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
            } else {
                alert('Sorry, the maximum value was reached');
                $(this).val($(this).data('oldValue'));
            }


        });
        $(".input-number").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
        // ********qunantity minus plus close*********** //
        </script>




    <script>
        $(function(){
            $('.slick-carousel').slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                autoplay: true,
                infinite: true,
                centerMode: true,
                variableWidth: true,
                dots: true,
                autoplaySpeed: 2000,
                responsive: [
                    {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                    },
                    {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                    },
                    {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            });
        })
    </script>


@endpush
