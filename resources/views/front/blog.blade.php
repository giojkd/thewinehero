@extends('front.main')

@section('content')
    <div class="blog">


        <h1 class="page-title">@lang('all.Read our blog')</h1>
        <h2 class="text-center">{{ Setting::get('blog_subtitle_'.App::getLocale()) }}</h2>
        @hss('10')

        {{-- <div class="grid-filter-top-sticky text-center my-4 py-2">  --}}
        <div class="text-center my-4 py-2">

            @foreach ($articleCategories as $articleCategory)
                <a class="btn btn-sm btn-warning btn-round text-white mb-2"
                    href="{{ $articleCategory->makeUrl() }}">{{ $articleCategory->name }}</a>
            @endforeach

        </div>
        @hss('40')



        <div class="container-fluid">
            @foreach ($blog->chunk(4) as $chunks)
                <div class="row">
                    @foreach ($chunks as $article)
                        <div class="col-md-3">
                            @include('front.components.article-result')
                        </div>
                    @endforeach
                </div>
            @endforeach
        </div>

    </div>

@endsection

@push('scripts')

@endpush
