@extends('front.main')


@section('content')
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <div class="mt-4">
                        <h1 class="text-warning">@lang('all.Thank you!')</h1>
                        <p>
                            @lang('all.thank you for your subscription to this deal!')
                            <br>
                            <br>
                            <a href="{{$return_url}}" class="btn btn-warning text-white">@lang('all.go back to the deal')</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
@endsection
