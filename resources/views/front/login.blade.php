@extends('front.main')

@section('content')
  <div class="user-area vh-100">
    <div class="user-box d-flex justify-content-center">
      <div class="d-flex align-self-center">
        <div class="user-box-inner">
          <ul class="nav nav-tabs"  id="myTab" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" href="{{Route('showLoginForm')}}">@lang('all.I already have an account')</a>
            </li>
            <li class="nav-item">
              <a class="nav-link"  href="{{Route('showRegisterForm')}}">@lang("all.It's my first access")</a>
            </li>
          </ul>
          <div class="tab-content mt-4">
            <div class="tab-pane active" id="already-have-an-account" role="tabpanel" aria-labelledby="already-have-an-account">
              <form class="" action="{{Route('login')}}" method="post">
                @csrf
                <div class="form-group">
                  <label for="">Email</label>
                  <input type="email" class="form-control" id="" placeholder="" name="email" value="{{old('email')}}">
                </div>
                <div class="form-group">
                  <label for="">Password</label>
                  <input type="password" class="form-control" id="" placeholder="" name="password">
                </div>
                @if ($errors->any())
                  <div class="alert alert-danger">
                    <ul class="list-unstyled mb-0">
                      @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                  </div>
                @endif
                <button type="submit" class="btn btn-lg btn-warning btn-block" name="button">@lang('all.Enter')</button>
                <div class="text-center">
                  <small><a href="{{Route('password.request')}}" class="text-muted">@lang('all.Did you forget your password?')</a></small>
                </div>
              </form>
              <p class="text-center text-muted my-4">
                <i>@lang('all.or')</i>
              </p>
              <a href="{{Route('socialLogin',['social' => 'facebook'])}}" class="btn btn-outline-primary btn-block btn-lg">@lang('all.Login with Facebook')</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
