@extends('front.main')

@section('content')
<div class="blog">


  <h1 class="page-title">{{ $meta['h1'] }}</h1>

  @foreach($blog as $month => $articles)
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-3">
        <span class="sticky-index sticky-top">
          {{$month}}
        </span>
      </div>
      <div class="col-md-9">
        @foreach($articles->chunk(4) as $chunks)
        <div class="row">
          @foreach($chunks as $article)
          <div class="col-md-3">
            @include('front.components.article-result')
          </div>
          @endforeach
        </div>
        @endforeach
      </div>
    </div>
  </div>
  @endforeach
</div>

@endsection

@push('scripts')

@endpush
