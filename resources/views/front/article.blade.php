@extends('front.main')

@section('content')

  <div class="container article" style="margin-top: 25px">
    <div class="row">
      <div class="col-md-8 offset-md-2">

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a class="text-warning" href="{{ url(App::getLocale()) }}">Home</a></li>
                <li class="breadcrumb-item"><a class="text-warning" href="{{ $article->articleCategory->makeUrl() }}">{{ $article->articleCategory->name }}</a></li>
            </ol>
        </nav>
        <h1>{{$article->name}} </h1>
        @if ($article->is_ntts_processed)
            <audio controls>
                <source src="{{ $article->ntts_path }}" type="audio/mpeg">
            </audio>
        @endif
        <p>{{$article->subtitle}} @include('front.components.favorite-toggler',['item' => $article])</p>
        @include('front.components.social-share')
        @hss('20')
        @if (!is_null($article->user))
            <span class="text-warning">
                <b>
                    @if ($article->user->has_profile)
                        @lang('all.by') <a class="text-warning" href="{{ $article->user->makeUrl() }}">{{ $article->user->name }} {{ $article->user->surname }}</a>
                    @else
                        @lang('all.by') <span class="text-warning">{{ $article->user->name }} {{ $article->user->surname }}</span>
                    @endif

                </b>
            </span>
            <br>
        @endif
        <span class="text-muted">
            {{ $article->published_at->formatLocalized('%b %d,  %Y') }}
        </span>

{{--
        <p class="text-muted">
            <small>
                {{ $article->views }} @lang('all.views')
            </small>
        </p>
 --}}

      </div>
      {{-- <div class="col-md-3"></div>  --}}
    </div>
    {{-- <hr>  --}}
    <div class="row mt-4">
      <div class="col-md-8 offset-md-2">
        <img
        class="img-fluid article-cover"
        src="@cim(Route('ir',['size' => 800,'filename' => $article->photos[0]]))"
        alt="@altTagMl($article->photos[0])">
      </div>
    </div>

  </div>
{{--
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 p-0">
         <div class="slick-carousel carousel-stripe">
          @foreach($article->photos as $image)
              <img src="@cim(Route('ir',['size' => 'h240','filename' => $image]))" alt="@altTagMl($image)">
          @endforeach
        </div>
      </div>
    </div>
  </div>
 --}}
<div class="text-center">
 @foreach($article->photos as $index => $image)
        {{-- @if ($index > 0 && $index < 5)
            <img class="mw-100" src="@cim(Route('ir',['size' => 'h240','filename' => $image]))" alt="@altTagMl($image)">
        @endif --}}
    @endforeach
</div>
  <div class="container article" style="margin-top: 20px">
    <div class="row">
      <div class="col-md-8 offset-md-2">

            @if($article->podcasts->count() > 0)
                <div class="mb-3">
                    <h3>Podcast</h3>
                    @foreach ($article->podcasts as $podcast)
                        {!! $podcast->spotify_embeddable_code	 !!}
                    @endforeach
                </div>
            @endif

            @if ($article->is_legacy)
                <div class="best-readable article-content">
                    {!!$article->content!!}
                </div>
            @else

                @foreach (json_decode($article->body,1) as $bodyBlock)
                    <div class="best-readable article-content">
                        {!!$bodyBlock['content']!!}
                    </div>

                    @if (isset($bodyBlock['image_link']) && $bodyBlock['image_link'] != '')
                        <a href="{{ $bodyBlock['image_link'] }}">
                            <img class="w-100" alt="{{ $bodyBlock['image_alt'] }}" src="@cim(Route('ir',['size' => 'h480','filename' => $bodyBlock['image']]))">
                        </a>
                    @else
                        <img class="w-100" alt="{{ $bodyBlock['image_alt'] }}" src="@cim(Route('ir',['size' => 'h480','filename' => $bodyBlock['image']]))">
                    @endif

                    @if (isset($bodyBlock['credits']) && $bodyBlock['credits'] != '')
                        {!! $bodyBlock['credits'] !!}
                    @endif


                    @if ($bodyBlock['cta'] != '')
                        @include('front.includible-articles-ctas.'.$bodyBlock['cta'])
                    @endif
                @endforeach

            @endif

        @foreach($article->photos as $index => $image)
            @if ($index >= 5 && $index < 8)
                <img class="mb-1 mw-100" src="@cim(Route('ir',['size' => 'h240','filename' => $image]))" alt="@altTagMl($image)">
            @endif
        @endforeach
        <hr>
         <div class="row">
                @foreach($article->sameCategoryArticles() as $sca)
                    <div class="col-md-4">
                        @include('front.components.article-result',['article' => $sca])
                    </div>
                @endforeach
            </div>
        <hr>
        @include('front.components.reviews-container',['reviews' => $article->reviews,'id' => $article->id,'reviewable_type' => $article->getModelNameFull()])
        <hr>
        @include('front.components.newsletter-subscription-form')
        <hr>
      </div>
      {{--
      <div class="col-md-3">
        @foreach($article->photos as $index => $image)
            @if ($index < 5)
                <img class="mb-2 mw-100" src="@cim(Route('ir',['size' => 'h240','filename' => $image]))" alt="@altTagMl($image)">
            @endif
        @endforeach
        @foreach($article->sameCategoryArticles() as $sca)
          @include('front.components.article-result',['article' => $sca])
        @endforeach
      </div>
       --}}
    </div>
  </div>

@endsection
{{--
@push('scripts')
   <script>
        $(function(){
            $('.slick-carousel').slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                autoplay: true,
                infinite: true,
                centerMode: true,
                variableWidth: true,
                dots: true,
                autoplaySpeed: 2000,
                responsive: [
                    {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                    },
                    {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                    },
                    {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            });
        })
    </script>
@endpush
 --}}
