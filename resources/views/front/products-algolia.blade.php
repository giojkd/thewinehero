@extends('front.main')
@section('content')
  @hss('30')

{{--
    <section class="page-banner bg_cover" style="">
        <div class="container">
            <div class="page-banner-content text-center">
                <h2 class="title">Shop</h2>
                <ol class="breadcrumb justify-content-center">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                </ol>
            </div>
        </div>
    </section>
     --}}

    <!--====== Shop Page Start ======-->


    <section class="shop-page">
        <div class="container-fluid shop-container">
            <div class="row flex-md-row justify-content-between">


                <div class="col-md-3 @if(!$isMobile) @else p-0 @endif" style="overflow: scroll">


                    <div class="d-block d-md-none">
                        <a style="height: 40px; line-height: 40px" href="javascript:" onclick="$('#filtersBox').toggleClass('d-none')" class="main-btn d-block orange-bg">@lang('all.show filters')</a>
                        @if (!$isMobile)
                            @hss('200')
                        @endif
                    </div>
                    <div class="d-none d-md-block shop-sidebar @if ($isMobile) p-3 @endif" id="filtersBox">
                        <div class="shop-sidebar-search mt-50 d-none">
                            <span class="sidebar-title mb-2 h4">@lang('all.search here')</span>
                            <div id="searchbox"></div>
                            <div id="clear-refinements"></div>
                        </div>


                        <div class="shop-sidebar-color mt-25">
                            <span class="sidebar-title mb-2 h4">@lang('all.product type')</span>
                            <div id="producttype-refinement"></div>
                        </div>


                        <div class="shop-sidebar-color mt-25">
                            <span class="sidebar-title mb-2 h4">@lang('all.wine color')</span>
                            <div id="wine-color-refinement"></div>
                        </div>

                         <div class="shop-sidebar-color mt-25">
                            <span class="sidebar-title mb-2 h4">@lang('all.wine type')</span>
                            <div id="wine-type-refinement"></div>
                        </div>

                         <div class="shop-sidebar-color mt-25">
                            <span class="sidebar-title mb-2 h4">@lang('all.region')</span>
                            <div id="region-refinement"></div>
                        </div>


                        <div class="shop-sidebar-color mt-25">
                            <span class="sidebar-title mb-2 h4">@lang('all.year')</span>
                            <div id="year-refinement"></div>
                        </div>

                         <div class="shop-sidebar-color mt-40">
                            <span class="sidebar-title mb-2 h4">@lang('all.denomination')</span>
                            <div id="denomination-refinement"></div>
                        </div>


                         <div class="shop-sidebar-color mt-40">
                            <span class="sidebar-title mb-2 h4">@lang('all.grape variety')</span>
                            <div id="grape-variety-refinement"></div>
                        </div>

                        <div class="shop-sidebar-color mt-40">
                            <span class="sidebar-title mb-2 h4">@lang('all.alcohol')</span>
                            <div id="alcohol-refinement"></div>
                        </div>

                        <div class="shop-sidebar-color mt-40">
                            <span class="sidebar-title mb-2 h4">@lang('all.aromas')</span>
                            <div id="aromas-refinement"></div>
                        </div>

                         <div class="shop-sidebar-color mt-40">
                            <span class="sidebar-title mb-2 h4">@lang('all.ageing potential')</span>
                            <div id="ageing-potential-refinement"></div>
                        </div>
                        {{--
                        <div class="shop-sidebar-color mt-40">
                            <h4 class="sidebar-title mb-2">@lang('all.serving temperature')</h4>
                            <div id="serving-temperature-refinement"></div>
                        </div>
                         --}}




                        <!--
                        <div class="shop-sidebar-color mt-25">
                            <h4 class="sidebar-title mb-2">@lang('all.price')</h4>
                            <div id="numeric-menu"></div>
                        </div>
                    -->
                    <!--
                        <div class="shop-sidebar-color mt-50">
                            <h4 class="sidebar-title mb-2">@lang('all.price slider')</h4>
                            <div id="range-slider"></div>
                        </div>
                    -->


<!--
                         <div class="shop-sidebar-color mt-50">
                            <h4 class="sidebar-title mb-2">Durata</h4>
                            <div id="refinement-list-durata"></div>
                        </div>
                    -->
<!--
                        <div class="shop-sidebar-color mt-50">
                            <h4 class="sidebar-title">Color</h4>

                            <div class="sidebar-color">
                                <ul class="color-list">
                                    <li data-tooltip="tooltip" data-placement="top" title="Black"  data-color="#171d3d" class="active"></li>
                                    <li data-tooltip="tooltip" data-placement="top" title="Blue"  data-color="#4b59a3"></li>
                                    <li data-tooltip="tooltip" data-placement="top" title="Bronze"  data-color="#b9afa1"></li>
                                    <li data-tooltip="tooltip" data-placement="top" title="Green"  data-color="#61c58d"></li>
                                    <li data-tooltip="tooltip" data-placement="top" title="Pink"  data-color="#f6b7cf"></li>
                                    <li data-tooltip="tooltip" data-placement="top" title="Red"  data-color="#ef5619"></li>
                                </ul>
                            </div>
                        </div>
                    -->





                    </div>
                </div>
                <div class="col-md-9">

                    <div class="row mb-3">
                        <div class="col-md-7">
                            <div id="current-refinements"></div>
                        </div>
                        <div class="col-md-5 ">
                            <div id="sort-by" class="text-right"></div>
                        </div>
                    </div>
                    <div class="" style="overflow: scroll" id="productsWrapper">
                        <div class="shop-product" id="hits">

                        </div>
                        @hss('70')
                        <div id="paginator"></div>
                        @hss('70')
                    </div>
<!--
                    <div class="pagination-items mt-80">
                        <ul class="pagination justify-content-center">
                            <li><a class="prev" href="#">Previous</a></li>
                            <li><a href="#">1</a></li>
                            <li><a class="active" href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a class="next" href="#">Next</a></li>
                        </ul>
                    </div>
                -->
                </div>
            </div>
        </div>
    </section>

    <!--====== Shop Page Ends ======-->
@endsection

@push('scripts')

    <script>
    var ALGOLIA_INSIGHTS_SRC = "https://cdn.jsdelivr.net/npm/search-insights@1.8.0";

    !function(e,a,t,n,s,i,c){e.AlgoliaAnalyticsObject=s,e[s]=e[s]||function(){
    (e[s].queue=e[s].queue||[]).push(arguments)},i=a.createElement(t),c=a.getElementsByTagName(t)[0],
    i.async=1,i.src=n,c.parentNode.insertBefore(i,c)
    }(window,document,"script",ALGOLIA_INSIGHTS_SRC,"aa");


          aa('init', {
                appId: '{{ env('ALGOLIA_APP_ID')  }}',
                apiKey: '{{ env('ALGOLIA_SECRET') }}',
            });
    </script>






    <script src="https://cdn.jsdelivr.net/npm/algoliasearch@4.5.1/dist/algoliasearch-lite.umd.js" integrity="sha256-EXPXz4W6pQgfYY3yTpnDa3OH8/EPn16ciVsPQ/ypsjk=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/instantsearch.js@4.8.3/dist/instantsearch.production.min.js" integrity="sha256-LAGhRRdtVoD6RLo2qDQsU2mp+XVSciKRC8XPOBWmofM=" crossorigin="anonymous"></script>


    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/instantsearch.css@7.3.1/themes/algolia-min.css" integrity="sha256-HB49n/BZjuqiCtQQf49OdZn63XuKFaxcIHWf0HNKte8=" crossorigin="anonymous">

    <script>

        var indexName = 'products{{ (env('APP_ENV') == 'local') ?  '_local'  : '' }}';

        var country_id = {{ $guest->country_id }};

        var labels = {
            clean_refinements : '@lang('all.clean refinements')',
            on_deal : '@lang('all.on deal')',

        }
        var guest_id = {{ $guest->id }};

        var cloudimage = 'https://www.mamablip.com';

        $.fn.isInViewport = function() {
            if($(this).length > 0){
                var elementTop = $(this).offset().top - 200;
                var elementBottom = elementTop + $(this).outerHeight();
                var viewportTop = $(window).scrollTop();
                var viewportBottom = viewportTop + $(window).height();
                return elementBottom > viewportTop && elementTop < viewportBottom;
            }
        };

        $(function(){
            $('footer,.newsletter-area-2').hide();
        })

        $('#productsWrapper').on('resize scroll', function() {

            /*
            if($(window).scrollTop() > 500){
                $('.scrollTopBtn').fadeIn()
            }else{
                $('.scrollTopBtn').fadeOut()
            }
            */

            if($('.ais-InfiniteHits-loadMore').isInViewport()){
                $('.ais-InfiniteHits-loadMore').trigger('click');
            }

        });

        var presetFacets = @json($facets);
        var onlyOnSale = {{ $onlyOnSale }};
        const searchClient = algoliasearch('{{ env('ALGOLIA_APP_ID') }}', '{{ env('ALGOLIA_SECRET') }}');
        var locale = '{{ $locale }}';
        var fzProducts = [];
        var price_index = 'price_'+country_id;
        var price_compare_index = 'price_compare_'+country_id;


        var keyword = '{{ $keyword }}';

        @verbatim

         var showMoreText = `
                        {{#isShowingMore}}
                            Show less
                        {{/isShowingMore}}
                        {{^isShowingMore}}
                            Show more
                        {{/isShowingMore}}
                    `;

        $('.header-search').removeClass('d-flex').hide();

        const insightsMiddleware = instantsearch.middlewares.createInsightsMiddleware({
            insightsClient: window.aa,
        });

        const search = instantsearch({

            indexName: indexName,
            searchClient,
            routing: true,
            initialUiState: {
                products: {
                    query: keyword,
                    refinementList:presetFacets,
                    toggle:{
                        is_on_sale: onlyOnSale
                    }
                },
            },
        })

        search.use(
            instantsearch.middlewares.createInsightsMiddleware({
                insightsClient: aa
            })
        );

        aa('setUserToken',guest_id);





        search.addWidgets([
            instantsearch.widgets.configure({
                //hitsPerPage: 8,
                //enablePersonalization: true,
                filters: "type:simple"
            }),

            instantsearch.widgets.searchBox({
                container: '#searchbox',
            }),

            instantsearch.widgets.hits({
                container: '#hits',
                templates:{
                    empty: 'No results for <q>{{ query }}</q>',
                    item: (hit, bindEvent) => { return `<div class="product-item">
                                <div class="row">
                                    <div class="col-md-4 text-center">
                                        <div class="product-img-wrapper">
                                            <a ${bindEvent('click',hit,'ProductClicked')} class="link" href="${ hit.url }">
                                                <img class="product-img" src="${ hit.cover }" alt="product">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-md-12">

                                                <!-- <span class="pg-on-sale-badge-${ hit.is_on_sale } badge badge-warning orange-bg text-white">${ hit.on_deal_label }</span> -->
                                                <h4 class="title">
                                                    <a ${bindEvent('click',hit,'ProductClicked')} href="${ hit.url }">${ hit.name } </a>
                                                </h4>
                                                <div class="features">${ hit.features }</div>
                                                <p class="description mb-3">${ hit.description }</p>
                                                <div class="${ hit.show_prices_class }">
                                                    <small>${ hit.price_formatted }</small>
                                                    <small><span class="pg-compare-price-${ hit.has_discount }"><del>${ hit.price_compare_formatted }</del></span></small>
                                                    <small><span class="pg-discount-percentage-${ hit.has_discount }">(${ hit.discount_percentage })</span></small>

                                                    <div class="pg-on-deal-${ hit.isOnDeal }">
                                                        <a href="${ hit.currentDealUrl }">${ hit.on_deal_label }</a>
                                                    </div>

                                                </div>
                                                <a href="${ hit.url }" target="_blank" class="btn btn-outline-warning btn-block">View product</a>
                                                <div class="text-center">
                                                    <a href="${ hit.brand_link }" class="manufacturer-link">${ hit.brand }</a>
                                                </div>
                                            </div>
                                            <!--
                                            <div class="col-md-6">

                                            </div>
                                            -->
                                        </div>


                                    </div>
                                </div>
                            </div>` },

                },
                 transformItems(items) {

                    fzProducts = [];
                    var productIndex = 0 ;

                    var itemsReturn = items.map(function(item){

                        productIndex++;

                        var price = item[price_index];

                        var price_compare = item[price_compare_index];



                        var discountAmount = price_compare-price;
                        var discountPercentage = discountAmount*100/price_compare;
                        var features = [];
                        if(item['wine_type_'+locale] != 'undefined'){
                            features.push(item['wine_type_'+locale]);
                            features.push('·');
                        }
                        if(item['region_'+locale] != 'undefined'){
                            features.push(item['region_'+locale]);
                        }
                        var returnItem = {
                            name: item['name_'+locale],
                            url: item['url_'+locale],
                            price_formatted: '€ '+parseFloat(price).toFixed(2),
                            price_compare_formatted: '€ '+parseFloat(price_compare).toFixed(2),
                            price: price,
                            cover: cloudimage+item.cover,
                            second_cover: cloudimage+item.second_cover,
                            is_on_sale: (item.is_on_sale == 1) ? 1 : 0,
                            has_discount: (price != price_compare && price_compare > 0) ? 1 : 0,
                            discount_percentage: '-'+Math.ceil(discountPercentage)+'%',
                            on_deal_label: labels.on_deal,
                            show_prices_class: (price > 0) ? 'd-block' : 'd-none',
                            features: features.join(' '),
                            description: item['short_description_'+locale],
                            brand: item['brand_'+locale],
                            brand_link: item['brand_link_'+locale],
                            isOnDeal: (item['isOnDeal'] == 1) ? 1 : 0,
                            currentDealUrl: item['currentDealUrl']
                        };



                        return returnItem;

                    })




                    return itemsReturn;
                },
            }),

            instantsearch.widgets.sortBy({
                container: '#sort-by',
                items: [
                    { label: 'Più rilevanti', value: 'products' },
                    /*{ label: 'Prezzo (prima meno costosi)', value: 'harrisproductgroups_price_asc' },
                    { label: 'Prezzo (prima più costosi)', value: 'harrisproductgroups_price_desc' },*/
                ],
            }),

            instantsearch.widgets.clearRefinements({
                container: '#clear-refinements',
                cssClasses:{
                    button: ['orange-btn']
                },
                templates:{
                    resetLabel() {
                        return labels.clean_refinements;
                    },
                }
            }),

            instantsearch.widgets.refinementList({
                container: '#ageing-potential-refinement',
                attribute: 'ageing_potential_'+locale,
                limit: 50,
                showMore: false,
                templates:{
                    showMoreText: showMoreText
                },
                //searchable: true
            }),
            instantsearch.widgets.refinementList({
                container: '#producttype-refinement',
                attribute: 'producttype_'+locale,
                limit: 50,
                showMore: false,
                templates:{
                    showMoreText: showMoreText
                },
                //searchable: true
            }),

            instantsearch.widgets.refinementList({
                container: '#alcohol-refinement',
                attribute: 'alcohol_'+locale,
                 limit: 50,
                showMore: false,
                templates:{
                    showMoreText: showMoreText
                },
                //searchable: true
            }),
             instantsearch.widgets.refinementList({
                container: '#aromas-refinement',
                attribute: 'aromas_'+locale,
                limit: 5,
                showMore: true,
                templates:{
                    showMoreText: showMoreText
                },
                searchable: true
            }),
                instantsearch.widgets.refinementList({
                container: '#denomination-refinement',
                attribute: 'denomination_'+locale,
                limit: 10,
                showMore: true,
                templates:{
                    showMoreText: showMoreText
                },
                searchable: true
            }),
            instantsearch.widgets.refinementList({
                container: '#grape-variety-refinement',
                attribute: 'grape_variety_'+locale,
                limit: 10,
                showMore: true,
                templates:{
                    showMoreText: showMoreText
                },
                searchable: true
            }),
              instantsearch.widgets.refinementList({
                container: '#wine-color-refinement',
                attribute: 'wine_color_'+locale,
                limit: 10,
                showMore: false,
                templates:{
                    showMoreText: showMoreText
                },

                //searchable: true
            }),
               instantsearch.widgets.refinementList({
                container: '#wine-type-refinement',
                attribute: 'wine_type_'+locale,
                limit: 50,
                showMore: false,
                templates:{
                    showMoreText: showMoreText
                },
                //searchable: true
            }),
              instantsearch.widgets.refinementList({
                container: '#year-refinement',
                attribute: 'year_'+locale,
                limit: 50,
                showMore: false,
                templates:{
                    showMoreText: showMoreText
                },
                //searchable: true
            }),
              instantsearch.widgets.refinementList({
                container: '#region-refinement',
                attribute: 'region_'+locale,
                limit: 10,
                showMore: true,
                templates:{
                    showMoreText: showMoreText
                },
                searchable: true
            }),

            instantsearch.widgets.pagination({
                container: '#paginator',
                // Optional parameters
                /*showFirst: boolean,
                showPrevious: boolean,
                showNext: boolean,
                showLast: boolean,
                padding: number,
                totalPages: number,
                scrollTo: string|HTMLElement|boolean,
                templates: object,
                cssClasses: object,*/
            })


        ]);




        search.start();








        $(function(){
            setInterval(function(){
                $('.shop-sidebar-color').each(function(){
                    var refinements = $(this).find('.ais-RefinementList');
                    var title = $(this).find('h4');
                    var search = $(this).find('.ais-SearchBox-input');
                    if(search.length > 0 && search.is(":focus")){
                        return;
                    }
                    if(refinements.hasClass('ais-RefinementList--noRefinement')){
                        title.hide();
                        refinements.hide();
                    }else{
                        title.show();
                        refinements.show();
                    }
                })
            },200)
        })


        @endverbatim
    </script>

@endpush
