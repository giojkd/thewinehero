<div class="text-left">
    <p class="deal-short-description" style="background-color: #e6a005">
        {{ $product->name }}
    </p>
    <div class="best-readable">
        {!! $product->description !!}
    </div>
    @hss('15')
    <div class="row justify-content-center ">
        @foreach ($product->prices as $index => $price)
            <div class="col-md-4">
                <div class="product-bundle-price-option mb-2" @if ($index == 1) style="box-shadow: 0px 0px 14px rgba(0,0,0,0.25); border-radius: 6px" @endif>
                    <form action="{{ Route('addToCart') }}" method="POST">
                        @csrf
                        <input type="hidden" name="quantity" value="{{ $price->quantity  }}">
                        <input type="hidden" name="product_id" value="{{ $product->id }}">

                        @php
                        #dd($product->nphotos);
                        try{
                             if(!is_null($product->nphotos) && count($product->nphotos) > 0){
                                $photosByQuantity = collect($product->nphotos)->pluck('image','quantity');
                                if(count($photosByQuantity) > 0){
                                    $img = (isset($photosByQuantity[$price->quantity])) ? $photosByQuantity[$price->quantity] : collect($product->photos)->first();
                                }else{
                                    $img = collect($product->photos)->first();
                                }

                            }else{
                                $img = collect($product->photos)->first();
                            }
                        }catch(Exception $e){
                            $img = collect($product->photos)->first();
                        }


                        @endphp



                        <img class="img-fluid mb-2" src="@cim(Route('ir',['size' => 'h800','filename' => $img]))" alt="@altTagMl($img)">



                        @foreach ($product->products as $item)
                            <div class=" mb-1">

                                <div class="row">
                                    {{--
                                    <div class="col-12 text-center">
                                        <span class="contained-quantity"> {{ $item->pivot->quantity * $price->quantity }} </span>
                                    </div> --}}
                                    <div class="col-12">
                                        @php
                                            $aux = json_decode($item->pivot->description,1);
                                            $locale = App::getLocale();
                                            $bundleDescription = (isset($aux[$locale])) ? $aux[$locale] : '' ;
                                        @endphp
                                        <div>
                                           <a style="color: #595959" href="javascript:" data-toggle="modal" data-target="#modalProduct_{{ $item->id }}">{{ $item->name }}</a>
                                           <p class="text-warning"><small>{{ $item->pivot->quantity * $price->quantity }} {{ $bundleDescription }}</small> </p>
                                        </div>
                                    </div>
                                </div>




                                @include('front.components.product-modal')
                            </div>
                        @endforeach
                        <div class="text-center">
                            <div class="row">
                                <div class="col-md-5 text-left">
                                    <small style="white-space: nowrap">@lang('all.total cost')</small>
                                    <span class="text-left product-bundle-price">
                                        <b>@fp($price->price)</b>
                                        @if (!is_null($price->price_compare) && $price->price_compare > 0)
                                            <span class="price-cut">@fp($price->price_compare)</span>
                                        @endif
                                    </span>
                                    <div class="btn-group dropup">
                                        <button class="btn btn-sm dropdown-toggle p-0" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            @lang('all.price for') {{$availableCountries->keyBy('id')[$guest->country_id]->name}}
                                        </button>
                                        <div class="dropdown-menu">
                                            @foreach ($availableCountries as $country)
                                                <a class="dropdown-item @if ($country->id == $guest->country_id) active @endif" href="{{$deal->makeUrl()}}?country_id={{ $country->id }}">{{ $country->name }}</a>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <small class="text-white">.</small>
                                    @if (!$deal->hasStarted())
                                        <a href="javascript" onclick="openNotStartedYetDealModal()" style="margin-top: 5px;" class="btn btn-secondary btn-block ">@lang('subscribed to this deal')</a>
                                    @endif
                                    @if ($deal->hasStarted() && !$deal->hasExpired())
                                        <button style="margin-top: 5px;" type="submit" class="btn btn-warning text-white btn-block ">@lang('add to cart')</button>
                                    @endif
                                    @if ($deal->hasExpired())
                                        <a href="javascript" onclick="openExpiredDealModal()" style="margin-top: 5px;" class="btn btn-secondary btn-block ">@lang('subscribed to similar')</a>
                                    @endif
                                </div>
                            </div>


                        </div>
                    </form>
                </div>
            </div>
        @endforeach
    </div>
    @hss('15')
    <div class="row mb-3">
        <div class="col text-center">
            <a href="javascript:" onclick="tidioChatApi.messageFromVisitor('@lang('all.buy other quantity message')')" class="text-warning">@lang('all.buy other quantity')</a>
        </div>
    </div>
</div>

