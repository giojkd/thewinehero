<div class="course-card">
  <div class="row m-0 p-0">
    <div class="col-md-2 m-0 p-0 text-right">
      <h2>Level {{$indexLevel+1}}</h2>
      <h3>{{$level->modules->count()}} modules</h3>
    </div>
    <div class="col-md-1">
      <div class="text-center" style="height:30px; ">
        <div class="bg-warning d-inline-block" style="height: 30px; width: 30px; border-radius: 50%"></div>
      </div>
      @if($indexLevel < $training->levels->count()-1)
        <div class="text-center h-100">
          <div class="d-inline-block bg-warning" style="width: 5px; height: 100%"></div>
        </div>
      @endif
    </div>
    <div class="col-md-9">
      <div class="card shadow">
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
              <h5 class="card-title">{{$level->name}}</h5>
              <p class="card-text">{{$level->subtitle}}</p>
              <div class="best-readable">
                {!!$level->description!!}
              </div>
              <div class="course-card-level-icons">
                <div class="d-inline-block">
                  <i class="fas fa-video text-warning"></i> {{$level->videos->count()}} @lang('all.videos')
                </div>
                <div class="d-inline-block ml-2">
                    <i class="fas fa-align-justify text-warning"></i> {{$level->files->count()}} @lang('all.downloadable papers')
                </div>
                <div class="d-inline-block ml-2">
                    <i class="fas fa-align-justify text-warning"></i> {{$level->quizzs->count()}} @lang('all.quizzs')
                </div>
                <div class="d-inline-block">
                  in {{$level->modules->count()}} @lang('all.modules')
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      @if($indexLevel > 0)
        <div>
          <div class="row m-0 p-0">
            <div class="col-md-2 m-0 p-0">

            </div>
            <div class="col-md-1">
              <div class="text-center h-100">
                <div class="d-inline-block bg-warning" style="width: 5px; height: 100%"></div>
              </div>
            </div>

          </div>
        </div>
      @else
        @hss('30')
      @endif
      <div class="level-modules">
        @foreach($level->modules as $indexModule => $module)
          @include('front.components.course-card-inner')
        @endforeach
      </div>
    </div>
  </div>
</div>
