<div class="team-member">
  <div class="team-member-avatar">
    <img src="{{Route('ir',['size' => 'h180','filename' => $user->avatar])}}" alt="">
  </div>

  <h1>{{$user->name}}  {{$user->surname}}</h1>

  <div class="team-member-bio">
    {{$user->biography}}
  </div>

</div>
@push('scripts')
  <script type="text/javascript">
    $(function(){
      $('.team-member-bio').readmore();
    })
  </script>
@endpush
