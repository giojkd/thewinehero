<a target="_blank" class="video-item" href="{{$video->makeUrl()}}" data-video-id="{{$video->id}}">
  <div class="video-item-header" @if (isset($coverHeight)) style="height: {{ $coverHeight }}px" @endif>
    <div class="video-item-cover-wrapper" @if (isset($coverHeight)) style="height: {{ $coverHeight }}px" @endif>

        @if ($video->cover)
                <img class="w-100" src="@cim(Route('ir',['size' => isset($coverImageHeight) ? $coverImageHeight : '480', 'filename' => $video->cover]))" alt="@altTagMl($video->cover)">
        @endif


        <div class="video-item-cover-wrapper-play align-items-center justify-content-center">
            <div class="d-flex">
                <img src="{{ url('images/play.png') }}" alt="">
            </div>
        </div>
    </div>
    {{--
    <video id="video-{{$video->id}}" style="height: auto; width: 100%" class="twh-video" preload="none" muted poster="{{Route('ir',['size' => 480, 'filename' => $video->cover])}}" >
      <source src="{{url($video->file)}}" type="video/mp4" />
    </video>
     --}}
      </div>
      <div class="video-item-body">
        <div class="row">
            {{--
          <div class="col-md-2">
             @include('front.components.avatar',['user' => $video->user])
          </div>
          --}}
          <div class="col-md-12">
            <h2>{{$video->name}}</h2>
            {{-- <p>{{$video->user->name}}</p>  --}}
            <span>
                @auth
                    @role('Admin')
                        @if ( $video->views > 0 )
                            {{$video->views}} @lang('all.views') |
                        @endif
                    @endrole
                @endauth


                {{Str::lower($video->created_at->format('l jS \\of F Y'))}}
                @if ($video->user)
                    | @lang('all.by') {{ $video->user->name }} {{ $video->user->surname }}
                @endif
            </span>
          </div>
        </div>
      </div>
    </a>
