<div class="enrollment-options" id="enrollment_options">
  <h1>Enrollment options</h1>
  <div class="container">
    <div class="row justify-content-center">
      @foreach($enrollmentoptiongroups as $group)
        <div class="col-md-4 mb-4">
          <div class="enrollment-option bg-white shadow p-4 text-center @if(isset($currentOption)) current-option @endif">
            @foreach($group->enrollmentoptions as $index => $option)
            <div>
                <div class="option-price text-warning ">
                    {{money($option->price,'EUR')->format()}}
                </div>

                <div class="option-price-info">
                    @if($option->is_subscription)
                    @lang('all.Every n days',['n' => $option->frequency])

                    @else
                        @lang('all.Pay once')
                    @endif
                </div>
                @if($group->enrollmentoptions->count() > 1 && $index < ($group->enrollmentoptions->count()-1))
                 <div class="text-center">
                    <small><span class="text-muted">or</span></small>
                </div>
                @endif
            </div>
            @endforeach


            <!--<span class="text-warning">Free until June 30th 2020!</span>-->
            <hr>
            <h3 class="text-warning">@lang('all.What you will get')</h3>
            <div class="option-what-you-get">
              {!!$group->enrollmentoption->what_you_get!!}
              <hr>
            </div>
            <div>
              <h3 class="text-warning">@lang('all.Included in this plan')</h3>
              <ul>
                  <li><b>{{ $group->enrollmentoption->events }} </b> @lang('all.live classes')</li>
                  <li><b>{{ $group->enrollmentoption->videos }} </b> @lang('all.videos')</li>
                  <li><b>{{ $group->enrollmentoption->trainings }} </b> @lang('all.learning paths')</li>
                  <li><b>{{ $group->enrollmentoption->articles }} </b> @lang('all.articles')</li>
                  <li><b>{{ $group->enrollmentoption->recipes }} </b> @lang('all.recipes')</li>
              </ul>
              <h3 class="text-warning">and discounts</h3>
                <ul>
                  <li><b>-{{ $group->enrollmentoption->events_discount }}% </b> @lang('all.live classes')</li>
                  <li><b>-{{ $group->enrollmentoption->videos_discount }}% </b> @lang('all.videos')</li>
                  <li><b>-{{ $group->enrollmentoption->trainings_discount }}% </b> @lang('all.learning paths')</li>
                  <li><b>-{{ $group->enrollmentoption->articles_discount }}% </b> @lang('all.articles')</li>
                  <li><b>-{{ $group->enrollmentoption->recipes_discount }}% </b> @lang('all.recipes')</li>
              </ul>
            </div>
             <form class="" action="{{Route('enrollmentOptions')}}" method="get">
              <input type="hidden" name="enrollmentoptiongroup_id" value="{{$group->id}}">
              <button type="submit" class="btn btn-lg btn-outline-warning btn-block">@lang('all.Enroll now')</button>
            </form>
            {{--
            <form class="" action="{{Route('enrollTraining')}}" method="post">
              @csrf
              <input type="hidden" name="training_id" value="{{$training->id}}">
              <button type="submit" class="btn btn-lg btn-outline-warning btn-block">Enroll now</button>
            </form>
             --}}
          </div>
        </div>
      @endforeach
      @if($content->price > 0)
      <div class="col-md-4">
        @include('front.components.buy-single-content',
            [
              'content' => [
                  'name' => $content->getModelName(),
                  'price_compare' => ($content->applyPlanDiscount($content->getModelName(),$content->price) != $content->price) ? $content->price : 0,
                  'price' => $content->applyPlanDiscount($content->getModelName(),$content->price),
                  'id' => $content->id,
                  'is_premium' => $content->is_premium
                  ]
            ]
        )
      </div>
      @endif
    </div>
  </div>
</div>
