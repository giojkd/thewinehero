 <div class="newsletter-subscription mt-2 mt-md-0">
            {{--
        <iframe width="100%" height="290" src="" id="sendinblueIframe" frameborder="0" scrolling="auto" allowfullscreen style="display: block;margin-left: auto;margin-right: auto;"></iframe>
         --}}

          <form class="ajax-form" action="{{ Route('subscribeNewsletter') }}" method="post">
            @csrf
            @method('post')
            <span style="
            font-weight: normal;
            display: block;
    text-align: center;
    font-size: 25px;
    margin-bottom: 2px;
    padding: 25px 15px 15px;
    margin-top: 0px;

    color: rgb(153, 153, 153);">@lang('all.subscribe our newsletter')</span>
            <span style="display: block; text-align: center; margin-bottom: 15px; color: rgb(153, 153, 153);">@lang('all.stay up to date')</span>

            <div class="form-group">
              <input name="name" type="text" class="form-control" id="" placeholder="@lang('all.name')">
            </div>
             <div class="form-group">
              <input name="surname" type="text" class="form-control" id="" placeholder="@lang('all.surname')">
            </div>
            <div class="form-group">
              <input name="email" required type="text" class="form-control" id="" placeholder="@lang('all.email address')">
            </div>

            {{--
            <div class="form-group">
              <input type="text" class="form-control" id="" placeholder="@lang('all.date of birth')">
            </div>
            <div class="form-group">
              <input type="text" class="form-control" id="" placeholder="@lang('all.nationality')">
            </div>
            <div class="form-group">
              <input type="text" class="form-control" id="" placeholder="@lang('all.gender')">
            </div>
             --}}
            <button type="submit" name="button" class="btn btn-warning btn-lg btn-block p-4">@lang('all.subscribe newsletter now')</button>
            <div class="alert alert-success d-none mt-2">@lang('all.check your email to confirm the subscription')</div>
            <div class="alert alert-danger d-none mt-2"></div>
          </form>

        </div>
