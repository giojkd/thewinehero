<div class="twh-event">
    <div class="row">
        <div class="col-md-4 col-4">
            <a href="{{ $event->makeUrl() }}">
                <img class="w-100" src="{{ Route('ir', ['size' => 320, 'filename' => $event->images[0]]) }}" alt="@altTagMl($event->images[0])">
            </a>
        </div>
        <div class="col-md-4 col-8">
            <div class="row">

            </div>
            <div class="row">
                <div class="col">
                    @if ($event->is_class)
                        <span class="badge badge-pill badge-warning text-white">@lang('all.Live Class')</span>
                    @else
                        <span class="badge badge-pill badge-secondary">@lang('all.Live Event')</span>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-md-10">
                    <span class="event-title">{{ $event->name }}</span>
                </div>
                {{--
                <div class="col-md-2">

                </div>
                --}}
            </div>

            <span class="event-description">{{ $event->description_short }}</span>


        </div>
        <div class="col-md-4">

            <a href="{{ $event->makeUrl() }}" class="btn btn-sm btn-outline-warning mb-1 d-block">
                {{ \Carbon\Carbon::createFromFormat('Y-m-d', $event->starts_at_day)->format('l jS \\of F Y') }}
            </a>

            <div class="row">
                <div class="text-nowrap col-md-4 col-4 event-spech text-warning event-complexity text-center text-nowrap">
                    <i class="fas fa-chart-bar"></i> {{ $event->complexity->name }}
                </div>
                <div class="text-nowrap col-md-4 col-3 event-spech text-warning event-duration text-center text-nowrap">
                    <i class="fas fa-hourglass-end"></i> {{ $event->duration }}m
                </div>
                <div class="text-nowrap col-md-4 col-5 event-spech text-warning event-starts_at text-center text-nowrap">
                    <i class="far fa-clock"></i> {{ \Str::limit($event->starts_at_time, 5, '') }}
                </div>
            </div>
            @if( $event->custom_icons != '')
                <div class="row mt-2 event-custom-icons">
                    @foreach (json_decode($event->custom_icons,1) as $icon)
                        <div class="col text-center">
                            <img class="mb-1" style="height: 40px" src="{{ url($icon['image']) }}" alt=""><br>
                            <b>{{ $icon['title'] }}</b>
                        </div>
                    @endforeach
                </div>
            @endif
            <a href="{{ $event->makeUrl() }}" class="btn btn-warning text-white btn-block mt-4">@lang('all.Find out more')</a>

            <!--
          <a href="#" class="btn btn-warning text-white"><i class="far fa-calendar-plus"></i> Subscribe event</a>
          <a href="#" class="btn btn-outline-warning text-warning"><i class="far fa-calendar-minus"></i> Event subscribed</a>
          -->
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">

        </div>
    </div>
</div>
