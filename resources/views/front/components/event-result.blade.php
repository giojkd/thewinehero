<a class="twh-event" href="{{$event->makeUrl()}}">
  <div class="row">
    <div class="col-12">

        <img class="img-fluid w-100" src="@cim(Route('ir',['size' => 320,'filename' => $event->images[0]]))" alt="@altTagMl($event->images[0])">

        <div class="row">
            <div class="col-md-12">
                <span class="btn btn-sm btn-outline-warning mb-1">{{\Carbon\Carbon::createFromFormat('Y-m-d', $event->starts_at_day)->format('l jS \\of F Y')}}</span>
            </div>
        </div>
        <div class="row">
            <div class="col">
                @if($event->is_class)
                    <span class="badge badge-pill badge-warning text-white">@lang('all.Live Class')</span>
                @else
                    <span class="badge badge-pill badge-secondary">@lang('all.Live Event')</span>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-10">
                <span class="event-title">{{$event->name}}</span>
            </div>
            {{--
            <div class="col-md-2">

            </div>
             --}}
        </div>

      <span class="event-description">{{$event->description_short}}</span>

      <div class="row">
          {{--
        <div class="text-nowrap col-md-4 col-4 event-spech text-warning event-complexity">
          <i class="fas fa-chart-bar"></i> {{$event->complexity->name}}
        </div>
         --}}

        <div class="text-nowrap col-md-8 col-8 event-spech text-warning event-starts_at">
          <i class="far fa-clock"></i> <span data-original_time="{{$event->starts_at_time}}" class="convertable-time">{{date('g:i a',strtotime($event->starts_at_time))}} (GMT+1)</span>
        </div>
        <div class="text-nowrap col-md-3 col-3 event-spech text-warning event-duration">
          <i class="fas fa-hourglass-end"></i> {{$event->duration}}m
        </div>
      </div>

    </div>
  </div>
</a>
