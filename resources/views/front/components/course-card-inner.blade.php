<div class="course-card-inner pb-4">
  <div class="row m-0 p-0">
    <div class="col-md-2 m-0 p-0">

    </div>
    <div class="col-md-1">
      <div class="text-center" style="height:30px; ">
        <div class="d-inline-block" style="border: 6px solid #fab417 !important; height: 35px; width: 35px; border-radius: 50%"></div>
      </div>
      @if($indexModule < $level->modules->count()-1)
        <div class="text-center h-100">
          <div class="d-inline-block bg-warning" style="width: 5px; height: 100%"></div>
        </div>
      @endif
    </div>
    <div class="col-md-9">
      <div class="card shadow">
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
              <h5 class="card-title">{{$module->name}}</h5>
              <p class="card-text">{{$module->subtitle}}</p>
              <div class="best-readable">
                {!!$module->description!!}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
