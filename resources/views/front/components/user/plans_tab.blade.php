
            <div class="tab-pane" id="plans">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            @if ($userHasActivePlan)
                                <table class="table table-hover table-striped">
                                    <thead>
                                        <tr>
                                            <th>@lang('all.Content')</th>
                                            <th>@lang('all.How many left')</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>@lang('all.Events')</td>
                                            <td>{{ $plan->events }}</td>
                                        </tr>
                                        <tr>
                                            <td>@lang('all.Videos')</td>
                                            <td>{{ $plan->videos }}</td>
                                        </tr>
                                        <tr>
                                            <td>@lang('all.Trainings')</td>
                                            <td>{{ $plan->trainings }}</td>
                                        </tr>
                                        <tr>
                                            <td>@lang('all.Articles')</td>
                                            <td>{{ $plan->articles }}</td>
                                        </tr>
                                        <tr>
                                            <td>@lang('all.Recipes')</td>
                                            <td>{{ $plan->recipes }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                                @lang('all.Next billing:')
                                {{ date('d/m/Y', strtotime($plan->created_at . ' +' . $plan->enrollmentoption->frequency . 'days')) }}
                            @else

                            @endif
                        </div>
                    </div>
                </div>
            </div>
