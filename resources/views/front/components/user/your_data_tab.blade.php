<div class="tab-pane" id="yourdata">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            @if ($payment_methods->count() > 0)
                                <h2>@lang('all.your payment method')</h2>
                                @foreach ($payment_methods as $payment_method)
                                    <div class="card mb-3">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-8">
                                                    @lang('all.Credit card:')
                                                    <b>{{ Str::ucfirst($payment_method->card->brand) }}
                                                        ({{ $payment_method->card->last4 }})</b>
                                                    @lang('all.Exp:')
                                                    <b>{{ $payment_method->card->exp_month }}/{{ $payment_method->card->exp_year }}</b>
                                                </div>
                                                <div class="col-md-4 col-12 text-right">
                                                    <form action="{{ Route('removePaymentMethod') }}" method="POST">
                                                        @csrf
                                                        <button name="id" value="{{ $payment_method->id }}"
                                                            type="submit"
                                                            class="mt-2 mt-md-0 btn btn-danger text-white btn-block">@lang('all.remove
                                                            this card')</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                <hr>
                            @endif

                            <h2>@lang('all.delete your account')</h2>
                            <div class="row">
                                <div class="col-md-4">
                                    <form action="{{ Route('deleteUserData') }}" method="POST">
                                        @csrf
                                        <input type="hidden" name="locale" value="{{ App::getLocale() }}">
                                        <div class="form-group">
                                            <input name="passphrase" type="text" class="form-control form-control-lg" placeholder="@lang('all.write DELETE MY DATA')">
                                        </div>
                                        <button class="btn btn-danger btn-lg btn-block">@lang('all.delete your account for
                                            good')</button>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
