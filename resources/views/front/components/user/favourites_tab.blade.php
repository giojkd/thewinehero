<div class="tab-pane active" id="favourites">

                @if ($countFavorites > 0)
                    <div class="search-results">


                        @foreach ($favoriteableModels as $result)
                            @if (count($result['items']) > 0)
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <span class="sticky-index sticky-top">{{ $result['name'] }}</span>
                                        </div>
                                        <div class="col-md-9">

                                            @foreach ($result['items']->chunk(12 / $result['col_width']) as $items)
                                                <div class="row">
                                                    @foreach ($items as $item)
                                                        <div class="col-md-{{ $result['col_width'] }}">
                                                            @include($result['item_view'],[$result['item_name'] => $item])
                                                        </div>
                                                    @endforeach
                                                </div>
                                            @endforeach

                                        </div>
                                    </div>

                                </div>
                                @hss('50')

                            @endif

                        @endforeach
                    </div>
                @else
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <div class="alert alert-secondary">
                                    @lang("all.You didn't join any learning path yet!")
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

            </div>
