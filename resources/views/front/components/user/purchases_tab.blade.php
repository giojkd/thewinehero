 <div class="tab-pane" id="purchases">
                @if ($countPurchases > 0)
                    <div class="search-results">
                        @foreach ($purchaseableModels as $model => $results)
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <span class="sticky-index sticky-top">{{ $model }}</span>
                                        </div>
                                        <div class="col-md-9">
                                            @foreach ($results->chunk(3) as $items)
                                                <div class="row">
                                                    @foreach ($items as $item)
                                                        <div class="col-md-4">
                                                            @include('front.components.'.$item->purchaseable->getModelName().'-result',[$item->purchaseable->getModelName() => $item->purchaseable])
                                                            @if (!is_null($item->grand_total))
                                                                <div class="text-center">
                                                                    <a href="{{ Route('requestPurchaseInvoice',['id' => $item->id]) }}" class="btn btn-warning text-white btn-sm">@lang('all.request invoice for this purchase')</a>
                                                                </div>
                                                            @endif
                                                        </div>
                                                    @endforeach
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                @hss('50')
                        @endforeach
                    </div>
                @else
                    <div class="row">
                        <div class="col">
                            <div class="alert alert-secondary">
                                @lang("You didn't buy any content yet!")
                            </div>
                        </div>
                    </div>
                @endif

            </div>
