@if ($reviews)
    <div class="reviews-container">
    @foreach($reviews as $review)
        <div class="review">
            <div class="row">
                <div class="col-md-1 text-center px-0">
                @include('front.components.avatar',['user' => $review->user])
                </div>
                <div class="col-md-9 px-0">
                <p class="h2 text-warning">{{$review->user->name}} <span class="small">{{$review->created_at->format('l jS \\of F Y')}}</span></p>
                <p class="h4">{{$review->name}}</p>
                <p>{{$review->description}}</p>
                @for ($i = 0; $i < $review->score; $i++)
                    <span class="review-star"></span>
                @endfor
                @for ($i = $review->score; $i < 5; $i++)
                    <span class="review-star-disabled"></span>
                @endfor
                </div>
            </div>
        </div>
    @endforeach
    </div>
@endif

@auth

    <div class="row">
        <div class="col-md-6">
            <h4>@lang('all.leave a review')</h4>
            <div class="review-form-wrapper">
                <form action="{{ Route('leaveAReview') }}" method="POST" id="leave-a-review-form">
                    @csrf
                    <input type="hidden" name="locale" value="{{ App::getLocale() }}">
                    <input type="hidden" name="reviewable_type" value="{{ \trim($reviewable_type,'\\') }}">
                    <input type="hidden" name="reviewable_id" value="{{ $id }}">
                    <div class="form-group">
                        <input name="alias" placeholder="@lang('all.alias')" value="{{ (!is_null(Auth::user()->name)) ? Auth::user()->name : '' }} {{ !is_null(Auth::user()->surname) ? Auth::user()->surname[0].'.' : ''}}" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <input name="name" placeholder="@lang('all.your review title')" type="text" class="form-control">
                    </div>
                    <div class="from-group" style="margin-top: -10px; margin-bottom: 10px;">
                        <select id="review-rating-select" name="rating" id="">
                            @for ($i = 1; $i <= 5; $i++)
                                <option @if ($i == 5) selected @endif value="{{ $i }}">{{ $i }}</option>
                            @endfor
                        </select>
                    </div>
                    <div class="form-group">
                        <textarea name="description" placeholder="@lang('all.your review content')" rows="6" id="" class="form-control"></textarea>
                    </div>
                    <div class="alert alert-danger d-none"></div>
                    <div class="alert alert-success d-none"></div>
                    <div class="form-group">
                        <button type="submit" name="button" class="btn btn-warning btn-lg text-white">@lang('all.send your review')</button>
                    </div>
                </form>

            </div>
        </div>
    </div>

@endauth
@guest

    <a href="{{Route('showRegisterForm')}}" class="btn btn-outline-warning text-warning btn-lg">@lang('all.sign in to leave a review')</a>

@endguest

@push('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-bar-rating/1.2.2/jquery.barrating.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-bar-rating/1.2.2/themes/css-stars.min.css">

<script>
    $(function(){

        $('#review-rating-select').barrating({
            theme: 'css-stars',
        });

        var reviewSubmitForm = $('#leave-a-review-form');

        reviewSubmitForm.submit(function(e){
            reviewSubmitForm.find('.alert').addClass('d-none')
            e.preventDefault();
            $.post(reviewSubmitForm.attr('action'),reviewSubmitForm.serialize(),function(r){

                reviewSubmitForm.find('.alert-success').html(r.message).removeClass('d-none');
            },'json')
            .fail(function(r){

                var reviewSubmitFormErrorMessage = '<ul class="list list-unstyled">';
                for(var i in r.responseJSON.errors){
                    var errs = r.responseJSON.errors[i];
                    for(var j in errs){
                        reviewSubmitFormErrorMessage += '<li>'+errs[j]+'</li>';
                    }
                }
                reviewSubmitFormErrorMessage += '</ul>';
                reviewSubmitForm.find('.alert-danger').html(reviewSubmitFormErrorMessage).removeClass('d-none');

            });
        });
    })
</script>

<style>
    span.review-star::after{
        content: "\2605";
        color: #e6a005;
    }
     span.review-star-disabled::after{
        content: "\2605";
        color: #d2d2d2;
    }
</style>
@endpush
