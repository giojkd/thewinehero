<a class="training row" href="{{$training->makeUrl()}}">
  <div class="training-headr col-md-12 p-0 col-4">
    <img class="img-fluid" src="{{Route('ir',['size' => 'h800','filename' => $training->photos[0]])}}" alt="">
  </div>
  <div class="training-body col-md-12 col-8">

    <span class="training-name">{{$training->name}}</span>
    <br>
    <span class="training-format">{{$training->format_quantity}}{{$training->format_measurement_unit}}</span>
    <p class="training-description-short">{{$training->description_short}}</p>

    <span class="btn btn-outline-warning btn-block">@lang('all.Watch')</span>

    <div class="text-center mt-2">

      @lang('all.with') {{$training->user->printFullName()}}
    </div>

  </div>
</a>
