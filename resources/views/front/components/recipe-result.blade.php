<a class="recipe w-100 w-md-auto " href="{{$recipe->makeUrl()}}">
    <div class="row">
        <div class="recipe-heading col-md-12 col-12">
            <img src="@cim(Route('ir',['size' => 240,'filename' => $recipe['photos'][0]]))" alt="@altTagMl($recipe['photos'][0])">
        </div>
        <div class="recipe-body col-md-12 col-12">
            <div class="pl-3">
                <h3 class="recipe-title">{{$recipe->name}}</h3>
                <p class="recipe-info">
                    <span class="text-nowrap"><i class="fas fa-hourglass-end"></i> {{$recipe->preparation_time}} @lang('all.minutes')</span>
                    @if(isset($recipe->ingredients))
                        <span class="text-nowrap"><i class="fa fa-list-alt"></i> {{$recipe->ingredients->count()}} @lang('all.ingredients')</span>
                    @endif
                    <span class="text-nowrap"><i class="fas fa-thermometer"></i> {{$recipe->calories}} kCal</span>
                </p>

                <div class="recipe-stars">
                    @if($recipe->reviews_count > 0)
                    @for($i = 0 ; $i < ceil($recipe->reviews_avg); $i++)
                    <i class="fas fa-star"></i>
                    @endfor
                    @for($i =ceil($recipe->reviews_avg) ; $i<5; $i++)
                    <i class="far fa-star"></i>
                    @endfor
                    ({{$recipe->reviews_count}})
                    @endif
                    @if(isset($recipe->complexity))
                    <span href="#" class="btn btn-sm btn-warning btn-round text-white">{{$recipe->complexity->name}}</span>
                    @endif
                </div>
            </div>
        </div>
    </div>
</a>
