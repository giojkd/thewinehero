<div class="twh-event">

    <div class="row">
        <div class="col-12">
            <div class="deal-cover">
                <a href="{{ $deal->makeUrl() }}">
                    <img class="img-fluid w-100" src="@cim(Route('ir',['size' => 320,'filename' => $deal->images[0]]))" alt="@altTagMl($deal->images[0])">
                </a>
                <div class="deal-cover-info">

                    @if ($deal->hasExpired())
                        <span class="badge badge-pill badge-danger">@lang('all.deal cover expired')</span>
                    @endif

                    @if (!$deal->hasStarted())
                        <span class="badge badge-pill badge-warning text-white">@lang('all.deal cover future')</span>
                    @endif

                    @if ($deal->hasStarted() && !$deal->hasExpired())
                        <span class="badge badge-pill badge-success">@lang('all.deal cover running')</span>
                    @endif

                </div>
            </div>

            <div class="row">
                <div class="col-md-12">

                        @if (!$deal->hasStarted())
                        <span class="btn btn-sm btn-outline-warning mb-1">
                            @lang('all.starts at') {{ date('d-m-Y H:i', strtotime($deal->starts_at)) }}
                        </span>
                        @endif
                        @if ($deal->hasStarted() && !$deal->hasExpired())
                        <span class="btn btn-sm btn-outline-warning mb-1">
                            @lang('all.ends at') {{ date('d-m-Y H:i', strtotime($deal->ends_at)) }}
                        </span>
                        @endif

                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    @hss('15')
                    <span class="event-title">{{ $deal->name }}</span>
                    {{ $deal->short_description }}
                    @hss('15')
                </div>
            </div>

            <div class="row mb-2">
                <div class="col">

                </div>
            </div>


            @if ($deal->hasExpired())
                <a href="{{ $deal->makeUrl() }}" class="btn btn-danger text-white" href="{{ $deal->makeUrl() }}">@lang('all.deal thumbnail has expired')</a>
            @endif

            @if (!$deal->hasStarted())
                <a href="{{ $deal->makeUrl() }}" class="btn btn-warning text-white" href="{{ $deal->makeUrl() }}">@lang('all.deal thumbnail not started yet')</a>
            @endif

            @if ($deal->hasStarted() && !$deal->hasExpired())
            <a href="{{ $deal->makeUrl() }}" class="btn btn-success" href="{{ $deal->makeUrl() }}">@lang('all.deal thumbnail current')</a>
            @endif



            <div class="row mt-3 d-none">
                <div class="col-md-8 text-right">
                    <small> @lang('all.add this deal to your favourites to be notified when it starts') </small>
                </div>
                <div class="col-md-4">
                    @include('front.components.favorite-toggler',['item' => $deal])
                </div>
            </div>


        </div>
    </div>
</div>
