<div class="favorite-toggler">
  @guest
    <a class="btn btn-outline-warning text-warning btn-sm" href="{{Route('showLoginForm')}}" class="text-warning">
        @lang('all.favorite')
    </a>
  @endguest

  @auth
  @if(auth()->user()->isFavorite($item))
    <a class="btn btn-warning text-white btn-sm" href="{{Route('toggleFavorite',['model'=> $item->getModelName(),'id' => $item->id])}}" class="text-warning">
        @lang('all.favorite')
    </a>
    @else
    <a class="btn btn-outline-warning text-warning btn-sm" href="{{Route('toggleFavorite',['model'=> $item->getModelName(),'id' => $item->id])}}" class="text-warning">
        @lang('all.favorite')
    </a>
    @endif
  @endauth
</div>
