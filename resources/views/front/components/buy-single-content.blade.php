@if((float)$content['price'] > 0)
    @if(!$content['is_premium'])
    @hss('30')
    @lang('all.or')
    @hss('30')
    @endif
    <h4>@lang('all.You can buy this') {{ $content['name'] }} @lang('all.for') {{ money($content['price'],'EUR') }} @if(isset($content['price_compare']) && $content['price_compare'] > 0) <small><span class="text-muted">@lang('all.instead of') {{ money($content['price_compare'],'EUR') }}</span></small> @endif</h4>
    @hss('10')
    <a
    href="{{ Route('buySingleContentCheckout',['model' => $content['name'],'id' => $content['id']]) }}"
    class="btn btn-warning btn-lg text-white">
        @lang('all.Buy it now!')
    </a>
@endif
