<a class="product" href="{{$experience->makeUrl()}}">
  <div class="row">

    <div class="col-md-12">
      <img class="img-fluid" src="{{Route('ir',['size' => 'h800','filename' => $experience->photos[0]])}}" alt="@altTagMl($experience->photos[0])">
      <h3 class="product-name">{{$experience->name}}</h3>
      <br>
      <span class="product-format"></span>
      <p class="product-description-short">{{$experience->short_description}}</p>

      @if(!is_null($experience->starting_price))
        <div class="product-prices">
            <small>@lang('all.Starting at') </small>
            <span class="product-price">@fp($experience->starting_price)</span>
        </div>
        @else
        @hss('15')
      @endif

      <span class="btn btn-outline-warning btn-block">@lang('all.Learn more')</span>

      <div class="text-center mt-2">
        <span class="text-warning">{{$experience->manufacturer->name}}</span>
      </div>

    </div>
  </div>
</a>
