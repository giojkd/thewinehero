 <div class="homepage-video-cover d-none d-md-block">


        <section id="home-banner-box" class="home-banner loading">

         {{--    <div class="image video-slide">
                <div class="video-background">
                    <div class="video-foreground" id="YouTubeBackgroundVideoPlayer">
                    </div>
                </div>
            </div>  --}}

        </section>

        <div class="homepage-video-cover-overlay justify-content-center" style="background: url(@cim('https://www.mamablip.com/images/mamablip-home-background_'.rand(1,1).'.jpg')); background-color: #555; background-repeat: no-repeat; background-size: cover;  background-attachment: fixed;">
          <div class="d-flex align-items-center">
            <div class="text-center">
              <h1 style="text-shadow: 2px 4px 12px rgb(0 0 0 / 45%);">{!! __('all.Cook like an Italiano.<br>Enjoy and Share Together.') !!}</h1><br>
              <a href="#bravissimi-section" scrolloffset="-70" class="btn btn-lg btn-warning text-uppercase text-white">{{ __('all.find out more') }}</a>
            </div>

          </div>
        </div>
      </div>
