<div class="container videos-container">
  <div class="row">
    <div class="col-md-6">
      <h2 class="d-none d-md-inline-block">@lang('all.Most watched this week')</h2>
      <div class="most-watched-videos">

          @foreach($videos as $video)
          <a class="most-watched-video" href="{{$video->makeUrl()}}">
            <div class="row">
              <div class="col-md-6 col-4">
                <img src="@cim(Route('ir',['size' => 180,'filename' => $video->cover]))" class="img-fluid" alt="@altTagMl($video->cover)">
              </div>
              <div class="col-md-6 col-8">
                <div class="video-title">
                  <h3>{{$video->name}}</h3>
                </div>
                {{--
                <div class="video-description mt-2">
                  {{Str::limit($video->subtitle,100)}}
                </div>
                 --}}
                <div class="video-info">
                  @if($video->views > 0)
                    {{$video->views}} @lang('all.views')
                  @endif
                  @if ($video->reviews_count > 0 )
                    {{$video->reviews_count}} @lang('all.comments')
                  @endif

                </div>
                @if ($video->reviews_count > 0 )
                    <div class="video-stars">
                    @for($i = 0; $i < $video->reviews_avg; $i++)
                        <i class="fas fa-star"></i>
                    @endfor
                    @for($i = $video->reviews_avg; $i < 4; $i++)
                        <i class="far fa-star"></i>
                    @endfor
                    ({{ $video->reviews_count }})
                    </div>
                @endif
              </div>
            </div>
          </a>
        @endforeach
      </div>

    </div>
    <div class="col-md-1">

    </div>
    <div class="col-md-5 text-right">
      <h2 class="text-left">@lang('all.Trending now')</h2>
      <div class="most-watched-videos">
        @foreach($videos_right_column as $video)
          <a class="most-watched-video" href="{{$video->makeUrl()}}">
            <div class="row">
              <div class="col-md-4 col-3">
                <img dq-src="{{Route('ir',['size' => 200,'filename' => $video->cover])}}" src="@cim(Route('ir',['size' => 50,'filename' => $video->cover]))" class="img-fluid w-100" alt="@altTagMl($video->cover)">
              </div>
              <div class="col-md-8 text-left col-9">
                <div class="video-title">
                  <h3>{{$video->name}}</h3>
                </div>
                {{--
                <div class="video-description d-none d-md-block">
                  {{Str::limit($video->subtitle,80)}}
                </div>
                 --}}
                <div class="video-info">
                    {{--
                    @if($video->views > 0 )
                        {{$video->views}} @lang('all.views')
                    @endif
                     --}}
                    @if($video->reviews_count > 0 )
                        {{$video->reviews_count}} @lang('all.comments')
                    @endif
                </div>
                @if ($video->reviews_count > 0 )
                    <div class="video-stars">
                        @for($i = 1; $i < $video->reviews_avg; $i++)
                            <i class="fas fa-star"></i>
                        @endfor
                        @for($i = $video->reviews_avg; $i < 5; $i++)
                            <i class="far fa-star"></i>
                        @endfor
                    </div>
                @endif
              </div>
            </div>
          </a>
        @endforeach
      </div>
    </div>
  </div>
  <div class="row">
      <div class="col-md-12">
          <div class="text-center mt-4">
      <a href="{{Route('videos')}}" class="btn btn-outline-light text-white btn-lg my-4 btn-block p-4">@lang('all.Watch all')</a>

          </div>
      </div>
  </div>
</div>
