<div class="container-fluid recipes-section numbers-section">
    <div class="row m-0">
        <div class="col-md-12 text-center p-0 ">
            <h2>@lang('all.Most Loved Recipes',['icon' => '<i class="fas fa-heart"></i>'])</h2>
            <span class="h2-subtitle">
                {!! __('all.Mediterranean Aroma, intense zest,<br>culinary tradition and passion: nourish your soul and please your palate') !!}
            </span>
            @hss('80')
        </div>
    </div>
    <div class="row">
        @foreach ($recipes as $recipe)
            <div class="mb-4 col-md-2 col-6">

                @include('front.components.recipe-result')

            </div>
        @endforeach
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            <a href="{{ Route('recipes') }}"
                class="btn btn-warning text-white px-4 py-2 btn-lg text-white">@lang('all.Watch all')</a>
        </div>
    </div>

</div>
