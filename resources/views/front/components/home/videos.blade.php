
{{-- section videos --}}
    <div class="container-fluid section-video-wrapper">
      <div class="row ">
        <div class="col-md-12 p-0">
          <div class="section-video">
            <img dq-src="@cim(Route('ir',['size' => 1280,'filename' => (!is_null($videos->first()->homepage_cover) ? $videos->first()->homepage_cover : $videos->first()->cover)]))" class="w-100" src="@cim(Route('ir',['size' => 200,'filename' => $videos->first()->cover]))" alt="@altTagMl($videos->first()->cover)">
            <div class="section-video-overlay justify-content-center">
              <div class="d-flex align-self-center">
                <a href="{{$videos->first()->makeUrl()}}">
                  <img height="@if($agent->isMobile()) 90 @else 160 @endif" src="@cim(url('/front/images/section-video-play.png'))?height=160" alt="">
                </a>
              </div>
            </div>
            <div class="section-video-footer">
              <div class="container">
                <div class="row">
                  <div class="col-md-12">
                    <div class="d-none d-md-block">
                        <h1>{{$videos->first()->name}}</h1>
                        <h2>{{$videos->first()->subtitle}}</h2>
                        <a href="{{$videos->first()->makeUrl()}}" class="btn btn-warning text-white btn-lg px-4 py-3">@lang('all.Watch now')</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>

        </div>
      </div>
      @include('front.components.home.videos-most-watched')
    </div>
{{-- section videos end --}}
