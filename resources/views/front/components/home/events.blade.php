 <div class="event-section bg-warning mb-4">
          <div class="container">
            <div class="row">
              <div class="col-md-12">
                <div class="py-4">
                  <h1 class="text-center">@lang("all.Insieme: Upcoming events you can't miss!")</h1>
                  {{-- <p>Upcoming events you can't miss</p>  --}}
                </div>
              </div>
              <div class="col-md-6">
                {{--
                <ul class="list-inline time-options">
                <li class="list-inline-item">Today</li>
                <li class="list-inline-item">This week</li>
                <li class="list-inline-item active">Next week</li>
                <li class="list-inline-item">This month</li>
              </ul>
              --}}
            </div>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row mb-4">


          @foreach($dates as $day => $events)
            <!--<div class="col-md-12 twh-event-date">
            <h2>{{\Carbon\Carbon::createFromFormat('Y-m-d', $day)->format('l jS \\of F Y')}}</h2>
          </div>-->
          @foreach($events as $event)
            <div class="col-md-6">
              @include('front.components.event-result')
            </div>
          @endforeach
        @endforeach
      </div>
      <div class="row mt-4">
        <div class="col-md-12 text-center">
          <a href="{{Route('events')}}" class="btn btn-warning text-white btn-lg my-4 px-4">@lang('all.Watch all events')</a>
        </div>
      </div>
    </div>
