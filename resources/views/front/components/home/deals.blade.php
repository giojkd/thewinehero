 <div class="event-section bg-warning mb-4">
          <div class="container">
            <div class="row">
              <div class="col-md-12">
                <div class="py-4">
                  <h1 class="text-center">@lang("all.deals title homepage")</h1>
                </div>
              </div>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row mb-4">

            @foreach($homepageDeals as $deal)
                <div class="col-md-6">
                    @include('front.components.deal-result')
                </div>
            @endforeach

        </div>
      <div class="row mt-4">
        <div class="col-md-12 text-center">
          <a href="{{Route('deals')}}" class="btn btn-warning text-white btn-lg my-4 px-4">@lang('all.watch all deals')</a>
        </div>
      </div>
    </div>
