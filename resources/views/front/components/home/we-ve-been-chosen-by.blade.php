<div class="container-fluid" id="we-ve-been-chosen-by">
        <div class="row">
          <div class="col text-center my-4">
            {{ __("all.We've been chosen by") }}
          </div>
        </div>
        <div class="row">
          <div class="col p-0">
            <div class="clients-logos d-none">
              <div class="glider-carousel glider-carousel-autoplay logos-owl-carousel" id="manufacturers-carousel" style="">
                <?php foreach (collect($sponsor_manufacturers)->shuffle() as $key => $manufacturer) {?>

                    <a href="{{ $manufacturer->makeUrl() }}">
                        <img data-src="@cim(Route('ir',['size' => 'h120','filename' => $manufacturer->logo ]))" class="lazyload" alt="{{ $manufacturer->name }}">
                    </a>
                <?php } ?>
                   <?php foreach (collect($sponsor_consortiums)->shuffle() as $key => $consortium) {?>
                    <a href="{{ $consortium->makeUrl() }}">
                        <img data-src="@cim(Route('ir',['size' => 'h120','filename' => $consortium->logo ]))" class="lazyload" alt="{{ $consortium->name }}">
                    </a>
                <?php } ?>
                </div>
              </div>
            </div>
          </div>
        </div>
