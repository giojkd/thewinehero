 @if($homepage_users)
       <div class="homepage-experts-section text-center" id="bravissimi-section">

            <h2 class="text-white">{{ __('all.Bravissimo!') }}</h2>
            <hr>
            <span class="h2-subtitle my-4 text-white">
                {!! __('all.Learn and savour with a true Maestro:<br>Live hands-on, interactive online cooking classes & wine classes, articles, videos and more!') !!}
            </span>
            <hr>
            <div class="experts-carousel d-none" style="overflow-x: scroll">
                <div
                @if ($agent->isMobile())
                    style="width: {{ count($homepage_users)*260 }}px"
                @endif
                >
                    @foreach($homepage_users as $homepage_user)
                        <div
                        class="expert"
                        @if (!$agent->isMobile())
                            style="width: {{ (100/count($homepage_users)-1) }}vw"
                        @else
                            style="width: 250px; display: inline-block"
                        @endif
                        >
                            <a target="_blank" href="{{ $homepage_user->makeUrl() }}">

                                <img class="lazyload" data-src="@cim(Route('ir',['size' => 50,'filename' => $homepage_user->avatar ]))" dq-src="{{Route('ir',['size' => 240,'filename' => $homepage_user->avatar ])}}" alt="{{ $homepage_user->full_name }}">
                            </a>
                            <div class="expert-info">
                                <h3 class="expert-name">
                                    {{ $homepage_user->full_name }}
                                </h3>
                                {{-- <a href="{{ $homepage_user->makeUrl() }}" class="btn btn-warning text-white btm-sm">@lang('all.read more')</a>  --}}
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
       </div>
        @endif
