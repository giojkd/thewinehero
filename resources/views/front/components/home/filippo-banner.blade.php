  <div class="container-fluid bg-warning mt-4">
          <div class="row">
            <div class="col-md-6">
              <img src="@cim(Route('ir',['size' => 100,'filename' => 'front/images/filippo-pan.png' ]))" alt="@lang('all.cooking and wine classes')" dq-src="{{ Route('ir',['size' => 800,'filename' => 'front/images/filippo-pan.png' ]) }}" class="filippo-pan w-100">
            </div>
            <div class="col-md-6 text-center">
              <div class="filippo-pan-info">
                <h2>{!! __("all.Discover the marvellous<br>world of Italian wine") !!}</h2>
                <span>{{ __("all.with") }}</span><br>
                <img class="filippo-signature my-2" src="@img('filippo-signature.png')" alt="Filippo Bartolotta"><br>
                <a href="https://www.mamablip.com/en/videocategory/italian-wine-review/2" class="btn btn-lg btn-light text-uppercase text-warning px-4 mb-4">{{ __('all.find out more') }}</a>
              </div>
            </div>
          </div>
        </div>
