
    <div class="container blog-section">
      <div class="row">
        <div class="col-md-12 text-center">
            <h2>@lang('all.Dive in to our Blog!')</h2>
            <span class="h2-subtitle">@lang('all.Daily, freshly wipped information about Italian wine and food')</span>
            @hss('30')
            @foreach ($articleCategories as $articleCategory)
                <a class="btn btn-sm btn-warning btn-round text-white mb-2" href="{{ $articleCategory->makeUrl() }}">{{ $articleCategory->name }}</a>
            @endforeach
            @hss('30')
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">


                @foreach($mainArticles as $mainArticle)
                 <div class="row">
                    <div class="col-md-12">
                        <a class="main-article" href="{{$mainArticle->makeUrl()}}">
                            <h2 style="line-height: 45px; font-size: 35px; color: #595959;">{{$mainArticle->name}}</h2>
                            <img dq-src="@cim(Route('ir',['size' => 800,'filename' => $mainArticle->photos[0]]))" src="@cim(Route('ir',['size' => 100,'filename' => $mainArticle->photos[0]]))" alt="@altTagMl($mainArticle->photos[0])" class="img-fluid w-100 round-corners-12">
                            <p class="main-article-brief">{{$mainArticle->brief}} <span class="read-more">@lang('all.Read more')</span>  </p>
                        </a>
                    </div>
                </div>
                @endforeach

        </div>
        <div class="col-md-6">
          <div class="articles-list">
            @foreach($blog as $article)
              <a class="article" href="{{$article->makeUrl()}}">

                <div class="row">
                  <div class="col-md-3 col-3">

                    <div class="article-img">
                      <img src="@cim(Route('ir',['size' => 'h100','filename' => $article->photos[0]]))" alt="@altTagMl($article->photos[0])">
                    </div>
                  </div>
                  <div class="col-md-9 col-9">
                    <h3 class="article-title">{{$article->name}}</h3>
                    <span class="article-brief">{{$article->subtitle}}</span>
                    <span class="article-info">
                      <?=$article['published_at']->formatLocalized('%a, %b %d')?><br>
                        @if (!is_null($article->user))
                            <b><small>@lang('all.by') {{ $article->user->name }} {{ $article->user->surname }}</small></b>
                        @endif
                      @if($article->reviews_count > 0)
                        | <?=$article['comments']?> @lang('all.comments') |
                        <span class="article-stars">
                          @for($i = 0; $i < ceil($article->reviews_avg); $i++)
                            <i class="fas fa-star"></i>
                          @endfor
                          @for($i = ceil($article->reviews_avg); $i < 5; $i++)
                            <i class="far fa-star"></i>
                          @endfor
                        </span>
                      @endif
                    </span>
                  </div>
                </div>
              </a>
            @endforeach
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 text-center">
          <a href="{{Route('blog')}}" class="btn btn-warning text-white btn-lg px-4 py-3">@lang('all.Read our blog')</a>
        </div>
      </div>
    </div>
