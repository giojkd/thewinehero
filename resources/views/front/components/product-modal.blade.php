<div class="modal fade" id="modalProduct_{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                @if (!is_null($item->videos) && $item->videos->whereNotNull('embed_code')->count() > 0)
                                    {!! $item->videos->whereNotNull('embed_code')->first()->embed_code !!}
                                @endif
                                <div class="row">
                                    <div class="col-md-6">
                                        <img class="img-fluid" src="{{Route('ir',['size' => 'h1280','filename' => $item->photos[1]])}}" alt="@altTagMl($item->photos[1])">
                                        @if (isset( $item->photos[2]) &&  $item->photos[2] != '')
                                            <img class="img-fluid" src="{{Route('ir',['size' => 'h1280','filename' => $item->photos[2]])}}" alt="@altTagMl($item->photos[2])">
                                        @endif
                                    </div>
                                    <div class="col-md-6">
                                        <h1 class="text-warning">{{ $item->name }}</h1>
                                        <h2 class="text-warning">
                                            @if(!is_null($item->manufacturer))
                                                @if($item->manufacturer->enabled)
                                                    {{$item->manufacturer->name}}
                                                @else
                                                    {{$item->manufacturer->name}}
                                                @endif
                                            @endif
                                            @if(!is_null($item->consortium))
                                                @if($item->consortium->enabled)
                                                    <a class="manufacturer-link" href="{{$item->consortium->makeUrl()}}">{{$item->consortium->name}}</a>
                                                @else
                                                    <a class="manufacturer-link" href="#">{{$item->consortium->name}}</a>
                                                @endif
                                            @endif
                                        </h2>
                                        <hr>
                                        <h3>@lang('all.Description')</h3>
                                        <div class="best-readable text-justify">
                                            {!!$item->description!!}
                                        </div>
                                        <h3>@lang('all.Features')</h3>
                                        <table class="table table-striped">
                                            @foreach ($item->categories
                                                        ->groupBy('parent_id') as $group
                                                    )
                                                @if (is_object($group->first()->parent))
                                                    <tr>
                                                        <td>
                                                            <b>{{$group->first()->parent->name}}</b>
                                                        </td>
                                                        <td>
                                                            {{implode(', ',$group->pluck('name')->toArray())}}
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
