
<div class="product">
  <div class="row">
    <div class="col-md-4 text-center">
      <a href="{{ $product->makeUrl() }}">
        @if (!is_null( $product->photos))
            <img class="img-fluid" src="@cim(Route('ir',['size' => 'h800','filename' => $product->photos[0]]))" alt="@altTagMl($product->photos[0])">
        @endif

      </a>
    </div>
    <div class="col-md-8">
      <h3 class="product-name h5">{{$product->name}}</h3>
      <span class="product-format">{{$product->format_quantity}}{{$product->format_measurement_unit}}</span>
      <p class="product-description-short">{{$product->description_short}}</p>

      @if(!is_null($product->price))
        <div class="product-prices">
            <span class="product-price">@fp($product->price)</span>
            @if(!is_null($product->price_compare))
                <span class="product-price_compare">@fp($product->price_compare)</span>
            @endif
        </div>
        @else
        @hss('15')
      @endif
        @if ($product->isOnDeal())
             <a class="text-warning" href="{{ $product->deals->first()->makeUrl() }}">@lang('all.this product is currently on deal')</a>
             @hss('10')
        @else

        @endif
      <a href="{{ $product->makeUrl() }}" class="btn btn-outline-warning btn-block">@lang('all.View product')</a>

      <div class="text-center mt-2">
          @if(!is_null($product->manufacturer))
            @if ($product->manufacturer->enabled)
                <a class="text-warning" href="{{ $product->manufacturer->makeUrl() }}">@lang('all.about') {{$product->manufacturer->name}}</a>
            @else
                <a class="text-warning" href="javascript:">{{$product->manufacturer->name}}</a>
            @endif
          @endif
        @if(!is_null($product->consortium))
            <a class="text-warning" href="{{ $product->consortium->makeUrl() }}">@lang('all.about') {{$product->consortium->name}}</a>
          @endif
      </div>
    </div>
  </div>
</div>
