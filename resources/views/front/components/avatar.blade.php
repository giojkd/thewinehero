@if ($user)


<!--<a class="avatar" href="{{$user->makeUrl()}}">-->
<div class="avatar">
  @if(isset($user->avatar))
    @if($user->avatar != '')
      <img src="{{Route('ir',['size' => 'h40','filename' => $user->avatar])}}" alt="">
    @endif
  @else
    <span class="avatar-letter">
      {{Str::limit($user->name,1,'')}}
    </span>
  @endif
</div>
<!--</a>-->

@endif
