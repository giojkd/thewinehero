
@if (isset($guest) && $guest->showLangSwitchBlock())
    @if (isset($showLangSwitchBlock))
        <div id="langSwitchBlock" style="white-space: nowrap; padding: 0px 10px; position: relative; text-shadow: 2px 2px 4px rgb(0 0 0 / 30%); z-index: 999; font-size: 22px; color:#fff; height: 55px; background-color:#ffc107; width:100%; overflow-x:scroll;  position: fixed; top: 0px; left: 0px; text-align: center; line-height: 55px;">
            <a style="color:#fff; text-decoration: none" href="{{ $showLangSwitchBlock['url'] }}">Vai alla versione in italiano! <img src="https://cdn.countryflags.com/thumbs/italy/flag-button-round-250.png" height="25" alt=""></a>
            <a style="color:#fff; position: absolute; right: 10px" href="{{ url()->full() }}?disableLangSwitch=true" onclick="$('#langSwitchBlock').remove(); $('.navbar').css('margin-top','0px')">x</a>
        </div>
    @endif
@endif

  <nav class="navbar navbar-expand-lg navbar-light bg-white fixed-top navbar-main" @if (isset($showLangSwitchBlock) && isset($guest) && $guest->showLangSwitchBlock()) style="margin-top: 55px" @endif>

    <a class="navbar-brand" href="{{Route('home')}}">
      <img src="@cim(url('/front/images/logo.png'))?width=250"  height="30" class="d-inline-block align-top" alt="@lang('all.pay off')">
      <span class="logo-pay-off">@lang('all.pay off')</span>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <form class="form-inline my-2 my-lg-0 navbar-search" action="{{Route('search-gateway',['locale' => app()->getLocale()])}}">
        <input name="keyword" class="form-control form-control-lg mr-sm-2 " type="search" placeholder="@lang('all.Search wines, ingredients, recipes and pairings...')" aria-label="Search">
      </form>
      <ul class="navbar-nav mr-auto">

      </ul>
      <ul class="navbar-nav ml-auto">
        {{--
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Subject
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="#">Action</a>
            <a class="dropdown-item" href="#">Another action</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">Something else here</a>
          </div>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Courses
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="#">Action</a>
            <a class="dropdown-item" href="#">Another action</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">Something else here</a>
          </div>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="#">Action</a>
            <a class="dropdown-item" href="#">Another action</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">Something else here</a>
          </div>
        </li>
        --}}
        <li class="nav-item ml-md-3 text-center">
          <a class="btn btn-warning text-white text-uppercase" href="{{Route('videos')}}" tabindex="-1" aria-disabled="true">{{--  <i class="fas fa-video"></i>  --}} @lang('all.Videos')</a>
        </li>
        <!--
        <li class="nav-item ml-md-3 text-center">
          <a style="font-size: 16px;" class=" nav-link @if(Route::currentRouteName() == 'events') active @endif" href="{{Route('events')}}" tabindex="-1" aria-disabled="true">{{-- <i class="far fa-calendar-alt"></i>  --}} @lang('all.Live Classes')</a>
        </li>
    -->
         <li class="nav-item ml-md-3 text-center">
          <a class="nav-link @if(Route::currentRouteName() == 'blog') active @endif" href="{{Route('blog')}}" tabindex="-1" aria-disabled="true">{{-- <i class="fa fa-rss"></i>  --}} @lang('all.Blog')</a>
        </li>
         <li class="nav-item ml-md-3 text-center">
          <a class="nav-link @if(Route::currentRouteName() == 'recipes') active @endif" href="{{Route('recipes')}}" tabindex="-1" aria-disabled="true">{{-- <i class="fas fa-pizza-slice"></i>  --}} @lang('all.Recipes')</a>
        </li>
        {{--
        <li class="nav-item ml-md-3 text-center">
          <a class="nav-link @if(Route::currentRouteName() == 'courses') active @endif" href="{{Route('courses')}}" tabindex="-1" aria-disabled="true"><i class="fas fa-graduation-cap"></i> @lang('all.Learn')</a>
        </li>
         --}}

        <li class="nav-item ml-md-3 text-center dropdown">
          <a data-toggle="dropdown" class="nav-link dropdown-toggle @if(Route::currentRouteName() == 'products') active @endif" href="javascript:" tabindex="-1" aria-disabled="true">{{-- <i class="fas fa-wine-bottle"></i> --}} @lang('all.Shop')</a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                {{--
                <a href="{{ Route('deals') }}" class="text-center dropdown-item" href="#">@lang('all.deals')</a>
                 <div class="dropdown-divider"></div>
                --}}
                <a href="{{ Route('deals') }}" class="text-center dropdown-item" href="#">@lang('all.mamaboxes navbar')</a>
                @foreach ($producttypes as $type )
                    <a href="{{ Route('products',['producttype' => $type->name]) }}" class="text-center dropdown-item" href="#">{{ $type->name }}</a>
                @endforeach
            </div>
        </li>
{{--
         <li class="nav-item ml-md-3 text-center">
          <a class="nav-link @if(Route::currentRouteName() == 'i-mama') active @endif" href="{{Route('iMama')}}" tabindex="-1" aria-disabled="true"><i class="far fa-lightbulb"></i> @lang('all.aimama')</a>
        </li>
 --}}
        <li class="nav-item dropdown ml-md-3">
            <a class="text-center nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{ Str::upper(App::getLocale()) }} {{--  -
                 @foreach ($availableCountries as $country)
                    @if ($country->id == $guest->country_id)
                        {{ $country->iso_3166_3 }}
                    @endif
                @endforeach  --}}

            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <span class="dropdown-header text-center">@lang('all.language')</span>
                @foreach (config('backpack.crud')['locales'] as $localeShort => $localeLong )
                    <a href="{{ Route('home',['locale' => $localeShort]) }}" class="text-center dropdown-item @if(App::getLocale() == $localeShort) active @endif" href="#">
                        {{ $localeLong }}
                    </a>
                @endforeach

                <div class="dropdown-divider"></div>
                <span class="dropdown-header text-center">@lang('all.country')</span>
                @foreach ($availableCountries as $country)
                    <a href="{{ url(App::getLocale()) }}?country_id={{ $country->id }}" class="text-center dropdown-item @if ($country->id == $guest->country_id) active @endif" href="#">
                        {{ $country->name }}
                    </a>
                @endforeach

            </div>
        </li>



        @guest
          <li class="nav-item ml-md-3 text-center">
              <a class="nav-link text-warning" href="{{Route('showLoginForm')}}" tabindex="-1" aria-disabled="true"> @lang('all.Log In')</a>
          </li>
          <li class="nav-item ml-md-3 text-center">
            <a class="btn btn-warning text-white" href="{{Route('showRegisterForm')}}" tabindex="-1" aria-disabled="true">@lang('all.Join for free')</a>
          </li>
        @else
          <li class="nav-item ml-md-3 mt-2 mt-md-0 text-center">
            <a class="btn btn-warning text-white" href="{{Route('user')}}" tabindex="-1" aria-disabled="true">{{Auth::user()->name}}</a>
          </li>
        @endguest

        @if ($lead->products->count() > 0)
            <li class="nav-item ml-md-3 text-center">
                <a class="nav-link @if(Route::currentRouteName() == 'cart') active @endif" href="{{Route('cart')}}" tabindex="-1" aria-disabled="true"><i class="fa fa-cart"></i> @lang('all.cart') ({{ $lead->products->count() }})</a>
            </li>
        @endif



      </ul>
    </div>
  </nav>
