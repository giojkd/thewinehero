<a class="article-result" href="{{$article->makeUrl()}}">
  <img alt="@altTagMl($article->photos[0])" class="img-fluid" src="@cim(Route('ir',['size' => 320,'filename' => $article->photos[0]]))" alt="">
  <div class="body">
    <h3>{{$article->name}}</h3>
    <p>{{$article->brief}}</p>
    <small><span class="text-muted"><i class="fa fa-clock"></i> {{ date(Setting::get('date_format_'.App::getLocale()),strtotime($article->published_at)) }}  @auth @if(Auth::user()->hasRole('Admin')) | {{ $article->views }} views @endif  @endauth  <br></span></small>
    @if (!is_null($article->user))
       <span class="text-warning"><b><small>@lang('all.by') {{ $article->user->name }} {{ $article->user->surname }}</small></b> </span>
    @endif

  </div>
</a>
