<div class="social-buttons-wrapper">
    <small><span class="text-muted">@lang('all.share with')</span></small>

    @foreach ($social_share as $social => $link)
        <a target="_blank" href="{{ $link }}" class="{{ $social }}-social-button social-button">{{ $social }}</a>
    @endforeach
</div>

<style>

    .social-buttons-wrapper{
        display: block;
        position: relative;
    }

    .social-button{
        font-size: 14px;
        padding: 0px 8px;
        border: 1px solid #ccc;
        border-radius: 4px;
        margin-right: 2px;
        background: #fff;
        transition: 0.5s all;
        border-bottom-width: 2px;
        display: inline-block;
    }

    .social-button:hover{
        color: #fff;
        margin-top: -2px;
        /*position: absolute;*/
    }

    .social-button:hover{
        text-decoration: none;
    }

    .facebook-social-button{
        color: #0068E2;
        border-color: #0068E2;
    }

    .facebook-social-button:hover{
        background: #0068E2;
    }

    .twitter-social-button{
        color: #00A3F2;
        border-color: #00A3F2;
    }

    .twitter-social-button:hover{
        background: #00A3F2;
    }

    .linkedin-social-button{
        color: #0577B5;
        border-color: #0577B5;
    }

    .linkedin-social-button:hover{
        background: #0577B5;
    }

 .whatsapp-social-button{
        color: #2CB642;
        border-color: #2CB642;
    }

    .whatsapp-social-button:hover{
        background: #2CB642;
    }





</style>
