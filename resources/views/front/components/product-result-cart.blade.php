<div class="product" >
  <div class="row">
    @if ($product->type != 'bundle')
        <div class="col-md-3 text-center">
            <a href="{{$product->makeUrl()}}">
                <img class="img-fluid" src="{{Route('ir',['size' => 'h800','filename' => $product->photos[0]])}}" alt="">
            </a>
        </div>
    @endif
    <div class="col-md-9">

        @if ($product->type != 'bundle')
            <a href="{{$product->makeUrl()}}" class="product-name">{{$product->name}}</a>
        @endif

        <br>
        @if ($product->type == 'bundle')
            @foreach ($product->products as $item)
                <div class="mb-1">
                    <div class="row mb-1">
                        <div class="col-12">
                            @php
                                $aux = json_decode($item->pivot->description,1);
                                $locale = App::getLocale();
                                $bundleDescription = (isset($aux[$locale])) ? $aux[$locale] : '' ;
                            @endphp
                            <div class="my-2 text-warning">
                                <b>{{ $item->pivot->quantity * $product->pivot->quantity }} {{ $bundleDescription }}</b>
                            </div>
                        </div>
                        <div class="col-12">
                            {{ $item->name }}
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
        @if ($product->type == 'simple')
            <span class="product-format">{{$product->format_quantity}}{{$product->format_measurement_unit}}</span>
        @endif


        @if(!is_null($product->getPrice($guest->country_id,'price')))
            <div class="product-prices">
                <span class="product-price text-nowrap">@lang('all.total cost') @fp($product->pivot->sub_total) @if ($product->type == 'simple') ({{ $product->pivot->quantity }} x @fp($product->getPrice($guest->country_id,'price'))) @endif </span><br>
                @if($product->getPrice($guest->country_id,'price_compare') > 0)
                    <span class="product-price_compare ml-0 text-nowrap">
                        @fp($product->pivot->quantity*$product->getPrice($guest->country_id,'price_compare')) ({{ $product->pivot->quantity }}

                            x
                            @fp($product->getPrice($guest->country_id,'price_compare')))

                    </span>
                @endif
            </div>
            @else
            @hss('15')
        @endif
        <form action="{{ Route('addToCart') }}" class="d-inline-block" method="POST">
            @csrf
            <input type="hidden" name="product_id" value="{{ $product->id }}">
            <div class="form-group">

                @if ($product->type == 'simple')
                    <select onchange="$(this).closest('form').submit()" name="quantity" id="" class="form-control form-control-sm">
                        @for ($i = 1; $i < $product->getMaxQuantity(); $i++)
                            <option @if ($product->pivot->quantity == $i) selected @endif value="{{ $i }}">{{ $i }}</option>
                        @endfor
                    </select>
                @endif
                @if ($product->type == 'bundle')
                    <select onchange="$(this).closest('form').submit()" name="quantity" id="" class="form-control form-control-sm">
                        @foreach ($product->getBundleQuantityOptions(380)  as $quantityOption)
                            <option @if ($product->pivot->quantity == $quantityOption) selected @endif value="{{ $quantityOption }}">{{ $quantityOption }}</option>
                        @endforeach
                    </select>
                @endif

            </div>
        </form>
        <form class="d-inline-block" action="{{ Route('removeFromCart') }}" method="POST">
            @csrf
            <input type="hidden" name="lead_product_id" value="{{ $product->pivot->id }}">
            <button class="btn btn-outline-secondary btn-sm">@lang('all.remove')</button>
        </form>

      {{--
      <select name="" id="">
          @for ($i = 1; $i < $count; $i++)

          @endfor
      </select>
       --}}
    </div>
  </div>
</div>
