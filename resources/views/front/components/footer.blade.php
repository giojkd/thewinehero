<div class="footer">
  <div class="container footer-content">
    <div class="row">
      <div class="col-md-4 ">
        <div class="p-4 bg-light rounded shadow">

            <img src="@img('logo.png')"  height="25" class="d-inline-block align-top logo" alt="" >
            <ul class="list-unstyled">
              <li>{{ Setting::get('company_name') }}</li>
              <li>{{ Setting::get('address_line_1') }}</li>
              <li>{{ Setting::get('address_line_2') }}</li>

              <li><a  href="mailto:{{ Setting::get('contact_email') }}">{{ Setting::get('contact_email') }}</a></li>
              <li>{{ Setting::get('vat_number') }}</li>
              <li>
                @foreach(['facebook','twitter','instagram','pinterest','youtube'] as $social)
                    @if(Setting::get($social.'_link') != '')
                        <a target="_blank" rel="noreferrer" href="{{ Setting::get($social.'_link') }}" class="social-link social-link-{{ $social }}"> <i class="fab fa-{{ $social }}"></i> </a>
                    @endif
                @endforeach
              </li>
              <li><a href="javascript:" id="changeCookiePreferencesBtn">@lang('all.change cookie preferences')</a></li>
              <li><a target="_blank" href="https://www.iubenda.com/privacy-policy/16810470">@lang('all.privacy policy')</a></li>
              @if (isset($guest))
                <li>GID{{ $guest->id }}</li>
              @endif

              @if (isset($lead))
                <li>LID{{ $lead->id }}</li>
              @endif

            </ul>
            @hss('70')
        </div>
      </div>
      <div class="col-md-4 ">
            <div class="p-3 bg-light rounded text-justify shadow">
                <span style="    font-weight: normal;
    text-align: center;
    font-size: 25px;
    margin-bottom: 2px;
    padding: 25px 15px 15px;
    margin-top: 0px;
    display: block;

    color: rgb(153, 153, 153);">@lang('all.about us')</span>
                <p class="best-readable">
                    @lang('all.about us content')
                </p>
            </div>
      </div>

      <!--<div class="col-md-2">

        <ul class="list-unstyled">
          <li><a href="#">About us</a></li>
          <li><a href="#">Privacy policy</a></li>
          <li><a href="#">Terms and conditions</a></li>
          <li><a href="#">Contacts</a></li>
          <li><a href="#">Start a subscription</a></li>
        </ul>

      </div>
      -->
      <div class="col-md-4">
          <div class="shadow">
            @include('front.components.newsletter-subscription-form-footer')
          </div>

      </div>
    </div>
  </div>
</div>

@push('scripts')

    <script>

        $(function(){

            $('.ajax-form').submit(function (e) {


                e.preventDefault();




                var form = $(this);

                form.find('.alert').addClass('d-none');

                var submitButton = form.find('button[type="submit"]');
                submitButton.prop('disabled', true);

                var action = form.attr('action');
                var type = form.find('input[name="_method"]').val();

                $.ajax({
                    url: action,
                    type: type,
                    dataType: 'json',
                    data: form.serialize(),
                    success: function (r) {
                        submitButton.prop('disabled', false);
                        if (r.status == 1) {
                            form.find('.alert.alert-success').removeClass('d-none');
                        }else{
                            form.find('.alert.alert-danger').html(r.message).removeClass('d-none');
                        }
                    }
                });

            });

        });

    </script>

{{--    <script
            id="AgeVerifyScript"
            data-bgmethod="Upload Your Own Background Image"
            data-bgurl="https://api.ageverify.co/wp-content/uploads/2022/02/Puglia_wines-1241c3a4e99f988b4d4a0f5850235173.jpeg"
            data-template=""
            data-logomethod="Upload Your Own Logo"
            data-altlogo="https://api.ageverify.co/wp-content/uploads/2022/02/logo-aa7f6c68de57d36715a86ccd3d74054b.png"
            data-logoh="35" data-textprompt=""
            data-fontsize="24"
            data-method="Age Button Prompt"
            data-entertext="Yes"
            data-exittext="No"
            data-bfontsize="18"
            data-dobmethod=""
            data-yytext=""
            data-mmtext=""
            data-ddtext=""
            data-age=""
            data-remembertext="Remember Me"
            data-countup=""
            data-underageredirect="https://www.skipperzuegg.it/it/succhi/senza-zuccheri-aggiunti-1000-ml/"
            data-colorp="#ffc107"
            data-colors=""
            src="https://pro.ageverify.co/jsv9/av9.2.js" ></script> --}}


@endpush
