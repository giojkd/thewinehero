<a class="manufacturer" href="{{$manufacturer->makeUrl()}}">
  <div class="row">
    <div class="col-md-4 text-center">
      <img class="img-fluid" src="{{Route('ir',['size' => 'h800','filename' => $manufacturer->logo])}}" alt="">
    </div>
    <div class="col-md-8">
      <span class="product-name">{{$manufacturer->name}}</span>
      <br>



    </div>
  </div>
</a>
