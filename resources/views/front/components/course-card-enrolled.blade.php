<div class="course-card">
  <div class="row m-0 p-0">
    <div class="col-md-2 m-0 p-0 text-right">
      <h2>@lang('all.Level') {{$indexLevel+1}}</h2>
      <h3>{{$level->modules->count()}} @lang('all.modules')</h3>
    </div>
    <div class="col-md-1">
      <div class="text-center" style="height:30px; ">
        <div class="bg-warning d-inline-block" style="height: 30px; width: 30px; border-radius: 50%"></div>
      </div>
      @if($indexLevel < $training->levels->count()-1)
        <div class="text-center h-100">
          <div class="d-inline-block bg-warning" style="width: 5px; height: 100%"></div>
        </div>
      @endif
    </div>
    <div class="col-md-9">
      <div class="card shadow">
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
              <h5 class="card-title">{{$level->name}}</h5>
              <p class="card-text">{{$level->subtitle}}</p>
              <div class="best-readable">
                {!!$level->description!!}
              </div>
              <div class="course-card-level-icons">
                <ul class="nav nav-tabs">
                  @if($level->videos->count() > 0)
                    <li class="nav-item">
                      <a data-toggle="tab" class="nav-link active" href="javascript:" data-target="#level_{{$level->id}}_videos"><i class="fas fa-video text-warning"></i> {{$level->videos->count()}} @lang('all.videos')</a>
                    </li>
                  @endif
                  @if($level->files->count() > 0)
                    <li class="nav-item">
                      <a data-toggle="tab" class="nav-link" href="javascript:" data-target="#level_{{$level->id}}_files"><i class="fas fa-align-justify text-warning"></i> {{$level->files->count()}} @lang('all.downloadable papers')</a>
                    </li>
                  @endif
                  @if($level->quizzs->count() > 0)
                    <li class="nav-item">
                      <a data-toggle="tab" class="nav-link" href="javascript:" data-target="#level_{{$level->id}}_quizzs"><i class="fas fa-align-justify text-warning"></i> {{$level->quizzs->count()}} @lang('all.quizzs')</a>
                    </li>
                  @endif
                </ul>
                <div class="tab-content">
                  @if($level->videos->count() > 0)
                    <div class="tab-pane active" id="level_{{$level->id}}_videos">
                      <div class="row">
                        @foreach ($level->videos as $key => $video)
                          <div class="col-md-4">
                            @include('front.components.video-result')
                          </div>
                        @endforeach
                      </div>

                    </div>
                  @endif
                  @if($level->files->count() > 0)
                    <div class="tab-pane fade" id="level_{{$level->id}}_files">
                      <table class="table table-hover">
                        <tbody>
                          @foreach($level->files as $file)
                            <tr>
                              <td>{{$file->name}}</td>
                              <td>
                                <a href="{{$file->makeUrlDownload()}}" class="btn btn-outline-warning">@lang('all.Download')</a>
                              </td>
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  @endif
                  @if($level->quizzs->count() > 0)
                    <div class="tab-pane fade" id="level_{{$level->id}}_quizzs">

                      <table class="table table-hover">
                        <tbody>
                          @foreach($level->quizzs as $quizz)
                            <tr>
                              <td>{{$quizz->name}}</td>
                              <td>
                                <a target="_blank" href="{{$quizz->makeUrl()}}" class="btn btn-outline-warning">@lang('all.Go to quizz')</a>
                              </td>
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                     </div>
                  @endif
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      @if($indexLevel > 0)
        <div>
          <div class="row m-0 p-0">
            <div class="col-md-2 m-0 p-0">

            </div>
            <div class="col-md-1">
              <div class="text-center h-100">
                <div class="d-inline-block bg-warning" style="width: 5px; height: 100%"></div>
              </div>
            </div>

          </div>
        </div>
      @else
        @hss('30')
      @endif
      <div class="level-modules">
        @foreach($level->modules as $indexModule => $module)
          @include('front.components.course-card-inner')
        @endforeach
      </div>
    </div>
  </div>
</div>
