@extends('front.main')

@section('content')

  <div class="container article" style="margin-top: 25px">
    <div class="row">
      <div class="col-md-8 offset-md-2">

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a class="text-warning" href="{{ url(App::getLocale()) }}">Home</a></li>
            </ol>
        </nav>
        <h1>{{$cms->name}} </h1>

        @hss('20')

      </div>

    </div>

    @if(!is_null($cms->photos) && count($cms->photos) > 0)
        <div class="row mt-4">
        <div class="col-md-8 offset-md-2">
            <img
            class="img-fluid article-cover"
            src="@cim(Route('ir',['size' => 800,'filename' => $cms->photos[0]]))"
            alt="@altTagMl($cms->photos[0])">
        </div>
        </div>
    @endif

  </div>
<div class="text-center">
    @if(!is_null($cms->photos) && count($cms->photos) > 0)
        @foreach($cms->photos as $index => $image)
            @if ($index > 0 && $index < 5)
                <img class="mw-100" src="@cim(Route('ir',['size' => 'h240','filename' => $image]))" alt="@altTagMl($image)">
            @endif
        @endforeach
    @endif
</div>
  <div class="container article" style="margin-top: 20px">
    <div class="row">
      <div class="col-md-8 offset-md-2">

        <div class="best-readable article-content">
            {!!$cms->content!!}
        </div>

        @include('front.components.newsletter-subscription-form')

      </div>

    </div>
  </div>

@endsection
