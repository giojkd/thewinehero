@extends('front.main')

@section('content')
@hss('10')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1 class="page-title">@lang('all.Our favourites')</h1>
      </div>
    </div>
</div>
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-2">
            <h3 class="mb-4">@lang('all.Filter by')</h3>
            @foreach ($categories as $categoryGroupId => $group)
                <div class="card mb-3">

                    <h5 class="card-header">{{ $group->first()->parent->name }}</h5>
                    <div class="card-body" style="max-height: 400px; overflow-y:scroll">

                        @foreach ($group as $item)
                            <div class="category-filter-btn" data-id="{{ $item->id }}">
                                {{ $item->name }}
                            </div>
                            {{-- <h3>{{ $group->first()->parent->name }}</h3>  --}}
                        @endforeach
                    </div>
                </div>
            @endforeach
        </div>
        <div class="col-md-10">
            {{-- @foreach ($products->chunk(3) as $chunks)
                <div class="row">
                        @foreach ($chunks as $key => $product)
                        <div class="col-md-4 col-6 product-wrapper" data-categories="{{ $product->categories->pluck('id')->implode(',') }}">
                            @include('front.components.product-result')
                        </div>
                    @endforeach
                </div>
            @endforeach  --}}
            <div class="row">
            @foreach ($products as $product)
                        <div class="col-md-3 col-6 product-wrapper" data-categories="{{ $product->categories->pluck('id')->implode(',') }}">
                            @include('front.components.product-result')
                        </div>
            @endforeach
            </div>
        </div>
      </div>
  </div>
  <style>
      .category-filter-btn{
        cursor: pointer;
        transition: 0.8s all;
        margin-bottom: 10px;
        border-bottom: 1px solid white;
      }
      .category-filter-btn:hover{
        border-color:   #212529;
      }
      .category-filter-btn.active{

        border-color:   #212529;
      }
  </style>
@endsection

@push('scripts')
    <script>
        $(function(){

            $('.category-filter-btn').click(function(){

                var appliedFilters = new Array();
                $(this).toggleClass('active');
                $('.category-filter-btn').each(function(){
                    if($(this).hasClass('active')){
                        appliedFilters.push(String($(this).data('id')));
                    }
                });
                console.log('Applied filters: ',appliedFilters);

                if(appliedFilters.length == 0){
                    console.log('There is no applied filters');
                    $('.product-wrapper').removeClass('d-none');
                }else{

                    $('.product-wrapper').each(function(){
                        var categories = String($(this).data('categories')).split(',');
                        console.log(categories);
                        var foundFilters = 0;

                        for(var i in appliedFilters){
                            var filter = appliedFilters[i];
                            if(categories.includes(filter)){
                                foundFilters++;
                            }
                        }

                        if(foundFilters >0 ){
                            console.log(foundFilters);
                            $(this).removeClass('d-none');
                        }else{
                            $(this).addClass('d-none');
                        }

                        /*
                        if(foundFilters == appliedFilters.length){
                            $(this).show();
                        }else{
                            $(this).hide();
                        }
                        */
                    })

                }




            })
        })
    </script>

@endpush
