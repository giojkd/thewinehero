@extends('front.main')
@section('content')


  @if(!is_null(session('results')))
    @php
    #dd(session('results')['questions']);
    @endphp
  @endif

  <div class="container">
    <div class="row">
      <div class="col-md-8 offset-md-2">
        <div id="quizz" class="my-4">
          <h1 class="mb-4">{{$quizz->name}}</h1>
          <form class="" action="{{route('quizzSubmit')}}" method="post">
            @csrf
            <input type="hidden" name="quizz_id" value="{{$quizz->id}}">
            @foreach($quizz->questions as $question)

              <div class="quizz-question my-4">
                <h2 class="question @if(!is_null(session('results'))) @if(session('results')['questions'][$question->id]['status'] == 1) text-success @else text-danger @endif @endif">Q: {{$question->name}} @if(!is_null(session('results'))) @if(session('results')['questions'][$question->id]['status'] == 1) <i class="far fa-thumbs-up"></i> @else <i class="far fa-thumbs-down"></i> @endif @endif</h2>
                  <div class="question-available-answers">
                    <div class="form-group">


                      @if($question -> type_id == 1)
                        @foreach($question->answers as $answer)
                          <div class="answer form-check">
                            <!-- RADIO SINGLE -->
                            <input class="form-check-input" @if(!is_null(session('results'))) disabled  @if(session('results')['questions'][$question->id]['old'] == $answer->id) checked @endif @endif id="answer_{{$answer->id}}_{{$question->id}}" type="radio" name="question[{{$question->id}}]" value="{{$answer->id}}">
                              <label for="answer_{{$answer->id}}_{{$question->id}}">{{$answer->name}}</label>
                            </div>
                          @endforeach
                        @else
                          @foreach($question->answers as $answer)
                            <div class="answer form-check">
                              <!-- CHECKBOX MULTIPLE -->
                              <input class="form-check-input" @if(!is_null(session('results'))) disabled @if(in_array($answer->id,session('results')['questions'][$question->id]['old'])) checked @endif @endif id="answer_{{$answer->id}}_{{$question->id}}" type="checkbox" name="question[{{$question->id}}][]" value="{{$answer->id}}">
                                <label for="answer_{{$answer->id}}_{{$question->id}}">{{$answer->name}}</label>
                              </div>
                            @endforeach
                          @endif
                          @if(!is_null(session('results')) && !session('results')['questions'][$question->id]['status'] == 1)
                            <hr>
                            <div class="alert alert-secondary" role="alert">
                              @lang('all.The correct answer was')
                              @if($question -> type_id == 1)
                                {{$question->answers->keyBy('id')->whereIn('id',session('results')['questions'][$question->id]['correct_answers'])->pluck('name')->join(' or ')}}
                              @else
                                {{$question->answers->keyBy('id')->whereIn('id',session('results')['questions'][$question->id]['correct_answers'])->pluck('name')->join(' and ')}}
                              @endif
                            </div>
                          @endif
                        </div>
                      </div>
                    </div>
                  @endforeach
                  <hr>
                  @if(!is_null(session('results')))
                    <div class="quizz-result my-4">
                      <div class="row">
                        <div class="col text-center">
                          <h1 class="@if(session('results')['exam_passed']) text-success @else text-danger @endif">@lang('all.You scored') {{session('results')['score']}}%</h1>
                            @if(session('results')['exam_passed'])
                              <h2 class="text-success">@lang('all.Great! You passed the test!')</h2>
                            @else
                              <h2 class="text-danger">@lang("Ouch! You'll do better next time!")</h2>
                            @endif
                        </div>
                      </div>
                    </div>
                    @if(session('results')['exam_passed'])
                      <div class="row">
                        <div class="col">
                          <a href="javascript:location.reload()" class="btn btn-secondary btn-lg btn-block">@lang('all.I want to improve my score')</a>
                        </div>
                        <div class="col">
                          <a href="{{Route('user')}}" class="btn btn-success btnl-lg btn-block btn-lg">@lang('all.Continue my training')</a>
                        </div>
                      </div>
                    @else
                      <div class="row">
                        <div class="col">
                          <a href="javascript:location.reload()" class="btn btn-danger btn-lg btn-block">@lang('all.Try again')</a>
                        </div>
                      </div>
                    @endif
                  @else
                    <button type="submit" class="btn btn-outline-warning btn-lg btn-block" name="button">@lang('all.Send your answers!')</button>
                  @endif
                </form>
              </div>

            </div>
          </div>
        </div>







      @endsection


      @push('scripts')

        <script type="text/javascript">

        </script>

      @endpush
