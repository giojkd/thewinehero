@extends('front.main')

@section('content')

  <div class="container article">
    <div class="row">
      <div class="col-md-9">
        <h1>{{$article->name}} </h1>
        <h2>{{$article->subtitle}} @include('front.components.favorite-toggler',['item' => $article])</h2>
        @include('front.components.social-share')
        @if (!is_null($article->user))
            <b><small>@lang('all.by') {{ $article->user->name }} {{ $article->user->surname }}</small></b>
        @endif

        <p class="text-muted">
            <small>
                {{ $article->views }} @lang('all.views')
            </small>
        </p>


      </div>
      <div class="col-md-3"></div>
    </div>
    <hr>
    <div class="row mt-4">
      <div class="col">
        <img class="img-fluid article-cover"
        src="{{Route('ir',['size' => 1200,'filename' => $article->photos[0]])}}" alt="@altTagMl($article->photos[0])">
      </div>
    </div>
    @hss('50')
  </div>
  {{--
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="slick-carousel carousel-stripe">
          @foreach($article->photos as $image)
              <img src="{{Route('ir',['size' => 'h240','filename' => $image])}}" alt="@altTagMl($image)">
          @endforeach
        </div>
      </div>
    </div>
  </div>
 --}}

  <div class="container article">
    <div class="row">
      <div class="col-md-9">
           @if($article->podcasts->count() > 0)
                <div class="mb-3">
                    <h3>Podcast</h3>
                    @foreach ($article->podcasts as $podcast)
                        {!! $podcast->spotify_embeddable_code	 !!}
                    @endforeach
                </div>
            @endif
        <div class="best-readable article-content">
          {!!Str::words(strip_tags($article->content),100,'...')!!}
        </div>
        <hr>
        @include('front.components.newsletter-subscription-form')
        <hr>


        @hss('50')
    <div class="container-fluid">
        <div class="row">
            <div class="col text-center">
                <h2>@lang('all.How to read the entire article?')</h2>
                @if(!$article->is_premium)
                    @auth
                        @if(Auth::user()->hasActivePlan())
                            @if($canPurchase)

                                @lang('More readable articles',['n' => Auth::user()->plan->articles])

                                <br>
                                <div class="mt-2">
                                    <form method="POST" action="{{ Route('purchaseContent',['model' => 'article','id' => $article->id]) }}">
                                        @csrf
                                        <button type="submit" class="btn btn-warning btn-large text-white btn-lg">@lang('all.Read now!')</button>
                                    </form>
                                </div>

                            @else
                                <div class="alert alert-danger">
                                    @lang('all.The articles included in your plan for this month are over, but you can still upgrade to another plan')
                                </div>
                                @include('front.components.enrollment-options',['content' => $article])

                            @endif
                        @else
                            @include('front.components.enrollment-options',['content' => $article])
                        @endif
                    @endauth
                    @guest
                        @include('front.components.enrollment-options',['content' => $article])
                    @endguest
                @endif
   @include('front.components.buy-single-content',
            [
              'content' => [
                  'name' => $article->getModelName(),
                  'price_compare' => ($article->applyPlanDiscount($article->getModelName(),$article->price) != $article->price) ? $article->price : 0,
                  'price' => $article->applyPlanDiscount($article->getModelName(),$article->price),
                  'id' => $article->id,
                  'is_premium' => $article->is_premium
                  ]
            ]
        )
            </div>
        </div>
    </div>
    <hr>
    @include('front.components.reviews-container',['reviews' => $article->reviews,'id' => $article->id,'reviewable_type' => $article->getModelNameFull()])

      </div>
      <div class="col-md-3">
        @foreach($article->sameCategoryArticles() as $sca)
          @include('front.components.article-result',['article' => $sca])
        @endforeach
      </div>
    </div>
  </div>

@endsection

{{--
@push('scripts')
   <script>
        $(function(){
            $('.slick-carousel').slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                autoplay: true,
                infinite: true,
                centerMode: true,
                variableWidth: true,
                dots: true,
                autoplaySpeed: 2000,
                responsive: [
                    {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                    },
                    {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                    },
                    {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            });
        })
    </script>
@endpush
 --}}
