@extends('front.main')
@section('content')

    <div class="container mt-4">
        <div class="row">
            <div class="col">
                <h1>Confirm your reservavtion</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5">
                @include("front.components.experience-result",['product' => $experience])
                @hss('30')

                <table class="table table-striped">
                    <tr>
                        <td>Adults</td>
                        <td>{{ $experience_config['adults'] }}</td>
                    </tr>
                    <tr>
                        <td>Children</td>
                        <td>{{ $experience_config['children'] }}</td>
                    </tr>
                    <tr>
                        <td>Date</td>
                        <td>{{ $experience_config['date'] }}</td>
                    </tr>
                    <tr>
                        <td>Time</td>
                        <td>{{ $experience_config['time'] }}</td>
                    </tr>
                    <tr>
                        <td>What is included</td>
                        <td>{!! $experience->whats_included !!}</td>

                    </tr>
                    <tr>
                        <td>What is not includeed</td>
                        <td>{!! $experience->whats_not_included !!}</td>
                    </tr>
                </table>


                @hss('30')
                <div class="text-center">
                    <h3>
                        for: {{ money($experience->applyPlanDiscount($experience,$total),'EUR') }}
                    </h3>
                </div>
            </div>
            <div class="col-md-7">
                @if($payment_methods->count() > 0)
                <h3>Choose a payment method</h3>
                   @foreach($payment_methods as $payment_method)
                    <div class="card mb-3">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-8">
                                    Credit card: <b>{{ Str::ucfirst($payment_method->card->brand) }} ({{ $payment_method->card->last4 }})</b>
                                    Exp: <b>{{ $payment_method->card->exp_month }}/{{ $payment_method->card->exp_year }}</b>
                                </div>
                                <div class="col-md-4 col-12 text-right">
                                    <form action="{{ Route('buyExperience') }}" method="POST">
                                        @csrf
                                        <input type="hidden" name="payment_method" value="{{ $payment_method->id }}">
                                        <input type="hidden" name="experience_id" value="{{ $experience_config['experience_id']  }}">
                                        <input type="hidden" name="adults" value="{{ $experience_config['adults']  }}">
                                        <input type="hidden" name="children" value="{{ $experience_config['children']  }}">
                                        <input type="hidden" name="date" value="{{ $experience_config['date']  }}">
                                        <input type="hidden" name="time" value="{{ $experience_config['time']  }}">
                                        <input type="hidden" name="locale" value="{{ $locale }}">

                                        <button type="submit" class="mt-2 mt-md-0 btn btn-warning text-white btn-block">Use this card</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @endif
                    <div>
                        @if($payment_methods->count() > 0)
                        <h3>Or set a new one</h3>
                        @endif
                        <div class="form-group">
                            <label for="">Cardholder's name</label>
                            <input id="card-holder-name" type="text" class="form-control" required value="{{ $user->full_name() }}">
                        </div>
                        <label for="">Credit card number</label>
                        <div id="new-card-form-wrapper" class="form-control-stripe"></div>

                    </div>


                <button data-secret="{{ $client_secret_intent->client_secret }}" id="card-button" type="button" class="btn btn-warning mt-4 btn-block text-white btn-lg">Confirm</button>
            </div>
        </div>
    </div>

@endsection
@push('scripts')


    <script src="https://js.stripe.com/v3/"></script>
    <script>



        $(() => {
            const stripe = Stripe('{{ env('STRIPE_KEY') }}');
            const elements = stripe.elements();
            const cardElement = elements.create('card');
            cardElement.mount('#new-card-form-wrapper');
            const cardHolderName = document.getElementById('card-holder-name');
            const cardButton = document.getElementById('card-button');
            const clientSecret = cardButton.dataset.secret;
            cardButton.addEventListener('click', async (e) => {
                const { setupIntent, error } = await stripe.confirmCardSetup(
                    clientSecret, {
                        payment_method: {
                            card: cardElement,
                            billing_details: { name: cardHolderName.value }
                        }
                    }
                );
                if (error) {
                    console.log(error);
                    alert(error.message);
                    //alert('unsuccess')
                } else {
                    console.log(setupIntent);
                    setupIntent.experience_id = {{ $experience_config['experience_id'] }};
                    setupIntent.adults = "{{ $experience_config['adults'] }}";
                    setupIntent.children = "{{ $experience_config['children'] }}";
                    setupIntent.date = "{{ $experience_config['date'] }}";
                    setupIntent.time = "{{ $experience_config['time'] }}";
                    setupIntent.locale = "{{ $locale }}";

                    setupIntent._token = '{{ csrf_token() }}';
                    $.post('{{ Route('buyExperience') }}',setupIntent,function(r){
                        if(r.status == 1){
                            location.href= '{{ $experience->makeUrl() }}';
                        }
                    },'json')
                }
            });
        })
    </script>


@endpush
