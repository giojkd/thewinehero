@extends('front.main')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <iframe src="{{$event->enrolment_link}}" width="100%" height="800" style="border: none"></iframe>
    </div>
  </div>
</div>

@endsection

@push('scripts')

@endpush
