@extends('front.main')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                @hss('20')
                <h1>@lang('all.request an invoice') {{ $purchase_id }}</h1>
                @hss('20')
                <form action="{{ Route('confirmPurchaseInvoice') }}" method="POST">
                    @csrf
                    <input type="hidden" name="id" value="{{ $purchase_id }}">
                    <div class="form-group">
                        <label for="">Email <span class="required-field-asterisk">*</span></label>
                        <input name="billing_address[email]" type="text" data-name="email" class="form-control @error('billing_address.email') is-invalid @enderror" value="{{ $billing_address['email'] }}">
                        @error('billing_address.email')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="">@lang('all.Name') <span class="required-field-asterisk">*</span></label>
                        <input name="billing_address[name]" type="text" data-name="name" class="form-control @error('billing_address.name') is-invalid @enderror" value="{{ $billing_address['name'] }}">
                        @error('billing_address.surname')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="">@lang('all.Surname') <span class="required-field-asterisk">*</span></label>
                        <input name="billing_address[surname]" type="text" data-name="surname" class="form-control @error('billing_address.surname') is-invalid @enderror" value="{{ $billing_address['surname'] }}">
                        @error('billing_address.surname')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                     <div class="form-group">
                        <label for="">@lang('all.vat number') <span class="required-field-asterisk">*</span></label>
                        <input name="billing_address[vat_number]" type="text" data-name="vat_number" class="form-control @error('billing_address.vat_number') is-invalid @enderror" value="{{ $billing_address['vat_number'] }}">
                        @error('billing_address.vat_number')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                     <div class="form-group">
                        <label for="">@lang('all.company name') <span class="required-field-asterisk">*</span></label>
                        <input name="billing_address[company_name]" type="text" data-name="company_name" class="form-control @error('billing_address.company_name') is-invalid @enderror" value="{{ $billing_address['company_name'] }}">
                        @error('billing_address.company_name')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="">@lang('all.Telephone') <span class="required-field-asterisk">*</span></label>
                        <input name="billing_address[telephone]" type="text" data-name="telephone" class="form-control @error('billing_address.telephone') is-invalid @enderror" value="{{ $billing_address['telephone'] }}">
                        @error('billing_address.telephone')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="">@lang('all.Shipping address') <span class="required-field-asterisk">*</span></label>
                        <input id="address-input" name="billing_address[address]" data-name="address" type="text" class="form-control @error('billing_address.address') is-invalid @enderror" value="{{ $billing_address['address'] }}">
                        @error('billing_address.address')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <button class="btn btn-lg btn-warning text-white btn-block" type="submit">@lang('all.confirm')</button>
                </form>
            </div>
        </div>
    </div>
@endsection
