@extends('front.main')
@section('content')
  <div class="video-player">
    @if(!is_null($video->file_teaser))
        <video src="{{url($video->file_teaser)}}" controls poster="posterimage.jpg" controlsList="nodownload">

        </video>
    @endif
  </div>
  <div class="video-spechs">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-9">
            <div class="row">
                <div class="col-md-8">
                    <h1 class="d-inline-block">{{$video->name}}</h1><br>
                </div>
                <div class="col-md-4 text-right">

                            @include('front.components.social-share')

                </div>
            </div>

          <div class="video-infos">
            <div class="row">
              <div class="col-md-6">@lang('all.views'): {{$video->views}} • @lang('all.released') {{$video->created_at->format('l jS \\of F Y H:i')}} @include('front.components.favorite-toggler',['item' => $video])</div>
              <div class="col-md-6"></div>
            </div>
          </div>
          <p class="video-subtitle">
            {{$video->subtitle}}
          </p>
          @hss('50')
          @if($video->podcasts->count() > 0)
                        @foreach ($video->podcasts as $podcast)
                            {!! $podcast->spotify_embeddable_code	 !!}
                        @endforeach
                        @hss('70')
                    @endif
          <div class="video-description best-readable style-first-letter">
            {!!$video->description!!}
          </div>
          <hr>
          @include('front.components.newsletter-subscription-form')
          <hr>
          @hss('70')
          @include('front.components.reviews-container',['reviews' => $video->reviews,'id' => $video->id,'reviewable_type' => $video->getModelNameFull()])
        </div>
        <div class="col-md-3 p-4">
          @foreach($video->sameCategoryItems() as $item)
            @include('front.components.video-result',['video' => $item])
          @endforeach
        </div>
      </div>
    </div>
  </div>

<div class="text-center">
      <div class="content-purchase-wrapper">

    <div class="container-fluid">
        <div class="row">
            <div class="col text-center">
                <h2>@lang('all.How to watch the full version of this video?')</h2>
                @if(!$video->is_premium)
                    @auth
                        @if(Auth::user()->hasActivePlan())
                            @if($canPurchase)
                            @lang('all.More watchable videos',['n' => Auth::user()->plan->videos])

                                <br>
                                <div class="mt-2">
                                    <form method="POST" action="{{ Route('purchaseContent',['model' => 'video','id' => $video->id]) }}">
                                        @csrf
                                        <button type="submit" class="btn btn-warning btn-large text-white btn-lg">@lang('all.Buy now!')</button>
                                    </form>
                                </div>
                            @else
                                <div class="alert alert-danger">
                                    @lang('all.The videos included in your plan for this month are over, but you can still upgrade to another plan')
                                </div>
                                @include('front.components.enrollment-options',['content' => $video])
                            @endif
                        @else
                            @include('front.components.enrollment-options',['content' => $video])
                        @endif
                    @endauth
                    @guest
                        @include('front.components.enrollment-options',['content' => $video])
                    @endguest
                @endif
        @include('front.components.buy-single-content',
            [
              'content' => [
                  'name' => $video->getModelName(),
                  'price_compare' => ($video->applyPlanDiscount($video->getModelName(),$video->price) != $video->price) ? $video->price : 0,
                  'price' => $video->applyPlanDiscount($video->getModelName(),$video->price),
                  'id' => $video->id,
                  'is_premium' => $video->is_premium
                  ]
            ]
        )
            </div>
        </div>
    </div>
  </div>
</div>

  <style>
      .content-purchase-wrapper{
        border-radius: 6px;


        padding: 15px;

      }
  </style>


@endsection



@push('scripts')
  <script type="text/javascript">
  var video_id;
  var video;
  $('.video-item').hover(function(){
    video_id = $(this).data('video-id');
    video = $('#video-'+video_id).get(0);
    video.play();
  },function(){
    //video.pause();
    //video.load();
    video.currentTime = 0;
    video.pause();
  })
  </script>
@endpush
