@extends('front.main')

@section('content')

@hss('50')
    @if(!is_null($products))
        <div class="container">
            @if($lead->grand_total > 0)
            <div class="row">
                <div class="col-md-5">
                    <h3 class="text-center mb-3">@lang('all.Summary')</h3>
                    <div class="card shadow">
                        <div class="card-body">
                            <div>
                                @foreach($products as $product)
                                    @if($product->pivot->quantity > 0)
                                        @include('front.components.product-result-cart')
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="mt-3 text-center d-none">
                        <div class="alert-alert-secondary">
                            @if ($lead->shipping_total > 0)
                                @lang('all.shipping cost is') @fp($lead->shipping_total)
                            @else
                                @lang('all.shipping is for free')
                            @endif
                        </div>
                    </div>
                    <div class="mt-3 text-center">
                        @hss('30')
                        <h2>@lang('all.Total:') @fp($lead->grand_total)</h2>
                        @hss('30')
                    </div>
                    @hss(' ')
                    <div class="card">
                        <div class="card-body">
                            <small>@lang('all.help checkout 1')</small>
                        </div>
                    </div>
                    <div class="card mt-2">
                        <div class="card-body">
                            <small>@lang('all.help checkout 2')</small>
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <form action="{{ Route('confirmCart') }}" id="cart-confirmation-form" method="POST">
                        @csrf
                            <input type="hidden" name="payment_method" >
                            <input type="hidden" name="address_json" >
                            <div>
                                <h3 class="text-center mb-3">@lang('all.shipping')</h3>
                                <div class="card shadow">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label for="">@lang('all.Recipient email') <span class="required-field-asterisk">*</span></label>
                                            <input id="card-holder-email" name="shipping_address[email]" type="text" data-name="email" class="form-control @error('shipping_address.email') is-invalid @enderror" value="{{ $shipping_address['email'] }}">
                                            @error('shipping_address.email')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                         <div class="form-group">
                                            <label for="">@lang('all.Recipient confirm email') <span class="required-field-asterisk">*</span></label>
                                            <input name="shipping_address[email_confirmation]" type="text" data-name="email_confirmation" class="form-control @error('shipping_address.email_confirmation') is-invalid @enderror" value="{{ $shipping_address['email_confirmation'] }}">
                                            @error('shipping_address.email_confirmation')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="">@lang('all.Recipient name') <span class="required-field-asterisk">*</span></label>
                                            <input name="shipping_address[name]" type="text" data-name="name" class="form-control @error('shipping_address.name') is-invalid @enderror" value="{{ $shipping_address['name'] }}">
                                            @error('shipping_address.surname')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label for="">@lang('all.Recipient surname') <span class="required-field-asterisk">*</span></label>
                                            <input name="shipping_address[surname]" type="text" data-name="surname" class="form-control @error('shipping_address.surname') is-invalid @enderror" value="{{ $shipping_address['surname'] }}">
                                            @error('shipping_address.surname')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="">@lang('all.Recipient telephone') <span class="required-field-asterisk">*</span></label>
                                            <input name="shipping_address[telephone]" type="text" data-name="telephone" class="form-control @error('shipping_address.telephone') is-invalid @enderror" value="{{ $shipping_address['telephone'] }}">
                                            @error('shipping_address.telephone')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="">@lang('all.Recipient shipping address') <span class="required-field-asterisk">*</span></label>
                                            <input id="address-input" name="shipping_address[address]" data-name="address" type="text" class="form-control @error('shipping_address.address') is-invalid @enderror" value="{{ $shipping_address['address'] }}">
                                            @error('shipping_address.address')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                            <p class="text-muted">
                                                <small>@lang('all.address country hint')</small>
                                            </p>
                                        </div>
                                        <div class="form-group">
                                            <label for="">@lang('all.Doorbell')</label>
                                            <input name="shipping_address[doorbell]" type="text" data-name="doorbell" class="form-control @error('shipping_address.doorbell') is-invalid @enderror" value="{{ $shipping_address['doorbell'] }}">
                                        </div>

                                        <div class="form-group">
                                            <label for="">@lang('all.Other important shipping information')</label>
                                            <textarea name="shipping_address[notes]" type="text" data-name="notes" class="form-control @error('shipping_address.notes') is-invalid @enderror">{{ $shipping_address['notes'] }}</textarea>
                                        </div>

                                        <hr>

                                        <div class="form-group">
                                            <label for="">@lang('all.Need an invoice') <input type="checkbox" name="has_requested_invoice" id="" value="1"></label>
                                            <p class="text-muted"><small>@lang('all.Check this box to request an invoice we will get in touch with you shortly to arrange it')</small></p>
                                        </div>

                                        <div class="form-group">
                                            <label for="">@lang('all.Accept terms and conditions') <input type="checkbox" name="has_accepted_terms_and_conditions" id="" value="1"></label>
                                            <p class="text-muted"><small>@lang('all.Terms and conditions cart hint')</small></p>
                                        </div>
{{--
                                        <label><input onchange="if($(this).is(':checked')){$('#billing-address').removeClass('d-none')} else{$('#billing-address').addClass('d-none')}" type="checkbox" name="bill_to_a_different_address" value="1"> @lang('all.bill to a different address')</label>
                                        --}}
                                    </div>
                                </div>
                                {{--
                                <div id="billing-address" class="d-none">
                                    <h3 class="text-center mb-3 mt-3">@lang('all.billing')</h3>
                                    <div class="card shadow" >
                                        <div class="card-body">
                                            <div class="form-group">
                                                <label for="">Email <span class="required-field-asterisk">*</span></label>
                                                <input name="billing_address[email]" type="text" data-name="email" class="form-control @error('billing_address.email') is-invalid @enderror" value="{{ $billing_address['email'] }}">
                                                @error('billing_address.email')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <label for="">@lang('all.Name') <span class="required-field-asterisk">*</span></label>
                                                <input name="billing_address[name]" type="text" data-name="name" class="form-control @error('billing_address.name') is-invalid @enderror" value="{{ $billing_address['name'] }}">
                                                @error('billing_address.surname')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </div>

                                            <div class="form-group">
                                                <label for="">@lang('all.Surname') <span class="required-field-asterisk">*</span></label>
                                                <input name="billing_address[surname]" type="text" data-name="surname" class="form-control @error('billing_address.surname') is-invalid @enderror" value="{{ $billing_address['surname'] }}">
                                                @error('billing_address.surname')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <label for="">@lang('all.Telephone') <span class="required-field-asterisk">*</span></label>
                                                <input name="billing_address[telephone]" type="text" data-name="telephone" class="form-control @error('billing_address.telephone') is-invalid @enderror" value="{{ $billing_address['telephone'] }}">
                                                @error('billing_address.telephone')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <label for="">@lang('all.Billing address') <span class="required-field-asterisk">*</span></label>
                                                <input id="address-input-2" name="billing_address[address]" data-name="address" type="text" class="form-control @error('billing_address.address') is-invalid @enderror" value="{{ $billing_address['address'] }}">
                                                @error('billing_address.address')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                                <p class="text-muted">
                                                    <small>@lang('all.address country hint')</small>
                                                </p>
                                            </div>

                                            <div class="form-group">
                                                <label for="">@lang('all.Need an invoice') <input type="checkbox" name="has_requested_invoice" id=""></label>
                                                <p class="text-muted"><small>@lang('all.Check this box to request an invoice we will get in touch with you shortly to arrange it')</small></p>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                 --}}
                                <h3 class="text-center my-3">@lang('all.Payment')</h3>
                                <div class="card shadow">
                                    <div class="card-body">
                                        {{--@if($payment_methods->count() > 0)
                                            @foreach($payment_methods as $payment_method)
                                                <div class="card mb-3">
                                                    <div class="card-body">
                                                        <div class="row">
                                                            <div class="col-md-8">
                                                                @lang('all.Credit card:') <b>{{ Str::ucfirst($payment_method->card->brand) }} ({{ $payment_method->card->last4 }})</b>
                                                                @lang('all.Exp:') <b>{{ $payment_method->card->exp_month }}/{{ $payment_method->card->exp_year }}</b>
                                                            </div>
                                                            <div class="col-md-4 col-12 text-right">

                                                                    <button name="payment_method" value="{{ $payment_method->id }}" type="submit" class="mt-2 mt-md-0 btn btn-warning text-white btn-block">@lang('all.Use this card')</button>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif --}}
                                        <div>
                                            {{--
                                            @if($payment_methods->count() > 0)
                                                <h3>@lang('all.Or set a new one')</h3>
                                            @endif
                                            --}}
                                            <div class="form-group">
                                                <label for="">@lang("Cardholder's name")</label>
                                                @auth
                                                    <input id="card-holder-name" type="text" class="form-control" value="{{ $user->full_name() }}">
                                                @endauth
                                                @guest
                                                    <input id="card-holder-name" type="text" class="form-control" value="">
                                                @endguest

                                            </div>
                                            <label for="">@lang('all.Credit card number')</label>
                                            <div id="new-card-form-wrapper" class="form-control-stripe"></div>
                                        </div>
                                    </div>
                                </div>
                                    {{--
                                @if($errors->any())
                                    <div class="alert alert-warning mt-3">
                                        <ul>
                                            @foreach($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <button type="submi">invia</button>
                                     --}}
                                    @include('front.components.safe-payment')
                                    <button type="submit" id="card-button" data-secret="{{ $client_secret_intent->client_secret }}"  class="btn btn-warning text-white btn-block btn-lg mt-3">@lang('all.Confirm your purchase of') @fp($lead->grand_total)</button>
                            </div>

                    </form>
                </div>
            </div>
            @else
                <div class="alert alert-secondary text-center">
                    @lang('all.cart is empty')
                </div>
            @endif
        </div>
    @endif

@hss('50')

@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>

    @if (App::getLocale() != 'en')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/localization/messages_{{App::getLocale()}}.min.js"></script>
    @endif

    <script src="https://maps.googleapis.com/maps/api/js?key={{ config('services.google_places.key') }}&libraries=places"></script>
    {{-- <script src="https://cdn.jsdelivr.net/npm/places.js@1.19.0"></script>  --}}

    <script src="https://js.stripe.com/v3/"></script>
    <script>
        var lead_id = {{ $lead->id }};
        var paymentStatus = false;
        var addressData = new Object();


        $.validator.addMethod(
            "deliverableAddress",
            function(value, element) {

                if(typeof(addressData.street_number) == 'undefined'){
                    return false;
                }

                return true; //true or false
            },
            "It looks like you didn't add a street number. Please check again."
        );



        $(() => {

            const input = document.getElementById("address-input");
            const autocomplete = new google.maps.places.Autocomplete(input);
            autocomplete.setComponentRestrictions({
                //country: ["us", "pr", "vi", "gu", "mp"],
                country: ["{{ strtolower($guest->country->iso_3166_2) }}"],
            });


            setTimeout(function(){
                $('#address-input').attr('autocomplete','my-custom-field-name')
            },200);


            autocomplete.addListener("place_changed", () => {
                    const place = autocomplete.getPlace();
                    addressData = new Object();
                    $.each(place.address_components, function(index,item){
                        var data = {
                            long_name : item.long_name,
                            short_name : item.short_name
                        }
                        addressData[item.types[0]] = data
                    });
                    addressData['formatted_address'] = place.formatted_address;


                    console.log(addressData);
                    $('input[type="hidden"][name="address_json"]').val(JSON.stringify(addressData));

                    //check for street number mandatory





                    ////////////

            });

            $('input[data-name]').blur(function(){
                $.post('{{ Route('setCartField') }}',{
                    _token:'{{ csrf_token() }}',
                    lead_id,
                    name:$(this).data('name'),
                    value:$(this).val()
                })
            })

            var cartConfirmationForm = $('#cart-confirmation-form');

            cartConfirmationForm.validate({
                // Specify validation rules
                rules: {
                        "shipping_address[name]": "required",
                        "shipping_address[surname]": "required",
                        "shipping_address[address]": {
                            required: true,
                            deliverableAddress: true
                        },
                        "shipping_address[telephone]": "required",
                        "shipping_address[email]": "required",
                        "shipping_address[email_confirmation]": {  equalTo: '[name="shipping_address[email]"]'},
                        "has_accepted_terms_and_conditions" : "required"
                        /*
                        password: {
                            required: true,
                            minlength: 5
                        }*/
                },
                messages: {

                },
                submitHandler: function(form) {
                    //alert('Daje');

                    formIsValid = true;


                    if(!paymentStatus){
                        payWithStripe();
                    }else{
                        form.submit();
                    }


                }
            });




            const stripe = Stripe('{{ env('STRIPE_KEY') }}');
            const elements = stripe.elements();
            const cardElement = elements.create('card');
            cardElement.mount('#new-card-form-wrapper');

            const cardHolderName = document.getElementById('card-holder-name');
            const cardHolderEmail = document.getElementById('card-holder-email');
            const cardButton = document.getElementById('card-button');
            const clientSecret = cardButton.dataset.secret;

            async function payWithStripe(){

                const clientSecret = '{{ $client_secret_intent->client_secret }}';

                const { setupIntent, error } = await stripe.confirmCardPayment(
                    clientSecret, {
                        payment_method: {
                            card: cardElement,
                            billing_details: {
                                name: cardHolderName.value,
                                email:  cardHolderEmail.value
                            }
                        }
                    }
                );

                /*

                const { setupIntent, error } = await stripe.confirmCardSetup(
                    clientSecret, {
                        payment_method: {
                            card: cardElement,
                            billing_details: { name: cardHolderName.value }
                        }
                    }
                );

                */

                if (error) {
                    console.log(error);
                    alert(error.message);
                    //alert('unsuccess')
                } else {
                    paymentStatus = true;
                    console.log(setupIntent);
                    //cartConfirmationForm.find('input[type="hidden"][name="payment_method"]').val(setupIntent.payment_method);
                    cartConfirmationForm.submit();

                }
            }

            /*
            cardButton.addEventListener('click', async (e) => {

                payWithStripe();

            });
            */


        })
    </script>

<style>
    label.error{
        color: red;
        font-size: 12px;
        margin-top: 5px;
    }
</style>

@endpush
