@extends('front.main')
@section('content')
  @hss('30')
  <div class="product-page">

    <div class="container">
      <div class="row">
        <div class="col-md-5">

          <div class="d-none d-md-block">
            <img class="img-fluid" src="{{Route('ir',['size' => 'h1280','filename' => $product->photos[1]])}}" alt="@altTagMl($product->photos[1])">
            @if (isset( $product->photos[2]) &&  $product->photos[2] != '')
                <img class="img-fluid" src="{{Route('ir',['size' => 'h1280','filename' => $product->photos[2]])}}" alt="@altTagMl($product->photos[2])">
            @endif
          </div>

          <div class="d-md-none row mb-4">
            <div class="col text-center">
                <img class="img-fluid" src="{{Route('ir',['size' => 'h1280','filename' => $product->photos[1]])}}" alt="@altTagMl($product->photos[1])">
            </div>
            @if (isset( $product->photos[2]) &&  $product->photos[2] != '')
                <div class="col text-center">
                    <img class="img-fluid" src="{{Route('ir',['size' => 'h1280','filename' => $product->photos[2]])}}" alt="@altTagMl($product->photos[2])">
                </div>
            @endif
          </div>

        </div>
        <div class="col-md-7">
          <div class="sticky-top" style="top: 100px;">
            <h1>{{$product->name}}</h1>

            @include('front.components.social-share')

            <div class="row">
                <div class="col-md-12">

                    <h2>
                        @if(!is_null($product->manufacturer))
                            @if($product->manufacturer->enabled)
                                <a class="manufacturer-link" href="{{$product->manufacturer->makeUrl()}}">{{$product->manufacturer->name}}</a>
                            @else
                                <a class="manufacturer-link" href="#">{{$product->manufacturer->name}}</a>
                            @endif
                        @endif
                         @if(!is_null($product->consortium))
                            @if($product->consortium->enabled)
                                <a class="manufacturer-link" href="{{$product->consortium->makeUrl()}}">{{$product->consortium->name}}</a>
                            @else
                                <a class="manufacturer-link" href="#">{{$product->consortium->name}}</a>
                            @endif
                        @endif
                    </h2>

                    <div class="">
                        <span class="product-format">{{$product->format_quantity}}{{$product->format_measurement_unit}}</span>
                    </div>
                    @hss('10')
                    @include('front.components.favorite-toggler',['item' => $product])

                     @if($product->hasPricesSet())
                        <div class="product-prices">
                            @if($product->getPrice($guest->country_id,'price_compare') > 0)
                                <span class="product-price_compare">@fp($product->getPrice($guest->country_id,'price_compare'))</span><br>
                            @endif
                            @if ($product->getPrice($guest->country_id,'price') > 0)
                                <span class="product-price">@fp($product->getPrice($guest->country_id,'price'))</span>

                                <form action="{{ Route('addToCart') }}" method="post" class="my-3">
                                    @csrf
                                    <input type="hidden" name="product_id" value="{{ $product->id }}">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="quantity-selector">
                                                <a href="javascript:" class="btn btn-secondary btn-lg">-</a>
                                                <input type="text" min="1" value="1" name="quantity">
                                                <a href="javascript:" class="btn btn-secondary btn-lg">+</a>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <button type="submit" class="btn btn-lg btn-block btn-warning">@lang('all.Add to cart')</button>
                                        </div>
                                    </div>
                                </form>
                            @endif
                        </div>
                    @else
                        @hss('15')
                    @endif
                </div>
            </div>
            @push('scripts')
                <script>
                    $(function(){
                        var minus = $('.quantity-selector').find('a.btn').eq(0);
                        var plus = $('.quantity-selector').find('a.btn').eq(1);
                        var quantityInput = $('.quantity-selector').find('input');
                        quantityInput.focus(function(){
                            $(this).blur();
                        })
                        var min = (typeof(quantityInput.attr('min')) != 'undefined') ? quantityInput.attr('min') : 1;
                        var max = (typeof(quantityInput.attr('max')) != 'undefined') ? quantityInput.attr('max') : 10;
                        var quantity = quantityInput.val();
                        minus.click(function(){
                            if(quantity == min){
                                return
                            }else{
                                quantity--;
                                quantityInput.val(quantity);
                            }
                        })
                        plus.click(function(){
                            if(quantity == max){
                                return
                            }else{
                                quantity++;
                                quantityInput.val(quantity);
                            }
                        })

                    })
                </script>
            @endpush
            <style>

                div.quantity-selector input{
                    display: inline-block;
                    width: 45px;
                    height: 45px;
                    border: none;
                    border-top: 1px solid lightgray;
                    border-bottom: 1px solid lightgray;
                    border-radius: 4px!important;
                    text-align: center;
                    padding: 0px;
                    margin-left: -5px;
                    background: none;
                }
                div.quantity-selector input:focus{
                    outline: none;
                }
                div.quantity-selector a.btn{
                    margin-top: -3px;
                    border-radius: 0px;
                }

                div.quantity-selector a.btn:first-child{

                }

                div.quantity-selector a.btn:last-child{
                    margin-left: -5px;
                }
            </style>
            <h2>@lang('all.Features')</h2>

            <table class="table table-striped">
                @foreach ($product->categories
                            /*->sortBy(function($category,$key){
                                if(is_object($category->parent)){
                                    return $category->parent->lft;
                                }

                            })*/
                            ->groupBy('parent_id') as $group
                        )
                    @if (is_object($group->first()->parent))
                        <tr>
                            <td>
                                <b>{{$group->first()->parent->name}}</b>
                            </td>
                            <td>
                                {{implode(', ',$group->pluck('name')->toArray())}}
                            </td>
                        </tr>
                    @endif
                @endforeach
            </table>
{{--
            @foreach ($product->categories->groupBy('parent_id')->chunk(2) as $groups)
              <div class="container">

                  <div class="row">
                      @php
                          $indexFromZero = 0;
                      @endphp
                @foreach($groups as $index =>  $group)
                  <div class="col-md-6 pr-0 @if($indexFromZero == 0) pl-0 @endif">
                    <div class="product-feature mb-3">
                        <div class="row ">


                    <div class="col-md-4 col-4">
                        <img class="img-fluid" src="/{{$group->first()->parent->first()->icon}}" alt="">
                    </div>

                      <div class="col-md-12 col-12">
                        <span class="feature-label">
                          {{$group->first()->parent->first()->name}}
                        </span>
                        <div class="feature-value">
                          {{implode(', ',$group->pluck('name')->toArray())}}
                        </div>
                      </div>
                      </div>
                    </div>
                  </div>
                  @php
                      $indexFromZero++;
                  @endphp
                @endforeach

              </div>
              </div>
            @endforeach
            --}}
            <h2>@lang('all.Description')</h2>
            <div class="best-readable text-justify">
              {!!$product->description!!}
            </div>
            @if($product->podcasts->count() > 0)
                <h3>Podcast</h3>
                @foreach ($product->podcasts as $podcast)
                    {!! $podcast->spotify_embeddable_code	 !!}
                @endforeach
            @endif
            @if(!is_null($product->manufacturer))
                @if($product->manufacturer->experiences->count() > 0)
                <h2>@lang('all.Experiences at') {{ $product->manufacturer->name }}</h2>
                <div class="best-readable ">
                    <p>
                    {!! $product->manufacturer->short_description !!}
                    </p>
                </div>
                @hss('20')

                    @foreach ($product->manufacturer->experiences->chunk(3) as $experiences)
                        <div class="row">
                            @foreach ($experiences as $index =>  $experience)
                                <div class="col-md-4 col-6 @if($index == 0) pl-0 @endif">

                                    @include('front.components.experience-result',['experience' => $experience])
                                </div>
                            @endforeach
                        </div>
                    @endforeach
                    <h2>
                        <a class="manufacturer-link d-block text-center" href="{{$product->manufacturer->makeUrl()}}#our_experiences">@lang('all.All experiences!')</a>
                    </h2>
                @endif
            @endif
            <hr>

            <h2>@lang('all.Recommended with')</h2>
            <div class="row">
              @foreach($product->recipes()->limit(6)->get() as $recipe)
                <div class="col-md-4">
                  @include('front.components.recipe-result')
                </div>
              @endforeach
            </div>
            @if ($product->isOnDeal())
                <h3>@lang('all.this product is currently on deal')</h3>
                <div class="row">
                    @foreach ($product->activeDeals as $deal)
                        @include('front.components.deal-result')
                    @endforeach
                </div>
            @else

            @endif
            <h2>@lang('all.reviews')</h2>
            @include('front.components.reviews-container',['reviews' => $product->reviews,'id' => $product->id,'reviewable_type' => $product->getModelNameFull()])
          </div>
        </div>
      </div>
      <div class="row">
        @if(!is_null($product->manufacturer))
            <div class="col-md-3 offset-md-9">
                <a href="{{ $product->manufacturer->makeUrl() }}#our_products" class="btn btn-outline-warning">
                    @lang('all.All the wines from') {{ $product->manufacturer->name }}
                </a>
            </div>
        @endif
      </div>
      @if(!is_null($product->getRecommendations())  && $product->getRecommendations()->count() > 0)
        <div class="row">
            <div class="col-md-9">
                <h2>@lang('all.You might also like...')</h2>
            </div>
        </div>
        <div class="row">
            @foreach($product->getRecommendations() as $product)
            <div class="col-md-4">
                @include('front.components.product-result')
            </div>
            @endforeach
        </div>
      @endif

    </div>

  </div>
  <style media="screen">
  body{
    background: #fff;
  }
</style>

@endsection
