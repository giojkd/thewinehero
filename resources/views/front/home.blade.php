@extends('front.main')


@section('content')
     <script  src="https://www.youtube.com/iframe_api"></script>



    {{-- @include('front.components.home.home-banner')  --}}

    @include('front.components.home.videos')
    @include('front.components.home.blog')
    @include('front.components.home.recipes')


    @if (isset($homepageDeals) && $homepageDeals->count() > 0)
        @hss('70')
        @include('front.components.home.deals')
    @endif
    @include('front.components.home.filippo-banner')

    {{-- @include('front.components.home.events') --}}
    @include('front.components.home.homepage-experts')
    @include('front.components.home.numbers')
    @include('front.components.home.we-ve-been-chosen-by')

  @endsection

  @push('scripts')
      <script>

            window.addEventListener('load', function () {

                $('.clients-logos').removeClass('d-none');
                $('.experts-carousel').removeClass('d-none');

                var glider = new Glider(document.querySelector('#manufacturers-carousel'), {
                    slidesToShow: 10,
                    draggable: true,
                    rewind: true
                })

                //.glider-slider-experts


                function sliderAuto(slider, miliseconds) {

                    slider.isLastSlide = function () {
                        var allPages = Math.ceil(slider.slides.length / slider.opt.slidesToShow);

                        return slider.slide >= allPages;
                    }

                    var slide = function () {
                        console.log(slider);
                        slider.slideTimeout = setTimeout(function () {
                            function slideTo() {
                                return slider.isLastSlide() ? 0 : slider.page + 1;
                            }
                            slider.scrollItem(slideTo(), true);
                        }, miliseconds);
                    }

                    slider.ele.addEventListener('glider-animated', function (event) {
                        window.clearInterval(slider.slideTimeout);
                        slide();
                    });

                    slide();

                }

                sliderAuto(glider, 5000)

            })

      </script>
  @endpush

