@extends('front.main')
@push('scripts')
    <script>
        var tz_default_time = '{{ date('Y-m-d',strtotime($event->starts_at_day)) }} {{ \Str::limit($event->starts_at_time,5,'') }}';
        $(function(){
            $('.select2').select2({
                theme: 'bootstrap4'
            });
        })
    </script>
    <script type="text/javascript" src="{{mix('/js/tz-converter.js')}}"></script>
@endpush
@section('content')
<div class="d-none d-md-block">
    @hss('50')
</div>

{{--

  @if (!$agent->isMobile())


  <div class="slick-carousel carousel-stripe">
      @foreach($event->images as $image)

          <img src="{{Route('ir',['size' => 'h320','filename' => $image])}}" alt="@altTagMl($image)">

      @endforeach
    </div>

    @endif

     --}}

    @hss('50')
  <div class="event">
    <div class="container border border-warning py-3">
      <div class="row">
        <div class="col-md-6">
          <img class="img-fluid" src="{{Route('ir',['size' => 'h480','filename' =>$event->images[0]])}}" alt="@altTagMl($event->images[0])" >
        </div>
        <div class="col-md-6">
              <div class="row mb-3">
                <div class="col-md-6">
                    <label for="">@lang('all.select timezone')</label>
                    <select name="" id="tz-select" class="select2"></select>
                </div>
                <div class="col-md-6">

                </div>
            </div>
          <div class="row">
            <div class="col-md-10">
              <h3 class="d-inline-block mb-3">
                <span class="badge badge-warning text-white">
                  <i class="far fa-calendar-alt"></i>
                  <span class="tz-converterd">
                  {{\Carbon\Carbon::createFromFormat('Y-m-d', $event->starts_at_day)->format('l jS \\of F Y')}} at {{\Str::limit($event->starts_at_time,5,'')}}
                  </span>
                </span>
              </h3>
            </div>
            <div class="col-md-2 text-right">
              @include('front.components.favorite-toggler',['item' => $event])
            </div>
          </div>


          <h1 class="text-left p-0 d-inline-block mb-3"> {{$event->name}}</h1>
          <div class="row mb-3">
            <div class="col-md-12">
              <p class="bg-white shadow rounded border d-inline-block px-4 py-2 border-dark"><i class="fas fa-hourglass-end"></i> @lang('all.Duration:') {{$event->duration}}m</p>
              <p class="bg-white shadow rounded border d-inline-block px-4 py-2 border-dark"><i class="fas fa-chart-line"></i> @lang('all.Complexity:') {{$event->complexity->name}}</p>
            </div>
          </div>

          <div class="bg-white shadow rounded border d-block px-4 mb-4 border-dark">
            <div class="row my-3 ">
                <div class="col">



                    <h2 class="mb-0">
                        @if ($event->price > 0)
                            {{money($event->applyPlanDiscount($event->getModelName(),$event->price),'EUR')}}
                        @else
                            @lang('all.event for free')
                        @endif
                    </h2>

                    @if(($event->price > 0 && $event->applyPlanDiscount($event->getModelName(),$event->price) != $event->price) ? $event->price : 0) <small><span class="text-muted">@lang('all.instead of') {{ money($event->price,'EUR') }}</span></small>@endif


                </div>
                <div class="col">
                    <a
                    href="{{ Route('buySingleContentCheckout',['model' => 'event','id' => $event->id]) }}"
                    class="btn btn-warning btn-lg text-white btn-block">
                    @if ($event->price > 0)
                        @lang('all.Buy it now!')
                    @else
                        @lang('all.enroll free event now')
                    @endif
                    </a>
                </div>
            </div>
            </div>


          <div class="row">
              <div class="col-md-12">
                  <p>{{$event->description_short}}</p>
              </div>
          </div>
          <div class="row">
              <div class="col-md-12">
                  @include('front.components.social-share')
              </div>
          </div>


          <!--
          <p>
            <a class="btn btn-outline-warning btn-sm btn-block" href="{{$event->enrollment_link}}">Convert the time in your timezone</a>
          </p>
        -->
        </div>
      </div>
    </div>
    @hss('50')



    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <div class="best-readable ">
              <h2>{{ $event->subtitle }}</h2>
              <hr>
            {!!$event->description!!}
            <hr>
            <!--<a class="btn btn-outline-warning btn-sm btn-block" href="{{$event->enrollment_link}}">Convert the time in your timezone</a>-->
          </div>

          @if(!is_null($event->custom_blocks))
            @foreach (json_decode($event->custom_blocks,1) as $block)
                <div class="best-readable ">
                    <h2>{{ $block['title'] }}</h2>
                        {!! $block['content'] !!}
                    <hr>
                </div>
            @endforeach
          @endif
                @include('front.components.reviews-container',['reviews' => $event->reviews,'id' => $event->id,'reviewable_type' => $event->getModelNameFull()])
        </div>
        <div class="col-md-4 text-center">
          @include('front.components.team-member-bio',['user' => $event->user])
        </div>
      </div>

    </div>

  </div>
      <div class="container">
        <div class="row">
            <div class="col text-center">
                <div class="purchase-content-box">
                <h2>@lang('all.How to enroll this event?')</h2>
                @if(!$event->is_premium)
                    @auth
                        @if(Auth::user()->hasActivePlan())
                            @if($canPurchase)

                                @lang('all.More enrollable events',['n' => Auth::user()->plan->events])

                                <br>

                                <div class="mt-2">
                                    <form method="POST" action="{{ Route('purchaseContent',['model' => 'event','id' => $event->id]) }}">
                                        @csrf
                                        <button type="submit" class="btn btn-warning btn-large text-white btn-lg">@lang('all.Enroll now!')</button>
                                    </form>
                                </div>
                            @else
                                <div class="alert alert-danger">

                                </div>
                                @include('front.components.enrollment-options',['content' => $event])
                            @endif
                        @else
                            @include('front.components.enrollment-options',['content' => $event])
                        @endif
                    @endauth
                    @guest
                        @include('front.components.enrollment-options',['content' => $event])
                    @endguest
                @endif
                @include('front.components.buy-single-content',
            [
              'content' => [
                  'name' => $event->getModelName(),
                  'price_compare' => ($event->applyPlanDiscount($event->getModelName(),$event->price) != $event->price) ? $event->price : 0,
                  'price' => $event->applyPlanDiscount($event->getModelName(),$event->price),
                  'id' => $event->id,
                  'is_premium' => $event->is_premium
                  ]
            ]
        )

            </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(function(){
            $('.slick-carousel').slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                autoplay: true,
                infinite: true,
                centerMode: true,
                variableWidth: true,
                dots: true,
                autoplaySpeed: 2000,
                responsive: [
                    {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                    },
                    {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                    },
                    {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            });
        })
    </script>
@endpush
