@extends('front.main')
@hss('10')
@section('content')
  <div class="container">
    @foreach ($trainings->chunk(4) as $chunks)
      <div class="row">
        <div class="col-md-12">
          <h1 class="page-title">Our classes</h1>
        </div>
      </div>
      <div class="row">
        @foreach ($chunks as $key => $training)
            <div class="col-md-4">
              @include('front.components.training-result')
            </div>
        @endforeach
      </div>
    @endforeach
  </div>
@endsection
