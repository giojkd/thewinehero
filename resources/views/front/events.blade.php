@extends('front.main')
@section('content')
  <div class="events">
    <h1 class="page-title">Classes & Events</h1>
    @foreach($dates as $day => $events)
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">
            <span class="sticky-index sticky-top">
              {{\Carbon\Carbon::createFromFormat('Y-m-d', $day)->format('l jS \\of F Y')}}
            </span>
          </div>
          <div class="col-md-9">
            @foreach($events->chunk(2) as $eventsChunk)
            <div class="row">
                @foreach ($eventsChunk as $event)
                    <div class="col-md-10">
                        @include('front.components.event-result-wide')
                    </div>
                @endforeach
            </div>
            @endforeach
          </div>
        </div>
      </div>
      @hss('90')

    @endforeach
  </div>
@endsection
