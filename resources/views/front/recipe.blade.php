@extends('front.main')

@section('content')


  <div class="new-curry overview">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-12">
          <div class="overview-inner">

            <nav class="navbar navbar-expand-md navbar-dark">
              <button class="navbar-toggler" type="button" data-toggle="collapse"
              data-target="#collapsibleNavbar">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="collapsibleNavbar">
              <ul class="navbar-nav">
                <li class="nav-item active">
                  <a class="nav-link" href="#overview" toggleActiveLink scrollOffset="-75">@lang('all.Overview')</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#ingredients" toggleActiveLink  scrollOffset="-75">@lang('all.Ingredients')</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#preparation" toggleActiveLink  scrollOffset="-75">@lang('all.Preparation')</a>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="#winepairing" toggleActiveLink  scrollOffset="-75">@lang('all.Wine Pairing')</a>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="#reviews" toggleActiveLink  scrollOffset="-75">@lang('all.Reviews')</a>
                </li>

                <li class="nav-item" style="padding-top: 5px;">
                  @include('front.components.favorite-toggler',['item' => $recipe])
                </li>
                <!--
                <li class="nav-item">
                <a class="nav-link" href="#">Enrollment options</a>
              </li>
            -->
          </ul>
        </div>
      </nav>

    </div>
  </div>
</div>
</div>

</div>

<div class="d-none d-md-block">
    @hss('70')
</div>

<div id="overview">
  <section class="curry">
    <div class="container">
      <div class="row">

         <div class="col-12 col-md-6 remove-padding d-block d-md-none">
          <div class="curry-right">
            <img src="{{Route('ir',['size' => 800,'filename'=>$recipe->photos[0]])}}" alt="@altTagMl($recipe->photos[0])">
            @if($recipe->videos && $recipe->videos->count() > 0)
                @foreach ($recipe->videos as $video)
                    <a class="curry-play-btn" href="{{  url($video->makeUrl()) }}" scrolloffset="-75">
                        <img class="plbtn" src="@img('play-btn.png')">
                    </a>
                @endforeach
            @endif
            @hss('25')
          </div>

          @if($recipe->videos && $recipe->videos->count() > 0)
                <div class="text-center py-3">
                    @foreach ($recipe->videos as $video)
                    <a target="_blank" href="{{ url($video->makeUrl()) }}" class="btn btn-warning text-white btn-lg"><i class="fab fa-youtube"></i> @lang('all.play video recipe')</a>
                    @endforeach
                </div>
            @endif
        </div>


        <div class="col-12 col-md-6 remove-padding">
          <div class="curry-left">
            <h1>{{$recipe->name}}</h1>
            <h2>{{$recipe->subtitle}} @include('front.components.favorite-toggler',['item' => $recipe])</h2>

            <ul class="review-score-stars">
              @for($i = 0; $i < (int)$recipe->reviews_avg; $i++)
                <li>
                  <i class="fas fa-star"></i>
                </li>
              @endfor
              @for( $i = (int)$recipe->reviews_avg; $i < 5;  $i++)
                <li>
                  <i class="far fa-star"></i>
                </li>
              @endfor
              ( @lang('all.Scored x on n reviews',['x' => $recipe->reviews_avg, 'n' => $recipe->reviews->count()]) )
            </ul>

            @if ($recipe->reviews->count() > 0)


                <div class="delicious">
                    <div class="delicious-inner row">
                        <div class="col-4 text-right">
                        <p class="new-color">{{$recipe->reviews->first()->user->name}}:</p>
                        </div>
                        <div class="col-8">
                        <p>{{Str::limit($recipe->reviews->first()->description,150,'...')}}</p>
                        <p><a href="#reviews">@lang('all.read all reviews...')</a></p>
                        </div>
                        {{--
                        <div class="col">
                        <p><img src="@img('new-play-btn.png')"></p>
                        </div>
                        --}}
                    </div>
                </div>
            @endif
          </div>
          @include('front.components.social-share')
        </div>



        <div class="col-12 col-md-6 remove-padding d-none d-md-block">
          <div class="curry-right">
            <img src="{{Route('ir',['size' => 800,'filename'=>$recipe->photos[0]])}}" alt="@altTagMl($recipe->photos[0])">

            <a class="curry-play-btn" href="#preparation" scrolloffset="-75">
             {{--  <img src="@img('play-btn.png')">  --}}
            </a>
          </div>

          @if($recipe->videos && $recipe->videos->count() > 0)
                <div class="text-center py-3">
                    @foreach ($recipe->videos as $video)
                    <a target="_blank" href="{{ url($video->makeUrl()) }}" class="btn btn-warning text-white btn-lg"><i class="fab fa-youtube"></i> @lang('all.play video recipe')</a>
                    @endforeach
                </div>
            @endif
        </div>


      </div>
      @hss('50')
      <div class="row">
        <div class="col-md-12">

          <div class="best-readable recipe-description style-first-letter">
            {!!$recipe->description!!}
          </div>
        </div>
      </div>
    </div>
  </section>
</div>


<section class="calander">
  <div class="container">
    <ul class="row bg-white features-list list-unstyled">
        <li class="col-6 col-md-3 text-center py-3 border-right d-block">
                <i class="fa fa-hourglass-2 fa-3x"></i>
                <h3>{{$recipe->preparation_time}} @lang('all.minutes to prepare')</h3>
                <h3>{{$recipe->cooking_time}} @lang('all.minutes to cook')</h3>
                @if(!is_null($recipe->preparation_time_notes))
                    {{$recipe->preparation_time_notes}}
                @endif
        </li>
        @if(isset($recipe->ingredients))
        <li class="col-6 col-md-3 text-center py-3 border-right d-block">
                <i class="fa fa-list fa-3x"></i><br>
                <b></b>
                <h3>{{$recipe->ingredients->count()}} @lang('all.ingredients')</h3>
        </li>
        @endif
        <li class="col-6 col-md-3 text-center py-3 border-right d-block">
                <i class="fa fa-running fa-3x"></i><br>
                <h3>{{$recipe->calories}} kCal</h3>
        </li>
        <div class="col-6 col-md-3 text-center py-3 border-right d-block">
                <i class="fa fa-list-ol fa-3x"></i><br>
                <h3>{{$recipe->steps->count()}} @lang('all.steps to complete the preparation')</h3>

        </li>

    </ul>
    {{--
    <div class="button enroll">
        <a href="#">Cooked 212k times</a>
      </div>
       --}}
    </div>
</section>


<section class="ingredients-section" id="ingredients">
    @if($recipe->podcasts->count() > 0)
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <div class="mb-4">
                        <div class="ingredients-section-inner">
                        <h3>Podcast</h3>
                        </div>
                        @foreach ($recipe->podcasts as $podcast)
                            {!! $podcast->spotify_embeddable_code	 !!}
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        @hss('70')
    @endif
  <div class="container">
    <div class="row">

      <div class="col-12 col-md-12">
        <div class="ingredients-section-inner">
          <h2><img src="@img('ingrediant.png')"> @lang('all.Ingredients')</h2>
           @foreach ($recipe->videos as $video)
                <a target="_blank" href="{{ url($video->makeUrl()) }}" class="btn btn-warning text-white btn-lg">
                    <i class="fab fa-youtube"></i> @lang('all.play video recipe')
                </a>
            @endforeach

          @lang('all.for n servings',['n' => $recipe->servings])
          @if(!is_null($recipe->servings_notes) && $recipe->servings_notes != '')
            <h3>{{$recipe->servings_notes}}</h3>
          @endif
          <!--<p>Check the ingredients you have and thos you still need to buy as a shopping list and send it to your email or sms</p>-->
        </div>
      </div>
    </div>
  </div>

  <div class="ingredients">
    <div class="container">
      <div class="row">
        <div class="col-12 col-md-6">
          <div class="ingredients-section-inner-left">
            @if(is_null($recipe->ingredientslists))
            <ul>
              @foreach($recipe->recipeingredients as $ingredientIndex =>  $ingredient)
                <li>
                    {{--
                    <!--<img src="@img('circle.png')">-->
                    <!--<img src="@img('check.jpg')">-->
                     --}}
                    <span class="new-color">{{$ingredient->ingredient->name}}</span>
                    {{$ingredient->quantity}} @lang('all.'.$ingredient->measurement_unit)

                    @if(!is_null($ingredient->notes))
                        <span class="new-size">({{$ingredient->notes}})</span>
                    @endif
                </li>
              @endforeach
            </ul>
            @else
                @foreach($recipe->ingredientslists as $list)
                    <h3 class="mb-4">{{ $list->name }}</h3>
                    <ul class="pl-0">
                        @foreach($list->recipeingredients as $ingredientIndex =>  $ingredient)
                            <li>
                                {{--
                                <!--<img src="@img('circle.png')">-->
                                <!--<img src="@img('check.jpg')">-->
                                 --}}
                                <span class="new-color">{{$ingredient->ingredient->name}}</span>
                                {{$ingredient->quantity}} @lang('all.'.$ingredient->measurement_unit)

                                @if(!is_null($ingredient->notes))
                                    <span class="new-size">({{$ingredient->notes}})</span>
                                @endif
                            </li>
                        @endforeach
                    </ul>
                @endforeach
            @endif
          </div>

          <!--
          <div class="ingredients-btn">
          <a href="#"><img src="images/add-cart.png"> Add the ingredients to your shopping list</a>
        </div>
      -->
    </div>
    <div class="offset-md-1"></div>

    <div class="col-12 col-md-5 d-none d-md-block">
        <div class="rounded bg-white p-4 shadow">
      <div class="ingredients-section-inner-right">
        <div class="ingredients-section-inner-left-inner">
          <div class="ingredients-section-inner-left">
            <p class="h1 text-warning">@lang('all.Perfect Wine Match')</p>
            <p></p>
            <p><a href="#winepairing" scrollOffset="-75">@lang('all.more pairings...')</a></p>
          </div>
          <div class="ingredients-section-inner-right text-center">
            <img style="width: 60px;" src="@img('flippo.png')" alt="Filippo Bartolotta">
            <p class="new-color">Filippo Bartolotta</p>
          </div>
        </div>
        <section class=" slider" id="filippo-slider">
          <div class="repeater">
            <div class="slippo-slider">
              <div class="Filippo Bartolotta-inner">
                @include('front.components.product-result',['product' => $recipe->products->first()])
              </div>
            </div>
          </div>
        </section>
      </div>
      </div>

    </div>
  </div>
</div>
</div>
</section>
@if (isset($banners['recipe_mid_banner']))
    <div class="container">
        <div class="row"><div class="col-md-12">

                    {!! $banners['recipe_mid_banner']->content !!}

            </div>
        </div>

    </div>
@endif
<div id="preparation">
  <section class="ingredients-section">
    <div class="container">
      <div class="row">

        <div class="col-12 col-md-12">
          <div class="ingredients-section-inner">
            <h2><img src="@img('ingrediant.png')">
              @lang('all.Preparation')
              {{--
              <!--
              <span class="new-design">
              <img src="@img('full-screen.png')"> Go Full Screen
            </span>
          -->
           --}}
        </h2>
         @foreach ($recipe->videos as $video)
                    <a target="_blank" href="{{ url($video->makeUrl()) }}" class="btn btn-warning text-white btn-lg"><i class="fab fa-youtube"></i> @lang('all.play video recipe')</a>
                    @endforeach
        <p>
            @lang('all.for n servings',['n' => $recipe->servings])
        </p>
        @if(!is_null($recipe->servings_notes))
          <h3>{{$recipe->servings_notes}}</h3>
        @endif
        <!--<p>Check the ingredients you have and thos you still need to buy as a shopping list and send it to your email or sms</p>-->
      </div>
    </div>
  </div>
</div>
</section>



@foreach($recipe->steps as $index => $step)
  <section class="curry new-border">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12 col-md-2">
          <div class="step">
            <p>@lang('all.Step')</p>
            <p><b>{{$index+1}}</b></p>
            <p class="new-color">@lang('all.Of') {{$recipe->steps->count()}}</p>
          </div>
        </div>

        <div class="col-12 col-md-9">
          <div class="transfer">
            <div class="transfer-inner-left">
              <h3>{{$step->name}}</h3>
              <div class="more-text">
                {!! $step->description !!}
              </div>
              @isset($step->photos[0])


                @foreach($step->photos as $photo)
                  <div class="recipe-step-photo">
                    <img src="{{Route('ir',['size' => 'h240','filename' => $photo])}}" alt="@altTagMl($photo)">
                  </div>
                @endforeach


              @endif
            </div>


          </div>
          <!-- <p class="btn"><a href="#">Done</a></p> -->
        </div>
        <div class="col-12 col-md-2">
        </div>
      </div>
    </div>
  </section>
@endforeach
</div>


@if($recipe->general_notes != '')
<div id="general_notes">
    <section class="ingredients-section">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-12">
                <div class="ingredients-section-inner">
                    <h3>
                        @lang('all.General notes')
                        {{--   <!-- <span class="new-design"> <img src="@img('full-screen.png')"> Go Full Screen </span> -->  --}}
                    </h3>
                    {{-- <!--<p>Check the ingredients you have and thos you still need to buy as a shopping list and send it to your email or sms</p>-->  --}}
                </div>
            </div>
        </div>
    </div>
    </section>
</div>
@hss('50')
<div class="container">
    <div class="row">
        <div class="col-md-12">
          <div class="best-readable recipe-description style-first-letter">
            <p>{{ $recipe->general_notes }}</p>
          </div>
        </div>
      </div>
</div>
@endif

<div class="container d-block d-md-none">
    <div class="row">
        <div class="col-12 col-md-5">
        <div class="rounded bg-white p-4 shadow">
      <div class="ingredients-section-inner-right">
        <div class="ingredients-section-inner-left-inner">
          <div class="ingredients-section-inner-left">
            <p class="h1 text-warning">@lang('all.Perfect Wine Match')</p>
            <p></p>
            <p><a href="#winepairing" scrollOffset="-75">@lang('all.more pairings...')</a></p>
          </div>
          <div class="ingredients-section-inner-right text-center">
            <img style="width: 60px;" src="@img('flippo.png')" alt="Filippo Bartolotta">
            <p class="new-color">Filippo Bartolotta</p>
          </div>
        </div>
        <section class=" slider" id="filippo-slider">
          <div class="repeater">
            <div class="slippo-slider">
              <div class="Filippo Bartolotta-inner">
                @include('front.components.product-result',['product' => $recipe->products->first()])
              </div>
            </div>
          </div>
        </section>
      </div>
      </div>

    </div>
    </div>
</div>

<div id="winepairing">


  <section class="ingredients-section">
    <div class="container">
      <div class="row">

        <div class="col-12 col-md-6">
          <div class="ingredients-section-inner">
            <h2><img src="@img('cup.png')" style="height: 60px; width: auto;">
              @lang('all.Pairing')
              <!--
              <span class="new-design">
              <img src="@img('full-screen.png')"> Go Full Screen
            </span>
          -->
        </h2>
      </div>
    </div>
    <div class="col-12 col-md-6">

    </div>
  </div>
</div>
</section>



<section class="wine-text">
  <div class="row paired-wines d-none d-md-flex justify-content-center">
    @foreach($recipe->products as $product)
    <div class="col-3">
      @include('front.components.product-result')
    </div>
    @endforeach

  </div>
   <div class="paired-wines d-md-none">
       <div class="row">


            @foreach($recipe->products as $product)
                <div class="col-6">
                    @include('front.components.product-result')
                </div>
            @endforeach
        </div>
  </div>
</section>
</div>




@if(!is_null($recipe->getRecommendations()))
  <div id="recommendations">
    <section class="ingredients-section">
      <div class="container">
        <div class="row">
          <div class="col-12 col-md-6">
            <div class="ingredients-section-inner">
              <h2 class="h1 text-warning">
                @lang('all.You may like')
              </h2>
            </div>
          </div>
        </section>
        <div class="container">
          <div class="row">
            @foreach($recipe->getRecommendations() as $item)
              <div class="col-12 col-md-3">
                @include('front.components.recipe-result',['recipe' => $item])
              </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
@endif

<div id="reviews">

    <div class="container">
      <div class="row">
        <div class="col-12 col-md-6">
          <div class="ingredients-section-inner">
            <h2 class="h1 text-warning">
              @lang('all.Reviews')
            </h2>
          </div>
        </div>
        </div>

      <div class="container">
        <div class="row">
          <div class="col-12 col-md-12">
            @include('front.components.reviews-container',['reviews' => $recipe->reviews,'id' => $recipe->id,'reviewable_type' => $recipe->getModelNameFull()])
          </div>
        </div>
      </div>

    </div>
  </div>
</div>

<!--
@include('front.components.enrollment')
-->
@endsection
