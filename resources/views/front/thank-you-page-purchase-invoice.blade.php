@extends('front.main')


@section('content')
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <div class="mt-4">
                        <h1 class="text-warning">@lang('all.Thank you!')</h1>
                        <p>
                            @lang('all.Thank you for your invoice request') <br>
                            {{--
                            <a href="{{ Route('user') }}" class="btn btn-warning text-white">@lang('all.Go now!')</a>
                             --}}
                        </p>
                    </div>
                </div>
            </div>


        </div>
@endsection
