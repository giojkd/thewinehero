@extends('front.main')
@section('content')

  <div class="course-cover background-cover justify-content-center d-flex" id="overview">
    <div class="">
      <img class="d-flex img-fluid background-cover-img" src="{{Route('ir',['size' => '2560','filename' => $training->photos[0]])}}" alt="@altTagMl($training->photos[0])">
    </div>
    <div class="course-cover-content-wrapper">
      <div class="d-flex h-100 w-100 justify-content-center">
        <div class="d-flex align-self-center">
          <div class="text-center">
            <h1>{{$training->name}}</h1>
            <span class="text-white">@lang('all.with')</span><br>
            <div class="course-cover-avatar my-2">
              <img src="{{Route('ir',['size' => '120','filename' => $training->user->avatar])}}" alt="{{ $training->user->full_name }}">
            </div>
            <h2 class="text-white">{{$training->user->name}} {{$training->user->surname}}</h2>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="training-section" id="syllabus">
    <h1>@lang('all.Your training')</h1>
    <div id="syllabus" class="container-fluid">
      @foreach($training->levels as $indexLevel =>  $level)

        @include('front.components.course-card-enrolled')

      @endforeach
    </div>
  </div>


@endsection

@push('scripts')
  <script type="text/javascript">
  $(function(){

  })
</script>
@endpush
