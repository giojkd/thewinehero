@extends('front.main')
@section('content')
    <div class="user-area-logged ">


        @hss('50')
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <a class="d-inline-block d-md-none btn btn-outline-warning" href="{{ Route('logout') }}"><i class="fas fa-sign-out-alt"></i> @lang('all.exit')</a>
                    <h1 class="page-title text-left pl-0 mb-0 pb-0">
                        Ciao {{ Auth::user()->name }}!
                        <small><a class="d-none d-md-inline-block btn btn-outline-warning btn-lg" href="{{ Route('logout') }}"><i class="fas fa-sign-out-alt"></i> @lang('all.exit')</a></small>
                    </h1>
                    <div class="best-readable">
                        <p>@lang('all.Find here collected your favourites in our site, articles, recipes, videos, all
                            collected and organized in one place!')</p>
                    </div>
                </div>

                <div class="col-md-3 text-right">

                </div>

            </div>
        </div>
        @hss('40')

        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <ul class="nav nav-pills text-center">

                        <li class="nav-item text-center">
                            <a data-toggle="tab" class="nav-link active" href="javascript:" data-target="#favourites">
                                <h3 class="text-warning"><i class="fas fa-heart"></i> @lang('all.Your favourites')</h3>
                            </a>
                        </li>
                        {{--
                        <li class="nav-item">
                            <a data-toggle="tab" class="nav-link" href="javascript:" data-target="#trainings">
                                <h3 class="text-warning"><i class="fas fa-graduation-cap"></i> Your courses</h3>
                            </a>
                        </li>
                        --}}
                        <li class="nav-item">
                            <a data-toggle="tab" class="nav-link" href="javascript:" data-target="#purchases">
                                <h3 class="text-warning"><i class="fa fa-bank"></i> @lang('all.Your purchases')</h3>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a data-toggle="tab" class="nav-link" href="javascript:" data-target="#plans">
                                <h3 class="text-warning"><i class="fa fa-sync"></i> @lang('all.Your plan')</h3>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a data-toggle="tab" class="nav-link" href="javascript:" data-target="#yourdata">
                                <h3 class="text-warning"><i class="fa fa-user"></i> @lang('all.Your data')</h3>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ Route('iMama') }}" >
                                <h3 class="text-warning"><i class="fa fa-user"></i> @lang('all.aimama')</h3>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        @hss('40')
        <div class="tab-content">

            @include('front.components.user.trainings_tab')

            @include('front.components.user.favourites_tab')

            @include('front.components.user.plans_tab')

            @include('front.components.user.purchases_tab')

            @include('front.components.user.your_data_tab')

        </div>
    </div>
@endsection
