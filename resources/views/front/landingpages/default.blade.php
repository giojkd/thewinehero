@extends('front.main')

@section('content')

<div class="container ">

    <div class="row">
        <div class="col-md-12">
            @if (count($lp->images) > 0)
                <img class="img-fluid w-100" src="{{ url(collect($lp->images)->first()) }}" alt="">
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">

            <h1 class="page-title text-left pl-0">{{ $lp->name }}</h1>
            <p>{{ $lp->short_description }}</p>
            <p class="best-readable">{!! $lp->description !!}</p>
        </div>
        <div class="col-md-6">
             <div>
                 @hss('20')
                {!! $lp->embeddable_code !!}
            </div>
        </div>
    </div>
</div>

@endsection
