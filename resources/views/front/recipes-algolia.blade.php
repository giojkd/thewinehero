
@extends('front.main')
@section('content')
  @hss('30')

{{--
    <section class="page-banner bg_cover" style="">
        <div class="container">
            <div class="page-banner-content text-center">
                <h2 class="title">Shop</h2>
                <ol class="breadcrumb justify-content-center">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                </ol>
            </div>
        </div>
    </section>
     --}}

    <!--====== Shop Page Start ======-->


    <section class="shop-page">
        <div class="container-fluid shop-container">
            <div class="row flex-md-row justify-content-between">


                <div class="col-md-3 @if(!$isMobile)  @else p-0 @endif" style="overflow: scroll">


                    <div class="d-block d-md-none px-3">
                        <a style="" href="javascript:" onclick="$('#filtersBox').toggleClass('d-none')" class="w-100 btn btn-warning text-white mb-3">@lang('all.show filters')</a>
                        @if (!$isMobile)
                            @hss('200')
                        @endif
                    </div>
                    <div class="d-none d-md-block shop-sidebar @if ($isMobile) p-3 @endif" id="filtersBox">
                        <span class="h4 d-inline-block my-3 sidebar-title my-2">@lang('all.search your favorite dish')</span>
                        <div id="searchbox"></div>

                        @hss('40')

                        <div class="shop-sidebar-search mt-50 d-none">
                            <span class="h4 d-inline-block my-3 sidebar-title my-2">@lang('all.search here')</span>
                            <div id="searchbox"></div>
                            <div id="clear-refinements"></div>
                        </div>


                        <div class="shop-sidebar-color mt-25">
                            <span class="h4 d-inline-block sidebar-title my-2 text-capitalize">@lang('all.ingredients')</span>
                            <div id="ingredients-refinement"></div>
                        </div>

                        <div class="shop-sidebar-color mt-25">
                            <span class="h4 d-inline-block sidebar-title my-2 text-capitalize">@lang('all.dishcourse')</span>
                            <div id="dishcourse-refinement"></div>
                        </div>


                        <div class="shop-sidebar-color mt-25">
                            <span class="h4 d-inline-block sidebar-title my-2 text-capitalize">@lang('all.cuisine')</span>
                            <div id="cuisine-refinement"></div>
                        </div>

                        <div class="shop-sidebar-color mt-25">
                            <span class="h4 d-inline-block sidebar-title my-2 text-capitalize">@lang('all.complexity')</span>
                            <div id="complexity-refinement"></div>
                        </div>

                         <div class="shop-sidebar-color mt-25">
                            <span class="h4 d-inline-block sidebar-title my-2 text-capitalize">@lang('all.preparation time') (m)</span>
                            <div id="preparation-time-refinement"></div>
                        </div>



                        <div class="shop-sidebar-color mt-25">
                            <span class="h4 d-inline-block sidebar-title my-2 text-capitalize">@lang('all.calories')</span>
                            <div id="calories-range"></div>
                        </div>




                    </div>
                </div>
                <div class="col-md-9">

                    <div class="row mb-3">
                        <div class="col-md-7">
                            <div id="current-refinements"></div>
                        </div>
                        <div class="col-md-5 ">
                            <div id="sort-by" class="text-right"></div>
                        </div>
                    </div>
                    <div class="" style="overflow: scroll" id="productsWrapper">
                        <div class="shop-product" id="hits">

                        </div>
                        @hss('70')
                        <div id="paginator"></div>
                        @hss('70')
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--====== Shop Page Ends ======-->
@endsection

@push('scripts')

    <script>
    var ALGOLIA_INSIGHTS_SRC = "https://cdn.jsdelivr.net/npm/search-insights@1.8.0";

    !function(e,a,t,n,s,i,c){e.AlgoliaAnalyticsObject=s,e[s]=e[s]||function(){
    (e[s].queue=e[s].queue||[]).push(arguments)},i=a.createElement(t),c=a.getElementsByTagName(t)[0],
    i.async=1,i.src=n,c.parentNode.insertBefore(i,c)
    }(window,document,"script",ALGOLIA_INSIGHTS_SRC,"aa");


          aa('init', {
                appId: '{{ env('ALGOLIA_APP_ID')  }}',
                apiKey: '{{ env('ALGOLIA_SECRET') }}',
            });
    </script>






    <script src="https://cdn.jsdelivr.net/npm/algoliasearch@4.5.1/dist/algoliasearch-lite.umd.js" integrity="sha256-EXPXz4W6pQgfYY3yTpnDa3OH8/EPn16ciVsPQ/ypsjk=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/instantsearch.js@4.8.3/dist/instantsearch.production.min.js" integrity="sha256-LAGhRRdtVoD6RLo2qDQsU2mp+XVSciKRC8XPOBWmofM=" crossorigin="anonymous"></script>


    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/instantsearch.css@7.3.1/themes/algolia-min.css" integrity="sha256-HB49n/BZjuqiCtQQf49OdZn63XuKFaxcIHWf0HNKte8=" crossorigin="anonymous">

    <script>

//        var indexName = 'products{{ (env('APP_ENV') == 'local') ?  '_local'  : '' }}';

        var indexName = 'recipes{{ (env('APP_ENV') == 'local') ?  '_local'  : '' }}';
        //var indexName = 'recipes';

        var country_id = {{ $guest->country_id }};

        var labels = {
            clean_refinements : '@lang('all.clean refinements')',
            on_deal : '@lang('all.on deal')',
            minutes: '@lang('all.minutes')',
            ingredients: '@lang('all.ingredients')',
            view_recipe: '@lang('all.view recipe')'
        }
        var guest_id = {{ $guest->id }};

        var cloudimage = '';

        $.fn.isInViewport = function() {
            if($(this).length > 0){
                var elementTop = $(this).offset().top - 200;
                var elementBottom = elementTop + $(this).outerHeight();
                var viewportTop = $(window).scrollTop();
                var viewportBottom = viewportTop + $(window).height();
                return elementBottom > viewportTop && elementTop < viewportBottom;
            }
        };

        $(function(){
            $('footer,.newsletter-area-2').hide();
        })

        $('#productsWrapper').on('resize scroll', function() {

            /*
            if($(window).scrollTop() > 500){
                $('.scrollTopBtn').fadeIn()
            }else{
                $('.scrollTopBtn').fadeOut()
            }
            */

            if($('.ais-InfiniteHits-loadMore').isInViewport()){
                $('.ais-InfiniteHits-loadMore').trigger('click');
            }

        });

        const searchClient = algoliasearch('{{ env('ALGOLIA_APP_ID') }}', '{{ env('ALGOLIA_SECRET') }}');
        var locale = '{{ $locale }}';



        @verbatim

         var showMoreText = `
                        {{#isShowingMore}}
                            Show less
                        {{/isShowingMore}}
                        {{^isShowingMore}}
                            Show more
                        {{/isShowingMore}}
                    `;

        $('.header-search').removeClass('d-flex').hide();

        const insightsMiddleware = instantsearch.middlewares.createInsightsMiddleware({
            insightsClient: window.aa,
        });

        const search = instantsearch({

            indexName: indexName,
            searchClient,
            routing: true
        })

        search.use(
            instantsearch.middlewares.createInsightsMiddleware({
                insightsClient: aa
            })
        );

        aa('setUserToken',guest_id);

        search.addWidgets([
            /*
            instantsearch.widgets.configure({
                //hitsPerPage: 8,
                //enablePersonalization: true,
                filters: "type:simple"
            }),
            */
            instantsearch.widgets.searchBox({
                container: '#searchbox',
            }),

            instantsearch.widgets.hits({
                container: '#hits',
                templates:{
                    empty: 'No results for <q>{{ query }}</q>',
                    item: (hit, bindEvent) => { return `
                        <div class="product-item">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="product-img-wrapper">
                                        <a ${bindEvent('click',hit,'ProductClicked')} class="link" href="${ hit.url }">
                                            <img style="" class="product-img w-100 rounded" src="${ hit.cover }" alt="product">
                                        </a>
                                    </div>
                                    <h5 class="title mt-3">
                                        <b><a ${bindEvent('click',hit,'RecipeClicked')} href="${ hit.url }">${ hit.name } </a></b>
                                    </h5>
                                    <div class="features">${ hit.features }</div>
                                    <p class="description mb-3">${ hit.description }</p>
                                    <a href="${ hit.url }" target="_blank" class="btn btn-warning text-white  btn-sm btn-block">${labels.view_recipe}</a>

                                </div>
                            </div>
                        </div>` },

                },
                 transformItems(items) {

                    var itemsReturn = items.map(function(item){

                        var returnItem = {
                            name: item['name_'+locale],

                            url: item['url_'+locale],


                            cover: cloudimage+item.cover,


                            features: item['complexity_'+locale]+' recipe',
                            description: item['preparation_time']+' '+labels.minutes+' '+item['ingredients_'+locale].length+' '+labels.ingredients+' '+item['calories']+' kCal',

                        };



                        return returnItem;

                    })




                    return itemsReturn;
                },
            }),

            instantsearch.widgets.sortBy({
                container: '#sort-by',
                items: [
                    { label: 'Più rilevanti', value: 'recipes' },
                    /*{ label: 'Prezzo (prima meno costosi)', value: 'harrisproductgroups_price_asc' },
                    { label: 'Prezzo (prima più costosi)', value: 'harrisproductgroups_price_desc' },*/
                ],
            }),

            instantsearch.widgets.clearRefinements({
                container: '#clear-refinements',
                cssClasses:{
                    button: ['orange-btn']
                },
                templates:{
                    resetLabel() {
                        return labels.clean_refinements;
                    },
                }
            }),



            instantsearch.widgets.refinementList({
                container: '#dishcourse-refinement',
                attribute: 'dishcourse_'+locale,
                limit: 10,
                showMore: true,
                templates:{
                    showMoreText: showMoreText
                },
                searchable: true
            }),


            instantsearch.widgets.refinementList({
                container: '#ingredients-refinement',
                attribute: 'ingredients_'+locale,
                limit: 10,
                showMore: true,
                templates:{
                    showMoreText: showMoreText
                },
                searchable: true
            }),


            instantsearch.widgets.refinementList({
                container: '#cuisine-refinement',
                attribute: 'cuisine_'+locale,
                limit: 10,
                showMore: true,
                templates:{
                    showMoreText: showMoreText
                },
                //searchable: true
            }),
            instantsearch.widgets.refinementList({
                container: '#complexity-refinement',
                attribute: 'complexity_'+locale,
                limit: 10,
                showMore: true,
                templates:{
                    showMoreText: showMoreText
                },
                //searchable: true
            }),
            instantsearch.widgets.refinementList({
                container: '#preparation-time-refinement',
                attribute: 'preparation_time',
                limit: 10,
                showMore: true,
                templates:{
                    showMoreText: showMoreText
                },
                //searchable: true
            }),
            instantsearch.widgets.rangeSlider({
                container: '#calories-range',
                attribute: 'calories',
                // Optional parameters
                /*min: number,
                max: number,
                precision: number,
                step: number,
                pips: boolean,
                tooltips: boolean|object,
                cssClasses: object,*/
            }),
            instantsearch.widgets.pagination({
                container: '#paginator',
                // Optional parameters
                /*showFirst: boolean,
                showPrevious: boolean,
                showNext: boolean,
                showLast: boolean,
                padding: number,
                totalPages: number,
                scrollTo: string|HTMLElement|boolean,
                templates: object,
                cssClasses: object,*/
            })



        ]);




        search.start();








        $(function(){
            setInterval(function(){
                $('.shop-sidebar-color').each(function(){
                    var refinements = $(this).find('.ais-RefinementList');
                    var title = $(this).find('.h4');
                    var search = $(this).find('.ais-SearchBox-input');
                    if(search.length > 0 && search.is(":focus")){
                        return;
                    }
                    if(refinements.hasClass('ais-RefinementList--noRefinement')){
                        title.hide();
                        refinements.hide();
                    }else{
                        title.show();
                        refinements.show();
                    }
                })
            },200)
        })


        @endverbatim
    </script>

    <style>
        .ais-InfiniteHits-item {
            /*width: 22% !important;*/
        }

        .ais-RangeSlider .rheostat-progress{
            background-color: #fab417;
        }
        .ais-RangeSlider .rheostat-background{
            border-color: #e4e4e4;
        }

    </style>

@endpush
