@extends('front.main')
@push('scripts')
    <script>
        var tz_default_time = '{{ date('Y-m-d',strtotime($event->starts_at_day)) }} {{ \Str::limit($event->starts_at_time,5,'') }}';
        $(function(){
            $('.select2').select2({
                theme: 'bootstrap4'
            });
        })
    </script>
    <script type="text/javascript" src="{{mix('/js/tz-converter.js')}}"></script>
@endpush
@section('content')
<div class="d-none d-md-block">
    @hss('50')
</div>

  <div class="event">
      <div class="container">
           <div class="row">
                <div class="col-md-12 p-0">
                    <div class="alert alert-success p4 my-4 text-center ">
                        @lang('all.event purchase final confirmation notes')
                    </div>
                </div>
            </div>
      </div>
      @hss('30')
      {{--
      @if (!$agent->isMobile())
        @if(!is_null($purchase->final_confirmaion_sent_at))
            <div class="slick-carousel carousel-stripe">
            @foreach($event->images as $image)

                <img src="{{Route('ir',['size' => 'h320','filename' => $image])}}" alt="@altTagMl($image)">

            @endforeach
            </div>
        @endif
      @endif
       --}}
    @hss('50')
    <div class="container border border-warning py-3">

      <div class="row">
        <div class="col-md-6">
          <img class="img-fluid" src="{{Route('ir',['size' => 'h480','filename' =>$event->images[0]])}}" alt="@altTagMl($event->images[0])">
        </div>
        <div class="col-md-6">
             <div class="row mb-3">
                <div class="col-md-6">
                    <label for="">@lang('all.select timezone')</label>
                    <select name="" id="tz-select" class="select2"></select>
                </div>
                <div class="col-md-6">

                </div>
            </div>
          <div class="row">
            <div class="col-md-10">
              <h3 class="d-inline-block mb-3">
                <span class="badge badge-warning text-white">
                  <i class="far fa-calendar-alt"></i>
                  <span class="tz-converterd">
                    {{\Carbon\Carbon::createFromFormat('Y-m-d', $event->starts_at_day)->format('l jS \\of F Y')}} @lang('all.at_time') {{\Str::limit($event->starts_at_time,5,'')}}
                  </span>
                </span>
              </h3>
            </div>
            <div class="col-md-2 text-right">
              @include('front.components.favorite-toggler',['item' => $event])
            </div>
          </div>
          <div>


          </div>


          <h1 class="text-left p-0 d-inline-block mb-3"> {{$event->name}}</h1>
          <div class="row mb-3">
            <div class="col-md-12">
              <p class="event-feature"><i class="fas fa-hourglass-end"></i> @lang('all.Duration:') {{$event->duration}}m</p>
              <p class="event-feature"><i class="fas fa-chart-line"></i> @lang('all.Complexity:') {{$event->complexity->name}}m</p>
            </div>
          </div>
          <p>{{$event->description_short}}</p>
          <p>by {{$event->user->name}} {{$event->user->surname}} @include('front.components.avatar',['user' => $event->user])</p>

           <div class="row">
              <div class="col-md-12">
                  @include('front.components.social-share')
              </div>
          </div>

          @if(!is_null($purchase->final_confirmaion_sent_at))
            <hr>
            <p>
                <a class="btn text-white btn-warning btn-block mb-4" href="{{Route('enrollEvent',['id' => $event->id])}}">@lang('all.Enroll this event!')</a>
                <!--<a class="btn btn-outline-warning btn-sm btn-block" href="{{$event->enrollment_link}}">Convert the time in your timezone</a>-->
            </p>
          @endif

        </div>
      </div>
    </div>
     @hss('50')

       <div class="container">
            <div class="row">
                <div class="col text-center">
                 <a href="{{ Route('events') }}" class="btn btn-warning text-white btn-lg">@lang('all.book another class')</a>
                </div>
            </div>
        </div>

        @if(!is_null($purchase->final_confirmaion_sent_at))
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <div class="best-readable ">
              <h2>{{ $event->subtitle }}</h2>
              <hr>
            {!!$event->description!!}
            @if(!is_null($purchase->final_confirmaion_sent_at))
                <hr>
                <a class="btn text-white btn-warning btn-block mb-4" href="{{Route('enrollEvent',['id' => $event->id])}}">@lang('all.Enroll this event!')</a>
            @endif
            <!--<a class="btn btn-outline-warning btn-sm btn-block" href="{{$event->enrollment_link}}">Convert the time in your timezone</a>-->
          </div>

            @if(!is_null($event->custom_blocks))
                @foreach (json_decode($event->custom_blocks,1) as $block)
                    <div class="best-readable ">
                        <h2>{{ $block['title'] }}</h2>
                            {!! $block['content'] !!}

                    </div>
                @endforeach
            @endif

                <div class="best-readable">
                    <hr>
                    <h2>@lang('all.confirmation notes')</h2>
                    {!! $event->final_confirmation_notes !!}
                </div>
            @include('front.components.reviews-container',['reviews' => $event->reviews,'id' => $event->id,'reviewable_type' => $event->getModelNameFull()])
        </div>
        <div class="col-md-4 text-center">
          @include('front.components.team-member-bio',['user' => $event->user])
        </div>
      </div>

    </div>
      @endif
  </div>
@endsection


@push('scripts')
    <script>
        $(function(){
            $('.slick-carousel').slick({
                 slidesToShow: 3,
                slidesToScroll: 1,
                autoplay: true,
                infinite: true,
                centerMode: true,
                variableWidth: true,
                dots: true,
                autoplaySpeed: 2000,
                responsive: [
                    {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                    },
                    {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                    },
                    {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            });
        })
    </script>
@endpush
