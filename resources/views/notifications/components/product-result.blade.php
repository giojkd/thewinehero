
<div class="product">
  <div class="row">
    <div class="col-md-4 text-center">
      <a href="{{ $product->makeUrl() }}">
          <img class="img-fluid" src="{{Route('ir',['size' => 'h800','filename' => $product->photos[0]])}}" alt="">
      </a>
    </div>
    <div class="col-md-8">
      <span class="product-name">{{$product->name}}</span>
      <br>
      <span class="product-format">{{$product->format_quantity}}{{$product->format_measurement_unit}}</span>
      <p class="product-description-short">{{$product->description_short}}</p>

      @if(!is_null($product->price))
        <div class="product-prices">
            <span class="product-price">@fp($product->price)</span>
            @if(!is_null($product->price_compare))
                <span class="product-price_compare">@fp($product->price_compare)</span>
            @endif
        </div>
        @else
        @hss('15')
      @endif

      <a href="{{ $product->makeUrl() }}" class="btn btn-outline-warning btn-block">@lang('all.Read more')</a>

      <div class="text-center mt-2">
          @if(!is_null($product->manufacturer))
            <a class="text-warning" href="{{ $product->manufacturer->makeUrl() }}">@lang('all.about') {{$product->manufacturer->name}}</a>
          @endif
        @if(!is_null($product->consortium))
            <a class="text-warning" href="{{ $product->consortium->makeUrl() }}">@lang('all.about') {{$product->consortium->name}}</a>
          @endif
      </div>
    </div>
  </div>
</div>
