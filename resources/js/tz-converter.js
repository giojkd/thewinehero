require('./app');

var moment = require('moment-timezone');
moment.tz.setDefault("Europe/Rome");
var tz_list = moment.tz.names();
var tz_select = $('#tz-select');

$.each(tz_list,function(key,el){
    if (el == 'Europe/Rome'){
        tz_select.append('<option selected value="' + el + '">' + el + '</option>')
    }else{
        tz_select.append('<option value="' + el + '">' + el + '</option>')
    }

});

var default_moment = moment(tz_default_time);

tz_select.change(function(){

    var tz = default_moment.tz($(this).val()).format('dddd Do [of] MMMM YYYY [at] hh:mm a');
    $('.tz-converterd').html(tz);

});



